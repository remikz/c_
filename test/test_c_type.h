#ifndef TEST_C_TYPES_INCLUDED
#define TEST_C_TYPES_INCLUDED

#include "c_test.h"
#include "c_type.h"

void test_c_types()
{
    C_TEST_ASSERT(sizeof(c_float32_t) == 32, "Unexpected size");
    C_TEST_ASSERT(sizeof(c_float64_t) == 64, "Unexpected size");
    C_TEST_ASSERT(sizeof(c_int8_t)    == 8,  "Unexpected size");
    C_TEST_ASSERT(sizeof(c_int16_t)   == 16, "Unexpected size");
    C_TEST_ASSERT(sizeof(c_int32_t)   == 32, "Unexpected size");
    C_TEST_ASSERT(sizeof(c_int64_t)   == 64, "Unexpected size");
    C_TEST_ASSERT(sizeof(c_uint8_t)   == 8,  "Unexpected size");
    C_TEST_ASSERT(sizeof(c_uint16_t)  == 16, "Unexpected size");
    C_TEST_ASSERT(sizeof(c_uint32_t)  == 32, "Unexpected size");
    C_TEST_ASSERT(sizeof(c_uint64_t)  == 64, "Unexpected size");
}

int suite_test_c_types()
{
    C_TEST_ADD_SUITE("suite_test_c_types", NULL, NULL);
    C_TEST_ADD_TEST(test_c_types);
recover:
    C_TEST_RETURN();
}

#endif
/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/