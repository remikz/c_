#ifndef TEST_C_VECTOR_QUICKSORT_INCLUDED
#define TEST_C_VECTOR_QUICKSORT_INCLUDED

#include "c_test.h"
#include "c_vector_quicksort.h"

void test_c_vector_quicksort_iterative_value()
{
    #define TEST(N, T)                                                         \
    C_VECTOR_TYPE(N) v##N = 0;                                                 \
    T v1##N = 1;                                                               \
    T v2##N = 2;                                                               \
    T v3##N = 3;                                                               \
    T v4##N = 4;                                                               \
                                                                               \
    c_vector_quicksort_iterative##N(0);                                        \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    c_vector_append##N(v##N, v4##N);                                           \
    c_vector_append##N(v##N, v3##N);                                           \
    c_vector_append##N(v##N, v1##N);                                           \
    c_vector_append##N(v##N, v2##N);                                           \
                                                                               \
    c_vector_quicksort_iterative##N(v##N);                                     \
                                                                               \
    C_TEST_ASSERT_EQ(c_vector_get##N(v##N, 0), v1##N, "Expected value " #N);   \
    C_TEST_ASSERT_EQ(c_vector_get##N(v##N, 1), v2##N, "Expected value " #N);   \
    C_TEST_ASSERT_EQ(c_vector_get##N(v##N, 2), v3##N, "Expected value " #N);   \
    C_TEST_ASSERT_EQ(c_vector_get##N(v##N, 3), v4##N, "Expected value " #N);   \
                                                                               \
    c_vector_free##N(& v##N);

    C_TYPE_TEMPLATE_NAME_TYPE_VALUE(TEST)
    #undef TEST
}

void test_c_vector_quicksort_ptr()
{
    #define TEST(N, T)                                                         \
    C_VECTOR_TYPE(N) v##N = 0;                                                 \
    T v1##N;                                                                   \
    T v2##N;                                                                   \
    T v3##N;                                                                   \
    T v4##N;                                                                   \
                                                                               \
    c_vector_quicksort##N(0);                                                  \
                                                                               \
    C_MEMORY_NEW(v1##N);                                                       \
    C_MEMORY_NEW(v2##N);                                                       \
    C_MEMORY_NEW(v3##N);                                                       \
    C_MEMORY_NEW(v4##N);                                                       \
    *v1##N = 1;                                                                \
    *v2##N = 2;                                                                \
    *v3##N = 3;                                                                \
    *v4##N = 4;                                                                \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    c_vector_append##N(v##N, v4##N);                                           \
    c_vector_append##N(v##N, v3##N);                                           \
    c_vector_append##N(v##N, v1##N);                                           \
    c_vector_append##N(v##N, v2##N);                                           \
                                                                               \
    c_vector_quicksort##N(v##N);                                               \
                                                                               \
    C_TEST_ASSERT_EQ_PTR(c_vector_get##N(v##N, 0), v1##N, "Expected value " #N);\
    C_TEST_ASSERT_EQ_PTR(c_vector_get##N(v##N, 1), v2##N, "Expected value " #N);\
    C_TEST_ASSERT_EQ_PTR(c_vector_get##N(v##N, 2), v3##N, "Expected value " #N);\
    C_TEST_ASSERT_EQ_PTR(c_vector_get##N(v##N, 3), v4##N, "Expected value " #N);\
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    C_MEMORY_FREE(v1##N);                                                      \
    C_MEMORY_FREE(v2##N);                                                      \
    C_MEMORY_FREE(v3##N);                                                      \
    C_MEMORY_FREE(v4##N);                                                      \


    C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(TEST)
    #undef TEST
}

void test_c_vector_quicksort_value()
{
    #define TEST(N, T)                                                         \
    C_VECTOR_TYPE(N) v##N = 0;                                                 \
    T v1##N = 1;                                                               \
    T v2##N = 2;                                                               \
    T v3##N = 3;                                                               \
    T v4##N = 4;                                                               \
                                                                               \
    c_vector_quicksort##N(0);                                                  \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    c_vector_append##N(v##N, v4##N);                                           \
    c_vector_append##N(v##N, v3##N);                                           \
    c_vector_append##N(v##N, v1##N);                                           \
    c_vector_append##N(v##N, v2##N);                                           \
                                                                               \
    c_vector_quicksort##N(v##N);                                               \
                                                                               \
    C_TEST_ASSERT_EQ(c_vector_get##N(v##N, 0), v1##N, "Expected value " #N);   \
    C_TEST_ASSERT_EQ(c_vector_get##N(v##N, 1), v2##N, "Expected value " #N);   \
    C_TEST_ASSERT_EQ(c_vector_get##N(v##N, 2), v3##N, "Expected value " #N);   \
    C_TEST_ASSERT_EQ(c_vector_get##N(v##N, 3), v4##N, "Expected value " #N);   \
                                                                               \
    c_vector_free##N(& v##N);

    C_TYPE_TEMPLATE_NAME_TYPE_VALUE(TEST)
    #undef TEST
}

void test_c_vector_quicksort_udf()
{
    c_vector_t v = 0;
    c_str_t v1 = c_str_new("s1", 2);
    c_str_t v2 = c_str_new("s2", 2);
    c_str_t v3 = c_str_new("s3", 2);
    c_str_t v4 = c_str_new("s4", 2);
    c_str_t v5 = c_str_new("s5", 2);
    c_str_t v6 = c_str_new("s6", 2);

    c_vector_quicksort(0, c_str_cmp_voidp);

    v = c_vector_new(0);
    c_vector_append(v, v4);
    c_vector_append(v, v3);
    c_vector_append(v, v1);
    c_vector_append(v, v2);

    c_vector_quicksort(v, c_str_cmp_voidp);

    C_TEST_ASSERT_EQ_PTR(c_vector_get(v, 0), v1, "Expected value 1");
    C_TEST_ASSERT_EQ_PTR(c_vector_get(v, 1), v2, "Expected value 2");
    C_TEST_ASSERT_EQ_PTR(c_vector_get(v, 2), v3, "Expected value 3");
    C_TEST_ASSERT_EQ_PTR(c_vector_get(v, 3), v4, "Expected value 4");

    c_vector_free(& v);

    v = c_vector_new(0);
    c_vector_append(v, v6);
    c_vector_append(v, v5);
    c_vector_append(v, v4);
    c_vector_append(v, v3);
    c_vector_append(v, v2);
    c_vector_append(v, v1);
    c_vector_quicksort(v, c_str_cmp_voidp);
    c_vector_free(& v);

    c_str_free(& v1);
    c_str_free(& v2);
    c_str_free(& v3);
    c_str_free(& v4);
    c_str_free(& v5);
    c_str_free(& v6);
}

void test_c_vector_quicksorted_ptr()
{
    #define TEST(N, T)                                                         \
    C_VECTOR_TYPE(N) v##N = 0;                                                 \
    C_VECTOR_TYPE(N) w##N = 0;                                                 \
    T v1##N;                                                                   \
    T v2##N;                                                                   \
    T v3##N;                                                                   \
    T v4##N;                                                                   \
                                                                               \
    w##N = c_vector_quicksorted##N(0);                                         \
    c_vector_free##N(& w##N);                                                  \
                                                                               \
    C_MEMORY_NEW(v1##N);                                                       \
    C_MEMORY_NEW(v2##N);                                                       \
    C_MEMORY_NEW(v3##N);                                                       \
    C_MEMORY_NEW(v4##N);                                                       \
    *v1##N = 1;                                                                \
    *v2##N = 2;                                                                \
    *v3##N = 3;                                                                \
    *v4##N = 4;                                                                \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    c_vector_append##N(v##N, v4##N);                                           \
    c_vector_append##N(v##N, v3##N);                                           \
    c_vector_append##N(v##N, v1##N);                                           \
    c_vector_append##N(v##N, v2##N);                                           \
                                                                               \
    w##N = c_vector_quicksorted##N(v##N);                                      \
                                                                               \
    C_TEST_ASSERT_EQ_PTR(c_vector_get##N(w##N, 0), v1##N, "Expected value " #N);\
    C_TEST_ASSERT_EQ_PTR(c_vector_get##N(w##N, 1), v2##N, "Expected value " #N);\
    C_TEST_ASSERT_EQ_PTR(c_vector_get##N(w##N, 2), v3##N, "Expected value " #N);\
    C_TEST_ASSERT_EQ_PTR(c_vector_get##N(w##N, 3), v4##N, "Expected value " #N);\
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    c_vector_free##N(& w##N);                                                  \
    C_MEMORY_FREE(v1##N);                                                      \
    C_MEMORY_FREE(v2##N);                                                      \
    C_MEMORY_FREE(v3##N);                                                      \
    C_MEMORY_FREE(v4##N);                                                      \


    C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(TEST)
    #undef TEST
}

void test_c_vector_quicksorted_udf()
{
    c_vector_t v = 0, w = 0;
    c_str_t v1 = c_str_new("s1", 2);
    c_str_t v2 = c_str_new("s2", 2);
    c_str_t v3 = c_str_new("s3", 2);
    c_str_t v4 = c_str_new("s4", 2);

    w = c_vector_quicksorted(0, c_str_cmp_voidp);
    c_vector_free(& w);                                                        \

    v = c_vector_new(0);
    c_vector_append(v, v4);
    c_vector_append(v, v3);
    c_vector_append(v, v1);
    c_vector_append(v, v2);

    w = c_vector_quicksorted(v, c_str_cmp_voidp);

    C_TEST_ASSERT_EQ_PTR(c_vector_get(w, 0), v1, "Expected value 1");
    C_TEST_ASSERT_EQ_PTR(c_vector_get(w, 1), v2, "Expected value 2");
    C_TEST_ASSERT_EQ_PTR(c_vector_get(w, 2), v3, "Expected value 3");
    C_TEST_ASSERT_EQ_PTR(c_vector_get(w, 3), v4, "Expected value 4");

    c_vector_free(& v);
    c_vector_free(& w);
    c_str_free(& v1);
    c_str_free(& v2);
    c_str_free(& v3);
    c_str_free(& v4);
}

void test_c_vector_quicksorted_value()
{
    #define TEST(N, T)                                                         \
    C_VECTOR_TYPE(N) v##N = 0;                                                 \
    C_VECTOR_TYPE(N) w##N = 0;                                                 \
    T v1##N = 1;                                                               \
    T v2##N = 2;                                                               \
    T v3##N = 3;                                                               \
    T v4##N = 4;                                                               \
                                                                               \
    w##N = c_vector_quicksorted##N(0);                                         \
    c_vector_free##N(& w##N);                                                  \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    c_vector_append##N(v##N, v4##N);                                           \
    c_vector_append##N(v##N, v3##N);                                           \
    c_vector_append##N(v##N, v1##N);                                           \
    c_vector_append##N(v##N, v2##N);                                           \
                                                                               \
    w##N = c_vector_quicksorted##N(v##N);                                      \
                                                                               \
    C_TEST_ASSERT_EQ(c_vector_get##N(w##N, 0), v1##N, "Expected value " #N);   \
    C_TEST_ASSERT_EQ(c_vector_get##N(w##N, 1), v2##N, "Expected value " #N);   \
    C_TEST_ASSERT_EQ(c_vector_get##N(w##N, 2), v3##N, "Expected value " #N);   \
    C_TEST_ASSERT_EQ(c_vector_get##N(w##N, 3), v4##N, "Expected value " #N);   \
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    c_vector_free##N(& w##N);

    C_TYPE_TEMPLATE_NAME_TYPE_VALUE(TEST)
    #undef TEST
}

void test_c_vector_quicksort_indices_ptr()
{
    #define TEST(N, T)                                                         \
    C_VECTOR_TYPE(N) v##N = 0;                                                 \
    c_vector_uint64_t i##N = 0;                                                \
    T v1##N;                                                                   \
    T v2##N;                                                                   \
    T v3##N;                                                                   \
    T v4##N;                                                                   \
                                                                               \
    i##N = c_vector_quicksort_indices##N(0);                                   \
    c_vector_free_uint64(& i##N);                                              \
                                                                               \
    C_MEMORY_NEW(v1##N);                                                       \
    C_MEMORY_NEW(v2##N);                                                       \
    C_MEMORY_NEW(v3##N);                                                       \
    C_MEMORY_NEW(v4##N);                                                       \
    *v1##N = 1;                                                                \
    *v2##N = 2;                                                                \
    *v3##N = 3;                                                                \
    *v4##N = 4;                                                                \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    c_vector_append##N(v##N, v4##N);                                           \
    c_vector_append##N(v##N, v3##N);                                           \
    c_vector_append##N(v##N, v1##N);                                           \
    c_vector_append##N(v##N, v2##N);                                           \
                                                                               \
    i##N = c_vector_quicksort_indices##N(v##N);                                \
                                                                               \
    C_TEST_ASSERT_EQ(c_vector_size_uint64(i##N), 4, "Expected size " #N);      \
    C_TEST_ASSERT_EQ_PTR(c_vector_get_uint64(i##N, 0), 2, "Expected index " #N);\
    C_TEST_ASSERT_EQ_PTR(c_vector_get_uint64(i##N, 1), 3, "Expected index " #N);\
    C_TEST_ASSERT_EQ_PTR(c_vector_get_uint64(i##N, 2), 1, "Expected index " #N);\
    C_TEST_ASSERT_EQ_PTR(c_vector_get_uint64(i##N, 3), 0, "Expected index " #N);\
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    c_vector_free_uint64(& i##N);                                              \
    C_MEMORY_FREE(v1##N);                                                      \
    C_MEMORY_FREE(v2##N);                                                      \
    C_MEMORY_FREE(v3##N);                                                      \
    C_MEMORY_FREE(v4##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(TEST)
    #undef TEST
}

void test_c_vector_quicksort_indices_udf()
{
    c_vector_t v = 0;
    c_vector_uint64_t i = 0;
    c_str_t v1 = c_str_new("s1", 2);
    c_str_t v2 = c_str_new("s2", 2);
    c_str_t v3 = c_str_new("s3", 2);
    c_str_t v4 = c_str_new("s4", 2);

    i = c_vector_quicksort_indices(0, c_str_cmp_voidp);
    c_vector_free_uint64(& i);

    v = c_vector_new(0);
    c_vector_append(v, v4);
    c_vector_append(v, v3);
    c_vector_append(v, v1);
    c_vector_append(v, v2);

    i = c_vector_quicksort_indices(v, c_str_cmp_voidp);

    C_TEST_ASSERT_EQ(c_vector_size_uint64(i), 4, "Expected size");
    C_TEST_ASSERT_EQ(c_vector_get_uint64(i, 0), 2, "Expected value");
    C_TEST_ASSERT_EQ(c_vector_get_uint64(i, 1), 3, "Expected value");
    C_TEST_ASSERT_EQ(c_vector_get_uint64(i, 2), 1, "Expected value");
    C_TEST_ASSERT_EQ(c_vector_get_uint64(i, 3), 0, "Expected value");

    c_vector_free(& v);
    c_vector_free_uint64(& i);
    c_str_free(& v1);
    c_str_free(& v2);
    c_str_free(& v3);
    c_str_free(& v4);
}

void test_c_vector_quicksort_indices_value()
{
    #define TEST(N, T)                                                         \
    C_VECTOR_TYPE(N) v##N = 0;                                                 \
    c_vector_uint64_t i##N = 0;                                                \
    T v1##N = 1;                                                               \
    T v2##N = 2;                                                               \
    T v3##N = 3;                                                               \
    T v4##N = 4;                                                               \
                                                                               \
    i##N = c_vector_quicksort_indices##N(0);                                   \
    c_vector_free_uint64(& i##N);                                              \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    c_vector_append##N(v##N, v4##N);                                           \
    c_vector_append##N(v##N, v3##N);                                           \
    c_vector_append##N(v##N, v1##N);                                           \
    c_vector_append##N(v##N, v2##N);                                           \
                                                                               \
    i##N = c_vector_quicksort_indices##N(v##N);                                \
                                                                               \
    C_TEST_ASSERT_EQ(c_vector_size_uint64(i##N), 4, "Expected size " #N);      \
    C_TEST_ASSERT_EQ(c_vector_get_uint64(i##N, 0), 2, "Expected index " #N);   \
    C_TEST_ASSERT_EQ(c_vector_get_uint64(i##N, 1), 3, "Expected index " #N);   \
    C_TEST_ASSERT_EQ(c_vector_get_uint64(i##N, 2), 1, "Expected index " #N);   \
    C_TEST_ASSERT_EQ(c_vector_get_uint64(i##N, 3), 0, "Expected index " #N);   \
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    c_vector_free_uint64(& i##N);

    C_TYPE_TEMPLATE_NAME_TYPE_VALUE(TEST)
    #undef TEST
}

int suite_test_c_vector_quicksort()
{
    C_TEST_ADD_SUITE("suite_test_c_vector_quicksort", NULL, NULL);

    C_TEST_ADD_TEST(test_c_vector_quicksort_ptr);
    C_TEST_ADD_TEST(test_c_vector_quicksort_value);
    C_TEST_ADD_TEST(test_c_vector_quicksort_udf);

    C_TEST_ADD_TEST(test_c_vector_quicksorted_ptr);
    C_TEST_ADD_TEST(test_c_vector_quicksorted_value);
    C_TEST_ADD_TEST(test_c_vector_quicksorted_udf);

    C_TEST_ADD_TEST(test_c_vector_quicksort_indices_ptr);
    C_TEST_ADD_TEST(test_c_vector_quicksort_indices_value);
    C_TEST_ADD_TEST(test_c_vector_quicksort_indices_udf);

    C_TEST_ADD_TEST(test_c_vector_quicksort_iterative_value);

    C_TEST_RETURN();
}

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/
