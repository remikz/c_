#ifndef TEST_C_VECTOR_SLICE_ITER_INCLUDED
#define TEST_C_VECTOR_SLICE_ITER_INCLUDED

#include "c_test.h"
#include "c_vector_slice_iter.h"

void test_c_vector_slice_iter_advance()
{
    #define TEST(N)                                                            \
    c_slice_t s##N = NULL;                                                     \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
    C_VECTOR_SLICE_ITER_TYPE(N) i##N = NULL;                                   \
                                                                               \
    v##N = c_vector_new_set##N(10, 10, 0);                                     \
    s##N = c_slice_new_start(0);                                               \
    i##N = c_vector_slice_iter_new##N(v##N, s##N);                             \
                                                                               \
    C_TEST_ASSERT_EQ(c_vector_slice_iter_position##N(i##N), 0,                 \
                     "Expected first position.");                              \
    c_vector_slice_iter_advance##N(i##N);                                      \
    C_TEST_ASSERT_EQ(c_vector_slice_iter_position##N(i##N), 1,                 \
                     "Expected next position.");                               \
                                                                               \
    c_vector_slice_iter_free##N(& i##N);                                       \
    c_vector_free##N(& v##N);                                                  \
    c_slice_free(& s##N);                                                      \
                                                                               \
    v##N = c_vector_new_set##N(10, 10, 0);                                     \
    s##N = c_slice_new_start_stride(1, 2);                                     \
    i##N = c_vector_slice_iter_new##N(v##N, s##N);                             \
                                                                               \
    C_TEST_ASSERT_EQ(c_vector_slice_iter_position##N(i##N), 1,                 \
                     "Expected first start position.");                        \
    c_vector_slice_iter_advance##N(i##N);                                      \
    C_TEST_ASSERT_EQ(c_vector_slice_iter_position##N(i##N), 3,                 \
                     "Expected next stride position.");                        \
                                                                               \
    c_vector_slice_iter_free##N(& i##N);                                       \
    c_vector_free##N(& v##N);                                                  \
    c_slice_free(& s##N);

    C_TYPE_TEMPLATE_NAME(TEST)
    #undef TEST
}

void test_c_vector_slice_iter_from_chars()
{
    #define TEST(N)                                                            \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
    C_VECTOR_SLICE_ITER_TYPE(N) i##N = NULL;                                   \
    const char* c##N = NULL;                                                   \
                                                                               \
    i##N = c_vector_slice_iter_from_chars##N(v##N, c##N, 0, 0, C_FALSE);       \
    C_TEST_ASSERT(i##N, "Failed with null chars.");                            \
    c_vector_slice_iter_free##N(& i##N);                                       \
                                                                               \
    v##N = c_vector_new_set##N(10, 10, 0);                                     \
    c##N = ":";                                                                \
    i##N = c_vector_slice_iter_from_chars##N(v##N, c##N, 1, 0, C_FALSE);       \
    C_TEST_ASSERT(i##N, "Failed with \":\"");                                  \
    C_TEST_ASSERT_EQ(c_vector_slice_iter_position##N(i##N), 0,                 \
                     "Expected position.");                                    \
    c_vector_slice_iter_free##N(& i##N);                                       \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new_set##N(10, 10, 0);                                     \
    c##N = "1::2";                                                             \
    i##N = c_vector_slice_iter_from_chars##N(v##N, c##N, 4, 0, C_FALSE);       \
    C_TEST_ASSERT(i##N, "Failed with \"1::2\"");                               \
    C_TEST_ASSERT_EQ(c_vector_slice_iter_position##N(i##N), 1,                 \
                     "Expected position.");                                    \
    c_vector_slice_iter_free##N(& i##N);                                       \
    c_vector_free##N(& v##N);

    C_TYPE_TEMPLATE_NAME(TEST)
    #undef TEST
}

void test_c_vector_slice_iter_from_str()
{
    #define TEST(N)                                                            \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
    C_VECTOR_SLICE_ITER_TYPE(N) i##N = NULL;                                   \
    c_str_t c##N = NULL;                                                       \
                                                                               \
    i##N = c_vector_slice_iter_from_str##N(v##N, c##N, 0, C_FALSE);            \
    C_TEST_ASSERT(i##N, "Failed with null str.");                              \
    c_vector_slice_iter_free##N(& i##N);                                       \
                                                                               \
    v##N = c_vector_new_set##N(10, 10, 0);                                     \
    c##N = c_str_new(":", 1);                                                  \
    i##N = c_vector_slice_iter_from_str##N(v##N, c##N, 0, C_FALSE);            \
    C_TEST_ASSERT(i##N, "Failed with \":\"");                                  \
    C_TEST_ASSERT_EQ(c_vector_slice_iter_position##N(i##N), 0,                 \
                     "Expected position.");                                    \
    c_vector_slice_iter_free##N(& i##N);                                       \
    c_vector_free##N(& v##N);                                                  \
    c_str_free(& c##N);                                                        \
                                                                               \
    v##N = c_vector_new_set##N(10, 10, 0);                                     \
    c##N = c_str_new("1::2", 4);                                               \
    i##N = c_vector_slice_iter_from_str##N(v##N, c##N, 0, C_FALSE);            \
    C_TEST_ASSERT(i##N, "Failed with \"1::2\"");                               \
    C_TEST_ASSERT_EQ(c_vector_slice_iter_position##N(i##N), 1,                 \
                     "Expected position.");                                    \
    c_vector_slice_iter_free##N(& i##N);                                       \
    c_vector_free##N(& v##N);                                                  \
    c_str_free(& c##N);

    C_TYPE_TEMPLATE_NAME(TEST)
    #undef TEST
}

void test_c_vector_slice_iter_is_valid()
{
    #define TEST(N)                                                            \
    c_slice_t s##N = NULL;                                                     \
    C_VECTOR_SLICE_ITER_TYPE(N) i##N = NULL;                                   \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
    c_bool_t b##N;                                                             \
                                                                               \
    b##N = c_vector_slice_iter_is_valid##N(NULL);                              \
    C_TEST_ASSERT_EQ(b##N, C_FALSE, "Expected not valid.");                    \
                                                                               \
    v##N = c_vector_new_set##N(1, 1, 0);                                       \
    s##N = c_slice_new_start(0);                                               \
    i##N = c_vector_slice_iter_new##N(v##N, s##N);                             \
    b##N = c_vector_slice_iter_is_valid##N(i##N);                              \
    C_TEST_ASSERT_EQ(b##N, C_TRUE, "Expected valid.");                         \
                                                                               \
    c_vector_slice_iter_advance##N(i##N);                                      \
    b##N = c_vector_slice_iter_is_valid##N(i##N);                              \
    C_TEST_ASSERT_EQ(b##N, C_FALSE, "Expected invalid.");                      \
                                                                               \
    c_vector_slice_iter_free##N(& i##N);                                       \
    c_slice_free(& s##N);                                                      \
    c_vector_free##N(& v##N);

    C_TYPE_TEMPLATE_NAME(TEST)
    #undef TEST
}

void test_c_vector_slice_iter_item_ptr()
{
    #define TEST(N, T)                                                         \
    c_slice_t s##N = NULL;                                                     \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
    C_VECTOR_SLICE_ITER_TYPE(N) i##N = NULL;                                   \
    T a##N = NULL;                                                             \
    T p##N = NULL;                                                             \
                                                                               \
    C_MEMORY_NEW(p##N);                                                        \
    i##N = c_vector_slice_iter_new##N(v##N, s##N);                             \
    a##N = c_vector_slice_iter_item##N(i##N);                                  \
    C_TEST_ASSERT_EQ_PTR(a##N, 0, "Expected zero for dataless iter.");         \
    c_vector_slice_iter_free##N(& i##N);                                       \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    i##N = c_vector_slice_iter_new##N(v##N, s##N);                             \
    a##N = c_vector_slice_iter_item##N(i##N);                                  \
    C_TEST_ASSERT_EQ_PTR(a##N, 0, "Expected zero for dataless iter.");         \
    c_vector_free##N(& v##N);                                                  \
    c_vector_slice_iter_free##N(& i##N);                                       \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    s##N = c_slice_new_start(0);                                               \
    i##N = c_vector_slice_iter_new##N(v##N, s##N);                             \
    a##N = c_vector_slice_iter_item##N(i##N);                                  \
    C_TEST_ASSERT_EQ_PTR(a##N, 0, "Expected zero for dataless iter.");         \
    c_slice_free(& s##N);                                                      \
    c_vector_free##N(& v##N);                                                  \
    c_vector_slice_iter_free##N(& i##N);                                       \
                                                                               \
    v##N = c_vector_new_set##N(1, 1, p##N);                                    \
    s##N = c_slice_new_start(0);                                               \
    i##N = c_vector_slice_iter_new##N(v##N, s##N);                             \
                                                                               \
    a##N = c_vector_slice_iter_item##N(i##N);                                  \
    C_TEST_ASSERT_EQ_PTR(a##N, p##N, "Expected value.");                       \
                                                                               \
    c_vector_slice_iter_advance##N(i##N);                                      \
    a##N = c_vector_slice_iter_item##N(i##N);                                  \
    C_TEST_ASSERT_EQ_PTR(a##N, 0, "Expected zero value for exhausted iter.");  \
    c_vector_slice_iter_advance##N(i##N);                                      \
                                                                               \
    c_slice_free(& s##N);                                                      \
    c_vector_free##N(& v##N);                                                  \
    c_vector_slice_iter_free##N(& i##N);                                       \
    C_MEMORY_FREE(p##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST)
    #undef TEST
}

void test_c_vector_slice_iter_item_value()
{
    #define TEST(N, T)                                                         \
    c_slice_t s##N = NULL;                                                     \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
    C_VECTOR_SLICE_ITER_TYPE(N) i##N = NULL;                                   \
    T a##N = 1;                                                                \
                                                                               \
    i##N = c_vector_slice_iter_new##N(v##N, s##N);                             \
    a##N = c_vector_slice_iter_item##N(i##N);                                  \
    C_TEST_ASSERT_EQ(a##N, 0, "Expected zero for dataless iter.");             \
    c_vector_slice_iter_free##N(& i##N);                                       \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    i##N = c_vector_slice_iter_new##N(v##N, s##N);                             \
    a##N = c_vector_slice_iter_item##N(i##N);                                  \
    C_TEST_ASSERT_EQ(a##N, 0, "Expected zero for dataless iter.");             \
    c_vector_free##N(& v##N);                                                  \
    c_vector_slice_iter_free##N(& i##N);                                       \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    s##N = c_slice_new_start(0);                                               \
    i##N = c_vector_slice_iter_new##N(v##N, s##N);                             \
    a##N = c_vector_slice_iter_item##N(i##N);                                  \
    C_TEST_ASSERT_EQ(a##N, 0, "Expected zero for dataless iter.");             \
    c_slice_free(& s##N);                                                      \
    c_vector_free##N(& v##N);                                                  \
    c_vector_slice_iter_free##N(& i##N);                                       \
                                                                               \
    v##N = c_vector_new_set##N(1, 1, 1);                                       \
    s##N = c_slice_new_start(0);                                               \
    i##N = c_vector_slice_iter_new##N(v##N, s##N);                             \
                                                                               \
    a##N = c_vector_slice_iter_item##N(i##N);                                  \
    C_TEST_ASSERT_EQ(a##N, 1, "Expected value.");                              \
                                                                               \
    c_vector_slice_iter_advance##N(i##N);                                      \
    a##N = c_vector_slice_iter_item##N(i##N);                                  \
    C_TEST_ASSERT_EQ(a##N, 0, "Expected zero value for exhausted iter.");      \
    c_vector_slice_iter_advance##N(i##N);                                      \
                                                                               \
    c_slice_free(& s##N);                                                      \
    c_vector_free##N(& v##N);                                                  \
    c_vector_slice_iter_free##N(& i##N);

    C_TYPE_TEMPLATE_NAME_TYPE_VALUE(TEST)
    #undef TEST
}

void test_c_vector_slice_iter_new()
{
    #define TEST(N)                                                            \
    c_slice_t s##N = NULL;                                                     \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
    C_VECTOR_SLICE_ITER_TYPE(N) i##N = NULL;                                   \
                                                                               \
    i##N = c_vector_slice_iter_new##N(v##N, s##N);                             \
    C_TEST_ASSERT(i##N, "Failed new with nulls.");                             \
    c_vector_slice_iter_free##N(& i##N);                                       \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    i##N = c_vector_slice_iter_new##N(v##N, s##N);                             \
    C_TEST_ASSERT(i##N, "Failed new with null slice.");                        \
    c_vector_slice_iter_free##N(& i##N);                                       \
    C_TEST_ASSERT_FALSE(i##N, "Failed to free iter.");                         \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector.");                       \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    s##N = c_slice_new_start(0);                                               \
    i##N = c_vector_slice_iter_new##N(v##N, s##N);                             \
    C_TEST_ASSERT(i##N, "Failed new with empty vector.");                      \
    c_vector_slice_iter_free##N(& i##N);                                       \
    C_TEST_ASSERT_FALSE(i##N, "Failed to free iter.");                         \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector.");                       \
    c_slice_free(& s##N);                                                      \
    C_TEST_ASSERT_FALSE(s##N, "Failed to free slice.");                        \
                                                                               \
    v##N = c_vector_new_set##N(10, 10, 0);                                     \
    s##N = c_slice_new_start(0);                                               \
    i##N = c_vector_slice_iter_new##N(v##N, s##N);                             \
    C_TEST_ASSERT(i##N, "Failed new.");                                        \
    c_vector_slice_iter_free##N(& i##N);                                       \
    C_TEST_ASSERT_FALSE(i##N, "Failed to free iter.");                         \
    c_vector_free##N(& v##N);                                                  \
    c_slice_free(& s##N);

    C_TYPE_TEMPLATE_NAME(TEST)
    #undef TEST
}

int suite_test_c_vector_slice_iter()
{
    C_TEST_ADD_SUITE("suite_test_c_vector_slice_iter", NULL, NULL);

    C_TEST_ADD_TEST(test_c_vector_slice_iter_new);
    C_TEST_ADD_TEST(test_c_vector_slice_iter_advance);
    C_TEST_ADD_TEST(test_c_vector_slice_iter_is_valid);
    C_TEST_ADD_TEST(test_c_vector_slice_iter_from_chars);
    C_TEST_ADD_TEST(test_c_vector_slice_iter_from_str);
    C_TEST_ADD_TEST(test_c_vector_slice_iter_item_ptr);
    C_TEST_ADD_TEST(test_c_vector_slice_iter_item_value);

    C_TEST_RETURN();
}

#endif
/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/