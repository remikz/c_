#ifndef C_KEYVALUE_HASH_INCLUDED
#define C_KEYVALUE_HASH_INCLUDED

#include "c_type.h"
#include "c_hash.h"
#include "c_str.h"

#ifdef __cplusplus
extern "C" {
#endif

/** Stores hash, key and a value. */
#define C_KEYVALUE_HASH_TYPE(KNAME, VNAME) c_keyvalue_hash##KNAME##VNAME##_t

#define C_KEYVALUE_HASH_TYPEDEF(KNAME, VNAME)                                  \
            C_TYPEDEF(C_KEYVALUE_HASH_TYPE(KNAME, VNAME))

#define C_KEYVALUE_HASH_CMP_NAME(KNAME, VNAME)                                 \
            c_keyvalue_hash_cmp##KNAME##VNAME

/** Accepts hash, const key and const keyvalue struct. */
#define C_KEYVALUE_HASH_CMP_TYPE(KNAME, VNAME)                                 \
            c_keyvalue_hash_cmp##KNAME##VNAME##_t

#define C_KEYVALUE_HASH_CMP_TYPEDEF(KNAME, KTYPE, VNAME, VTYPE)                \
    typedef c_int32_t (*C_KEYVALUE_HASH_CMP_TYPE(KNAME, VNAME))(               \
                            c_hash_t,                                          \
                            const KTYPE,                                       \
                            const C_KEYVALUE_HASH_TYPE(KNAME, VNAME));

#define C_KEYVALUE_HASH_TEMPLATE_NAME_NUMERIC(MACRO)                           \
    MACRO(_int16,      /* empty */)                                            \
    MACRO(_int32,      /* empty */)                                            \
    MACRO(_int64,      /* empty */)                                            \
    MACRO(_uint16,     /* empty */)                                            \
    MACRO(_uint32,     /* empty */)                                            \
    MACRO(_uint64,     /* empty */)

#define C_KEYVALUE_HASH_TEMPLATE_NAME_STR(MACRO)                               \
    MACRO(_str,        /* empty */)

#define C_KEYVALUE_HASH_TEMPLATE_NAME_PTR(MACRO)                               \
    MACRO(/* void* */, /* empty */)

#define C_KEYVALUE_HASH_TEMPLATE_NAME(MACRO)                                   \
    C_KEYVALUE_HASH_TEMPLATE_NAME_NUMERIC(MACRO)                               \
    C_KEYVALUE_HASH_TEMPLATE_NAME_PTR(MACRO)                                   \
    C_KEYVALUE_HASH_TEMPLATE_NAME_STR(MACRO)

#define C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE_NUMERIC(MACRO)                      \
    MACRO(_int16,  c_int16_t,  /* empty */, void*)                             \
    MACRO(_int32,  c_int32_t,  /* empty */, void*)                             \
    MACRO(_int64,  c_int64_t,  /* empty */, void*)                             \
    MACRO(_uint16, c_uint16_t, /* empty */, void*)                             \
    MACRO(_uint32, c_uint32_t, /* empty */, void*)                             \
    MACRO(_uint64, c_uint64_t, /* empty */, void*)

#define C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE_PTR(MACRO)                          \
    MACRO(/* empty */,  void*,      /* empty */, void*)

#define C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE_STR(MACRO)                          \
    MACRO(_str,         c_str_t,    /* empty */, void*)

#define C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(MACRO)                              \
    C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE_NUMERIC(MACRO)                          \
    C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE_PTR(MACRO)                              \
    C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE_STR(MACRO)

/** Compare keyvalue keys only if hashes are equal.
 * @return 0 if equal, 0> if key is less than keyvalue key, 0< if greater.
 */
#define C_KEYVALUE_HASH_CMP(KNAME, KTYPE, VNAME, VTYPE)                        \
    extern c_int32_t C_KEYVALUE_HASH_CMP_NAME(KNAME, VNAME)(                   \
                        const c_hash_t hash,                                   \
                        const KTYPE key,                                       \
                        const C_KEYVALUE_HASH_TYPE(KNAME, VNAME) keyvalue);

#define C_KEYVALUE_HASH_FREE(KNAME, KTYPE, VNAME, VTYPE)                       \
    extern void c_keyvalue_hash_free##KNAME##VNAME(                            \
                    C_KEYVALUE_HASH_TYPE(KNAME, VNAME) *kv);

/** @return Hash member. */
#define C_KEYVALUE_HASH_HASH(KNAME, KTYPE, VNAME, VTYPE)                       \
    extern c_hash_t c_keyvalue_hash_hash##KNAME##VNAME(                        \
                        C_KEYVALUE_HASH_TYPE(KNAME, VNAME) kv);

#define C_KEYVALUE_HASH_KEY(KNAME, KTYPE, VNAME, VTYPE)                        \
    extern KTYPE c_keyvalue_hash_key##KNAME##VNAME(                            \
                    C_KEYVALUE_HASH_TYPE(KNAME, VNAME) kv);

/** Manages own copy of key memory if key is not a primitive data type. */
#define C_KEYVALUE_HASH_NEW(KNAME, KTYPE, VNAME, VTYPE)                        \
    extern C_KEYVALUE_HASH_TYPE(KNAME, VNAME) c_keyvalue_hash_new##KNAME##VNAME(\
                                            c_hash_t hash,                     \
                                            KTYPE key,                         \
                                            VTYPE value);

#define C_KEYVALUE_HASH_VALUE(KNAME, KTYPE, VNAME, VTYPE)                      \
    extern VTYPE c_keyvalue_hash_value##KNAME##VNAME(                          \
                           C_KEYVALUE_HASH_TYPE(KNAME, VNAME) kv);

#define C_KEYVALUE_HASH_VALUE_SET(KNAME, KTYPE, VNAME, VTYPE)                  \
    extern void c_keyvalue_hash_value_set##KNAME##VNAME(                       \
                    C_KEYVALUE_HASH_TYPE(KNAME, VNAME) kv,                     \
                    VTYPE value);

C_KEYVALUE_HASH_TEMPLATE_NAME(C_KEYVALUE_HASH_TYPEDEF)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_KEYVALUE_HASH_CMP_TYPEDEF)

C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_KEYVALUE_HASH_CMP)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_KEYVALUE_HASH_FREE)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_KEYVALUE_HASH_HASH)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_KEYVALUE_HASH_KEY)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_KEYVALUE_HASH_NEW)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_KEYVALUE_HASH_VALUE)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_KEYVALUE_HASH_VALUE_SET)

#undef C_KEYVALUE_HASH_CMP
#undef C_KEYVALUE_HASH_FREE
#undef C_KEYVALUE_HASH_HASH
#undef C_KEYVALUE_HASH_KEY
#undef C_KEYVALUE_HASH_NEW
#undef C_KEYVALUE_HASH_VALUE
#undef C_KEYVALUE_HASH_VALUE_SET

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/