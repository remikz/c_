#!/bin/bash
set -e
src=/home/rsz/projects/c_/dev
bld=/tmp/c_/build

export GCOV_PREFIX=$bld/coverage
export GCOV_PREFIX_STRIP=4

mkdir -p $bld && cd $bld && rm -rf CMake*

echo Configure.
cmake \
    -G "Unix Makefiles" \
    -DCMAKE_BUILD_TYPE=Debug \
    -DCMAKE_C_COMPILER=gcc \
    -DCMAKE_C_FLAGS_DEBUG="-DNDEBUG -g3 -gdwarf-2 --coverage" \
    -DCMAKE_EXPORT_COMPILE_COMMANDS=ON \
    -DCMAKE_VERBOSE_MAKEFILE=ON \
    $src

echo Build.
make

echo Unit Test.
ctest # -VV

echo Memory Test.
sh $src/scripts/valgrind.sh ./test/test_c_all --color

#echo Examples.
#find examples/ -executable -type f -exec "{}" \;

echo Coverage Test.
cd $GCOV_PREFIX
mkdir -p lcov && cd lcov
find $bld -name "*.gcno" -exec cp -u "{}" . \; > /dev/null 2>&1
find $bld -name "*.gcda" -exec cp -u "{}" . \; > /dev/null 2>&1
lcov -t c_ -o c_.info -f -c -d .
genhtml -o html c_.info

python $src/scripts/markdown_coverage.py html/index.html $src/README.md
