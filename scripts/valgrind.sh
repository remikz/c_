#/bin/bash

tmp=tmp.valgrind

valgrind --tool=memcheck --leak-check=full --show-reachable=yes --log-file=$tmp $@

if [ `grep -c 'Invalid' $tmp` != "0" ];
then
	echo "ERROR: valgrind found invalid memory."
	exit 1
fi

if [ `grep -c 'uninitialised' $tmp` != "0" ];
then
	echo "ERROR: valgrind found uninitialised memory."
	exit 1
fi

if [ `grep -c 'no leaks' $tmp` = "0" ];
then
	echo "ERROR: valgrind found memory leaked."
	exit 1
fi
