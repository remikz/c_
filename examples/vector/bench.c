#include <c_bench.h>
#include <c_vector.h>
#include <stdio.h>
#include <stdlib.h> // rand, RAND_MAX

c_bench_t b = 0;
c_vector_int32_t v = 0;
c_vector_int32_t rands = 0;
const int MILLION = 1000000;
const int RAND_MID = RAND_MAX / 2;

void insert_bisect_left();
void insert_bisect_many();
void insert_bisect_mid();
void insert_bisect_right();
void reverse();
void setup();
void setup_rands();
void sort_rands();
void sortv();
void teardown();

void append()
{
    c_vector_append_int32(v, 0);
}

void appendn()
{
    int n = MILLION;

    c_vector_clear_int32(v);
    while (--n) {
        c_vector_append_int32(v, 0);
    }
}

void bench_append_many()
{
    setup();
    c_bench_run(b, 0, appendn);
    printf("append_many ");
    c_bench_print(b);
    teardown();
}

void bench_append_one()
{
    setup();
    c_bench_run(b, 0, append);
    printf("append_one ");
    c_bench_print(b);
    teardown();
}

void bench_insert_bisect_left()
{
    setup();
    setup_rands();
    sortv(rands);
    c_bench_run(b, 0, insert_bisect_left);
    printf("bench_insert_bisect_left ");
    c_bench_print(b);
    teardown();
}

void bench_insert_bisect_mid()
{
    setup();
    setup_rands();
    sortv(rands);
    c_bench_run(b, 0, insert_bisect_mid);
    printf("bench_insert_bisect_mid ");
    c_bench_print(b);
    teardown();
}

void bench_insert_bisect_right()
{
    setup();
    setup_rands();
    sortv(rands);
    c_bench_run(b, 0, insert_bisect_right);
    printf("bench_insert_bisect_right ");
    c_bench_print(b);
    teardown();
}

void bench_rands()
{
    setup();
    c_bench_run(b, 0, setup_rands);
    printf("bench_rands ");
    c_bench_print(b);
    teardown();
}

void bench_reverse()
{
    setup();
    appendn();
    c_bench_run(b, 0, reverse);
    printf("bench_reverse ");
    c_bench_print(b);
    teardown();
}

void bench_sort_rands()
{
    setup();
    setup_rands();
    c_bench_run(b, 0, sort_rands);
    printf("bench_sort_rands ");
    c_bench_print(b);
    teardown();
}

int cmp_int32(const void * elem1, const void * elem2)
{
    int f = *((int*) elem1);
    int s = *((int*) elem2);
    if (f > s) return  1;
    if (f < s) return -1;
    return 0;
}

void insert_bisect_left()
{
    c_vector_insert_bisect_int32(rands, 0);
}

void insert_bisect_mid()
{
    c_vector_insert_bisect_int32(rands, RAND_MID);
}

void insert_bisect_right()
{
    c_vector_insert_bisect_int32(rands, RAND_MAX);
}

void reverse()
{
    c_vector_reverse_int32(v);
}

void setup()
{
    b = c_bench_new();
    v = c_vector_new_int32(0);
}

void setup_rands()
{
    int n = MILLION;
    rands = c_vector_new_int32(n);
    while (--n) {
        c_vector_append_int32(rands, rand());
    }
}

void sort_rands()
{
    sortv(rands);
}

void sortv(c_vector_int32_t vec)
{
    qsort(c_vector_items_int32(vec),
          c_vector_size_int32(vec),
          sizeof(c_int32_t),
          cmp_int32);
}

void teardown()
{
    if (b) {
        c_bench_free(& b);
    }

    if (rands) {
        c_vector_free_int32(& rands);
    }

    if (v) {
        c_vector_free_int32(& v);
    }
}

int main(int argc, char **argv)
{
    bench_append_one();
    bench_append_many();
    bench_insert_bisect_left();
    bench_insert_bisect_mid();
    bench_insert_bisect_right();
    bench_rands();
    bench_sort_rands();
    bench_reverse();

    return 0;
}