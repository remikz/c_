#ifndef C_VECTOR_IMPL_INCLUDED
#define C_VECTOR_IMPL_INCLUDED

#include "c_vector.h"
#include "c_vector_t.h"
#include "c_type.h"
#include "c_log.h"
#include "c_memory.h"

#include <string.h>
#include <stdio.h>

#define C_VECTOR_APPEND(NAME, TYPE)                                            \
c_bool_t c_vector_append##NAME(C_VECTOR_TYPE(NAME) vector, TYPE item)          \
{                                                                              \
    if (vector) {                                                              \
        if (vector->size + 1 > vector->capacity) {                             \
            if (0 == vector->capacity) {                                       \
                vector->capacity = 1;                                          \
            } else {                                                           \
                vector->capacity *= 2;                                         \
            }                                                                  \
            C_MEMORY_RESIZE(vector->items, vector->capacity, TYPE);            \
        }                                                                      \
        vector->items[vector->size] = item;                                    \
        ++vector->size;                                                        \
        ++vector->timestamp;                                                   \
    }                                                                          \
                                                                               \
    return C_FALSE;                                                            \
}

#define C_VECTOR_APPEND_RATE(NAME, TYPE)                                       \
c_bool_t c_vector_append_rate##NAME(C_VECTOR_TYPE(NAME) vector, TYPE item,     \
                                    c_float32_t rate)                          \
{                                                                              \
    if (vector) {                                                              \
        if (vector->size + 1 > vector->capacity) {                             \
            if (rate < 0) {                                                    \
                vector->capacity += -rate;                                     \
            } else {                                                           \
                vector->capacity *= rate;                                      \
            }                                                                  \
            C_MEMORY_RESIZE(vector->items, vector->capacity, TYPE);            \
        }                                                                      \
        vector->items[vector->size] = item;                                    \
        ++vector->size;                                                        \
        ++vector->timestamp;                                                   \
    }                                                                          \
                                                                               \
    return C_FALSE;                                                            \
}

#define C_VECTOR_CAPACITY(NAME, TYPE)                                          \
c_uint64_t c_vector_capacity##NAME(C_VECTOR_TYPE(NAME) vector)                 \
{                                                                              \
    assert(vector);                                                            \
    if (vector) {                                                              \
        return vector->capacity;                                               \
    }                                                                          \
    return 0;                                                                  \
}

#define C_VECTOR_CLEAR(NAME, TYPE)                                             \
void c_vector_clear##NAME(C_VECTOR_TYPE(NAME) vector)                          \
{                                                                              \
    if (vector) {                                                              \
        vector->size = 0;                                                      \
        ++vector->timestamp;                                                   \
    }                                                                          \
}

#define C_VECTOR_COMPACT(NAME, TYPE)                                           \
void c_vector_compact##NAME(C_VECTOR_TYPE(NAME) vector)                        \
{                                                                              \
    if (vector && vector->size) {                                              \
        vector->capacity = vector->size;                                       \
        C_MEMORY_RESIZE(vector->items, vector->capacity, TYPE);                \
        ++vector->timestamp;                                                   \
    }                                                                          \
}

#define C_VECTOR_COPY(NAME, TYPE)                                              \
void c_vector_copy##NAME(C_VECTOR_TYPE(NAME) to,                               \
                         C_VECTOR_TYPE(NAME) from)                             \
{                                                                              \
    if (to && from) {                                                          \
        if (to->capacity < from->size) {                                       \
            to->capacity = from->size;                                         \
            C_MEMORY_RESIZE(to->items, from->size, TYPE);                      \
        }                                                                      \
        C_MEMORY_COPY_N_TYPES(to->items, from->items, from->size, TYPE);       \
        to->size = from->size;                                                 \
        ++to->timestamp;                                                       \
    }                                                                          \
}

#define C_VECTOR_COPY_UDF(NAME, TYPE)                                          \
void c_vector_copy_udf##NAME(C_VECTOR_TYPE(NAME) to,                           \
                             C_VECTOR_TYPE(NAME) from,                         \
                             c_funp_voidp_voidp_t copy_udf)                    \
{                                                                              \
    c_uint64_t i;                                                              \
    if (to && from) {                                                          \
        if (to->capacity < from->size) {                                       \
            to->capacity = from->size;                                         \
            C_MEMORY_RESIZE(to->items, from->size, TYPE);                      \
        }                                                                      \
                                                                               \
        for(i = 0; i < from->size; ++i) {                                      \
            to->items[i] = copy_udf(from->items[i]);                           \
        }                                                                      \
                                                                               \
        if (from->size) {                                                      \
            ++to->timestamp;                                                   \
        }                                                                      \
        to->size = from->size;                                                 \
    }                                                                          \
}

#define C_VECTOR_EMPTY(NAME, TYPE)                                             \
c_bool_t c_vector_empty##NAME(C_VECTOR_TYPE(NAME) vector)                      \
{                                                                              \
    if (vector) {                                                              \
        return 0 == vector->size ? C_TRUE : C_FALSE;                           \
    }                                                                          \
    return C_TRUE;                                                             \
}

#define C_VECTOR_EQUALS(NAME, TYPE)                                            \
c_bool_t c_vector_equals##NAME(C_VECTOR_TYPE(NAME) vector1,                    \
                               C_VECTOR_TYPE(NAME) vector2)                    \
{                                                                              \
    c_uint64_t i;                                                              \
                                                                               \
    if (vector1 && vector2) {                                                  \
        if (vector1->size != vector2->size) {                                  \
            return C_FALSE;                                                    \
        }                                                                      \
                                                                               \
        for(i = 0; i < vector1->size; ++i) {                                   \
            if (vector1->items[i] != vector2->items[i]) {                      \
                return C_FALSE;                                                \
            }                                                                  \
        }                                                                      \
        return C_TRUE;                                                         \
    } else if (!vector1 && !vector2) {                                         \
        return C_TRUE;                                                         \
    }                                                                          \
    return C_FALSE;                                                            \
}

#define C_VECTOR_EQUALS_UDF(NAME, TYPE)                                        \
c_bool_t c_vector_equals_udf##NAME(C_VECTOR_TYPE(NAME) vector1,                \
                                   C_VECTOR_TYPE(NAME) vector2,                \
                                   c_funp_int32_voidp_voidp_t cmp_udf)         \
{                                                                              \
    c_uint64_t i;                                                              \
                                                                               \
    if (vector1 && vector2) {                                                  \
        if (vector1->size != vector2->size) {                                  \
            return C_FALSE;                                                    \
        }                                                                      \
                                                                               \
        for(i = 0; i < vector1->size; ++i) {                                   \
            if (cmp_udf(vector1->items[i], vector2->items[i])) {               \
                return C_FALSE;                                                \
            }                                                                  \
        }                                                                      \
        return C_TRUE;                                                         \
    } else if (!vector1 && !vector2) {                                         \
        return C_TRUE;                                                         \
    }                                                                          \
    return C_FALSE;                                                            \
}

#define C_VECTOR_EXTEND(NAME, TYPE)                                            \
c_bool_t c_vector_extend##NAME(C_VECTOR_TYPE(NAME) to,                         \
                               C_VECTOR_TYPE(NAME) from)                       \
{                                                                              \
    c_bool_t result = C_FALSE;                                                 \
                                                                               \
    if (to && from) {                                                          \
        if (to->size + from->size > to->capacity) {                            \
            to->capacity = to->size + from->size;                              \
            C_MEMORY_RESIZE(to->items, to->capacity, TYPE);                    \
            result = C_TRUE;                                                   \
        }                                                                      \
        C_MEMORY_COPY_N_TYPES(to->items + to->size, from->items, from->size, TYPE);\
        to->size += from->size;                                                \
        if (from->size) {                                                      \
            ++to->timestamp;                                                   \
        }                                                                      \
    }                                                                          \
    return result;                                                             \
}

#define C_VECTOR_FIRST(NAME, TYPE)                                             \
TYPE c_vector_first##NAME(C_VECTOR_TYPE(NAME) vector)                          \
{                                                                              \
    if (vector && vector->size > 0) {                                          \
        return vector->items[0];                                               \
    }                                                                          \
    return 0;                                                                  \
}

#define C_VECTOR_FREE(NAME, TYPE)                                              \
void c_vector_free##NAME(C_VECTOR_TYPE(NAME) *vector)                          \
{                                                                              \
    assert(vector);                                                            \
    assert(*vector);                                                           \
    if (vector && *vector) {                                                   \
        C_MEMORY_FREE((*vector)->items);                                       \
        C_MEMORY_FREE(*vector);                                                \
    }                                                                          \
}

#define C_VECTOR_FREE_DEEP(NAME, TYPE)                                         \
void c_vector_free_deep##NAME(C_VECTOR_TYPE(NAME) *vector)                     \
{                                                                              \
    c_uint64_t i;                                                              \
    assert(vector);                                                            \
    assert(*vector);                                                           \
    if (vector && *vector) {                                                   \
        for(i = 0; i < (*vector)->size; ++i) {                                 \
            C_MEMORY_FREE((*vector)->items[i]);                                \
        }                                                                      \
        C_MEMORY_FREE((*vector)->items);                                       \
        C_MEMORY_FREE(*vector);                                                \
    }                                                                          \
}

#define C_VECTOR_FREE_DEEP_UDF(NAME, TYPE)                                     \
void c_vector_free_deep_udf##NAME(C_VECTOR_TYPE(NAME) *vector,                 \
                                     c_funp_void_voidp_t free_udf)             \
{                                                                              \
    c_uint64_t i;                                                              \
    assert(vector);                                                            \
    assert(*vector);                                                           \
    if (vector && *vector) {                                                   \
        for(i = 0; i < (*vector)->size; ++i) {                                 \
            free_udf((*vector)->items[i]);                                     \
        }                                                                      \
        C_MEMORY_FREE((*vector)->items);                                       \
        C_MEMORY_FREE(*vector);                                                \
    }                                                                          \
}

#define C_VECTOR_GET(NAME, TYPE)                                               \
TYPE c_vector_get##NAME(C_VECTOR_TYPE(NAME) vector, c_uint64_t index)          \
{                                                                              \
    return vector->items[index];                                               \
}

#define C_VECTOR_GET_SAFE(NAME, TYPE)                                          \
TYPE c_vector_get_safe##NAME(C_VECTOR_TYPE(NAME) vector, c_uint64_t index)     \
{                                                                              \
    assert(vector);                                                            \
    assert(vector->items);                                                     \
    assert(index < vector->size);                                              \
                                                                               \
    if (vector && vector->items && index < vector->size) {                     \
        return vector->items[index];                                           \
    } else {                                                                   \
        return 0;                                                              \
    }                                                                          \
}

#define C_VECTOR_INSERT(NAME, TYPE)                                            \
void c_vector_insert##NAME(C_VECTOR_TYPE(NAME) vector,                         \
                           c_uint64_t index, TYPE item)                        \
{                                                                              \
    c_uint64_t i;                                                              \
                                                                               \
    if (vector) {                                                              \
        if (index < vector->size) {                                            \
            if (vector->size + 1 > vector->capacity) {                         \
                c_vector_reserve##NAME(vector, vector->capacity * 2);          \
            }                                                                  \
            for(i = vector->size; i > index; --i) {                            \
                vector->items[i] = vector->items[i-1];                         \
            }                                                                  \
            ++vector->size;                                                    \
        } else {                                                               \
            /* New undefined items will be created. */                         \
            if (index >= vector->capacity) {                                   \
                c_vector_reserve##NAME(vector, index + 1);                     \
            }                                                                  \
            /* Initialize them. */                                             \
            for(i = vector->size; i < index; ++i) {                            \
                vector->items[i] = 0;                                          \
            }                                                                  \
            vector->size = index + 1;                                          \
        }                                                                      \
        vector->items[index] = item;                                           \
        ++vector->timestamp;                                                   \
    }                                                                          \
}

#define C_VECTOR_INSERT_BISECT(NAME, TYPE)                                     \
void c_vector_insert_bisect##NAME(C_VECTOR_TYPE(NAME) vector,                  \
                                  TYPE item)                                   \
{                                                                              \
    c_int64_t left, mid, right, index;                                         \
                                                                               \
    if (vector) {                                                              \
        if (vector->size) {                                                    \
            left = 0;                                                          \
            right = vector->size - 1;                                          \
            while (left <= right) {                                            \
                mid = (left + right) / 2;                                      \
                if (item == vector->items[mid]) {                              \
                    left = mid;                                                \
                    break;                                                     \
                } else if (item > vector->items[mid]) {                        \
                    left = mid + 1;                                            \
                } else {                                                       \
                    right = mid - 1;                                           \
                }                                                              \
            }                                                                  \
            index = left;                                                      \
        } else {                                                               \
            index = 0;                                                         \
        }                                                                      \
        c_vector_insert##NAME(vector, index, item);                            \
        ++vector->timestamp;                                                   \
    }                                                                          \
}

#define C_VECTOR_INSERT_BISECT_UDF(NAME, TYPE)                                 \
void c_vector_insert_bisect_udf##NAME(C_VECTOR_TYPE(NAME) vector,              \
                                      TYPE item,                               \
                                      c_funp_int32_voidp_voidp_t cmp_udf)      \
{                                                                              \
    c_int64_t left, mid, right, cmp_res, index;                                \
                                                                               \
    if (vector) {                                                              \
        if (vector->size) {                                                    \
            left = 0;                                                          \
            right = vector->size - 1;                                          \
            while (left <= right) {                                            \
                mid = (left + right) / 2;                                      \
                cmp_res = cmp_udf(item, vector->items[mid]);                   \
                if (0 == cmp_res) {                                            \
                    left = mid;                                                \
                    break;                                                     \
                } else if (0 < cmp_res) {                                      \
                    left = mid + 1;                                            \
                } else {                                                       \
                    right = mid - 1;                                           \
                }                                                              \
            }                                                                  \
            index = left;                                                      \
        } else {                                                               \
            index = 0;                                                         \
        }                                                                      \
        c_vector_insert##NAME(vector, index, item);                            \
        ++vector->timestamp;                                                   \
    }                                                                          \
}

#define C_VECTOR_ITEMS(NAME, TYPE)                                             \
TYPE* c_vector_items##NAME(C_VECTOR_TYPE(NAME) vector)                         \
{                                                                              \
    if (vector) {                                                              \
        return vector->items;                                                  \
    }                                                                          \
    return NULL;                                                               \
}

#define C_VECTOR_LAST(NAME, TYPE)                                              \
TYPE c_vector_last##NAME(C_VECTOR_TYPE(NAME) vector)                           \
{                                                                              \
    if (vector && vector->size > 0) {                                          \
        return vector->items[vector->size - 1];                                \
    }                                                                          \
    return 0;                                                                  \
}

#define C_VECTOR_MAP(NAME, TYPE)                                               \
void c_vector_map##NAME(C_VECTOR_TYPE(NAME) vector,                            \
                        void (*apply)(TYPE item, void *closure),               \
                        void *closure)                                         \
{                                                                              \
    c_uint64_t i, stamp;                                                       \
    if (vector) {                                                              \
        stamp = vector->timestamp;                                             \
        for(i = 0; i < vector->size; ++i) {                                    \
            if (stamp != vector->timestamp) {                                  \
                C_LOG_ERROR("vector changed during map apply.\n");             \
                assert(stamp == vector->timestamp);                            \
            }                                                                  \
            apply(vector->items[i], closure);                                  \
        }                                                                      \
    }                                                                          \
}

#define C_VECTOR_NEW(NAME, TYPE)                                               \
C_VECTOR_TYPE(NAME) c_vector_new##NAME(c_uint64_t capacity)                    \
{                                                                              \
    C_VECTOR_TYPE(NAME) result;                                                \
                                                                               \
    C_MEMORY_NEW(result);                                                      \
    result->items = NULL;                                                      \
    result->capacity = capacity;                                               \
    result->size = 0;                                                          \
    result->timestamp = 0;                                                     \
    if (capacity) {                                                            \
        C_MEMORY_NEW_N_BYTES(result->items, sizeof(TYPE) * capacity);          \
    }                                                                          \
                                                                               \
    return result;                                                             \
}

#define C_VECTOR_NEW_COPY(NAME, TYPE)                                          \
C_VECTOR_TYPE(NAME) c_vector_new_copy##NAME(c_uint64_t capacity,               \
                                          c_uint64_t size,                     \
                                          TYPE *items)                         \
{                                                                              \
    C_VECTOR_TYPE(NAME) result;                                                \
    C_MEMORY_NEW(result);                                                      \
    result->items = NULL;                                                      \
    result->capacity = capacity;                                               \
    result->size = size;                                                       \
    result->timestamp = 0;                                                     \
    C_MEMORY_NEW_N_BYTES(result->items, sizeof(TYPE) * capacity);              \
    C_MEMORY_COPY_N_TYPES(result->items, items, size, TYPE);                   \
                                                                               \
    return result;                                                             \
}

#define C_VECTOR_NEW_COPY_VECTOR(NAME, TYPE)                                   \
C_VECTOR_TYPE(NAME) c_vector_new_copy_vector##NAME(C_VECTOR_TYPE(NAME) from)   \
{                                                                              \
    C_VECTOR_TYPE(NAME) to;                                                    \
                                                                               \
    if (from) {                                                                \
        C_MEMORY_NEW(to);                                                      \
        to->capacity = from->size;                                             \
        to->size = from->size;                                                 \
        to->timestamp = 0;                                                     \
        C_MEMORY_NEW_N_BYTES(to->items, sizeof(TYPE) * to->capacity);          \
        C_MEMORY_COPY_N_TYPES(to->items, from->items, from->size, TYPE);       \
    } else {                                                                   \
        to = c_vector_new##NAME(0);                                            \
    }                                                                          \
                                                                               \
    return to;                                                                 \
}

#define C_VECTOR_NEW_SET(NAME, TYPE)                                           \
C_VECTOR_TYPE(NAME) c_vector_new_set##NAME(c_uint64_t capacity,                \
                                          c_uint64_t size,                     \
                                          TYPE value)                          \
{                                                                              \
    C_VECTOR_TYPE(NAME) result;                                                \
    c_uint64_t i = 0;                                                          \
                                                                               \
    C_MEMORY_NEW(result);                                                      \
    result->items = NULL;                                                      \
    result->capacity = capacity;                                               \
    result->size = size;                                                       \
    result->timestamp = 0;                                                     \
    C_MEMORY_NEW_N_TYPES(result->items, capacity, TYPE);                       \
                                                                               \
    for(; i < size; ++i) {                                                     \
        result->items[i] = value;                                              \
    }                                                                          \
                                                                               \
    return result;                                                             \
}

#define C_VECTOR_PRINT(NAME)                                                   \
void c_vector_print##NAME(C_VECTOR_TYPE(NAME) vector) {                        \
    c_int64_t i = 0;                                                           \
                                                                               \
    if (vector) {                                                              \
        printf("%d/%d ", (int)c_vector_size##NAME(vector),                     \
                         (int)c_vector_capacity##NAME(vector));                \
        for(; i < c_vector_size##NAME(vector); ++i) {                          \
            printf("%g ", (double) c_vector_get##NAME(vector, i));             \
        }                                                                      \
        printf("\n");                                                          \
    }                                                                          \
}

#define C_VECTOR_REMOVE(NAME, TYPE)                                            \
TYPE c_vector_remove##NAME(C_VECTOR_TYPE(NAME) vector, c_uint64_t index)       \
{                                                                              \
    TYPE item = 0;                                                             \
                                                                               \
    if (vector && index < vector->size) {                                      \
        item = vector->items[index];                                           \
        --vector->size;                                                        \
                                                                               \
        for(; index < vector->size; ++index) {                                 \
            vector->items[index] = vector->items[index+1];                     \
        }                                                                      \
        ++vector->timestamp;                                                   \
    }                                                                          \
    return item;                                                               \
}

#define C_VECTOR_REMOVE_FIRST(NAME, TYPE)                                      \
TYPE c_vector_remove_first##NAME(C_VECTOR_TYPE(NAME) vector)                   \
{                                                                              \
    return c_vector_remove##NAME(vector, 0);                                   \
}

#define C_VECTOR_REMOVE_LAST(NAME, TYPE)                                       \
TYPE c_vector_remove_last##NAME(C_VECTOR_TYPE(NAME) vector)                    \
{                                                                              \
    TYPE item = 0;                                                             \
                                                                               \
    if (vector && vector->size) {                                              \
        --vector->size;                                                        \
        item = vector->items[vector->size];                                    \
        ++vector->timestamp;                                                   \
    }                                                                          \
    return item;                                                               \
}

#define C_VECTOR_REMOVE_UNORDERED(NAME, TYPE)                                  \
TYPE c_vector_remove_unordered##NAME(C_VECTOR_TYPE(NAME) vector,               \
        c_uint64_t index)                                                      \
{                                                                              \
    TYPE item = 0;                                                             \
                                                                               \
    if (vector && index < vector->size) {                                      \
        item = vector->items[index];                                           \
        --vector->size;                                                        \
        vector->items[index] = vector->items[vector->size];                    \
        ++vector->timestamp;                                                   \
    }                                                                          \
    return item;                                                               \
}

#define C_VECTOR_RESERVE(NAME, TYPE)                                           \
void c_vector_reserve##NAME(C_VECTOR_TYPE(NAME) vector, c_uint64_t capacity)   \
{                                                                              \
    if (vector && capacity > vector->capacity) {                               \
        vector->capacity = capacity;                                           \
        C_MEMORY_RESIZE(vector->items, vector->capacity, TYPE);                \
        ++vector->timestamp;                                                   \
    }                                                                          \
}

#define C_VECTOR_RESIZE(NAME, TYPE)                                            \
void c_vector_resize##NAME(C_VECTOR_TYPE(NAME) vector, c_uint64_t size)        \
{                                                                              \
    if (vector) {                                                              \
        vector->size = size;                                                   \
        if (size > vector->capacity) {                                         \
            vector->capacity = size;                                           \
            C_MEMORY_RESIZE(vector->items, vector->capacity, TYPE);            \
            ++vector->timestamp;                                               \
        }                                                                      \
    }                                                                          \
}

#define C_VECTOR_RESIZE_SET(NAME, TYPE)                                        \
void c_vector_resize_set##NAME(C_VECTOR_TYPE(NAME) vector,                     \
                               c_uint64_t size, TYPE item)                     \
{                                                                              \
    c_uint64_t i;                                                              \
                                                                               \
    if (vector) {                                                              \
        if (size > vector->capacity) {                                         \
            vector->capacity = size;                                           \
            C_MEMORY_RESIZE(vector->items, vector->capacity, TYPE);            \
        }                                                                      \
                                                                               \
        for(i = vector->size; i < size; ++i) {                                 \
            vector->items[i] = item;                                           \
        }                                                                      \
                                                                               \
        if (vector->size != size) {                                            \
            ++vector->timestamp;                                               \
        }                                                                      \
        vector->size = size;                                                   \
    }                                                                          \
}

#define C_VECTOR_REVERSE(NAME, TYPE)                                           \
void c_vector_reverse##NAME(C_VECTOR_TYPE(NAME) vector)                        \
{                                                                              \
    c_uint64_t left, right;                                                    \
    TYPE tmp;                                                                  \
                                                                               \
    assert(vector);                                                            \
    if (vector && vector->size) {                                              \
        left = 0;                                                              \
        right = vector->size - 1;                                              \
        for(; left < right; ++left, --right) {                                 \
            tmp = vector->items[left];                                         \
            vector->items[left] = vector->items[right];                        \
            vector->items[right] = tmp;                                        \
        }                                                                      \
        ++vector->timestamp;                                                   \
    }                                                                          \
}

#define C_VECTOR_SET(NAME, TYPE)                                               \
void c_vector_set##NAME(C_VECTOR_TYPE(NAME) vector, c_uint64_t index,          \
                        TYPE item)                                             \
{                                                                              \
    vector->items[index] = item;                                               \
    ++vector->timestamp;                                                       \
}

#define C_VECTOR_SET_SAFE(NAME, TYPE)                                          \
void c_vector_set_safe##NAME(C_VECTOR_TYPE(NAME) vector, c_uint64_t index,     \
                        TYPE item)                                             \
{                                                                              \
    assert(vector);                                                            \
    assert(vector->items);                                                     \
    assert(index < vector->size);                                              \
    if (vector && vector->items && index < vector->size) {                     \
        vector->items[index] = item;                                           \
        ++vector->timestamp;                                                   \
    }                                                                          \
}

#define C_VECTOR_SIZE(NAME, TYPE)                                              \
c_uint64_t c_vector_size##NAME(C_VECTOR_TYPE(NAME) vector)                     \
{                                                                              \
    assert(vector);                                                            \
    if (vector) {                                                              \
        return vector->size;                                                   \
    }                                                                          \
    return 0;                                                                  \
}

#define C_VECTOR_SWAP(NAME, TYPE)                                              \
void c_vector_swap##NAME(C_VECTOR_TYPE(NAME) vector1,                          \
                         C_VECTOR_TYPE(NAME) vector2)                          \
{                                                                              \
    TYPE *items;                                                               \
    c_uint64_t capacity, size;                                                 \
                                                                               \
    if (vector1 && vector2) {                                                  \
        capacity = vector1->capacity;                                          \
        items = vector1->items;                                                \
        size = vector1->size;                                                  \
        vector1->capacity = vector2->capacity;                                 \
        vector1->items = vector2->items;                                       \
        vector1->size = vector2->size;                                         \
        vector2->capacity = capacity;                                          \
        vector2->items = items;                                                \
        vector2->size = size;                                                  \
        ++vector1->timestamp;                                                  \
        ++vector2->timestamp;                                                  \
    }                                                                          \
}

#define C_VECTOR_SWAP_AT(NAME, TYPE)                                           \
void c_vector_swap_at##NAME(C_VECTOR_TYPE(NAME) vector,                        \
                         c_uint64_t index1, c_uint64_t index2)                 \
{                                                                              \
    TYPE item;                                                                 \
    if (vector && index1 < vector->size && index2 < vector->size &&            \
        index1 != index2) {                                                    \
        item = vector->items[index1];                                          \
        vector->items[index1] = vector->items[index2];                         \
        vector->items[index2] = item;                                          \
        ++vector->timestamp;                                                   \
    }                                                                          \
}

#define C_VECTOR_TIMESTAMP(NAME, TYPE)                                         \
c_uint64_t c_vector_timestamp##NAME(C_VECTOR_TYPE(NAME) vector)                \
{                                                                              \
    if (vector) {                                                              \
        return vector->timestamp;                                              \
    }                                                                          \
    return 0;                                                                  \
}

C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_APPEND)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_APPEND_RATE)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_CAPACITY)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_CLEAR)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_COMPACT)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_COPY)
C_TYPE_TEMPLATE_NAME_TYPE_PTR(C_VECTOR_COPY_UDF)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_EMPTY)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_EQUALS)
C_TYPE_TEMPLATE_NAME_TYPE_PTR(C_VECTOR_EQUALS_UDF)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_EXTEND)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_FIRST)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_LAST)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_FREE)
C_TYPE_TEMPLATE_NAME_TYPE_PTR(C_VECTOR_FREE_DEEP)
C_TYPE_TEMPLATE_NAME_TYPE_PTR(C_VECTOR_FREE_DEEP_UDF)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_GET)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_GET_SAFE)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_ITEMS)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_INSERT)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_INSERT_BISECT)
C_TYPE_TEMPLATE_NAME_TYPE_PTR(C_VECTOR_INSERT_BISECT_UDF)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_MAP)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_NEW)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_NEW_COPY)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_NEW_COPY_VECTOR)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_NEW_SET)
C_TYPE_TEMPLATE_NAME_VALUE(C_VECTOR_PRINT)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_REMOVE)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_REMOVE_FIRST)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_REMOVE_LAST)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_REMOVE_UNORDERED)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_RESERVE)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_RESIZE)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_RESIZE_SET)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_REVERSE)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_SET)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_SET_SAFE)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_SIZE)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_SWAP)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_SWAP_AT)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_TIMESTAMP)

#undef C_VECTOR_APPEND
#undef C_VECTOR_APPEND_RATE
#undef C_VECTOR_CAPACITY
#undef C_VECTOR_CLEAR
#undef C_VECTOR_COMPACT
#undef C_VECTOR_COPY
#undef C_VECTOR_COPY_UDF
#undef C_VECTOR_EMPTY
#undef C_VECTOR_EQUALS
#undef C_VECTOR_EQUALS_UDF
#undef C_VECTOR_EXTEND
#undef C_VECTOR_FIRST
#undef C_VECTOR_FREE
#undef C_VECTOR_FREE_DEEP
#undef C_VECTOR_FREE_DEEP_UDF
#undef C_VECTOR_GET
#undef C_VECTOR_GET_SAFE
#undef C_VECTOR_INSERT
#undef C_VECTOR_INSERT_BISECT
#undef C_VECTOR_INSERT_BISECT_UDF
#undef C_VECTOR_ITEMS
#undef C_VECTOR_LAST
#undef C_VECTOR_MAP
#undef C_VECTOR_NEW
#undef C_VECTOR_NEW_COPY
#undef C_VECTOR_NEW_COPY_VECTOR
#undef C_VECTOR_NEW_SET
#undef C_VECTOR_PRINT
#undef C_VECTOR_REMOVE
#undef C_VECTOR_REMOVE_FIRST
#undef C_VECTOR_REMOVE_LAST
#undef C_VECTOR_REMOVE_UNORDERED
#undef C_VECTOR_RESERVE
#undef C_VECTOR_RESIZE
#undef C_VECTOR_RESIZE_SET
#undef C_VECTOR_REVERSE
#undef C_VECTOR_SET
#undef C_VECTOR_SET_SAFE
#undef C_VECTOR_SIZE
#undef C_VECTOR_SWAP
#undef C_VECTOR_SWAP_AT
#undef C_VECTOR_TIMESTAMP

#endif
/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/