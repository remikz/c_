#ifndef C_TYPE_INCLUDED
#define C_TYPE_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

#ifndef EXIT_FAILURE
    #define EXIT_FAILURE 1
#endif

#ifndef EXIT_SUCCESS
    #define EXIT_SUCCESS 0
#endif

#define C_FALSE 0
#define C_TRUE 1

typedef char                c_bool_t;
typedef float               c_float32_t;
typedef double              c_float64_t;
typedef char                c_int8_t;
typedef short               c_int16_t;
typedef int                 c_int32_t;
typedef long long           c_int64_t;
typedef unsigned char       c_uint8_t;
typedef unsigned short      c_uint16_t;
typedef unsigned int        c_uint32_t;
typedef unsigned long long  c_uint64_t;

/** Function pointer that accepts and returns nothing. */
typedef void (*c_funp_void_void_t)(void);

/** Function pointer that accepts void pointer and returns nothing. */
typedef void (*c_funp_void_voidp_t)(void*);

/** Function pointer that accepts void pointer and returns void pointer. */
typedef void* (*c_funp_voidp_voidp_t)(void*);

/** Function pointer that accepts two void pointers and returns boolean. */
typedef c_bool_t (*c_funp_bool_voidp_voidp_t)(void*, void*);

/** Function pointer that accepts two void pointers and returns signed byte. */
typedef c_int8_t (*c_funp_int8_voidp_voidp_t)(void*, void*);

/** Function pointer that accepts two void pointers and returns signed integer. */
typedef c_int32_t (*c_funp_int32_voidp_voidp_t)(void*, void*);

/** Function pointer that accepts two const void pointers and returns signed integer. */
typedef c_int32_t (*c_funp_int32_cvoidp_cvoidp_t)(const void*, const void*);

/** Remove const-ness. */
#define C_CONST_CAST(TYPE, PTR) (* (TYPE*) (& (PTR)))

#define C_TYPEDEF(NAME) typedef struct NAME *NAME;

#define C_TYPE_TEMPLATE_NAME_VALUE_SIGNED(MACRO)                               \
    MACRO(_float32)                                                            \
    MACRO(_float64)                                                            \
    MACRO(_int8)                                                               \
    MACRO(_int16)                                                              \
    MACRO(_int32)                                                              \
    MACRO(_int64)

#define C_TYPE_TEMPLATE_NAME_VALUE_UNSIGNED(MACRO)                             \
    MACRO(_uint8)                                                              \
    MACRO(_uint16)                                                             \
    MACRO(_uint32)                                                             \
    MACRO(_uint64)

#define C_TYPE_TEMPLATE_NAME_VALUE(MACRO)                                      \
    C_TYPE_TEMPLATE_NAME_VALUE_SIGNED(MACRO)                                   \
    C_TYPE_TEMPLATE_NAME_VALUE_UNSIGNED(MACRO)

#define C_TYPE_TEMPLATE_NAME_PTR_NOVOID_SIGNED(MACRO)                          \
    MACRO(_float32p)                                                           \
    MACRO(_float64p)                                                           \
    MACRO(_int8p)                                                              \
    MACRO(_int16p)                                                             \
    MACRO(_int32p)                                                             \
    MACRO(_int64p)

#define C_TYPE_TEMPLATE_NAME_PTR_NOVOID_UNSIGNED(MACRO)                        \
    MACRO(_uint8p)                                                             \
    MACRO(_uint16p)                                                            \
    MACRO(_uint32p)                                                            \
    MACRO(_uint64p)

#define C_TYPE_TEMPLATE_NAME_PTR_NOVOID(MACRO)                                 \
    C_TYPE_TEMPLATE_NAME_PTR_NOVOID_SIGNED(MACRO)                              \
    C_TYPE_TEMPLATE_NAME_PTR_NOVOID_UNSIGNED(MACRO)

#define C_TYPE_TEMPLATE_NAME_PTR_VOID(MACRO)                                   \
    MACRO(/* void* */)

#define C_TYPE_TEMPLATE_NAME_PTR(MACRO)                                        \
    C_TYPE_TEMPLATE_NAME_PTR_NOVOID(MACRO)                                     \
    C_TYPE_TEMPLATE_NAME_PTR_VOID(MACRO)

#define C_TYPE_TEMPLATE_NAME(MACRO)                                            \
    C_TYPE_TEMPLATE_NAME_VALUE(MACRO)                                          \
    C_TYPE_TEMPLATE_NAME_PTR(MACRO)

#define C_TYPE_TEMPLATE_NAME_TYPE_VALUE_SIGNED(MACRO)                          \
    MACRO(_float32,  c_float32_t)                                              \
    MACRO(_float64,  c_float64_t)                                              \
    MACRO(_int8,     c_int8_t)                                                 \
    MACRO(_int16,    c_int16_t)                                                \
    MACRO(_int32,    c_int32_t)                                                \
    MACRO(_int64,    c_int64_t)

#define C_TYPE_TEMPLATE_NAME_TYPE_VALUE_UNSIGNED(MACRO)                        \
    MACRO(_uint8,    c_uint8_t)                                                \
    MACRO(_uint16,   c_uint16_t)                                               \
    MACRO(_uint32,   c_uint32_t)                                               \
    MACRO(_uint64,   c_uint64_t)

#define C_TYPE_TEMPLATE_NAME_TYPE_VALUE(MACRO)                                 \
    C_TYPE_TEMPLATE_NAME_TYPE_VALUE_SIGNED(MACRO)                              \
    C_TYPE_TEMPLATE_NAME_TYPE_VALUE_UNSIGNED(MACRO)

#define C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID_SIGNED(MACRO)                     \
    MACRO(_float32p, c_float32_t*)                                             \
    MACRO(_float64p, c_float64_t*)                                             \
    MACRO(_int8p,    c_int8_t*)                                                \
    MACRO(_int16p,   c_int16_t*)                                               \
    MACRO(_int32p,   c_int32_t*)                                               \
    MACRO(_int64p,   c_int64_t*)

#define C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID_UNSIGNED(MACRO)                   \
    MACRO(_uint8p,   c_uint8_t*)                                               \
    MACRO(_uint16p,  c_uint16_t*)                                              \
    MACRO(_uint32p,  c_uint32_t*)                                              \
    MACRO(_uint64p,  c_uint64_t*)

#define C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(MACRO)                            \
    C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID_SIGNED(MACRO)                         \
    C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID_UNSIGNED(MACRO)

#define C_TYPE_TEMPLATE_NAME_TYPE_PTR_VOID(MACRO)                              \
    MACRO(/* blank */, void*)

#define C_TYPE_TEMPLATE_NAME_TYPE_PTR(MACRO)                                   \
    C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(MACRO)                                \
    C_TYPE_TEMPLATE_NAME_TYPE_PTR_VOID(MACRO)

#define C_TYPE_TEMPLATE_NAME_TYPE(MACRO)                                       \
    C_TYPE_TEMPLATE_NAME_TYPE_VALUE(MACRO)                                     \
    C_TYPE_TEMPLATE_NAME_TYPE_PTR(MACRO)

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/