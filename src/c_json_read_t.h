#ifndef C_JSON_READ_T_INCLUDED
#define C_JSON_READ_T_INCLUDED

#include "c_json_read.h"

#ifdef __cplusplus
extern "C" {
#endif

/** Info for each parsed terminal symbol.
 */
struct c_json_read_t {
    /** 1-based column number where data began. */
    c_uint64_t col;
    /** 1-based line number where data began. */
    c_uint64_t line;
    /** Error message, if any. */
    c_str_t message;
    /** Reference to content parsed. */
    c_json_t data;
    /** Vector of c_json_read_t for child nodes, if any. */
    c_vector_t children;
};

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/