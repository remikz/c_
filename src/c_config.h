#ifndef C_CONFIG_INCLUDED
#define C_CONFIG_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

extern int c_config(int argc, const char **argv);
extern void c_config_all();
extern void c_config_cc();
extern void c_config_cflags();
extern void c_config_cversion();
extern void c_config_date();
extern void c_config_help();
extern void c_config_host();
extern void c_config_includedir();
extern void c_config_libdir();
extern void c_config_libs();
extern void c_config_license();
extern void c_config_prefix();
extern void c_config_user();
extern void c_config_version();

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/