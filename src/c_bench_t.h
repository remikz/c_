#ifndef C_BENCH_T_INCLUDED
#define C_BENCH_T_INCLUDED

#include "c_bench.h"

#ifdef __cplusplus
extern "C" {
#endif

/** Benchmark a function. */
struct c_bench_t {
    /** Number of times the function was executed. */
    c_uint64_t ntrials;
    /** Max, average and min nanoseconds. */
    c_uint64_t ns_max, ns_mean, ns_min;
};

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/
