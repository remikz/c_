#!/bin/bash
set -e
src=/home/rsz/projects/c_/dev
bld=/tmp/c_/build

mkdir -p $bld
cd $bld
rm -rf CMake*

echo Configure.
cmake \
    -G "Unix Makefiles" \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_C_COMPILER=gcc \
    -DCMAKE_C_FLAGS_RELEASE="-DNDEBUG -g -O3" \
    -DCMAKE_EXPORT_COMPILE_COMMANDS=ON \
    -DCMAKE_VERBOSE_MAKEFILE=ON \
    $src

echo Build.
make

echo Memory Test.
sh $src/scripts/valgrind.sh ./test/test_c_all --color
