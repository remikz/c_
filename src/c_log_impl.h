#ifndef C_LOG_IMPL_INCLUDED
#define C_LOG_IMPL_INCLUDED

#include "c_log.h"
#include <string.h>
#include <sys/time.h>
#include <time.h>

#ifdef __cplusplus
extern "C" {
#endif

#define C_LOG_BUF_LEN 256
#define C_LOG_BUF_LEN_SHORT 64

void c_log_stream(const char *level, FILE *stream, const char *file, int line, const char *message, va_list args);
void c_log_str_args(const char *level, char *output, unsigned int output_size, const char *file, int line, const char *message, va_list args);

void c_log(const char *level, FILE *stream, const char *file, int line, ...)
{
    va_list args;
    va_start(args, line);
    const char *message = va_arg(args, const char*);
    c_log_stream(level, stream, file, line, message, args);
    va_end(args);
}

void c_log_str(const char *level, char *output, unsigned int output_size, const char *file, int line, ...)
{
    va_list args;
    va_start(args, line);
    const char *message = va_arg(args, const char*);
    c_log_str_args(level, output, output_size, file, line, message, args);
    va_end(args);
}

void c_log_str_args(
        const char *level,
        char *output,
        unsigned int output_size,
        const char *file,
        int line,
        const char *message,
        va_list args)
{
    char           tbuf[C_LOG_BUF_LEN_SHORT];
    struct timeval tv;
    struct tm     *tm = NULL;
    int            nprinted = 0;

    gettimeofday(& tv, NULL);
    tm = localtime(& tv.tv_sec);
    if(NULL != tm)
    {
        strftime(tbuf, C_LOG_BUF_LEN_SHORT, "%Y-%m-%d %H:%M:%S.%%06u %z ", tm);
        nprinted = snprintf(output, output_size, tbuf, tv.tv_usec);
    }

    nprinted = nprinted >= 0 ? nprinted : 0;
    nprinted += snprintf(output + nprinted, output_size - nprinted, "%s %s:%d ", level, file, line);
    nprinted = nprinted >= 0 ? nprinted : 0;
    snprintf(output + nprinted, output_size - nprinted, message, args);
}

void c_log_stream(
        const char *level,
        FILE *stream,
        const char *file,
        int line,
        const char *message,
        va_list args)
{
    char buf[C_LOG_BUF_LEN];
    c_log_str_args(level, buf, C_LOG_BUF_LEN, file, line, message, args);
    fprintf(stream, "%s", buf);
}

#ifdef __cplusplus
}
#endif

#undef C_LOG_BUF_LEN
#undef C_LOG_BUF_LEN_SHORT

#endif
/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/