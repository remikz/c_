#ifndef C_TEST_PRINT_INCLUDED
#define C_TEST_PRINT_INCLUDED

#include <stdio.h>
#include "c_type.h"

#ifdef __cplusplus
extern "C" {
#endif

#define C_TEST_DGRAY "\033[1;30m"
#define C_TEST_GREEN "\033[0;32m"
#define C_TEST_NOCOLOR "\033[0m"
#define C_TEST_RED "\033[0;31m"

#define C_TEST_STREAM stdout

#define C_TEST_PRINT_LINE()           fprintf(C_TEST_STREAM, "%s:%d", __FILE__, __LINE__)
#define C_TEST_PRINT_LINE1(M)         fprintf(C_TEST_STREAM, "%s:%d %s", __FILE__, __LINE__, M)
#define C_TEST_PRINT_LINE2(M, V1)     fprintf(C_TEST_STREAM, "%s:%d %s", __FILE__, __LINE__, M, V1)
#define C_TEST_PRINT_LINE3(M, V1, V2) fprintf(C_TEST_STREAM, "%s:%d %s", __FILE__, __LINE__, M, V1, V2)

#define C_TEST_PRINT_RAW(M) fprintf(C_TEST_STREAM, M)
#define C_TEST_PRINT_RAW1(M, V) fprintf(C_TEST_STREAM, M, V)
#define C_TEST_PRINT_RAW2(M, V1, V2) fprintf(C_TEST_STREAM, M, V1, V2)
#define C_TEST_PRINT_RAW3(M, V1, V2, V3) fprintf(C_TEST_STREAM, M, V1, V2, V3)

#define C_TEST_PRINT_MARGIN(PCT)                                               \
        C_TEST_PRINT_PCT(PCT);                                                 \
        C_TEST_PRINT_INDENT();                                                 \
        C_TEST_PRINT_RAW(" ");
#define C_TEST_PRINT_ASSERT_MARGIN(PCT)                                        \
        C_TEST_PRINT_MARGIN(PCT);                                              \
        C_TEST_PRINT_LINE();
#define C_TEST_PRINT_HR(PCT)                                                   \
        C_TEST_PRINT_PCT(PCT);                                                 \
        fprintf(C_TEST_STREAM, "--------------------------------------------------------------------\n");
#define C_TEST_PRINT_HR0(PCT)                                                  \
        C_TEST_PRINT_PCT(PCT);                                                 \
        fprintf(C_TEST_STREAM, "\n");
#define C_TEST_PRINT_HR2(PCT)                                                  \
        C_TEST_PRINT_PCT(PCT);                                                 \
        fprintf(C_TEST_STREAM, "====================================================================\n");

#define C_TEST_PRINT_INDENT()      fprintf(C_TEST_STREAM, "    ")
#define C_TEST_PRINT_PCT(V)        fprintf(C_TEST_STREAM, "[%3d%%] ", V)
#define C_TEST_PRINT_PASS_OR_FAIL(PASS, COLOR)                                 \
    if (PASS) {                                                                \
        if (COLOR) {                                                           \
            fprintf(C_TEST_STREAM, C_TEST_GREEN "PASS" C_TEST_NOCOLOR);        \
        } else {                                                               \
            fprintf(C_TEST_STREAM, "PASS");                                    \
        }                                                                      \
    } else {                                                                   \
        if (COLOR) {                                                           \
            fprintf(C_TEST_STREAM, C_TEST_RED "FAIL" C_TEST_NOCOLOR);          \
        } else {                                                               \
            fprintf(C_TEST_STREAM, "FAIL");                                    \
        }                                                                      \
    }

extern void c_test_print_header(const char * name,
        c_uint32_t nsuites,
        c_uint32_t ntests);
extern void c_test_print_start(
        c_uint32_t pct,
        const char *name,
        c_uint32_t major_id,
        c_uint32_t minor_id);
extern void c_test_print_status(
        c_uint32_t pct,
        const char *str1,
        const char *str2,
        c_uint32_t time_nsec);
extern void c_test_print_status_assert(
        unsigned int pct,
        const char  *id,
        c_bool_t     color,
        c_bool_t     passed,
        unsigned int time_nsec);
extern void c_test_print_time(c_uint64_t nsec);
extern void c_test_print_total(
        c_uint32_t pct,
        char       color,
        const char *name,
        c_uint32_t ntests_fail,
        c_uint32_t ntests_pass,
        c_uint32_t nasserts_fail,
        c_uint32_t nasserts_pass,
        c_uint32_t time_nsec);

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/