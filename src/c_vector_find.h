#ifndef C_VECTOR_FIND_INCLUDED
#define C_VECTOR_FIND_INCLUDED

#include "c_vector.h"

#ifdef __cplusplus
extern "C" {
#endif

/** Linear scan entire vector until first item is found.
 *  Runtime: O(n)
 * @param vector Must not be NULL, since no check is done.
 * @return -1 if not found, otherwise 0-based index.
 * @see c_vector_find_safe()
 */
#define C_VECTOR_FIND(NAME, TYPE)                                              \
    extern c_int64_t c_vector_find##NAME(const C_VECTOR_TYPE(NAME) vector,     \
                                         const TYPE item);

/** Logarithmic search for item in ascending sorted vector.
 *  Runtime: O(log_2(n))
 * @return -1 if not found, otherwise 0-based index.
 */
#define C_VECTOR_FIND_BISECT(NAME, TYPE)                                       \
    extern c_int64_t c_vector_find_bisect##NAME(const C_VECTOR_TYPE(NAME) vector,\
                                                const TYPE item);

/** Logarithmic search for index if item would be inserted into sorted vector.
 *  Runtime: O(log_2(n))
 * @return 0-based index. -1 if vector is invalid.
 */
#define C_VECTOR_FIND_BISECT_INSERT(NAME, TYPE)                                \
    extern c_int64_t c_vector_find_bisect_insert##NAME(                        \
                            const C_VECTOR_TYPE(NAME) vector,                  \
                            const TYPE item);

/** Logarithmic search for index if item would be inserted into sorted vector.
 *  Runtime: O(log_2(n))
 * @param cmp_udf Comparison user function that returns 0 if equal, -1 if less
 *                than, 1 if greater.
 * @return 0-based index. -1 if vector is invalid.
 */
#define C_VECTOR_FIND_BISECT_INSERT_UDF(NAME, TYPE)                            \
    extern c_int64_t c_vector_find_bisect_insert_udf##NAME(                    \
                            const C_VECTOR_TYPE(NAME) vector,                  \
                            const TYPE item,                                   \
                            c_int32_t (*cmp_udf)(const TYPE, const TYPE));

/** Logarithmic search for item in ascending sorted vector with user function.
 *  Runtime: O(log_2(n))
 * @param cmp_udf Returns 0 if items compared are equal,
 *                -1 if first item is less than second, 1 if greater.
 * @return -1 if not found, otherwise 0-based index.
 */
#define C_VECTOR_FIND_BISECT_UDF(NAME, TYPE)                                   \
    extern c_int64_t c_vector_find_bisect_udf##NAME(                           \
                        const C_VECTOR_TYPE(NAME) vector,                      \
                        const TYPE item,                                       \
                        c_int32_t (*cmp_udf)(const TYPE, const TYPE));

/** Linear scan entire vector until first item is found, for multiple items.
 *  Runtime: O(n1*n2)
 * @return Vector of -1 if not found, otherwise 0-based index, for each needle.
 *         If either input is NULL, then output is NULL.
 */
#define C_VECTOR_FIND_MANY(NAME, TYPE)                                         \
    extern c_vector_int64_t c_vector_find_many##NAME(const C_VECTOR_TYPE(NAME) haystack,\
                                                     const C_VECTOR_TYPE(NAME) needles);

/** Linear scan entire vector until first item is found, for multiple items.
 *  Runtime: O(n1*n2)
 * @param cmp_udf User defined comparison function, returns 0 if items are equal.
 * @return Vector of -1 if not found, otherwise 0-based index, for each needle.
 *         If either input is NULL, then output is NULL.
 */
#define C_VECTOR_FIND_MANY_UDF(NAME, TYPE)                                     \
    extern c_vector_int64_t c_vector_find_many_udf##NAME(                      \
                                const C_VECTOR_TYPE(NAME) haystack,            \
                                const C_VECTOR_TYPE(NAME) needles,             \
                                c_int32_t (*cmp_udf)(const TYPE, const TYPE));

/** Linear scan until first item is found.
 *  Runtime: O(n)
 * @param first Index to start scan.
 * @param last Index to end scan inclusively.
 * @return -1 if not found, otherwise 0-based index.
 */
#define C_VECTOR_FIND_RANGE(NAME, TYPE)                                        \
    extern c_int64_t c_vector_find_range##NAME(const C_VECTOR_TYPE(NAME) vector,\
                                               const TYPE item,                \
                                               c_uint64_t first,               \
                                               c_uint64_t last);

/** Linear scan until first item is found with user defined function.
 *  Runtime: O(n)
 * @param first Index to start scan.
 * @param last Index to end scan inclusively.
 * @param cmp_udf Returns 0 if two items are equal.
 * @return -1 if not found, otherwise 0-based index.
 */
#define C_VECTOR_FIND_RANGE_UDF(NAME, TYPE)                                    \
    extern c_int64_t c_vector_find_range_udf##NAME(                            \
                            const C_VECTOR_TYPE(NAME) vector,                  \
                            const TYPE item,                                   \
                            c_uint64_t first,                                  \
                            c_uint64_t last,                                   \
                            c_int32_t (*cmp_udf)(const TYPE, const TYPE));

/** Linear scan entire vector until first item is found.
 *  Checks if vector is valid before searching.
 *  Runtime: O(n)
 * @return -1 if not found, otherwise 0-based index.
 */
#define C_VECTOR_FIND_SAFE(NAME, TYPE)                                         \
    extern c_int64_t c_vector_find_safe##NAME(const C_VECTOR_TYPE(NAME) vector,\
                                              const TYPE item);

/** Linear scan until first item is found using user defined function.
 * Warning: Assumes vector is not NULL.
 *  Runtime: O(n)
 * @param cmp_udf Returns 0 if two items are equal.
 * @return -1 if not found, otherwise 0-based index.
 * @see c_vector_find_udf_safe()
 */
#define C_VECTOR_FIND_UDF(NAME, TYPE)                                          \
    extern c_int64_t c_vector_find_udf##NAME(                                  \
                        const C_VECTOR_TYPE(NAME) vector,                      \
                        const TYPE item,                                       \
                        c_int32_t (*cmp_udf)(const TYPE, const TYPE));

/** Linear scan until first item is found using user defined function.
 *  Runtime: O(n)
 * @param cmp_udf Returns 0 if two items are equal.
 * @return -1 if not found, otherwise 0-based index.
 */
#define C_VECTOR_FIND_UDF_SAFE(NAME, TYPE)                                     \
    extern c_int64_t c_vector_find_udf_safe##NAME(                             \
                        const C_VECTOR_TYPE(NAME) vector,                      \
                        const TYPE item,                                       \
                        c_int32_t (*cmp_udf)(const TYPE, const TYPE));

/** Linear scan entire vector from end until item is found.
 *  Runtime: O(n)
 * @return -1 if not found, otherwise 0-based index.
 */
#define C_VECTOR_RFIND(NAME, TYPE)                                             \
    extern c_int64_t c_vector_rfind##NAME(const C_VECTOR_TYPE(NAME) vector,    \
                                          const TYPE item);

/** Linear scan from end until item is found.
 *  Runtime: O(n)
 * @param first Index to start scan.
 * @param last Index to end scan inclusively.
 * @return -1 if not found, otherwise 0-based index.
 */
#define C_VECTOR_RFIND_RANGE(NAME, TYPE)                                       \
    extern c_int64_t c_vector_rfind_range##NAME(const C_VECTOR_TYPE(NAME) vector,\
                                                const TYPE item,               \
                                                c_uint64_t first,              \
                                                c_uint64_t last);

/** Linear scan from end of vector for item with user defined function.
 *  Runtime: O(n)
 * @param first Index to start scan.
 * @param last Index to end scan inclusively.
 * @param cmp_udf Returns 0 if two items are equal.
 * @return -1 if not found, otherwise 0-based index.
 */
#define C_VECTOR_RFIND_RANGE_UDF(NAME, TYPE)                                   \
    extern c_int64_t c_vector_rfind_range_udf##NAME(                           \
                        const C_VECTOR_TYPE(NAME) vector,                      \
                        const TYPE item,                                       \
                        c_uint64_t first,                                      \
                        c_uint64_t last,                                       \
                        c_int32_t (*cmp_udf)(const TYPE, const TYPE));

/** Linear scan from end of vector for item index using user defined function.
 *  Runtime: O(n)
 * @param cmp_udf Returns 0 if two items are equal.
 * @return -1 if not found, otherwise 0-based index.
 */
#define C_VECTOR_RFIND_UDF(NAME, TYPE)                                         \
    extern c_int64_t c_vector_rfind_udf##NAME(                                 \
                        const C_VECTOR_TYPE(NAME) vector,                      \
                        const TYPE item,                                       \
                        c_int32_t (*cmp_udf)(const TYPE, const TYPE));


C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_FIND)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_FIND_BISECT)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_FIND_BISECT_INSERT)
C_TYPE_TEMPLATE_NAME_TYPE_PTR(C_VECTOR_FIND_BISECT_INSERT_UDF)
C_TYPE_TEMPLATE_NAME_TYPE_PTR(C_VECTOR_FIND_BISECT_UDF)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_FIND_MANY)
C_TYPE_TEMPLATE_NAME_TYPE_PTR(C_VECTOR_FIND_MANY_UDF)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_FIND_RANGE)
C_TYPE_TEMPLATE_NAME_TYPE_PTR(C_VECTOR_FIND_RANGE_UDF)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_FIND_SAFE)
C_TYPE_TEMPLATE_NAME_TYPE_PTR(C_VECTOR_FIND_UDF)
C_TYPE_TEMPLATE_NAME_TYPE_PTR(C_VECTOR_FIND_UDF_SAFE)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_RFIND)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_RFIND_RANGE)
C_TYPE_TEMPLATE_NAME_TYPE_PTR(C_VECTOR_RFIND_RANGE_UDF)
C_TYPE_TEMPLATE_NAME_TYPE_PTR(C_VECTOR_RFIND_UDF)

#undef C_VECTOR_FIND
#undef C_VECTOR_FIND_BISECT
#undef C_VECTOR_FIND_BISECT_INSERT
#undef C_VECTOR_FIND_BISECT_INSERT_UDF
#undef C_VECTOR_FIND_BISECT_UDF
#undef C_VECTOR_FIND_MANY
#undef C_VECTOR_FIND_MANY_UDF
#undef C_VECTOR_FIND_RANGE
#undef C_VECTOR_FIND_RANGE_UDF
#undef C_VECTOR_FIND_SAFE
#undef C_VECTOR_FIND_UDF
#undef C_VECTOR_FIND_UDF_SAFE
#undef C_VECTOR_RFIND
#undef C_VECTOR_RFIND_RANGE
#undef C_VECTOR_RFIND_RANGE_UDF
#undef C_VECTOR_RFIND_UDF

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/