#ifndef TEST_C_BENCH_INCLUDED
#define TEST_C_BENCH_INCLUDED

#include "c_test.h"
#include "c_bench.h"

void run_dummy_noop()
{
}

void run_dummy_loop()
{
    c_uint64_t n = 100000;
    c_float64_t x = 2;

    while (--n) {
        x *= 2;
    }
}

void setup_dummy()
{
}

void teardown_dummy()
{
}

void test_c_bench_new()
{
    c_bench_t b = c_bench_new(0, NULL);
    C_TEST_ASSERT(b, "Expected new");

    c_bench_free(& b);
    C_TEST_ASSERT_FALSE(b, "Expected free");
}

void test_c_bench_run()
{
    c_bench_t b = c_bench_new();
    C_TEST_ASSERT(b, "Expected new");

    c_bench_run(b, 1000, run_dummy_noop);
    c_bench_print(b);

    c_bench_free(& b);
    C_TEST_ASSERT_FALSE(b, "Expected free");
}

void test_c_bench_run_auto()
{
    c_bench_t b = c_bench_new();
    C_TEST_ASSERT(b, "Expected new");

    c_bench_run(b, 0, run_dummy_loop);
    c_bench_print(b);

    c_bench_free(& b);
    C_TEST_ASSERT_FALSE(b, "Expected free");
}

void test_c_bench_run_setup_teardown()
{
    c_bench_t b = c_bench_new();
    C_TEST_ASSERT(b, "Expected new");

    c_bench_run_setup_teardown(b, 1, run_dummy_noop, NULL, NULL);
    c_bench_run_setup_teardown(b, 1,
            run_dummy_noop, setup_dummy, teardown_dummy);
    c_bench_run_setup_teardown(b, 0,
            run_dummy_loop, setup_dummy, teardown_dummy);

    c_bench_free(& b);
    C_TEST_ASSERT_FALSE(b, "Expected free");
}


int suite_test_c_bench()
{
    C_TEST_ADD_SUITE("suite_test_c_bench", NULL, NULL);

    C_TEST_ADD_TEST(test_c_bench_new);
    C_TEST_ADD_TEST(test_c_bench_run);
    C_TEST_ADD_TEST(test_c_bench_run_auto);
    C_TEST_ADD_TEST(test_c_bench_run_setup_teardown);

    C_TEST_RETURN();
}

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/
