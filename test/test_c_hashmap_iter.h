#ifndef TEST_C_HASHMAP_ITER_INCLUDED
#define TEST_C_HASHMAP_ITER_INCLUDED

#include "c_test.h"
#include "c_hashmap_iter.h"
#include "c_log.h"

void test_c_hashmap_iter()
{
    #define TEST(KN, KT, VN, VT)                                               \
                                                                               \
    C_HASHMAP_TYPE(KN, VN) hm##KN##VN = NULL;                                  \
    C_HASHMAP_ITER_TYPE(KN, VN) i##KN##VN = NULL;                              \
    c_int64_t v1##KN##VN = 1;                                                  \
    c_int64_t v2##KN##VN = 2;                                                  \
                                                                               \
    C_TEST_ASSERT_FALSE(c_hashmap_iter_is_valid##KN##VN(NULL),                 \
                        "Expected invalid " #KN#VN);                           \
                                                                               \
    i##KN##VN = c_hashmap_iter_new##KN##VN(NULL);                              \
    C_TEST_ASSERT(i##KN##VN, "Expected non null iter i" #KN#VN);               \
    C_TEST_ASSERT_FALSE(c_hashmap_iter_is_valid##KN##VN(i##KN##VN),            \
                        "Expected invalid i" #KN#VN);                          \
    C_TEST_ASSERT(c_hashmap_iter_is_done##KN##VN(i##KN##VN),                   \
                  "Expected done i" #KN#VN);                                   \
    c_hashmap_iter_free##KN##VN(& i##KN##VN);                                  \
    C_TEST_ASSERT_EQ_PTR(i##KN##VN, 0, "Expected iter free " #KN#VN);          \
                                                                               \
    i##KN##VN = c_hashmap_iter_new##KN##VN(hm##KN##VN);                        \
    C_TEST_ASSERT_FALSE(c_hashmap_iter_is_valid##KN##VN(i##KN##VN),            \
                  "Expected invalid " #KN#VN);                                 \
    C_TEST_ASSERT(c_hashmap_iter_is_done##KN##VN(i##KN##VN),                   \
                  "Expected done " #KN#VN);                                    \
    c_hashmap_iter_free##KN##VN(& i##KN##VN);                                  \
    C_TEST_ASSERT_EQ_PTR(i##KN##VN, 0, "Expected iter free " #KN#VN);          \
                                                                               \
    hm##KN##VN = c_hashmap_new##KN##VN(10, NULL, NULL);                        \
    c_hashmap_put##KN##VN(hm##KN##VN, 1, & v1##KN##VN);                        \
    c_hashmap_put##KN##VN(hm##KN##VN, 2, & v2##KN##VN);                        \
                                                                               \
    c_hashmap_iter_free##KN##VN(& i##KN##VN);                                  \
    C_TEST_ASSERT_EQ_PTR(i##KN##VN, 0, "Expected iter free " #KN#VN);          \
                                                                               \
    i##KN##VN = c_hashmap_iter_new##KN##VN(hm##KN##VN);                        \
    C_TEST_ASSERT(c_hashmap_iter_is_valid##KN##VN(i##KN##VN),                  \
                  "Expected new to be valid " #KN#VN);                         \
    C_TEST_ASSERT_FALSE(c_hashmap_iter_is_done##KN##VN(i##KN##VN),             \
                        "Expected not done " #KN#VN);                          \
    C_TEST_ASSERT(c_hashmap_iter_hash##KN##VN(i##KN##VN),                      \
                  "Expected iter hash " #KN#VN);                               \
    C_TEST_ASSERT(c_hashmap_iter_key##KN##VN(i##KN##VN),                       \
                  "Expected iter key " #KN#VN);                                \
    C_TEST_ASSERT(c_hashmap_iter_value##KN##VN(i##KN##VN),                     \
                  "Expected iter value " #KN#VN);                              \
    C_TEST_ASSERT_EQ(c_hashmap_iter_position##KN##VN(i##KN##VN),               \
                     0, "Expected iter position " #KN#VN);                     \
                                                                               \
    c_hashmap_iter_advance##KN##VN(i##KN##VN);                                 \
    C_TEST_ASSERT(c_hashmap_iter_is_valid##KN##VN(i##KN##VN),                  \
                  "Expected valid " #KN#VN);                                   \
    C_TEST_ASSERT_FALSE(c_hashmap_iter_is_done##KN##VN(i##KN##VN),             \
                        "Expected not done " #KN#VN);                          \
    C_TEST_ASSERT(c_hashmap_iter_hash##KN##VN(i##KN##VN),                      \
                  "Expected iter hash " #KN#VN);                               \
    C_TEST_ASSERT(c_hashmap_iter_key##KN##VN(i##KN##VN),                       \
                  "Expected iter key " #KN#VN);                                \
    C_TEST_ASSERT(c_hashmap_iter_value##KN##VN(i##KN##VN),                     \
                  "Expected iter value " #KN#VN);                              \
    C_TEST_ASSERT_EQ(c_hashmap_iter_position##KN##VN(i##KN##VN),               \
                     1, "Expected iter position " #KN#VN);                     \
                                                                               \
    c_hashmap_iter_advance##KN##VN(i##KN##VN);                                 \
    C_TEST_ASSERT(c_hashmap_iter_is_done##KN##VN(i##KN##VN),                   \
                  "Expected done " #KN#VN);                                    \
                                                                               \
    c_hashmap_iter_free##KN##VN(& i##KN##VN);                                  \
    C_TEST_ASSERT_EQ_PTR(i##KN##VN, 0, "Expected iter free " #KN#VN);          \
                                                                               \
    i##KN##VN = c_hashmap_iter_new##KN##VN(hm##KN##VN);                        \
    C_TEST_ASSERT(c_hashmap_iter_is_valid##KN##VN(i##KN##VN),                  \
                  "Expected new to be valid " #KN#VN);                         \
    c_hashmap_put##KN##VN(hm##KN##VN, 3, & v1##KN##VN);                        \
    C_TEST_ASSERT_FALSE(c_hashmap_iter_is_valid##KN##VN(i##KN##VN),            \
                        "Expected invalid after hashmap change " #KN#VN);      \
                                                                               \
    c_hashmap_iter_reset##KN##VN(i##KN##VN);                                   \
    C_TEST_ASSERT(c_hashmap_iter_is_valid##KN##VN(i##KN##VN),                  \
                  "Expected valid after reset " #KN#VN);                       \
                                                                               \
    c_hashmap_free##KN##VN(& hm##KN##VN);                                      \
    c_hashmap_iter_free##KN##VN(& i##KN##VN);                                  \
    C_TEST_ASSERT_EQ_PTR(i##KN##VN, 0, "Expected iter free " #KN#VN);

    C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE_NUMERIC(TEST)
    #undef TEST
}

void test_c_hashmap_iter_ptr()
{
    #define TEST(KN, KT, VN, VT)                                               \
                                                                               \
    C_HASHMAP_TYPE(KN, VN) hm##KN##VN = NULL;                                  \
    C_HASHMAP_ITER_TYPE(KN, VN) i##KN##VN = NULL;                              \
    c_int64_t v1##KN##VN = 1;                                                  \
    c_int64_t v2##KN##VN = 2;                                                  \
                                                                               \
    C_TEST_ASSERT_FALSE(c_hashmap_iter_is_valid##KN##VN(NULL),                 \
                        "Expected invalid " #KN#VN);                           \
                                                                               \
    i##KN##VN = c_hashmap_iter_new##KN##VN(NULL);                              \
    C_TEST_ASSERT(i##KN##VN, "Expected non null iter i" #KN#VN);               \
    C_TEST_ASSERT_FALSE(c_hashmap_iter_is_valid##KN##VN(i##KN##VN),            \
                        "Expected invalid i" #KN#VN);                          \
    C_TEST_ASSERT(c_hashmap_iter_is_done##KN##VN(i##KN##VN),                   \
                  "Expected done i" #KN#VN);                                   \
    c_hashmap_iter_free##KN##VN(& i##KN##VN);                                  \
    C_TEST_ASSERT_EQ_PTR(i##KN##VN, 0, "Expected iter free " #KN#VN);          \
                                                                               \
    i##KN##VN = c_hashmap_iter_new##KN##VN(hm##KN##VN);                        \
    C_TEST_ASSERT_FALSE(c_hashmap_iter_is_valid##KN##VN(i##KN##VN),            \
                  "Expected invalid " #KN#VN);                                 \
    C_TEST_ASSERT(c_hashmap_iter_is_done##KN##VN(i##KN##VN),                   \
                  "Expected done " #KN#VN);                                    \
    c_hashmap_iter_free##KN##VN(& i##KN##VN);                                  \
    C_TEST_ASSERT_EQ_PTR(i##KN##VN, 0, "Expected iter free " #KN#VN);          \
                                                                               \
    hm##KN##VN = c_hashmap_new##KN##VN(10, NULL, NULL);                        \
    c_hashmap_put##KN##VN(hm##KN##VN, & v1##KN##VN, & v1##KN##VN);             \
    c_hashmap_put##KN##VN(hm##KN##VN, & v2##KN##VN, & v2##KN##VN);             \
                                                                               \
    c_hashmap_iter_free##KN##VN(& i##KN##VN);                                  \
    C_TEST_ASSERT_EQ_PTR(i##KN##VN, 0, "Expected iter free " #KN#VN);          \
                                                                               \
    i##KN##VN = c_hashmap_iter_new##KN##VN(hm##KN##VN);                        \
    C_TEST_ASSERT(c_hashmap_iter_is_valid##KN##VN(i##KN##VN),                  \
                  "Expected new to be valid " #KN#VN);                         \
    C_TEST_ASSERT_FALSE(c_hashmap_iter_is_done##KN##VN(i##KN##VN),             \
                        "Expected not done " #KN#VN);                          \
    C_TEST_ASSERT(c_hashmap_iter_hash##KN##VN(i##KN##VN),                      \
                  "Expected iter hash " #KN#VN);                               \
    C_TEST_ASSERT(c_hashmap_iter_key##KN##VN(i##KN##VN),                       \
                  "Expected iter key " #KN#VN);                                \
    C_TEST_ASSERT(c_hashmap_iter_value##KN##VN(i##KN##VN),                     \
                  "Expected iter value " #KN#VN);                              \
    C_TEST_ASSERT_EQ(c_hashmap_iter_position##KN##VN(i##KN##VN),               \
                     0, "Expected iter position " #KN#VN);                     \
                                                                               \
    c_hashmap_iter_advance##KN##VN(i##KN##VN);                                 \
    C_TEST_ASSERT(c_hashmap_iter_is_valid##KN##VN(i##KN##VN),                  \
                  "Expected valid " #KN#VN);                                   \
    C_TEST_ASSERT_FALSE(c_hashmap_iter_is_done##KN##VN(i##KN##VN),             \
                        "Expected not done " #KN#VN);                          \
    C_TEST_ASSERT(c_hashmap_iter_hash##KN##VN(i##KN##VN),                      \
                  "Expected iter hash " #KN#VN);                               \
    C_TEST_ASSERT(c_hashmap_iter_key##KN##VN(i##KN##VN),                       \
                  "Expected iter key " #KN#VN);                                \
    C_TEST_ASSERT(c_hashmap_iter_value##KN##VN(i##KN##VN),                     \
                  "Expected iter value " #KN#VN);                              \
    C_TEST_ASSERT_EQ(c_hashmap_iter_position##KN##VN(i##KN##VN),               \
                     1, "Expected iter position " #KN#VN);                     \
                                                                               \
    c_hashmap_iter_advance##KN##VN(i##KN##VN);                                 \
    C_TEST_ASSERT(c_hashmap_iter_is_done##KN##VN(i##KN##VN),                   \
                  "Expected done " #KN#VN);                                    \
                                                                               \
    c_hashmap_iter_free##KN##VN(& i##KN##VN);                                  \
    C_TEST_ASSERT_EQ_PTR(i##KN##VN, 0, "Expected iter free " #KN#VN);          \
                                                                               \
    i##KN##VN = c_hashmap_iter_new##KN##VN(hm##KN##VN);                        \
    C_TEST_ASSERT(c_hashmap_iter_is_valid##KN##VN(i##KN##VN),                  \
                  "Expected new to be valid " #KN#VN);                         \
    c_hashmap_put##KN##VN(hm##KN##VN, & v2##KN##VN, & v1##KN##VN);             \
    C_TEST_ASSERT_FALSE(c_hashmap_iter_is_valid##KN##VN(i##KN##VN),            \
                        "Expected invalid after hashmap change " #KN#VN);      \
                                                                               \
    c_hashmap_iter_reset##KN##VN(i##KN##VN);                                   \
    C_TEST_ASSERT(c_hashmap_iter_is_valid##KN##VN(i##KN##VN),                  \
                  "Expected valid after reset " #KN#VN);                       \
                                                                               \
    c_hashmap_free##KN##VN(& hm##KN##VN);                                      \
    c_hashmap_iter_free##KN##VN(& i##KN##VN);                                  \
    C_TEST_ASSERT_EQ_PTR(i##KN##VN, 0, "Expected iter free " #KN#VN);

    C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE_PTR(TEST)
    #undef TEST
}

void test_c_hashmap_iter_str()
{
    c_hashmap_str_t hm = NULL;
    c_hashmap_iter_str_t i = NULL;
    c_int64_t v1 = 1;
    c_int64_t v2 = 2;
    c_str_t k1 = c_str_new("key1", 4);
    c_str_t k2 = c_str_new("key2", 4);

    C_TEST_ASSERT_FALSE(c_hashmap_iter_is_valid_str(NULL),
                        "Expected invalid");

    i = c_hashmap_iter_new_str(NULL);
    C_TEST_ASSERT(i, "Expected non null iter");
    C_TEST_ASSERT_FALSE(c_hashmap_iter_is_valid_str(i),
                        "Expected invalid");
    C_TEST_ASSERT(c_hashmap_iter_is_done_str(i),
                  "Expected done");
    c_hashmap_iter_free_str(& i);
    C_TEST_ASSERT_EQ_PTR(i, 0, "Expected iter free");

    i = c_hashmap_iter_new_str(hm);
    C_TEST_ASSERT_FALSE(c_hashmap_iter_is_valid_str(i),
                  "Expected invalid");
    C_TEST_ASSERT(c_hashmap_iter_is_done_str(i),
                  "Expected done");
    c_hashmap_iter_free_str(& i);
    C_TEST_ASSERT_EQ_PTR(i, 0, "Expected iter free");

    hm = c_hashmap_new_str(10, NULL, NULL);
    c_hashmap_put_str(hm, k1, & v1);
    c_hashmap_put_str(hm, k2, & v2);

    c_hashmap_iter_free_str(& i);
    C_TEST_ASSERT_EQ_PTR(i, 0, "Expected iter free");

    i = c_hashmap_iter_new_str(hm);
    C_TEST_ASSERT(c_hashmap_iter_is_valid_str(i),
                  "Expected new to be valid ");
    C_TEST_ASSERT_FALSE(c_hashmap_iter_is_done_str(i),
                        "Expected not done ");
    C_TEST_ASSERT(c_hashmap_iter_hash_str(i),
                  "Expected iter hash ");
    C_TEST_ASSERT(c_hashmap_iter_key_str(i),
                  "Expected iter key ");
    C_TEST_ASSERT(c_hashmap_iter_value_str(i),
                  "Expected iter value ");
    C_TEST_ASSERT_EQ(c_hashmap_iter_position_str(i),
                     0, "Expected iter position ");

    c_hashmap_iter_advance_str(i);
    C_TEST_ASSERT(c_hashmap_iter_is_valid_str(i),
                  "Expected valid ");
    C_TEST_ASSERT_FALSE(c_hashmap_iter_is_done_str(i),
                        "Expected not done ");
    C_TEST_ASSERT(c_hashmap_iter_hash_str(i),
                  "Expected iter hash ");
    C_TEST_ASSERT(c_hashmap_iter_key_str(i),
                  "Expected iter key ");
    C_TEST_ASSERT(c_hashmap_iter_value_str(i),
                  "Expected iter value ");
    C_TEST_ASSERT_EQ(c_hashmap_iter_position_str(i),
                     1, "Expected iter position ");

    c_hashmap_iter_advance_str(i);
    C_TEST_ASSERT(c_hashmap_iter_is_done_str(i),
                  "Expected done ");

    c_hashmap_iter_free_str(& i);
    C_TEST_ASSERT_EQ_PTR(i, 0, "Expected iter free ");

    i = c_hashmap_iter_new_str(hm);
    C_TEST_ASSERT(c_hashmap_iter_is_valid_str(i),
                  "Expected new to be valid ");
    c_hashmap_put_str(hm, k2, & v1);
    C_TEST_ASSERT_FALSE(c_hashmap_iter_is_valid_str(i),
                        "Expected invalid after hashmap change ");

    c_hashmap_iter_reset_str(i);
    C_TEST_ASSERT(c_hashmap_iter_is_valid_str(i),
                  "Expected valid after reset ");

    c_hashmap_free_str(& hm);
    c_hashmap_iter_free_str(& i);
    c_str_free(& k1);
    c_str_free(& k2);
    C_TEST_ASSERT_EQ_PTR(i, 0, "Expected iter free ");

}

int suite_test_c_hashmap_iter()
{
    C_TEST_ADD_SUITE("suite_test_c_hashmap_iter", NULL, NULL);

    C_TEST_ADD_TEST(test_c_hashmap_iter);
    C_TEST_ADD_TEST(test_c_hashmap_iter_ptr);
    C_TEST_ADD_TEST(test_c_hashmap_iter_str);

    C_TEST_RETURN();
}

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/
