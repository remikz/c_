#ifndef TEST_C_TIME_INCLUDED
#define TEST_C_TIME_INCLUDED

#include "c_test.h"
#include "c_time.h"

void test_c_time()
{
    c_uint64_t t;
    const c_uint32_t nbuf = 32;
    char buf[nbuf];

    t = 123e9;
    c_time_nsec_str(buf, nbuf, t);
    C_TEST_ASSERT(strcmp("123s", buf) == 0, "Wrong string.");
    t = 456e6;
    c_time_nsec_str(buf, nbuf, t);
    C_TEST_ASSERT(strcmp("456ms", buf) == 0, "Wrong string.");
    t = 789e3;
    c_time_nsec_str(buf, nbuf, t);
    C_TEST_ASSERT(strcmp("789us", buf) == 0, "Wrong string.");
    t = 321;
    c_time_nsec_str(buf, nbuf, t);
    C_TEST_ASSERT(strcmp("321ns", buf) == 0, "Wrong string.");

    C_TEST_ASSERT(c_time_now_msec(), "Expected non zero time.");
    C_TEST_ASSERT(c_time_now_nsec(), "Expected non zero time.");
    C_TEST_ASSERT(c_time_now_usec(), "Expected non zero time.");
}

int suite_test_c_time()
{
    C_TEST_ADD_SUITE("suite_test_c_time", NULL, NULL);
    C_TEST_ADD_TEST(test_c_time);
recover:
    C_TEST_RETURN();
}

#endif
/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/