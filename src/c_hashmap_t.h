#ifndef C_HASHMAP_T_INCLUDED
#define C_HASHMAP_T_INCLUDED

#include "c_hashmap.h"
#include "c_vector.h"

#ifdef __cplusplus
extern "C" {
#endif

#define C_HASHMAP_T(KNAME, KTYPE, VNAME, VTYPE)                                \
struct C_HASHMAP_TYPE(KNAME, VNAME) {                                          \
    /** Vector of vectors. A bucket (vector) is null until a hash is added. */ \
    c_vector_t buckets;                                                        \
    /** User defined function to compare hash and keys. */                     \
    C_KEYVALUE_HASH_CMP_TYPE(KNAME, VNAME) cmp;                                \
    /** User defined function to hash keys. */                                 \
    C_HASH_NAME(KNAME) hash;                                                   \
    /** Number of items stored in all buckets. */                              \
    c_uint64_t nitems;                                                         \
    /** An increasing stamp that increases during each change,
        for iterator validity checks. */                                       \
    c_uint64_t timestamp;                                                      \
};

C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_T)
#undef C_HASHMAP_T

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/
