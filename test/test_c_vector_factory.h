#ifndef TEST_C_VECTOR_FACTORY_INCLUDED
#define TEST_C_VECTOR_FACTORY_INCLUDED

#include "c_test.h"
#include "c_vector_factory.h"

void test_c_vector_factory_range_ptr()
{
    #define TEST(N, T)                                                         \
    C_VECTOR_TYPE(N) v##N = 0;                                                 \
    c_int64_t start##N;                                                        \
    c_int64_t stop##N;                                                         \
    c_int64_t step##N;                                                         \
                                                                               \
    start##N = 0; stop##N = 0; step##N = 0;                                    \
    v##N = c_vector_range##N(start##N, stop##N, step##N);                      \
    C_TEST_ASSERT(v##N, "Expected non null.");                                 \
    C_TEST_ASSERT(c_vector_empty##N(v##N), "Expected empty.");                 \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    start##N = 0; stop##N = 1; step##N = 1;                                    \
    v##N = c_vector_range##N(start##N, stop##N, step##N);                      \
    C_TEST_ASSERT(v##N, "Expected non null.");                                 \
    C_TEST_ASSERT_EQ(c_vector_size##N(v##N), 1, "Expected size " #N);          \
    C_TEST_ASSERT(c_vector_get##N(v##N, 0), "Expected ptr " #N);       \
    C_TEST_ASSERT_EQ(*(T) c_vector_get##N(v##N, 0), 0, "Expected value " #N);       \
    c_vector_free_deep##N(& v##N);                                                  \
                                                                               \
    start##N = 0; stop##N = 10; step##N = 1;                                   \
    v##N = c_vector_range##N(start##N, stop##N, step##N);                      \
    C_TEST_ASSERT(v##N, "Expected non null.");                                 \
    C_TEST_ASSERT_EQ(c_vector_size##N(v##N), 10, "Expected size " #N);         \
    C_TEST_ASSERT(c_vector_get##N(v##N, 0), "Expected ptr " #N);       \
    C_TEST_ASSERT_EQ(*(T) c_vector_get##N(v##N, 0), 0, "Expected value " #N);       \
    C_TEST_ASSERT(c_vector_get##N(v##N, 9), "Expected ptr " #N);       \
    C_TEST_ASSERT_EQ(*(T) c_vector_get##N(v##N, 9), 9, "Expected value " #N);       \
    c_vector_free_deep##N(& v##N);                                                  \
                                                                               \
    start##N = 0; stop##N = 10; step##N = 2;                                   \
    v##N = c_vector_range##N(start##N, stop##N, step##N);                      \
    C_TEST_ASSERT(v##N, "Expected non null.");                                 \
    C_TEST_ASSERT_EQ(c_vector_size##N(v##N), 5, "Expected size " #N);          \
    C_TEST_ASSERT(c_vector_get##N(v##N, 0), "Expected ptr " #N);       \
    C_TEST_ASSERT_EQ(*(T) c_vector_get##N(v##N, 0), 0, "Expected value " #N);       \
    C_TEST_ASSERT(c_vector_get##N(v##N, 4), "Expected ptr " #N);       \
    C_TEST_ASSERT_EQ(*(T) c_vector_get##N(v##N, 4), 8, "Expected value " #N);       \
    c_vector_free_deep##N(& v##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(TEST)
    #undef TEST
}

void test_c_vector_factory_range_value_unsigned()
{
    #define TEST(N, T)                                                         \
    C_VECTOR_TYPE(N) v##N = 0;                                                 \
    c_int64_t start##N;                                                        \
    c_int64_t stop##N;                                                         \
    c_int64_t step##N;                                                         \
                                                                               \
    start##N = 0; stop##N = 0; step##N = 0;                                    \
    v##N = c_vector_range##N(start##N, stop##N, step##N);                      \
    C_TEST_ASSERT(v##N, "Expected non null.");                                 \
    C_TEST_ASSERT(c_vector_empty##N(v##N), "Expected empty.");                 \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    start##N = 0; stop##N = 1; step##N = 1;                                    \
    v##N = c_vector_range##N(start##N, stop##N, step##N);                      \
    C_TEST_ASSERT(v##N, "Expected non null.");                                 \
    C_TEST_ASSERT_EQ(c_vector_size##N(v##N), 1, "Expected size " #N);          \
    C_TEST_ASSERT_EQ(c_vector_get##N(v##N, 0), 0, "Expected value " #N);       \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    start##N = 0; stop##N = 10; step##N = 1;                                   \
    v##N = c_vector_range##N(start##N, stop##N, step##N);                      \
    C_TEST_ASSERT(v##N, "Expected non null.");                                 \
    C_TEST_ASSERT_EQ(c_vector_size##N(v##N), 10, "Expected size " #N);         \
    C_TEST_ASSERT_EQ(c_vector_get##N(v##N, 0), 0, "Expected value " #N);       \
    C_TEST_ASSERT_EQ(c_vector_get##N(v##N, 9), 9, "Expected value " #N);       \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    start##N = 0; stop##N = 10; step##N = 2;                                   \
    v##N = c_vector_range##N(start##N, stop##N, step##N);                      \
    C_TEST_ASSERT(v##N, "Expected non null.");                                 \
    C_TEST_ASSERT_EQ(c_vector_size##N(v##N), 5, "Expected size " #N);          \
    C_TEST_ASSERT_EQ(c_vector_get##N(v##N, 0), 0, "Expected value " #N);       \
    C_TEST_ASSERT_EQ(c_vector_get##N(v##N, 4), 8, "Expected value " #N);       \
    c_vector_free##N(& v##N);

    C_TYPE_TEMPLATE_NAME_TYPE_VALUE(TEST)
    #undef TEST
}

void test_c_vector_factory_range_value_signed()
{
    #define TEST(N, T)                                                         \
    C_VECTOR_TYPE(N) v##N = 0;                                                 \
    c_int64_t start##N;                                                        \
    c_int64_t stop##N;                                                         \
    c_int64_t step##N;                                                         \
                                                                               \
    start##N = 10; stop##N = 0; step##N = -1;                                  \
    v##N = c_vector_range##N(start##N, stop##N, step##N);                      \
    C_TEST_ASSERT(v##N, "Expected non null.");                                 \
    C_TEST_ASSERT_EQ(c_vector_size##N(v##N), 10, "Expected size " #N);         \
    C_TEST_ASSERT_EQ(c_vector_get##N(v##N, 0), 10, "Expected value " #N);      \
    C_TEST_ASSERT_EQ(c_vector_get##N(v##N, 9), 1, "Expected value " #N);       \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    start##N = 10; stop##N = 0; step##N = -2;                                  \
    v##N = c_vector_range##N(start##N, stop##N, step##N);                      \
    C_TEST_ASSERT(v##N, "Expected non null.");                                 \
    C_TEST_ASSERT_EQ(c_vector_size##N(v##N), 5, "Expected size " #N);          \
    C_TEST_ASSERT_EQ(c_vector_get##N(v##N, 0), 10, "Expected value " #N);      \
    C_TEST_ASSERT_EQ(c_vector_get##N(v##N, 4), 2, "Expected value " #N);       \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    start##N = 0; stop##N = -10; step##N = -2;                                 \
    v##N = c_vector_range##N(start##N, stop##N, step##N);                      \
    C_TEST_ASSERT(v##N, "Expected non null.");                                 \
    C_TEST_ASSERT_EQ(c_vector_size##N(v##N), 5, "Expected size " #N);          \
    C_TEST_ASSERT_EQ(c_vector_get##N(v##N, 0), 0, "Expected value " #N);       \
    C_TEST_ASSERT_EQ(c_vector_get##N(v##N, 4), -8, "Expected value " #N);      \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    start##N = -10; stop##N = 0; step##N = 2;                                  \
    v##N = c_vector_range##N(start##N, stop##N, step##N);                      \
    C_TEST_ASSERT(v##N, "Expected non null.");                                 \
    C_TEST_ASSERT_EQ(c_vector_size##N(v##N), 5, "Expected size " #N);          \
    C_TEST_ASSERT_EQ(c_vector_get##N(v##N, 0), -10, "Expected value " #N);     \
    C_TEST_ASSERT_EQ(c_vector_get##N(v##N, 4), -2, "Expected value " #N);      \
    c_vector_free##N(& v##N);

    C_TYPE_TEMPLATE_NAME_TYPE_VALUE_SIGNED(TEST)
    #undef TEST
}
int suite_test_c_vector_factory()
{
    C_TEST_ADD_SUITE("suite_test_c_vector_factory", NULL, NULL);

    C_TEST_ADD_TEST(test_c_vector_factory_range_ptr);
    C_TEST_ADD_TEST(test_c_vector_factory_range_value_unsigned);
    C_TEST_ADD_TEST(test_c_vector_factory_range_value_signed);

    C_TEST_RETURN();
}

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/

