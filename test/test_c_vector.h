#ifndef TEST_C_VECTOR_INCLUDED
#define TEST_C_VECTOR_INCLUDED

#include "c_test.h"
#include "c_vector.h"
#include "c_memory.h"

void test_c_vector_append()
{
    #define TEST_APPEND(N, T)                                                  \
    c_uint64_t n##N = 1;                                                       \
    T a##N;                                                                    \
    C_VECTOR_TYPE(N) v##N = c_vector_new##N(n##N);                             \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT(c_vector_empty##N(v##N), "Failed empty " #N);                \
    c_vector_append##N(v##N, a##N);                                            \
    C_TEST_ASSERT_EQ(c_vector_size##N(v##N), 1, "Failed append " #N);          \
    c_vector_append##N(v##N, a##N);                                            \
    C_TEST_ASSERT_EQ(c_vector_size##N(v##N), 2, "Failed append " #N);          \
    c_vector_append##N(v##N, a##N);                                            \
    C_TEST_ASSERT_EQ(c_vector_size##N(v##N), 3, "Failed append " #N);          \
    C_TEST_ASSERT_EQ(c_vector_capacity##N(v##N), 4, "Failed append " #N);      \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_TYPE(TEST_APPEND)
    #undef TEST_APPEND
}

void test_c_vector_append_rate()
{
    #define TEST_APPEND_RATE(N, T)                                             \
    c_uint64_t n##N = 1;                                                       \
    T a##N;                                                                    \
    C_VECTOR_TYPE(N) v##N = c_vector_new##N(n##N);                             \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT(c_vector_empty##N(v##N), "Failed empty " #N);                \
    c_vector_append_rate##N(v##N, a##N, 2.0);                                  \
    C_TEST_ASSERT_EQ(c_vector_size##N(v##N), 1, "Failed append " #N);          \
    C_TEST_ASSERT_EQ(c_vector_capacity##N(v##N), 1, "Failed append " #N);      \
    c_vector_append_rate##N(v##N, a##N, 2.0);                                  \
    C_TEST_ASSERT_EQ(c_vector_size##N(v##N), 2, "Failed append " #N);          \
    c_vector_append_rate##N(v##N, a##N, -1);                                   \
    C_TEST_ASSERT_EQ(c_vector_size##N(v##N), 3, "Failed append " #N);          \
    C_TEST_ASSERT_EQ(c_vector_capacity##N(v##N), 3, "Failed append " #N);      \
    c_vector_append_rate##N(v##N, a##N, 3);                                    \
    C_TEST_ASSERT_EQ(c_vector_size##N(v##N), 4, "Failed append " #N);          \
    C_TEST_ASSERT_EQ(c_vector_capacity##N(v##N), 9, "Failed append " #N);      \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_TYPE(TEST_APPEND_RATE)
    #undef TEST_APPEND_RATE
}

void test_c_vector_capacity()
{
    #define TEST_CAPACITY(N)                                                   \
    c_uint64_t n##N = 3;                                                       \
    C_VECTOR_TYPE(N) v##N = c_vector_new##N(n##N);                             \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT_EQ(c_vector_capacity##N(v##N), 3, "Failed capacity " #N);    \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME(TEST_CAPACITY)
    #undef TEST_CAPACITY
}

void test_c_vector_clear()
{
    #define TEST_CAPACITY(N)                                                   \
    c_uint64_t n##N = 3;                                                       \
    C_VECTOR_TYPE(N) v##N = c_vector_new##N(n##N);                             \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    c_vector_clear##N(v##N);                                                   \
    C_TEST_ASSERT_EQ(c_vector_size##N(v##N), 0, "Failed clear " #N);           \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME(TEST_CAPACITY)
    #undef TEST_CAPACITY
}

void test_c_vector_compact()
{
    #define TEST_RESERVE(N, T)                                                 \
    c_uint64_t n##N = 2;                                                       \
    c_uint64_t m##N = 4;                                                       \
    C_VECTOR_TYPE(N) v##N = c_vector_new_set##N(m##N, n##N, 0);                \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT_EQ(c_vector_capacity##N(v##N), m##N, "Failed capacity " #N); \
    c_vector_compact##N(v##N);                                                 \
    C_TEST_ASSERT_EQ(c_vector_capacity##N(v##N), n##N, "Failed compact " #N);  \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_TYPE(TEST_RESERVE)
    #undef TEST_RESERVE
}

#define COPY_UDF(N, T)                                                         \
void* test_c_vector_copy_udf##N(void *item)                                    \
{                                                                              \
    T copy;                                                                    \
    C_MEMORY_NEW(copy);                                                        \
    *copy = *(T) item;                                                         \
    return (void*) copy;                                                       \
}
C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(COPY_UDF)
#undef COPY_UDF

void* test_c_vector_copy_udf(void *item)
{
    c_uint32_t *void_dummy;
    C_MEMORY_NEW(void_dummy);
    *void_dummy = 0;
    return (void*) void_dummy;
}

void test_c_vector_copy_udf_ptr()
{
    #define TEST_COPY_UDF_PTR(N, T)                                            \
    c_uint64_t n##N = 2;                                                       \
    C_VECTOR_TYPE(N) v1##N = NULL;                                             \
    C_VECTOR_TYPE(N) v2##N = NULL;                                             \
    C_VECTOR_TYPE(N) v3##N = NULL;                                             \
    T p1##N;                                                                   \
    C_MEMORY_NEW(p1##N);                                                       \
    c_vector_copy##N(NULL, NULL);                                              \
                                                                               \
    v1##N = c_vector_new_set##N(0, 0, 0);                                      \
    v2##N = c_vector_new_set##N(n##N, n##N, p1##N);                            \
    v3##N = c_vector_new_set##N(0, 0, 0);                                      \
    C_TEST_ASSERT(v1##N, "Failed to create vector " #N);                       \
    C_TEST_ASSERT(v2##N, "Failed to create vector " #N);                       \
    C_TEST_ASSERT(v3##N, "Failed to create vector " #N);                       \
                                                                               \
    c_vector_copy_udf##N(v1##N, v2##N, test_c_vector_copy_udf##N);             \
    C_TEST_ASSERT_EQ(n##N, c_vector_capacity##N(v1##N), "Failed copy " #N);    \
    C_TEST_ASSERT_EQ(n##N, c_vector_size##N(v1##N), "Failed copy " #N);        \
    C_TEST_ASSERT_NEQ_PTR(p1##N, c_vector_get##N(v1##N, 0), "Failed copy " #N);\
    C_TEST_ASSERT_NEQ_PTR(p1##N, c_vector_get##N(v1##N, 1), "Failed copy " #N);\
                                                                               \
    c_vector_copy_udf##N(v2##N, v3##N, test_c_vector_copy_udf##N);             \
    C_TEST_ASSERT_EQ(n##N, c_vector_capacity##N(v2##N), "Failed copy " #N);    \
    C_TEST_ASSERT_EQ(0, c_vector_size##N(v2##N), "Failed copy " #N);           \
                                                                               \
    c_vector_free_deep##N(& v1##N);                                            \
    c_vector_free##N(& v2##N);                                                 \
    c_vector_free##N(& v3##N);                                                 \
    C_TEST_ASSERT_FALSE(v1##N, "Failed to free vector " #N);                   \
    C_TEST_ASSERT_FALSE(v2##N, "Failed to free vector " #N);                   \
    C_TEST_ASSERT_FALSE(v3##N, "Failed to free vector " #N);                   \
    C_MEMORY_FREE(p1##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST_COPY_UDF_PTR)
    #undef TEST_COPY_UDF_PTR
}

void test_c_vector_copy_ptr()
{
    #define TEST_COPY_PTR(N, T)                                                \
    c_uint64_t n##N = 2;                                                       \
    C_VECTOR_TYPE(N) v1##N = NULL;                                             \
    C_VECTOR_TYPE(N) v2##N = NULL;                                             \
    C_VECTOR_TYPE(N) v3##N = NULL;                                             \
    T p1##N;                                                                   \
    C_MEMORY_NEW(p1##N);                                                       \
    c_vector_copy##N(NULL, NULL);                                              \
                                                                               \
    v1##N = c_vector_new_set##N(0, 0, 0);                                      \
    v2##N = c_vector_new_set##N(n##N, n##N, p1##N);                            \
    v3##N = c_vector_new_set##N(0, 0, 0);                                      \
    C_TEST_ASSERT(v1##N, "Failed to create vector " #N);                       \
    C_TEST_ASSERT(v2##N, "Failed to create vector " #N);                       \
    C_TEST_ASSERT(v3##N, "Failed to create vector " #N);                       \
    c_vector_copy##N(v1##N, v2##N);                                            \
    C_TEST_ASSERT_EQ(n##N, c_vector_capacity##N(v1##N), "Failed copy " #N);    \
    C_TEST_ASSERT_EQ(n##N, c_vector_size##N(v1##N), "Failed copy " #N);        \
    C_TEST_ASSERT_EQ_PTR(p1##N, c_vector_get##N(v1##N, 0), "Failed copy " #N); \
    C_TEST_ASSERT_EQ_PTR(p1##N, c_vector_get##N(v1##N, 1), "Failed copy " #N); \
    c_vector_copy##N(v2##N, v3##N);                                            \
    C_TEST_ASSERT_EQ(n##N, c_vector_capacity##N(v2##N), "Failed copy " #N);    \
    C_TEST_ASSERT_EQ(0, c_vector_size##N(v2##N), "Failed copy " #N);           \
    c_vector_free##N(& v1##N);                                                 \
    c_vector_free##N(& v2##N);                                                 \
    c_vector_free##N(& v3##N);                                                 \
    C_TEST_ASSERT_FALSE(v1##N, "Failed to free vector " #N);                   \
    C_TEST_ASSERT_FALSE(v2##N, "Failed to free vector " #N);                   \
    C_TEST_ASSERT_FALSE(v3##N, "Failed to free vector " #N);                   \
    C_MEMORY_FREE(p1##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST_COPY_PTR)
    #undef TEST_COPY_PTR
}

void test_c_vector_copy_value()
{
    #define TEST_COPY_VALUE(N, T)                                              \
    c_uint64_t n##N = 2;                                                       \
    C_VECTOR_TYPE(N) v1##N = NULL;                                             \
    C_VECTOR_TYPE(N) v2##N = NULL;                                             \
    C_VECTOR_TYPE(N) v3##N = NULL;                                             \
                                                                               \
    c_vector_copy##N(NULL, NULL);                                              \
                                                                               \
    v1##N = c_vector_new_set##N(0, 0, 0);                                      \
    v2##N = c_vector_new_set##N(n##N, n##N, 1);                                \
    v3##N = c_vector_new_set##N(0, 0, 0);                                      \
    C_TEST_ASSERT(v1##N, "Failed to create vector " #N);                       \
    C_TEST_ASSERT(v2##N, "Failed to create vector " #N);                       \
    C_TEST_ASSERT(v3##N, "Failed to create vector " #N);                       \
    c_vector_copy##N(v1##N, v2##N);                                            \
    C_TEST_ASSERT_EQ(n##N, c_vector_capacity##N(v1##N), "Failed copy " #N);    \
    C_TEST_ASSERT_EQ(n##N, c_vector_size##N(v1##N), "Failed copy " #N);        \
    C_TEST_ASSERT_EQ(1, c_vector_get##N(v1##N, 0), "Failed copy " #N);         \
    C_TEST_ASSERT_EQ(1, c_vector_get##N(v1##N, 1), "Failed copy " #N);         \
    c_vector_copy##N(v2##N, v3##N);                                            \
    C_TEST_ASSERT_EQ(n##N, c_vector_capacity##N(v2##N), "Failed copy " #N);    \
    C_TEST_ASSERT_EQ(0, c_vector_size##N(v2##N), "Failed copy " #N);           \
    c_vector_free##N(& v1##N);                                                 \
    c_vector_free##N(& v2##N);                                                 \
    c_vector_free##N(& v3##N);                                                 \
    C_TEST_ASSERT_FALSE(v1##N, "Failed to free vector " #N);                   \
    C_TEST_ASSERT_FALSE(v2##N, "Failed to free vector " #N);                   \
    C_TEST_ASSERT_FALSE(v3##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_TYPE_VALUE(TEST_COPY_VALUE)
    #undef TEST_COPY_VALUE
}

void test_c_vector_empty()
{
    #define TEST_EMPTY(N)                                                      \
    c_uint64_t n##N = 1;                                                       \
    C_VECTOR_TYPE(N) v##N = c_vector_new##N(n##N);                             \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT_EQ(c_vector_empty##N(v##N), C_TRUE, "Expected empty " #N);   \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);                    \
                                                                               \
    v##N = c_vector_new_set##N(n##N, n##N, 0);                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT_EQ(c_vector_empty##N(v##N), C_FALSE, "Expected non empty " #N);\
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME(TEST_EMPTY)
    #undef TEST_EMPTY
}

void test_c_vector_equals_ptr()
{
    #define TEST_EQUALS_PTR(N, T)                                              \
    c_uint64_t n##N = 4;                                                       \
    T p1##N;                                                                   \
    T p2##N;                                                                   \
    C_MEMORY_NEW(p1##N);                                                       \
    C_MEMORY_NEW(p2##N);                                                       \
    C_VECTOR_TYPE(N) v1##N = NULL;                                             \
    C_VECTOR_TYPE(N) v2##N = NULL;                                             \
    C_TEST_ASSERT(c_vector_equals##N(v1##N, v2##N), "Failed equals nulls " #N);\
                                                                               \
    v1##N = c_vector_new_set##N(0, 0, 0);                                      \
    v2##N = c_vector_new_set##N(0, 0, 0);                                      \
    C_TEST_ASSERT(v1##N, "Failed to create vector " #N);                       \
    C_TEST_ASSERT(v2##N, "Failed to create vector " #N);                       \
    C_TEST_ASSERT(c_vector_empty##N(v1##N), "Failed to create empty vector " #N);\
    C_TEST_ASSERT(c_vector_empty##N(v2##N), "Failed to create empty vector " #N);\
                                                                               \
    C_TEST_ASSERT(c_vector_equals##N(v1##N, v2##N), "Failed equals empty " #N);\
                                                                               \
    c_vector_free##N(& v1##N);                                                 \
    c_vector_free##N(& v2##N);                                                 \
                                                                               \
    v1##N = c_vector_new_set##N(n##N, n##N, p1##N);                            \
    v2##N = c_vector_new_set##N(n##N, n##N, p1##N);                            \
    C_TEST_ASSERT(v1##N, "Failed to create vector " #N);                       \
    C_TEST_ASSERT(v2##N, "Failed to create vector " #N);                       \
    C_TEST_ASSERT(c_vector_equals##N(v1##N, v2##N), "Failed equals " #N);      \
                                                                               \
    c_vector_set##N(v1##N, n##N-1, p2##N);                                     \
    C_TEST_ASSERT_FALSE(c_vector_equals##N(v1##N, v2##N),                      \
                        "Failed different vectors should not equal " #N);      \
                                                                               \
    c_vector_free##N(& v1##N);                                                 \
    c_vector_free##N(& v2##N);                                                 \
    C_TEST_ASSERT_FALSE(v1##N, "Failed to free vector " #N);                   \
    C_TEST_ASSERT_FALSE(v2##N, "Failed to free vector " #N);                   \
    C_MEMORY_FREE(p1##N);                                                      \
    C_MEMORY_FREE(p2##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST_EQUALS_PTR)
    #undef TEST_EQUALS_PTR
}

#define TEST_C_VECTOR_EQUALS_UDF(N, T)                                         \
c_int32_t test_c_vector_equals_udf##N(void *v1, void *v2)                      \
{                                                                              \
    return *(T)v1 == *(T)v2 ? 0 : -1;                                          \
}
C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(TEST_C_VECTOR_EQUALS_UDF)
#undef TEST_C_VECTOR_EQUALS_UDF

/** Simple compare udf for full coverage. */
c_int32_t test_c_vector_equals_udf_void(void *v1, void *v2)
{
    return v1 == v2 ? 0 : -1;
}

void test_c_vector_equals_udf_ptr()
{
    #define TEST_EQUALS_UDF_PTR(N, T)                                          \
    c_uint64_t n##N = 4;                                                       \
    T p1##N;                                                                   \
    T p2##N;                                                                   \
    C_MEMORY_NEW(p1##N);                                                       \
    C_MEMORY_NEW(p2##N);                                                       \
    *p1##N = 0;                                                                \
    *p2##N = 1;                                                                \
    C_VECTOR_TYPE(N) v1##N = NULL;                                             \
    C_VECTOR_TYPE(N) v2##N = NULL;                                             \
    C_TEST_ASSERT(c_vector_equals_udf##N(v1##N, v2##N,                         \
                                            test_c_vector_equals_udf##N),      \
                  "Failed equals_udf nulls " #N);                              \
                                                                               \
    v1##N = c_vector_new_set##N(0, 0, 0);                                      \
    v2##N = c_vector_new_set##N(0, 0, 0);                                      \
    C_TEST_ASSERT(v1##N, "Failed to create vector " #N);                       \
    C_TEST_ASSERT(v2##N, "Failed to create vector " #N);                       \
    C_TEST_ASSERT(c_vector_empty##N(v1##N), "Failed to create empty vector " #N);\
    C_TEST_ASSERT(c_vector_empty##N(v2##N), "Failed to create empty vector " #N);\
                                                                               \
    C_TEST_ASSERT(c_vector_equals_udf##N(v1##N, v2##N,                         \
                                            test_c_vector_equals_udf##N),      \
                  "Failed equals_udf empty " #N);                              \
                                                                               \
    c_vector_free##N(& v1##N);                                                 \
    c_vector_free##N(& v2##N);                                                 \
                                                                               \
    v1##N = c_vector_new_set##N(n##N, n##N, p1##N);                            \
    v2##N = c_vector_new_set##N(n##N, n##N, p1##N);                            \
    C_TEST_ASSERT(v1##N, "Failed to create vector " #N);                       \
    C_TEST_ASSERT(v2##N, "Failed to create vector " #N);                       \
    C_TEST_ASSERT(c_vector_equals_udf##N(v1##N, v2##N,                         \
                                            test_c_vector_equals_udf##N),      \
                  "Failed equals_udf same pointers " #N);                      \
                                                                               \
    c_vector_set##N(v1##N, n##N-1, p2##N);                                     \
    C_TEST_ASSERT_FALSE(c_vector_equals_udf##N(v1##N, v2##N,                   \
                                            test_c_vector_equals_udf##N),      \
                        "Failed equals_udf different last pointers " #N);      \
                                                                               \
    c_vector_free##N(& v1##N);                                                 \
    c_vector_free##N(& v2##N);                                                 \
    C_TEST_ASSERT_FALSE(v1##N, "Failed to free vector " #N);                   \
    C_TEST_ASSERT_FALSE(v2##N, "Failed to free vector " #N);                   \
                                                                               \
    C_MEMORY_FREE(p1##N);                                                      \
    C_MEMORY_FREE(p2##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(TEST_EQUALS_UDF_PTR)
    #undef TEST_EQUALS_UDF_PTR
}

void test_c_vector_equals_udf_ptr_void()
{
    c_int32_t p1, p2;
    c_uint64_t n = 4;
    c_vector_t v1 = NULL, v2 = NULL;

    C_TEST_ASSERT(c_vector_equals_udf(v1, v2, test_c_vector_equals_udf_void),
                  "Failed equals_udf nulls");

    v1 = c_vector_new_set(0, 0, 0);
    v2 = c_vector_new_set(0, 0, 0);
    C_TEST_ASSERT(v1, "Failed to create vector 1");
    C_TEST_ASSERT(v2, "Failed to create vector 2");
    C_TEST_ASSERT(c_vector_empty(v1), "Failed to create empty vector 1");
    C_TEST_ASSERT(c_vector_empty(v2), "Failed to create empty vector 2");
    C_TEST_ASSERT(c_vector_equals_udf(v1, v2, test_c_vector_equals_udf_void),
                  "Failed equals_udf empty");
    c_vector_free(& v1);
    c_vector_free(& v2);

    v1 = c_vector_new_set(n, n, & p1);
    v2 = c_vector_new_set(n, n, & p1);
    C_TEST_ASSERT(v1, "Failed to create vector 1");
    C_TEST_ASSERT(v2, "Failed to create vector 2");
    C_TEST_ASSERT(c_vector_equals_udf(v1, v2, test_c_vector_equals_udf_void),
                  "Failed equals_udf same pointers");

    c_vector_set(v1, n-1, & p2);
    C_TEST_ASSERT_FALSE(c_vector_equals_udf(v1, v2, test_c_vector_equals_udf_void),
                        "Failed equals_udf different last pointers");

    c_vector_free(& v1);
    c_vector_free(& v2);
    C_TEST_ASSERT_FALSE(v1, "Failed to free vector 1");
    C_TEST_ASSERT_FALSE(v2, "Failed to free vector 2");
}

void test_c_vector_equals_value()
{
    #define TEST_EQUALS_VALUE(N, T)                                            \
    c_uint64_t n##N = 4;                                                       \
    T p##N;                                                                    \
                                                                               \
    C_VECTOR_TYPE(N) v1##N = NULL;                                             \
    C_VECTOR_TYPE(N) v2##N = NULL;                                             \
    C_TEST_ASSERT(c_vector_equals##N(v1##N, v2##N), "Failed equals nulls " #N);\
                                                                               \
    v1##N = c_vector_new_set##N(0, 0, 0);                                      \
    v2##N = c_vector_new_set##N(0, 0, 0);                                      \
    C_TEST_ASSERT(v1##N, "Failed to create vector " #N);                       \
    C_TEST_ASSERT(v2##N, "Failed to create vector " #N);                       \
    C_TEST_ASSERT(c_vector_empty##N(v1##N), "Failed to create empty vector " #N);\
    C_TEST_ASSERT(c_vector_empty##N(v2##N), "Failed to create empty vector " #N);\
                                                                               \
    C_TEST_ASSERT(c_vector_equals##N(v1##N, v2##N), "Failed equals empty " #N);\
                                                                               \
    c_vector_free##N(& v1##N);                                                 \
    c_vector_free##N(& v2##N);                                                 \
                                                                               \
    v1##N = c_vector_new_set##N(n##N, n##N, 1);                                \
    v2##N = c_vector_new_set##N(n##N, n##N, 1);                                \
    C_TEST_ASSERT(v1##N, "Failed to create vector " #N);                       \
    C_TEST_ASSERT(v2##N, "Failed to create vector " #N);                       \
    C_TEST_ASSERT(c_vector_equals##N(v1##N, v2##N), "Failed equals " #N);      \
                                                                               \
    c_vector_set##N(v1##N, n##N-1, 9);                                         \
    C_TEST_ASSERT_FALSE(c_vector_equals##N(v1##N, v2##N),                      \
                        "Failed equals different vectors " #N);                \
                                                                               \
    c_vector_free##N(& v1##N);                                                 \
    c_vector_free##N(& v2##N);                                                 \
    C_TEST_ASSERT_FALSE(v1##N, "Failed to free vector " #N);                   \
    C_TEST_ASSERT_FALSE(v2##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_TYPE_VALUE(TEST_EQUALS_VALUE)
    #undef TEST_EQUALS_VALUE
}

void test_c_vector_extend()
{
    #define TEST_EXTEND(N, T)                                                  \
    c_uint64_t n##N = 1;                                                       \
    T a##N;                                                                    \
    C_VECTOR_TYPE(N) v1##N = c_vector_new##N(n##N);                            \
    C_VECTOR_TYPE(N) v2##N = c_vector_new##N(n##N);                            \
    C_TEST_ASSERT(v1##N, "Failed to create v1 " #N);                           \
    C_TEST_ASSERT(v2##N, "Failed to create v2 " #N);                           \
    C_TEST_ASSERT(c_vector_empty##N(v1##N), "Failed empty " #N);               \
    C_TEST_ASSERT(c_vector_empty##N(v2##N), "Failed empty " #N);               \
                                                                               \
    c_vector_extend##N(v1##N, v2##N);                                          \
    C_TEST_ASSERT(c_vector_empty##N(v1##N), "Failed empty after extend" #N);   \
                                                                               \
    c_vector_append##N(v2##N, a##N);                                           \
    C_TEST_ASSERT_EQ(c_vector_size##N(v2##N), 1, "Failed append " #N);         \
    c_vector_extend##N(v1##N, v2##N);                                          \
    C_TEST_ASSERT_EQ(c_vector_size##N(v1##N), 1, "Failed extend 1 " #N);       \
    c_vector_extend##N(v1##N, v2##N);                                          \
    C_TEST_ASSERT_EQ(c_vector_size##N(v1##N), 2, "Failed extend 1 more " #N);  \
                                                                               \
    c_vector_extend##N(v1##N, v2##N);                                          \
    C_TEST_ASSERT_EQ(c_vector_size##N(v1##N), 3, "Failed extend 1 more again " #N);\
    C_TEST_ASSERT_EQ(c_vector_capacity##N(v1##N), 3, "Failed append " #N);     \
                                                                               \
    c_vector_free##N(& v1##N);                                                 \
    c_vector_free##N(& v2##N);                                                 \
    C_TEST_ASSERT_FALSE(v1##N, "Failed to free vector " #N);                   \
    C_TEST_ASSERT_FALSE(v2##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_TYPE(TEST_EXTEND)
    #undef TEST_EXTEND
}

void test_c_vector_first_value()
{
    #define TEST_FIRST_VALUE(N)                                                \
    c_uint64_t n##N = 2;                                                       \
    C_VECTOR_TYPE(N) v##N = c_vector_new_set##N(n##N, n##N, 2);                \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    c_vector_set##N(v##N, 1, 1);                                               \
    C_TEST_ASSERT_EQ(c_vector_get##N(v##N, 0), 2, "Expected value " #N);       \
    C_TEST_ASSERT_EQ(c_vector_get##N(v##N, 1), 1, "Expected value " #N);       \
    C_TEST_ASSERT_EQ(c_vector_first##N(v##N), 2, "Expected first " #N);        \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_VALUE(TEST_FIRST_VALUE)
    #undef TEST_FIRST_VALUE
}

void test_c_vector_first_ptr()
{
    #define TEST_FIRST_PTR(N, T)                                               \
    c_uint64_t n##N = 2;                                                       \
    T p##N = NULL;                                                             \
    C_MEMORY_NEW(p##N);                                                        \
    C_VECTOR_TYPE(N) v##N = c_vector_new_set##N(n##N, n##N, p##N);             \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT_EQ_PTR(c_vector_first##N(v##N), p##N, "Expected first " #N); \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);                    \
    C_MEMORY_FREE(p##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST_FIRST_PTR)
    #undef TEST_FIRST_PTR
}

void test_c_vector_free_deep()
{
    #define TEST_FREE_DEEP(N, T)                                               \
    c_uint64_t n##N = 2;                                                       \
    T a##N = NULL;                                                             \
    T b##N = NULL;                                                             \
    C_MEMORY_NEW(a##N);                                                        \
    C_MEMORY_NEW(b##N);                                                        \
    C_TEST_ASSERT(a##N, "Failed to create a" #N);                              \
    C_TEST_ASSERT(b##N, "Failed to create b" #N);                              \
    C_VECTOR_TYPE(N) v##N = c_vector_new_set##N(n##N, n##N, 0);                \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    c_vector_set##N(v##N, 0, a##N);                                            \
    c_vector_set##N(v##N, 1, b##N);                                            \
    c_vector_free_deep##N(& v##N);                                             \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST_FREE_DEEP)
    #undef TEST_FREE_DEEP
}

void test_c_vector_free_deep_udf_impl(void *item)
{
    C_MEMORY_FREE(item);
}

void test_c_vector_free_deep_udf()
{
    #define TEST_FREE_DEEP_UDF(N, T)                                           \
    c_uint64_t n##N = 2;                                                       \
    T a##N = NULL;                                                             \
    T b##N = NULL;                                                             \
    C_MEMORY_NEW(a##N);                                                        \
    C_MEMORY_NEW(b##N);                                                        \
    C_TEST_ASSERT(a##N, "Failed to create a" #N);                              \
    C_TEST_ASSERT(b##N, "Failed to create b" #N);                              \
    C_VECTOR_TYPE(N) v##N = c_vector_new_set##N(n##N, n##N, 0);                \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    c_vector_set##N(v##N, 0, a##N);                                            \
    c_vector_set##N(v##N, 1, b##N);                                            \
    c_vector_free_deep_udf##N(& v##N, test_c_vector_free_deep_udf_impl);       \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST_FREE_DEEP_UDF)
    #undef TEST_FREE_DEEP_UDF
}

void test_c_vector_get_value()
{
    #define TEST_GET_VALUE(N, T)                                               \
    const c_uint64_t n##N = 2;                                                 \
    T items##N[n##N];                                                          \
    items##N[0] = 1;                                                           \
    items##N[1] = 2;                                                           \
    C_VECTOR_TYPE(N) v##N = c_vector_new_copy##N(n##N, n##N, items##N);        \
                                                                               \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT_EQ(c_vector_get##N(v##N, 0), items##N[0], "Failed to get " #N);\
    C_TEST_ASSERT_EQ(c_vector_get##N(v##N, 1), items##N[1], "Failed to get " #N);\
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_TYPE_VALUE(TEST_GET_VALUE)
    #undef TEST_GET_VALUE
}

void test_c_vector_get_ptr()
{
    #define TEST_GET_PTR(N, T)                                                 \
    const c_uint64_t n##N = 1;                                                 \
    T items##N[n##N];                                                          \
    C_MEMORY_NEW(items##N[0]);                                                 \
    C_VECTOR_TYPE(N) v##N = c_vector_new_copy##N(n##N, n##N, items##N);        \
                                                                               \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT_EQ_PTR(c_vector_get##N(v##N, 0), items##N[0], "Failed to get " #N);\
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);                    \
    C_MEMORY_FREE(items##N[0]);                                                \
    C_TEST_ASSERT_FALSE(items##N[0], "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST_GET_PTR)
    #undef TEST_GET_PTR
}

void test_c_vector_get_safe_value()
{
    char expect = 9;

    #define TEST_GET_SAFE_VALUE(N)                                             \
    c_uint64_t n##N = 1;                                                       \
    C_VECTOR_TYPE(N) v##N = c_vector_new_set##N(n##N, n##N, expect);           \
                                                                               \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT_EQ(c_vector_get_safe##N(v##N, 0), expect,                    \
                                             "Failed to get_safe " #N);        \
    C_TEST_ASSERT_EQ(c_vector_get_safe##N(v##N, 1), 0,                         \
                                             "Failed to get_safe " #N);        \
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_VALUE(TEST_GET_SAFE_VALUE)
    #undef TEST_GET_SAFE_VALUE
}

void test_c_vector_get_safe_ptr()
{
    #define TEST_GET_SAFE_PTR(N, T)                                            \
    c_uint64_t n##N = 1;                                                       \
    T expect##N = 0;                                                           \
    C_VECTOR_TYPE(N) v##N = c_vector_new_set##N(n##N, n##N,                    \
                                                         expect##N);           \
                                                                               \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT_EQ_PTR(c_vector_get_safe##N(v##N, 0), expect##N,             \
                         "Failed to get_safe " #N);                            \
    C_TEST_ASSERT_EQ_PTR(c_vector_get_safe##N(v##N, 1), 0,                     \
                         "Failed to get_safe " #N);                            \
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST_GET_SAFE_PTR)
    #undef TEST_GET_SAFE_PTR
}

void test_c_vector_insert_value()
{
    #define TEST(N, T)                                                         \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
                                                                               \
    c_vector_insert##N(NULL, 0, 0);                                            \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    C_TEST_ASSERT(c_vector_empty##N(v##N), "Failed empty " #N);                \
    c_vector_insert##N(v##N, 0, 0);                                            \
    C_TEST_ASSERT_EQ(1, c_vector_size##N(v##N), "Failed insert into empty " #N);\
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    C_TEST_ASSERT(c_vector_empty##N(v##N), "Failed empty " #N);                \
    c_vector_insert##N(v##N, 4, 0);                                            \
    C_TEST_ASSERT_EQ(5, c_vector_size##N(v##N), "Failed insert into index 4 of empty " #N);\
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new_set##N(2, 2, 0);                                       \
    C_TEST_ASSERT_EQ(2, c_vector_size##N(v##N), "Failed create " #N);          \
    c_vector_insert##N(v##N, 0, 1);                                            \
    C_TEST_ASSERT_EQ(3, c_vector_size##N(v##N), "Failed insert into non empty " #N);\
    C_TEST_ASSERT_EQ(1, c_vector_get##N(v##N, 0), "Failed insert into non empty " #N);\
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new_set##N(2, 2, 0);                                       \
    C_TEST_ASSERT_EQ(2, c_vector_size##N(v##N), "Failed create " #N);          \
    c_vector_insert##N(v##N, 2, 1);                                            \
    C_TEST_ASSERT_EQ(3, c_vector_size##N(v##N),                                \
                        "Failed insert at end into non empty " #N);            \
    C_TEST_ASSERT_EQ(1, c_vector_get##N(v##N, 2),                              \
                        "Failed insert into non empty " #N);                   \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new_set##N(2, 2, 0);                                       \
    C_TEST_ASSERT_EQ(2, c_vector_size##N(v##N), "Failed create " #N);          \
    c_vector_insert##N(v##N, 5, 1);                                            \
    C_TEST_ASSERT_EQ(6, c_vector_size##N(v##N),                                \
                        "Failed insert/resize index 5 into non empty " #N);    \
    C_TEST_ASSERT_EQ(1, c_vector_get##N(v##N, 5),                              \
                        "Failed insert 1 into non empty " #N);                 \
    c_vector_insert##N(v##N, 0, 2);                                            \
    C_TEST_ASSERT_EQ(7, c_vector_size##N(v##N),                                \
                        "Failed insert/resize index 0 into non empty " #N);    \
    C_TEST_ASSERT_EQ(2, c_vector_get##N(v##N, 0),                              \
                        "Failed get 0 " #N);                                   \
    C_TEST_ASSERT_EQ(0, c_vector_get##N(v##N, 1),                              \
                        "Failed get 1 " #N);                                   \
    C_TEST_ASSERT_EQ(0, c_vector_get##N(v##N, 5),                              \
                        "Failed get 5 " #N);                                   \
    C_TEST_ASSERT_EQ(1, c_vector_get##N(v##N, 6),                              \
                        "Failed get 6 " #N);                                   \
    c_vector_free##N(& v##N);

    C_TYPE_TEMPLATE_NAME_TYPE_VALUE(TEST)
    #undef TEST
}

void test_c_vector_insert_bisect_ptr()
{
    #define TEST(N, T)                                                         \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
    T p##N = NULL;                                                             \
    C_MEMORY_NEW(p##N);                                                        \
    c_vector_insert_bisect##N(NULL, 0);                                        \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    C_TEST_ASSERT(c_vector_empty##N(v##N), "Failed empty " #N);                \
    c_vector_insert_bisect##N(v##N, 0);                                        \
    C_TEST_ASSERT_EQ(1, c_vector_size##N(v##N), "Failed insert into empty " #N);\
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new_set##N(2, 2, 0);                                       \
    C_TEST_ASSERT_EQ(2, c_vector_size##N(v##N), "Failed create " #N);          \
                                                                               \
    c_vector_insert_bisect##N(v##N, 0);                                        \
    c_vector_insert_bisect##N(v##N, p##N);                                     \
    C_TEST_ASSERT_EQ(4, c_vector_size##N(v##N), "Failed insert bisect " #N);   \
    C_TEST_ASSERT_EQ_PTR(0, c_vector_get##N(v##N, 0), "Failed get 0 c " #N);   \
    C_TEST_ASSERT_EQ_PTR(p##N, c_vector_get##N(v##N, 3), "Failed get 0 c " #N);\
    c_vector_free##N(& v##N);                                                  \
    C_MEMORY_FREE(p##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST)
    #undef TEST
}

void test_c_vector_insert_bisect_udf()
{
    #define TEST(N, T)                                                         \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
                                                                               \
    c_vector_insert_bisect_udf##N(NULL, 0, test_c_vector_equals_udf_void);     \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    C_TEST_ASSERT(c_vector_empty##N(v##N), "Failed empty " #N);                \
    c_vector_insert_bisect_udf##N(v##N, 0, test_c_vector_equals_udf_void);     \
    C_TEST_ASSERT_EQ(1, c_vector_size##N(v##N), "Failed insert into empty " #N);\
    c_vector_free##N(& v##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST)
    #undef TEST
}

void test_c_vector_insert_bisect_value()
{
    #define TEST(N, T)                                                         \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
                                                                               \
    c_vector_insert_bisect##N(NULL, 0);                                        \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    C_TEST_ASSERT(c_vector_empty##N(v##N), "Failed empty " #N);                \
    c_vector_insert_bisect##N(v##N, 0);                                        \
    C_TEST_ASSERT_EQ(1, c_vector_size##N(v##N), "Failed insert into empty " #N);\
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new_set##N(2, 2, 0);                                       \
    C_TEST_ASSERT_EQ(2, c_vector_size##N(v##N), "Failed create " #N);          \
    c_vector_set##N(v##N, 0, 10);                                              \
    c_vector_set##N(v##N, 1, 20);                                              \
    C_TEST_ASSERT_EQ(10, c_vector_get##N(v##N, 0), "Failed get 0 b " #N);      \
    C_TEST_ASSERT_EQ(20, c_vector_get##N(v##N, 1), "Failed get 0 c " #N);      \
                                                                               \
    c_vector_insert_bisect##N(v##N, 0); /* [ (0) 10 20 ] */                    \
    C_TEST_ASSERT_EQ(3, c_vector_size##N(v##N), "Failed insert into non empty " #N);\
    C_TEST_ASSERT_EQ(0,  c_vector_get##N(v##N, 0), "Failed get 0 a " #N);      \
                                                                               \
    c_vector_insert_bisect##N(v##N, 1); /* [ 0 (1) 10 20 ] */                  \
    C_TEST_ASSERT_EQ(4, c_vector_size##N(v##N), "Failed insert into non empty " #N);\
    C_TEST_ASSERT_EQ(0,  c_vector_get##N(v##N, 0), "Failed get 1 a " #N);      \
    C_TEST_ASSERT_EQ(1,  c_vector_get##N(v##N, 1), "Failed get 1 b " #N);      \
    C_TEST_ASSERT_EQ(10, c_vector_get##N(v##N, 2), "Failed get 1 c " #N);      \
    C_TEST_ASSERT_EQ(20, c_vector_get##N(v##N, 3), "Failed get 1 d " #N);      \
                                                                               \
    c_vector_insert_bisect##N(v##N, 15); /* [ 0 1 10 (15) 20 ] */              \
    C_TEST_ASSERT_EQ(5, c_vector_size##N(v##N), "Failed insert into non empty " #N);\
    C_TEST_ASSERT_EQ(0,  c_vector_get##N(v##N, 0), "Failed get 2 a " #N);      \
    C_TEST_ASSERT_EQ(1,  c_vector_get##N(v##N, 1), "Failed get 2 b " #N);      \
    C_TEST_ASSERT_EQ(10, c_vector_get##N(v##N, 2), "Failed get 2 c " #N);      \
    C_TEST_ASSERT_EQ(15, c_vector_get##N(v##N, 3), "Failed get 2 d " #N);      \
    C_TEST_ASSERT_EQ(20, c_vector_get##N(v##N, 4), "Failed get 2 e " #N);      \
                                                                               \
    c_vector_insert_bisect##N(v##N, 50); /* [ 0 1 10 15 20 (50) ] */           \
    C_TEST_ASSERT_EQ(6, c_vector_size##N(v##N), "Failed insert into non empty " #N);\
    C_TEST_ASSERT_EQ(50, c_vector_get##N(v##N, 5), "Failed get 5 " #N);        \
                                                                               \
    c_vector_insert_bisect##N(v##N, 45); /* [ 0 1 10 15 20 (45) 50 ] */        \
    C_TEST_ASSERT_EQ(7, c_vector_size##N(v##N), "Failed insert into non empty " #N);\
    C_TEST_ASSERT_EQ(45, c_vector_get##N(v##N, 5), "Failed get 5 again " #N);  \
    c_vector_free##N(& v##N);

    C_TYPE_TEMPLATE_NAME_TYPE_VALUE(TEST)
    #undef TEST
}

void test_c_vector_items()
{
    #define TEST_ITEMS(N, T)                                                   \
    const c_uint64_t n##N = 1;                                                 \
    C_VECTOR_TYPE(N) v##N = c_vector_new##N(n##N);                             \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT(c_vector_items##N(v##N), "Failed to get items " #N);         \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_TYPE(TEST_ITEMS)
    #undef TEST_ITEMS
}

void test_c_vector_last_value()
{
    #define TEST_LAST_VALUE(N)                                                 \
    c_uint64_t n##N = 2;                                                       \
    C_VECTOR_TYPE(N) v##N = c_vector_new_set##N(n##N, n##N, 2);                \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    c_vector_set##N(v##N, 0, 1);                                               \
    C_TEST_ASSERT_EQ(c_vector_get##N(v##N, 0), 1, "Expected value " #N);       \
    C_TEST_ASSERT_EQ(c_vector_get##N(v##N, 1), 2, "Expected value " #N);       \
    C_TEST_ASSERT_EQ(c_vector_last##N(v##N), 2, "Expected last " #N);          \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_VALUE(TEST_LAST_VALUE)
    #undef TEST_LAST_VALUE
}

void test_c_vector_last_ptr()
{
    #define TEST_LAST_PTR(N, T)                                                \
    c_uint64_t n##N = 2;                                                       \
    T p##N = NULL;                                                             \
    C_MEMORY_NEW(p##N);                                                        \
    C_VECTOR_TYPE(N) v##N = c_vector_new_set##N(n##N, n##N, p##N);             \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT_EQ_PTR(c_vector_last##N(v##N), p##N, "Expected last " #N);   \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);                    \
    C_MEMORY_FREE(p##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST_LAST_PTR)
    #undef TEST_LAST_PTR
}

#define VECTOR_MAP(N, T)                                                       \
void test_c_vector_map_udf##N(T item, void *closure)                           \
{                                                                              \
}
C_TYPE_TEMPLATE_NAME_TYPE(VECTOR_MAP)
#undef VECTOR_MAP

void test_c_vector_map()
{
    #define TEST(N, T)                                                         \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
                                                                               \
    c_vector_map##N(v##N, test_c_vector_map_udf##N, NULL);                     \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    c_vector_map##N(v##N, test_c_vector_map_udf##N, NULL);                     \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new_set##N(2, 2, 0);                                       \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    c_vector_map##N(v##N, test_c_vector_map_udf##N, NULL);                     \
    c_vector_free##N(& v##N);

    C_TYPE_TEMPLATE_NAME_TYPE(TEST)
    #undef TEST
}

void test_c_vector_new()
{
    #define TEST_NEW(N)                                                        \
    c_uint64_t n##N = 3;                                                       \
    C_VECTOR_TYPE(N) v##N = c_vector_new##N(n##N);                             \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME(TEST_NEW)
    #undef TEST_NEW
}

void test_c_vector_new_copy_value()
{
    #define TEST_NEW_COPY_VALUE(N, T)                                          \
    const c_uint64_t n##N = 2;                                                 \
    T items##N[n##N];                                                          \
    items##N[0] = 1;                                                           \
    items##N[1] = 2;                                                           \
    C_VECTOR_TYPE(N) v##N = c_vector_new_copy##N(n##N, n##N, items##N);        \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_TYPE_VALUE(TEST_NEW_COPY_VALUE)
    #undef TEST_NEW_COPY_VALUE
}

void test_c_vector_new_copy_ptr()
{
    #define TEST_NEW_COPY_PTR(N, T)                                            \
    const c_uint64_t n##N = 2;                                                 \
    T items##N[n##N];                                                          \
    items##N[0] = NULL;                                                        \
    items##N[1] = NULL;                                                        \
    C_VECTOR_TYPE(N) v##N = c_vector_new_copy##N(n##N, n##N, items##N);        \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST_NEW_COPY_PTR)
    #undef TEST_NEW_COPY_PTR
}

void test_c_vector_new_copy_vector_ptr()
{
    #define TEST(N, T)                                                         \
    C_VECTOR_TYPE(N) v1##N = 0;                                                \
    C_VECTOR_TYPE(N) v2##N = 0;                                                \
    T p1##N = 0;                                                               \
    T p2##N = 0;                                                               \
                                                                               \
    v1##N = c_vector_new##N(0);                                                \
    v2##N = c_vector_new_copy_vector##N(v1##N);                                \
    C_TEST_ASSERT(v2##N, "Expected copy " #N);                                 \
    C_TEST_ASSERT(c_vector_empty##N(v2##N), "Expected copy " #N);              \
    c_vector_free##N(& v2##N);                                                 \
                                                                               \
    c_vector_append##N(v1##N, p1##N);                                          \
    c_vector_append##N(v1##N, p2##N);                                          \
    v2##N = c_vector_new_copy_vector##N(v1##N);                                \
    C_TEST_ASSERT(v2##N, "Expected copy " #N);                                 \
    C_TEST_ASSERT_EQ(c_vector_size##N(v2##N), 2, "Expected size " #N);         \
    C_TEST_ASSERT_EQ_PTR(c_vector_get##N(v2##N, 0), p1##N, "Expected value " #N);\
    C_TEST_ASSERT_EQ_PTR(c_vector_get##N(v2##N, 1), p2##N, "Expected value " #N);\
                                                                               \
    c_vector_free##N(& v1##N);                                                 \
    c_vector_free##N(& v2##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST)
    #undef TEST
}

void test_c_vector_new_copy_vector_value()
{
    #define TEST(N, T)                                                         \
    C_VECTOR_TYPE(N) v1##N = 0;                                                \
    C_VECTOR_TYPE(N) v2##N = 0;                                                \
                                                                               \
    v1##N = c_vector_new##N(0);                                                \
    v2##N = c_vector_new_copy_vector##N(v1##N);                                \
    C_TEST_ASSERT(v2##N, "Expected copy " #N);                                 \
    C_TEST_ASSERT(c_vector_empty##N(v2##N), "Expected copy " #N);              \
    c_vector_free##N(& v2##N);                                                 \
                                                                               \
    c_vector_append##N(v1##N, 1);                                              \
    c_vector_append##N(v1##N, 2);                                              \
    v2##N = c_vector_new_copy_vector##N(v1##N);                                \
    C_TEST_ASSERT(v2##N, "Expected copy " #N);                                 \
    C_TEST_ASSERT_EQ(c_vector_size##N(v2##N), 2, "Expected size " #N);         \
    C_TEST_ASSERT_EQ(c_vector_get##N(v2##N, 0), 1, "Expected value " #N);      \
    C_TEST_ASSERT_EQ(c_vector_get##N(v2##N, 1), 2, "Expected value " #N);      \
                                                                               \
    c_vector_free##N(& v1##N);                                                 \
    c_vector_free##N(& v2##N);

    C_TYPE_TEMPLATE_NAME_TYPE_VALUE(TEST)
    #undef TEST
}

void test_c_vector_new_set_value()
{
    char expect = 9;

    #define TEST_NEW_SET_VALUE(N)                                              \
    c_uint64_t n##N = 2;                                                       \
    C_VECTOR_TYPE(N) v##N = c_vector_new_set##N(n##N, n##N, expect);           \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_VALUE(TEST_NEW_SET_VALUE)
    #undef TEST_NEW_SET_VALUE
}

void test_c_vector_new_set_ptr()
{
    #define TEST_NEW_SET_PTR(N, T)                                             \
    c_uint64_t n##N = 2;                                                       \
    T expect##N = 0;                                                           \
    C_VECTOR_TYPE(N) v##N = c_vector_new_set##N(n##N, n##N,                    \
                                                         expect##N);           \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST_NEW_SET_PTR)
    #undef TEST_NEW_SET_PTR
}

void test_c_vector_print()
{
    #define TEST(N, T)                                                         \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
                                                                               \
    c_vector_print##N(v##N);                                                   \
                                                                               \
    v##N = c_vector_new_set##N(2, 2, 1);                                       \
    c_vector_print##N(v##N);                                                   \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_TYPE_VALUE(TEST)
    #undef TEST
}

void test_c_vector_remove_first_ptr()
{
    #define TEST_REMOVE_FIRST_PTR(N, T)                                        \
    c_uint64_t n##N = 4;                                                       \
    T p##N = NULL;                                                             \
    T r##N = NULL;                                                             \
    C_MEMORY_NEW(p##N);                                                        \
                                                                               \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
    v##N = c_vector_new_set##N(0, 0, 0);                                       \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT(c_vector_empty##N(v##N), "Failed to create empty vector " #N);\
                                                                               \
    r##N = c_vector_remove_first##N(NULL);                                     \
    C_TEST_ASSERT_EQ_PTR(0, r##N, "Failed invalid remove " #N);                \
                                                                               \
    r##N = c_vector_remove_first##N(v##N);                                     \
    C_TEST_ASSERT_EQ_PTR(0, r##N, "Failed invalid remove from empty " #N);     \
                                                                               \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new_set##N(n##N, n##N, p##N);                              \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT_EQ(n##N, c_vector_size##N(v##N), "Failed size " #N);         \
    C_TEST_ASSERT_EQ(n##N, c_vector_capacity##N(v##N), "Failed capacity " #N); \
    C_TEST_ASSERT_EQ_PTR(p##N, c_vector_get##N(v##N, 0), "Failed create " #N); \
    C_TEST_ASSERT_EQ_PTR(p##N, c_vector_get##N(v##N, 1), "Failed create " #N); \
                                                                               \
    r##N = c_vector_remove_first##N(v##N);                                     \
    C_TEST_ASSERT_EQ_PTR(p##N, r##N, "Failed remove_first " #N);               \
    C_TEST_ASSERT_EQ(n##N-1, c_vector_size##N(v##N), "Failed size after invalid remove " #N);\
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);                    \
    C_MEMORY_FREE(p##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST_REMOVE_FIRST_PTR)
    #undef TEST_REMOVE_FIRST_PTR
}

void test_c_vector_remove_first_value()
{
    #define TEST_REMOVE_FIRST_VALUE(N, T)                                      \
    c_uint64_t n##N = 4;                                                       \
    T p##N;                                                                    \
                                                                               \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
    v##N = c_vector_new_set##N(0, 0, 0);                                       \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT(c_vector_empty##N(v##N), "Failed to create empty vector " #N);\
                                                                               \
    p##N = c_vector_remove_first##N(NULL);                                     \
    C_TEST_ASSERT_EQ(0, p##N, "Failed invalid remove " #N);                    \
                                                                               \
    p##N = c_vector_remove_first##N(v##N);                                     \
    C_TEST_ASSERT_EQ(0, p##N, "Failed invalid remove on empty vector " #N);    \
                                                                               \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new_set##N(n##N, n##N, 1);                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT_EQ(n##N, c_vector_size##N(v##N), "Failed size " #N);         \
    C_TEST_ASSERT_EQ(n##N, c_vector_capacity##N(v##N), "Failed capacity " #N); \
    C_TEST_ASSERT_EQ(1, c_vector_get##N(v##N, 0), "Failed create " #N);        \
    C_TEST_ASSERT_EQ(1, c_vector_get##N(v##N, 1), "Failed create " #N);        \
                                                                               \
    p##N = c_vector_remove_first##N(v##N);                                     \
    C_TEST_ASSERT_EQ(1, p##N, "Failed invalid remove_first " #N);              \
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_TYPE_VALUE(TEST_REMOVE_FIRST_VALUE)
    #undef TEST_REMOVE_FIRST_VALUE
}

void test_c_vector_remove_last_ptr()
{
    #define TEST_REMOVE_LAST_PTR(N, T)                                         \
    c_uint64_t n##N = 4;                                                       \
    T p##N = NULL;                                                             \
    T r##N = NULL;                                                             \
    T s##N = NULL;                                                             \
    C_MEMORY_NEW(p##N);                                                        \
    C_MEMORY_NEW(s##N);                                                        \
                                                                               \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
    v##N = c_vector_new_set##N(0, 0, 0);                                       \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT(c_vector_empty##N(v##N), "Failed to create empty vector " #N);\
                                                                               \
    r##N = c_vector_remove_last##N(NULL);                                      \
    C_TEST_ASSERT_EQ_PTR(0, r##N, "Failed invalid remove " #N);                \
                                                                               \
    r##N = c_vector_remove_last##N(v##N);                                      \
    C_TEST_ASSERT_EQ_PTR(0, r##N, "Failed invalid remove from empty " #N);     \
                                                                               \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new_set##N(n##N, n##N, p##N);                              \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT_EQ(n##N, c_vector_size##N(v##N), "Failed size " #N);         \
    C_TEST_ASSERT_EQ(n##N, c_vector_capacity##N(v##N), "Failed capacity " #N); \
    c_vector_set##N(v##N, n##N-1, s##N);                                       \
    C_TEST_ASSERT_EQ_PTR(p##N, c_vector_get##N(v##N, 0), "Failed create " #N); \
    C_TEST_ASSERT_EQ_PTR(s##N, c_vector_get##N(v##N, n##N-1), "Failed create " #N);\
    C_TEST_ASSERT_NEQ_PTR(s##N, p##N, "Pointers not different " #N);           \
                                                                               \
    r##N = c_vector_remove_last##N(v##N);                                      \
    C_TEST_ASSERT_EQ_PTR(s##N, r##N, "Failed remove_last " #N);                \
    C_TEST_ASSERT_EQ(n##N-1, c_vector_size##N(v##N), "Failed size after invalid remove " #N);\
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);                    \
    C_MEMORY_FREE(p##N);                                                       \
    C_MEMORY_FREE(s##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST_REMOVE_LAST_PTR)
    #undef TEST_REMOVE_LAST_PTR
}

void test_c_vector_remove_last_value()
{
    #define TEST_REMOVE_LAST_VALUE(N, T)                                       \
    c_uint64_t n##N = 4;                                                       \
    T p##N;                                                                    \
                                                                               \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
    v##N = c_vector_new_set##N(0, 0, 0);                                       \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT(c_vector_empty##N(v##N), "Failed to create empty vector " #N);\
                                                                               \
    p##N = c_vector_remove_last##N(NULL);                                      \
    C_TEST_ASSERT_EQ(0, p##N, "Failed invalid remove_last " #N);               \
                                                                               \
    p##N = c_vector_remove_last##N(v##N);                                      \
    C_TEST_ASSERT_EQ(0, p##N, "Failed invalid remove_last on empty vector " #N);\
                                                                               \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new_set##N(n##N, n##N, 1);                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT_EQ(n##N, c_vector_size##N(v##N), "Failed size " #N);         \
    C_TEST_ASSERT_EQ(n##N, c_vector_capacity##N(v##N), "Failed capacity " #N); \
    C_TEST_ASSERT_EQ(1, c_vector_get##N(v##N, 0), "Failed create " #N);        \
    C_TEST_ASSERT_EQ(1, c_vector_get##N(v##N, 1), "Failed create " #N);        \
                                                                               \
    p##N = c_vector_remove_last##N(v##N);                                      \
    C_TEST_ASSERT_EQ(1, p##N, "Failed invalid remove_last " #N);               \
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_TYPE_VALUE(TEST_REMOVE_LAST_VALUE)
    #undef TEST_REMOVE_LAST_VALUE
}

void test_c_vector_remove_ptr()
{
    #define TEST_REMOVE_PTR(N, T)                                              \
    c_uint64_t n##N = 4;                                                       \
    T p##N = NULL;                                                             \
    T r##N = NULL;                                                             \
    C_MEMORY_NEW(p##N);                                                        \
                                                                               \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
    v##N = c_vector_new_set##N(0, 0, 0);                                       \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT(c_vector_empty##N(v##N), "Failed to create empty vector " #N);\
                                                                               \
    r##N = c_vector_remove##N(NULL, 0);                                        \
    C_TEST_ASSERT_EQ_PTR(0, r##N, "Failed invalid remove " #N);                \
                                                                               \
    r##N = c_vector_remove##N(v##N, 0);                                        \
    C_TEST_ASSERT_EQ_PTR(0, r##N, "Failed invalid remove " #N);                \
                                                                               \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new_set##N(n##N, n##N, p##N);                              \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT_EQ(n##N, c_vector_size##N(v##N), "Failed size " #N);         \
    C_TEST_ASSERT_EQ(n##N, c_vector_capacity##N(v##N), "Failed capacity " #N); \
    C_TEST_ASSERT_EQ_PTR(p##N, c_vector_get##N(v##N, 0), "Failed create " #N); \
    C_TEST_ASSERT_EQ_PTR(p##N, c_vector_get##N(v##N, 1), "Failed create " #N); \
                                                                               \
    r##N = c_vector_remove##N(v##N, 10);                                       \
    C_TEST_ASSERT_EQ_PTR(NULL, r##N, "Failed invalid remove " #N);             \
    C_TEST_ASSERT_EQ(n##N, c_vector_size##N(v##N), "Failed size after invalid remove " #N);\
                                                                               \
    r##N = c_vector_remove##N(v##N, 1);                                        \
    C_TEST_ASSERT_EQ_PTR(p##N, r##N, "Failed to remove " #N);                  \
    C_TEST_ASSERT_EQ(n##N-1, c_vector_size##N(v##N), "Failed size after remove " #N);\
                                                                               \
    r##N = c_vector_remove##N(v##N, 2);                                        \
    C_TEST_ASSERT_EQ_PTR(p##N, r##N, "Failed to remove " #N);                  \
    C_TEST_ASSERT_EQ(n##N-2, c_vector_size##N(v##N), "Failed size after remove " #N);\
                                                                               \
    r##N = c_vector_remove##N(v##N, 0);                                        \
    C_TEST_ASSERT_EQ_PTR(p##N, r##N, "Failed to remove " #N);                  \
    C_TEST_ASSERT_EQ(n##N-3, c_vector_size##N(v##N), "Failed size after remove " #N);\
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);                    \
    C_MEMORY_FREE(p##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST_REMOVE_PTR)
    #undef TEST_REMOVE_PTR
}

void test_c_vector_remove_unordered_ptr()
{
    #define TEST(N, T)                                                         \
    c_uint64_t n##N = 4;                                                       \
    T p##N = NULL;                                                             \
    T r##N = NULL;                                                             \
    C_MEMORY_NEW(p##N);                                                        \
                                                                               \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
    v##N = c_vector_new_set##N(0, 0, 0);                                       \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT(c_vector_empty##N(v##N), "Failed to create empty vector " #N);\
                                                                               \
    r##N = c_vector_remove_unordered##N(NULL, 0);                              \
    C_TEST_ASSERT_EQ_PTR(0, r##N, "Failed invalid remove " #N);                \
                                                                               \
    r##N = c_vector_remove_unordered##N(v##N, 0);                              \
    C_TEST_ASSERT_EQ_PTR(0, r##N, "Failed invalid remove " #N);                \
                                                                               \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new_set##N(n##N, n##N, p##N);                              \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT_EQ(n##N, c_vector_size##N(v##N), "Failed size " #N);         \
    C_TEST_ASSERT_EQ(n##N, c_vector_capacity##N(v##N), "Failed capacity " #N); \
    C_TEST_ASSERT_EQ_PTR(p##N, c_vector_get##N(v##N, 0), "Failed create " #N); \
    C_TEST_ASSERT_EQ_PTR(p##N, c_vector_get##N(v##N, 1), "Failed create " #N); \
                                                                               \
    r##N = c_vector_remove_unordered##N(v##N, 10);                             \
    C_TEST_ASSERT_EQ_PTR(NULL, r##N, "Failed invalid remove " #N);             \
    C_TEST_ASSERT_EQ(n##N, c_vector_size##N(v##N), "Failed size after invalid remove " #N);\
                                                                               \
    r##N = c_vector_remove_unordered##N(v##N, 1);                              \
    C_TEST_ASSERT_EQ_PTR(p##N, r##N, "Failed to remove " #N);                  \
    C_TEST_ASSERT_EQ(n##N-1, c_vector_size##N(v##N), "Failed size after remove " #N);\
                                                                               \
    r##N = c_vector_remove_unordered##N(v##N, 2);                              \
    C_TEST_ASSERT_EQ_PTR(p##N, r##N, "Failed to remove " #N);                  \
    C_TEST_ASSERT_EQ(n##N-2, c_vector_size##N(v##N), "Failed size after remove " #N);\
                                                                               \
    r##N = c_vector_remove_unordered##N(v##N, 0);                              \
    C_TEST_ASSERT_EQ_PTR(p##N, r##N, "Failed to remove " #N);                  \
    C_TEST_ASSERT_EQ(n##N-3, c_vector_size##N(v##N), "Failed size after remove " #N);\
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);                    \
    C_MEMORY_FREE(p##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST)
    #undef TEST
}

void test_c_vector_remove_unordered_value()
{
    #define TEST(N, T)                                                         \
    c_uint64_t n##N = 4;                                                       \
    T p##N;                                                                    \
                                                                               \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
    v##N = c_vector_new_set##N(0, 0, 0);                                       \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT(c_vector_empty##N(v##N), "Failed to create empty vector " #N);\
                                                                               \
    p##N = c_vector_remove_unordered##N(NULL, 0);                              \
    C_TEST_ASSERT_EQ(0, p##N, "Failed invalid remove " #N);                    \
                                                                               \
    p##N = c_vector_remove_unordered##N(v##N, 0);                              \
    C_TEST_ASSERT_EQ(0, p##N, "Failed invalid remove " #N);                    \
                                                                               \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new_set##N(n##N, n##N, 1);                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT_EQ(n##N, c_vector_size##N(v##N), "Failed size " #N);         \
    C_TEST_ASSERT_EQ(n##N, c_vector_capacity##N(v##N), "Failed capacity " #N); \
    C_TEST_ASSERT_EQ(1, c_vector_get##N(v##N, 0), "Failed create " #N);        \
    C_TEST_ASSERT_EQ(1, c_vector_get##N(v##N, 1), "Failed create " #N);        \
                                                                               \
    p##N = c_vector_remove_unordered##N(v##N, 10);                             \
    C_TEST_ASSERT_EQ(0, p##N, "Failed invalid remove " #N);                    \
    C_TEST_ASSERT_EQ(n##N, c_vector_size##N(v##N), "Failed size after invalid remove " #N);\
                                                                               \
    p##N = c_vector_remove_unordered##N(v##N, 1);                              \
    C_TEST_ASSERT_EQ(1, p##N, "Failed to remove " #N);                         \
    C_TEST_ASSERT_EQ(n##N-1, c_vector_size##N(v##N), "Failed size after remove " #N);\
                                                                               \
    p##N = c_vector_remove_unordered##N(v##N, 2);                              \
    C_TEST_ASSERT_EQ(1, p##N, "Failed to remove " #N);                         \
    C_TEST_ASSERT_EQ(n##N-2, c_vector_size##N(v##N), "Failed size after remove " #N);\
                                                                               \
    p##N = c_vector_remove_unordered##N(v##N, 0);                              \
    C_TEST_ASSERT_EQ(1, p##N, "Failed to remove " #N);                         \
    C_TEST_ASSERT_EQ(n##N-3, c_vector_size##N(v##N), "Failed size after remove " #N);\
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_TYPE_VALUE(TEST)
    #undef TEST
}

void test_c_vector_remove_value()
{
    #define TEST_REMOVE_VALUE(N, T)                                            \
    c_uint64_t n##N = 4;                                                       \
    T p##N;                                                                    \
                                                                               \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
    v##N = c_vector_new_set##N(0, 0, 0);                                       \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT(c_vector_empty##N(v##N), "Failed to create empty vector " #N);\
                                                                               \
    p##N = c_vector_remove##N(NULL, 0);                                        \
    C_TEST_ASSERT_EQ(0, p##N, "Failed invalid remove " #N);                    \
                                                                               \
    p##N = c_vector_remove##N(v##N, 0);                                        \
    C_TEST_ASSERT_EQ(0, p##N, "Failed invalid remove " #N);                    \
                                                                               \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new_set##N(n##N, n##N, 1);                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT_EQ(n##N, c_vector_size##N(v##N), "Failed size " #N);         \
    C_TEST_ASSERT_EQ(n##N, c_vector_capacity##N(v##N), "Failed capacity " #N); \
    C_TEST_ASSERT_EQ(1, c_vector_get##N(v##N, 0), "Failed create " #N);        \
    C_TEST_ASSERT_EQ(1, c_vector_get##N(v##N, 1), "Failed create " #N);        \
                                                                               \
    p##N = c_vector_remove##N(v##N, 10);                                       \
    C_TEST_ASSERT_EQ(0, p##N, "Failed invalid remove " #N);                    \
    C_TEST_ASSERT_EQ(n##N, c_vector_size##N(v##N), "Failed size after invalid remove " #N);\
                                                                               \
    p##N = c_vector_remove##N(v##N, 1);                                        \
    C_TEST_ASSERT_EQ(1, p##N, "Failed to remove " #N);                         \
    C_TEST_ASSERT_EQ(n##N-1, c_vector_size##N(v##N), "Failed size after remove " #N);\
                                                                               \
    p##N = c_vector_remove##N(v##N, 2);                                        \
    C_TEST_ASSERT_EQ(1, p##N, "Failed to remove " #N);                         \
    C_TEST_ASSERT_EQ(n##N-2, c_vector_size##N(v##N), "Failed size after remove " #N);\
                                                                               \
    p##N = c_vector_remove##N(v##N, 0);                                        \
    C_TEST_ASSERT_EQ(1, p##N, "Failed to remove " #N);                         \
    C_TEST_ASSERT_EQ(n##N-3, c_vector_size##N(v##N), "Failed size after remove " #N);\
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_TYPE_VALUE(TEST_REMOVE_VALUE)
    #undef TEST_REMOVE_VALUE
}

void test_c_vector_reserve()
{
    #define TEST_RESERVE(N, T)                                                 \
    c_uint64_t n##N = 2;                                                       \
    c_uint64_t m##N = 4;                                                       \
    C_VECTOR_TYPE(N) v##N = c_vector_new##N(n##N);                             \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT_EQ(c_vector_capacity##N(v##N), n##N, "Failed capacity " #N); \
    c_vector_reserve##N(v##N, m##N);                                           \
    C_TEST_ASSERT_EQ(c_vector_capacity##N(v##N), m##N, "Failed reserve " #N);  \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_TYPE(TEST_RESERVE)
    #undef TEST_RESERVE
}

void test_c_vector_resize()
{
    #define TEST_RESIZE(N, T)                                                  \
    c_uint64_t n0##N = 0;                                                      \
    C_VECTOR_TYPE(N) v0##N = c_vector_new_set##N(n0##N, n0##N, 0);             \
    C_TEST_ASSERT(c_vector_empty##N(v0##N), "Failed to create vector " #N);    \
    c_vector_resize##N(v0##N, 0);                                              \
    C_TEST_ASSERT_EQ(0, c_vector_size##N(v0##N), "Failed resize A " #N);       \
    c_vector_resize##N(v0##N, 1);                                              \
    C_TEST_ASSERT_EQ(1, c_vector_size##N(v0##N), "Failed resize B " #N);       \
    c_vector_free##N(& v0##N);                                                 \
    C_TEST_ASSERT_FALSE(v0##N, "Failed to free vector " #N);                   \
                                                                               \
    c_uint64_t n1##N = 1;                                                      \
    C_VECTOR_TYPE(N) v1##N = c_vector_new_set##N(n1##N, n1##N, 0);             \
    C_TEST_ASSERT(v1##N, "Failed to create vector " #N);                       \
    C_TEST_ASSERT_EQ(1, c_vector_size##N(v1##N), "Failed resize C " #N);       \
    c_vector_resize##N(v1##N, 0);                                              \
    C_TEST_ASSERT_EQ(0, c_vector_size##N(v1##N), "Failed resize D " #N);       \
    c_vector_resize##N(v1##N, 1);                                              \
    C_TEST_ASSERT_EQ(1, c_vector_size##N(v1##N), "Failed resize E " #N);       \
    c_vector_resize##N(v1##N, 3);                                              \
    C_TEST_ASSERT_EQ(3, c_vector_size##N(v1##N), "Failed resize F " #N);       \
    c_vector_free##N(& v1##N);                                                 \
    C_TEST_ASSERT_FALSE(v1##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_TYPE(TEST_RESIZE)
    #undef TEST_RESIZE
}

void test_c_vector_resize_set_ptr()
{
    #define TEST_RESIZE_SET_PTR(N, T)                                          \
    c_uint64_t n0##N = 0;                                                      \
    T p##N;                                                                    \
    C_MEMORY_NEW(p##N);                                                        \
    C_VECTOR_TYPE(N) v0##N = c_vector_new_set##N(n0##N, n0##N, 0);             \
    C_TEST_ASSERT(c_vector_empty##N(v0##N), "Failed to create vector " #N);    \
    c_vector_resize_set##N(v0##N, 0, p##N);                                    \
    C_TEST_ASSERT_EQ(0, c_vector_size##N(v0##N), "Failed resize_set A " #N);   \
    c_vector_resize_set##N(v0##N, 1, p##N);                                    \
    C_TEST_ASSERT_EQ(1, c_vector_size##N(v0##N), "Failed resize_set B " #N);   \
    C_TEST_ASSERT_EQ_PTR(p##N, c_vector_get##N(v0##N, 0), "Failed resize_set C " #N);\
    c_vector_free##N(& v0##N);                                                 \
    C_TEST_ASSERT_FALSE(v0##N, "Failed to free vector " #N);                   \
                                                                               \
    c_uint64_t n1##N = 1;                                                      \
    C_VECTOR_TYPE(N) v1##N = c_vector_new_set##N(n1##N, n1##N, 0);             \
    C_TEST_ASSERT(v1##N, "Failed to create vector " #N);                       \
    C_TEST_ASSERT_EQ(1, c_vector_size##N(v1##N), "Failed resize_set D " #N);   \
    c_vector_resize_set##N(v1##N, 0, p##N);                                    \
    C_TEST_ASSERT_EQ(0, c_vector_size##N(v1##N), "Failed resize_set E " #N);   \
    c_vector_resize_set##N(v1##N, 1, p##N);                                    \
    C_TEST_ASSERT_EQ(1, c_vector_size##N(v1##N), "Failed resize_set F " #N);   \
    C_TEST_ASSERT_EQ_PTR(p##N, c_vector_get##N(v1##N, 0), "Failed resize_set G " #N);\
    c_vector_resize_set##N(v1##N, 3, p##N);                                    \
    C_TEST_ASSERT_EQ(3, c_vector_size##N(v1##N), "Failed resize_set H " #N);   \
    C_TEST_ASSERT_EQ_PTR(p##N, c_vector_get##N(v1##N, 2), "Failed resize_set I " #N);\
    c_vector_free##N(& v1##N);                                                 \
    C_TEST_ASSERT_FALSE(v1##N, "Failed to free vector " #N);                   \
    C_MEMORY_FREE(p##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST_RESIZE_SET_PTR)
    #undef TEST_RESIZE_SET_PTR
}

void test_c_vector_resize_set_value()
{
    #define TEST_RESIZE_SET_VALUE(N, T)                                        \
    c_uint64_t n0##N = 0;                                                      \
    T i##N = 8;                                                                \
    C_VECTOR_TYPE(N) v0##N = c_vector_new_set##N(n0##N, n0##N, 0);             \
    C_TEST_ASSERT(c_vector_empty##N(v0##N), "Failed to create vector " #N);    \
    c_vector_resize_set##N(v0##N, 0, i##N);                                    \
    C_TEST_ASSERT_EQ(0, c_vector_size##N(v0##N), "Failed resize_set A " #N);   \
    c_vector_resize_set##N(v0##N, 1, i##N);                                    \
    C_TEST_ASSERT_EQ(1, c_vector_size##N(v0##N), "Failed resize_set B " #N);   \
    C_TEST_ASSERT_EQ(i##N, c_vector_get##N(v0##N, 0), "Failed resize_set C " #N);\
    c_vector_free##N(& v0##N);                                                 \
    C_TEST_ASSERT_FALSE(v0##N, "Failed to free vector " #N);                   \
                                                                               \
    c_uint64_t n1##N = 1;                                                      \
    C_VECTOR_TYPE(N) v1##N = c_vector_new_set##N(n1##N, n1##N, 0);             \
    C_TEST_ASSERT(v1##N, "Failed to create vector " #N);                       \
    C_TEST_ASSERT_EQ(1, c_vector_size##N(v1##N), "Failed resize_set D " #N);   \
    c_vector_resize_set##N(v1##N, 0, i##N);                                    \
    C_TEST_ASSERT_EQ(0, c_vector_size##N(v1##N), "Failed resize_set E " #N);   \
    c_vector_resize_set##N(v1##N, 1, i##N);                                    \
    C_TEST_ASSERT_EQ(1, c_vector_size##N(v1##N), "Failed resize_set F " #N);   \
    C_TEST_ASSERT_EQ(i##N, c_vector_get##N(v1##N, 0), "Failed resize_set G " #N);\
    c_vector_resize_set##N(v1##N, 3, i##N);                                    \
    C_TEST_ASSERT_EQ(3, c_vector_size##N(v1##N), "Failed resize_set H " #N);   \
    C_TEST_ASSERT_EQ(i##N, c_vector_get##N(v1##N, 2), "Failed resize_set I " #N);\
    c_vector_free##N(& v1##N);                                                 \
    C_TEST_ASSERT_FALSE(v1##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_TYPE_VALUE(TEST_RESIZE_SET_VALUE)
    #undef TEST_RESIZE_SET_VALUE
}

void test_c_vector_reverse_ptr()
{
    #define TEST_REVERSE_PTR(N, T)                                             \
    c_uint64_t n0##N = 0;                                                      \
    C_VECTOR_TYPE(N) v0##N = c_vector_new_set##N(n0##N, n0##N, 0);             \
    C_TEST_ASSERT(c_vector_empty##N(v0##N), "Failed to create vector " #N);    \
    c_vector_reverse##N(v0##N);                                                \
    c_vector_free##N(& v0##N);                                                 \
    C_TEST_ASSERT_FALSE(v0##N, "Failed to free vector " #N);                   \
                                                                               \
    c_uint64_t n1##N = 1;                                                      \
    C_VECTOR_TYPE(N) v1##N = c_vector_new_set##N(n1##N, n1##N, 0);             \
    C_TEST_ASSERT(v1##N, "Failed to create vector " #N);                       \
    c_vector_reverse##N(v1##N);                                                \
    c_vector_free##N(& v1##N);                                                 \
    C_TEST_ASSERT_FALSE(v1##N, "Failed to free vector " #N);                   \
                                                                               \
    c_uint64_t n2##N = 2;                                                      \
    T a21##N = NULL;                                                           \
    T a22##N = NULL;                                                           \
    C_MEMORY_NEW(a21##N);                                                      \
    C_MEMORY_NEW(a22##N);                                                      \
    C_VECTOR_TYPE(N) v2##N = c_vector_new_set##N(n2##N, n2##N, 0);             \
    c_vector_set##N(v2##N, 0, a21##N);                                         \
    c_vector_set##N(v2##N, 1, a22##N);                                         \
    c_vector_reverse##N(v2##N);                                                \
    C_TEST_ASSERT_EQ_PTR(a22##N, c_vector_get##N(v2##N, 0), "Failed reverse " #N);\
    C_TEST_ASSERT_EQ_PTR(a21##N, c_vector_get##N(v2##N, 1), "Failed reverse " #N);\
    c_vector_free##N(& v2##N);                                                 \
    C_TEST_ASSERT_FALSE(v2##N, "Failed to free vector " #N);                   \
    C_MEMORY_FREE(a21##N);                                                     \
    C_MEMORY_FREE(a22##N);                                                     \
                                                                               \
    c_uint64_t n3##N = 3;                                                      \
    T a31##N = NULL;                                                           \
    T a32##N = NULL;                                                           \
    T a33##N = NULL;                                                           \
    C_MEMORY_NEW(a31##N);                                                      \
    C_MEMORY_NEW(a32##N);                                                      \
    C_MEMORY_NEW(a33##N);                                                      \
    C_VECTOR_TYPE(N) v3##N = c_vector_new_set##N(n3##N, n3##N, 0);             \
    c_vector_set##N(v3##N, 0, a31##N);                                         \
    c_vector_set##N(v3##N, 1, a32##N);                                         \
    c_vector_set##N(v3##N, 2, a33##N);                                         \
    c_vector_reverse##N(v3##N);                                                \
    C_TEST_ASSERT_EQ_PTR(a33##N, c_vector_get##N(v3##N, 0), "Failed reverse " #N);\
    C_TEST_ASSERT_EQ_PTR(a32##N, c_vector_get##N(v3##N, 1), "Failed reverse " #N);\
    C_TEST_ASSERT_EQ_PTR(a31##N, c_vector_get##N(v3##N, 2), "Failed reverse " #N);\
    c_vector_free##N(& v3##N);                                                 \
    C_TEST_ASSERT_FALSE(v3##N, "Failed to free vector " #N);                   \
    C_MEMORY_FREE(a31##N);                                                     \
    C_MEMORY_FREE(a32##N);                                                     \
    C_MEMORY_FREE(a33##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST_REVERSE_PTR)
    #undef TEST_REVERSE_PTR
}

void test_c_vector_reverse_value()
{
    #define TEST_REVERSE(N, T)                                                 \
    c_uint64_t n0##N = 0;                                                      \
    C_VECTOR_TYPE(N) v0##N = c_vector_new_set##N(n0##N, n0##N, 0);             \
    C_TEST_ASSERT(c_vector_empty##N(v0##N), "Failed to create vector " #N);    \
    c_vector_reverse##N(v0##N);                                                \
    c_vector_free##N(& v0##N);                                                 \
    C_TEST_ASSERT_FALSE(v0##N, "Failed to free vector " #N);                   \
                                                                               \
    c_uint64_t n1##N = 1;                                                      \
    C_VECTOR_TYPE(N) v1##N = c_vector_new_set##N(n1##N, n1##N, 0);             \
    C_TEST_ASSERT(v1##N, "Failed to create vector " #N);                       \
    c_vector_reverse##N(v1##N);                                                \
    c_vector_free##N(& v1##N);                                                 \
    C_TEST_ASSERT_FALSE(v1##N, "Failed to free vector " #N);                   \
                                                                               \
    c_uint64_t n2##N = 2;                                                      \
    C_VECTOR_TYPE(N) v2##N = c_vector_new_set##N(n2##N, n2##N, 0);             \
    c_vector_set##N(v2##N, 0, 0);                                              \
    c_vector_set##N(v2##N, 1, 1);                                              \
    c_vector_reverse##N(v2##N);                                                \
    C_TEST_ASSERT_EQ(1, c_vector_get##N(v2##N, 0), "Failed reverse " #N);      \
    C_TEST_ASSERT_EQ(0, c_vector_get##N(v2##N, 1), "Failed reverse " #N);      \
    c_vector_free##N(& v2##N);                                                 \
    C_TEST_ASSERT_FALSE(v2##N, "Failed to free vector " #N);                   \
                                                                               \
    c_uint64_t n3##N = 3;                                                      \
    C_VECTOR_TYPE(N) v3##N = c_vector_new_set##N(n3##N, n3##N, 0);             \
    c_vector_set##N(v3##N, 0, 0);                                              \
    c_vector_set##N(v3##N, 1, 1);                                              \
    c_vector_set##N(v3##N, 2, 2);                                              \
    c_vector_reverse##N(v3##N);                                                \
    C_TEST_ASSERT_EQ(2, c_vector_get##N(v3##N, 0), "Failed reverse " #N);      \
    C_TEST_ASSERT_EQ(1, c_vector_get##N(v3##N, 1), "Failed reverse " #N);      \
    C_TEST_ASSERT_EQ(0, c_vector_get##N(v3##N, 2), "Failed reverse " #N);      \
    c_vector_free##N(& v3##N);                                                 \
    C_TEST_ASSERT_FALSE(v3##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_TYPE_VALUE(TEST_REVERSE)
    #undef TEST_REVERSE
}

void test_c_vector_set()
{
    #define TEST_SET(N, T)                                                     \
    c_uint64_t n##N = 2;                                                       \
    C_VECTOR_TYPE(N) v##N = c_vector_new_set##N(n##N, n##N, 0);                \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    c_vector_set##N(v##N, 0, 0);                                               \
    c_vector_set##N(v##N, 1, 0);                                               \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_TYPE(TEST_SET)
    #undef TEST_SET
}

void test_c_vector_set_safe()
{
    #define TEST_SET_SAFE(N, T)                                                \
    c_uint64_t n##N = 2;                                                       \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
    c_vector_set_safe##N(v##N, 10, 0);                                         \
    v##N = c_vector_new_set##N(n##N, n##N, 0);                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    c_vector_set_safe##N(v##N, 10, 0);                                         \
    c_vector_set_safe##N(v##N, 0, 0);                                          \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_TYPE(TEST_SET_SAFE)
    #undef TEST_SET_SAFE
}

void test_c_vector_size()
{
    #define TEST_SIZE_ZERO(N)                                                  \
    c_uint64_t n##N = 3;                                                       \
    C_VECTOR_TYPE(N) v##N = c_vector_new##N(n##N);                             \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT_EQ(c_vector_size##N(v##N), 0, "Failed size " #N);            \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME(TEST_SIZE_ZERO)
    #undef TEST_SIZE_ZERO

    #define TEST_SIZE(N)                                                       \
    C_VECTOR_TYPE(N) a##N = c_vector_new_set##N(n##N, n##N, 0);                \
    C_TEST_ASSERT(a##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT_EQ(c_vector_size##N(a##N), n##N, "Failed size " #N);         \
    c_vector_free##N(& a##N);                                                  \
    C_TEST_ASSERT_FALSE(a##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME(TEST_SIZE)
    #undef TEST_SIZE
}

void test_c_vector_swap_at_ptr()
{
    #define TEST_SWAP_AT_PTR(N, T)                                             \
    c_uint64_t n##N = 2;                                                       \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
    T p1##N = NULL;                                                            \
    T p2##N = NULL;                                                            \
                                                                               \
    c_vector_swap_at##N(v##N, 0, 1);                                           \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    c_vector_swap_at##N(v##N, 0 , 0);                                          \
    c_vector_swap_at##N(v##N, 0 , 10);                                         \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);                    \
                                                                               \
    C_MEMORY_NEW(p1##N);                                                       \
    C_MEMORY_NEW(p2##N);                                                       \
    v##N = c_vector_new_set##N(n##N, n##N, 0);                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    c_vector_set##N(v##N, 0, p1##N);                                           \
    c_vector_set##N(v##N, 1, p2##N);                                           \
    C_TEST_ASSERT_EQ_PTR(p1##N, c_vector_get##N(v##N, 0), "Failed swap " #N);  \
    C_TEST_ASSERT_EQ_PTR(p2##N, c_vector_get##N(v##N, 1), "Failed swap " #N);  \
    c_vector_swap_at##N(v##N, 0 , 1);                                          \
    C_TEST_ASSERT_EQ_PTR(p2##N, c_vector_get##N(v##N, 0), "Failed swap " #N);  \
    C_TEST_ASSERT_EQ_PTR(p1##N, c_vector_get##N(v##N, 1), "Failed swap " #N);  \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);                    \
    C_MEMORY_FREE(p1##N);                                                      \
    C_MEMORY_FREE(p2##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST_SWAP_AT_PTR)
    #undef TEST_SWAP_AT_PTR
}

void test_c_vector_swap_at_value()
{
    #define TEST_SWAP_AT_VALUE(N, T)                                           \
    c_uint64_t n##N = 2;                                                       \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
                                                                               \
    c_vector_swap_at##N(v##N, 0 , 1);                                          \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    c_vector_swap_at##N(v##N, 0 , 0);                                          \
    c_vector_swap_at##N(v##N, 0 , 10);                                         \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);                    \
                                                                               \
    v##N = c_vector_new_set##N(n##N, n##N, 0);                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    c_vector_swap_at##N(v##N, 0 , 1);                                          \
    c_vector_set##N(v##N, 1, 1);                                               \
    C_TEST_ASSERT_EQ(0, c_vector_get##N(v##N, 0), "Failed swap " #N);          \
    C_TEST_ASSERT_EQ(1, c_vector_get##N(v##N, 1), "Failed swap " #N);          \
    c_vector_swap_at##N(v##N, 0 , 1);                                          \
    C_TEST_ASSERT_EQ(1, c_vector_get##N(v##N, 0), "Failed swap " #N);          \
    C_TEST_ASSERT_EQ(0, c_vector_get##N(v##N, 1), "Failed swap " #N);          \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_TYPE_VALUE(TEST_SWAP_AT_VALUE)
    #undef TEST_SWAP_AT_VALUE
}

void test_c_vector_swap_ptr()
{
    #define TEST_SWAP_PTR(N, T)                                                \
    c_uint64_t n1##N = 1;                                                      \
    c_uint64_t n2##N = 2;                                                      \
    T p1##N;                                                                   \
    T p2##N;                                                                   \
    C_MEMORY_NEW(p1##N);                                                       \
    C_MEMORY_NEW(p2##N);                                                       \
                                                                               \
    C_VECTOR_TYPE(N) v1##N = c_vector_new_set##N(n1##N, n1##N, p1##N);         \
    C_VECTOR_TYPE(N) v2##N = c_vector_new_set##N(n2##N, n2##N, p2##N);         \
    C_TEST_ASSERT(v1##N, "Failed to create vector " #N);                       \
    C_TEST_ASSERT(v2##N, "Failed to create vector " #N);                       \
    C_TEST_ASSERT_EQ(c_vector_size##N(v1##N), n1##N, "Failed size " #N);       \
    C_TEST_ASSERT_EQ(c_vector_size##N(v2##N), n2##N, "Failed size " #N);       \
    c_vector_swap##N(v1##N, v2##N);                                            \
    C_TEST_ASSERT_EQ(c_vector_capacity##N(v1##N), n2##N, "Failed swap " #N);   \
    C_TEST_ASSERT_EQ(c_vector_capacity##N(v2##N), n1##N, "Failed swap " #N);   \
    C_TEST_ASSERT_EQ(c_vector_size##N(v1##N), n2##N, "Failed swap " #N);       \
    C_TEST_ASSERT_EQ(c_vector_size##N(v2##N), n1##N, "Failed swap " #N);       \
    C_TEST_ASSERT_EQ_PTR(c_vector_get##N(v1##N, 0), p2##N, "Failed swap " #N); \
    C_TEST_ASSERT_EQ_PTR(c_vector_get##N(v2##N, 0), p1##N, "Failed swap " #N); \
    c_vector_free##N(& v1##N);                                                 \
    c_vector_free##N(& v2##N);                                                 \
    C_TEST_ASSERT_FALSE(v1##N, "Failed to free vector " #N);                   \
    C_TEST_ASSERT_FALSE(v2##N, "Failed to free vector " #N);                   \
    C_MEMORY_FREE(p1##N);                                                      \
    C_MEMORY_FREE(p2##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST_SWAP_PTR)
    #undef TEST_SWAP_PTR
}

void test_c_vector_swap_value()
{
    #define TEST_SWAP_VALUE(N, T)                                              \
    c_uint64_t n1##N = 1;                                                      \
    c_uint64_t n2##N = 2;                                                      \
                                                                               \
    C_VECTOR_TYPE(N) v1##N = c_vector_new_set##N(n1##N, n1##N, 1);             \
    C_VECTOR_TYPE(N) v2##N = c_vector_new_set##N(n2##N, n2##N, 2);             \
    C_TEST_ASSERT(v1##N, "Failed to create vector " #N);                       \
    C_TEST_ASSERT(v2##N, "Failed to create vector " #N);                       \
    C_TEST_ASSERT_EQ(c_vector_size##N(v1##N), n1##N, "Failed size " #N);       \
    C_TEST_ASSERT_EQ(c_vector_size##N(v2##N), n2##N, "Failed size " #N);       \
    c_vector_swap##N(v1##N, v2##N);                                            \
    C_TEST_ASSERT_EQ(c_vector_capacity##N(v1##N), n2##N, "Failed swap " #N);   \
    C_TEST_ASSERT_EQ(c_vector_capacity##N(v2##N), n1##N, "Failed swap " #N);   \
    C_TEST_ASSERT_EQ(c_vector_size##N(v1##N), n2##N, "Failed swap " #N);       \
    C_TEST_ASSERT_EQ(c_vector_size##N(v2##N), n1##N, "Failed swap " #N);       \
    C_TEST_ASSERT_EQ(c_vector_get##N(v1##N, 0), 2, "Failed swap " #N);         \
    C_TEST_ASSERT_EQ(c_vector_get##N(v2##N, 0), 1, "Failed swap " #N);         \
    c_vector_free##N(& v1##N);                                                 \
    c_vector_free##N(& v2##N);                                                 \
    C_TEST_ASSERT_FALSE(v1##N, "Failed to free vector " #N);                   \
    C_TEST_ASSERT_FALSE(v2##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_TYPE_VALUE(TEST_SWAP_VALUE)
    #undef TEST_SWAP_VALUE
}

int suite_test_c_vector()
{
    C_TEST_ADD_SUITE("suite_test_c_vector", NULL, NULL);

    C_TEST_ADD_TEST(test_c_vector_new);

    C_TEST_ADD_TEST(test_c_vector_new_copy_ptr);
    C_TEST_ADD_TEST(test_c_vector_new_copy_value);

    C_TEST_ADD_TEST(test_c_vector_new_copy_vector_ptr);
    C_TEST_ADD_TEST(test_c_vector_new_copy_vector_value);

    C_TEST_ADD_TEST(test_c_vector_new_set_ptr);
    C_TEST_ADD_TEST(test_c_vector_new_set_value);

    C_TEST_ADD_TEST(test_c_vector_get_ptr);
    C_TEST_ADD_TEST(test_c_vector_get_value);

    C_TEST_ADD_TEST(test_c_vector_get_safe_ptr);
    C_TEST_ADD_TEST(test_c_vector_get_safe_value);

    C_TEST_ADD_TEST(test_c_vector_capacity);
    C_TEST_ADD_TEST(test_c_vector_size);

    C_TEST_ADD_TEST(test_c_vector_set);
    C_TEST_ADD_TEST(test_c_vector_set_safe);

    C_TEST_ADD_TEST(test_c_vector_free_deep);
    C_TEST_ADD_TEST(test_c_vector_free_deep_udf);

    C_TEST_ADD_TEST(test_c_vector_print);

    C_TEST_ADD_TEST(test_c_vector_items);

    C_TEST_ADD_TEST(test_c_vector_empty);

    C_TEST_ADD_TEST(test_c_vector_clear);

    C_TEST_ADD_TEST(test_c_vector_reserve);

    C_TEST_ADD_TEST(test_c_vector_compact);

    C_TEST_ADD_TEST(test_c_vector_first_ptr);
    C_TEST_ADD_TEST(test_c_vector_first_value);

    C_TEST_ADD_TEST(test_c_vector_last_ptr);
    C_TEST_ADD_TEST(test_c_vector_last_value);

    C_TEST_ADD_TEST(test_c_vector_append);
    C_TEST_ADD_TEST(test_c_vector_append_rate);

    C_TEST_ADD_TEST(test_c_vector_extend);

    C_TEST_ADD_TEST(test_c_vector_reverse_ptr);
    C_TEST_ADD_TEST(test_c_vector_reverse_value);

    C_TEST_ADD_TEST(test_c_vector_resize);

    C_TEST_ADD_TEST(test_c_vector_resize_set_ptr);
    C_TEST_ADD_TEST(test_c_vector_resize_set_value);

    C_TEST_ADD_TEST(test_c_vector_swap_ptr);
    C_TEST_ADD_TEST(test_c_vector_swap_value);

    C_TEST_ADD_TEST(test_c_vector_swap_at_ptr);
    C_TEST_ADD_TEST(test_c_vector_swap_at_value);

    C_TEST_ADD_TEST(test_c_vector_copy_ptr);
    C_TEST_ADD_TEST(test_c_vector_copy_value);

    C_TEST_ADD_TEST(test_c_vector_copy_udf_ptr);

    C_TEST_ADD_TEST(test_c_vector_remove_ptr);
    C_TEST_ADD_TEST(test_c_vector_remove_value);

    C_TEST_ADD_TEST(test_c_vector_remove_first_ptr);
    C_TEST_ADD_TEST(test_c_vector_remove_first_value);

    C_TEST_ADD_TEST(test_c_vector_remove_last_ptr);
    C_TEST_ADD_TEST(test_c_vector_remove_last_value);

    C_TEST_ADD_TEST(test_c_vector_remove_unordered_ptr);
    C_TEST_ADD_TEST(test_c_vector_remove_unordered_value);

    C_TEST_ADD_TEST(test_c_vector_equals_ptr);
    C_TEST_ADD_TEST(test_c_vector_equals_value);

    C_TEST_ADD_TEST(test_c_vector_equals_udf_ptr);
    C_TEST_ADD_TEST(test_c_vector_equals_udf_ptr_void);

    C_TEST_ADD_TEST(test_c_vector_insert_value);

    C_TEST_ADD_TEST(test_c_vector_insert_bisect_ptr);
    C_TEST_ADD_TEST(test_c_vector_insert_bisect_value);

    C_TEST_ADD_TEST(test_c_vector_insert_bisect_udf);

    C_TEST_ADD_TEST(test_c_vector_map);

    C_TEST_RETURN();
}

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/