#ifndef TEST_C_HASH_INCLUDED
#define TEST_C_HASH_INCLUDED

#include "c_test.h"
#include "c_hash.h"

void test_c_hash_djb()
{
    char *c = "akey";
    c_uint64_t expect, h;
    c_str_t s = c_str_new(c, 4);

    C_TEST_ASSERT(s, "Expected string");

    h = c_hash_djb_chars(NULL);
    expect = 5381;
    C_TEST_ASSERT_EQ(expect, h, "Expected null hash.");

    expect = 6385046767;
    h = c_hash_djb_chars(c);
    C_TEST_ASSERT_EQ(expect, h, "Expected hash.");

    h = c_hash_djb_str(s);
    C_TEST_ASSERT_EQ(expect, h, "Expected equal hash.");

    c_str_free(& s);
}

void test_c_hash_identity()
{
    #define TEST(N, T)                                                         \
    T k##N = 0;                                                                \
    c_hash_t h##N = (c_hash_t) k##N;                                           \
    C_TEST_ASSERT_EQ(c_hash_identity##N(k##N), h##N, "Expected equal.");

    C_HASH_TEMPLATE_NAME_TYPE(TEST)
    C_HASH_TEMPLATE_NAME_TYPE_STR(TEST)
    #undef TEST
}

void test_c_hash_murmur3_32()
{
    char *c = "akey";
    char *c2 = "longerkey";
    c_uint64_t expect, h;
    c_str_t s1 = c_str_new(c, 4);
    c_str_t s2 = c_str_new(c2, 5);
    c_str_t s3 = c_str_new(c2, 6);
    c_str_t s4 = c_str_new(c2, 7);

    C_TEST_ASSERT(s1, "Expected string");

    expect = 0;
    h = c_hash_murmur3_32_str(NULL);
    C_TEST_ASSERT_EQ(expect, h, "Expected null hash.");

    expect = 260167385;
    h = c_hash_murmur3_32_str(s1);
    C_TEST_ASSERT_EQ(expect, h, "Expected hash.");

    expect = 2738162770;
    h = c_hash_murmur3_32_str(s2);
    C_TEST_ASSERT_EQ(expect, h, "Expected hash.");

    expect = 3886613225;
    h = c_hash_murmur3_32_str(s3);
    C_TEST_ASSERT_EQ(expect, h, "Expected hash.");

    expect = 3960820800;
    h = c_hash_murmur3_32_str(s4);
    C_TEST_ASSERT_EQ(expect, h, "Expected hash.");

    c_str_free(& s1);
    c_str_free(& s2);
    c_str_free(& s3);
    c_str_free(& s4);
}

void test_c_hash_murmur64a()
{
    char *c = "akey";
    char *c2 = "longerkey";
    c_uint64_t expect, h;
    c_str_t s1 = c_str_new(c, 4);
    c_str_t s2 = c_str_new(c2, 5);
    c_str_t s3 = c_str_new(c2, 6);
    c_str_t s4 = c_str_new(c2, 7);
    c_str_t s5 = c_str_new(c2, 8);

    C_TEST_ASSERT(s1, "Expected string");

    expect = 0;
    h = c_hash_murmur64a_str(NULL);
    C_TEST_ASSERT_EQ(expect, h, "Expected null hash.");

    expect = 18293334918592887922u;
    h = c_hash_murmur64a_str(s1);
    C_TEST_ASSERT_EQ(expect, h, "Expected hash.");

    expect = 11265214470255733879u;
    h = c_hash_murmur64a_str(s2);
    C_TEST_ASSERT_EQ(expect, h, "Expected hash.");

    expect = 11996691778748882742u;
    h = c_hash_murmur64a_str(s3);
    C_TEST_ASSERT_EQ(expect, h, "Expected hash.");

    expect = 1795802339923209786u;
    h = c_hash_murmur64a_str(s4);
    C_TEST_ASSERT_EQ(expect, h, "Expected hash.");

    expect = 5539235969950211625u;
    h = c_hash_murmur64a_str(s5);
    C_TEST_ASSERT_EQ(expect, h, "Expected hash.");

    c_str_free(& s1);
    c_str_free(& s2);
    c_str_free(& s3);
    c_str_free(& s4);
    c_str_free(& s5);
}

void test_c_hash_shift()
{
    #define TEST(N, T)                                                         \
    T k##N = 40;                                                               \
    c_hash_t h##N = c_hash_shift##N(k##N);                                     \
    C_TEST_ASSERT_EQ(h##N, 10, "Expected equal.");

    C_HASH_TEMPLATE_NAME_TYPE_VALUE(TEST)
    #undef TEST

    void *k = NULL;
    c_hash_t h = c_hash_shift(k);
    C_TEST_ASSERT_EQ_PTR(h, 0, "Expected equal.");
}

int suite_test_c_hash()
{
    C_TEST_ADD_SUITE("suite_test_c_hash", NULL, NULL);

    C_TEST_ADD_TEST(test_c_hash_identity);
    C_TEST_ADD_TEST(test_c_hash_shift);
    C_TEST_ADD_TEST(test_c_hash_djb);
    C_TEST_ADD_TEST(test_c_hash_murmur3_32);
    C_TEST_ADD_TEST(test_c_hash_murmur64a);

    C_TEST_RETURN();
}

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/
