#!/usr/bin/env python

import re
import sys

if len(sys.argv) < 3:
    sys.exit('usage: {} html markdown'.format(sys.argv[0]))

RE = re.compile(r'''<td class="headerCovTableEntry">(?P<hitl>.*)</td>
            <td class="headerCovTableEntry">(?P<totl>.*)</td>
            <td class="headerCovTableEntryHi">(?P<covl>.*)</td>
          </tr>
          <tr>
            <td class="headerItem">Date:</td>
            <td class="headerValue">(?P<date>.*)</td>
            <td></td>
            <td class="headerItem">Functions:</td>
            <td class="headerCovTableEntry">(?P<hitf>.*)</td>
            <td class="headerCovTableEntry">(?P<totf>.*)</td>
            <td class="headerCovTableEntryHi">(?P<covf>.*)</td>''')

FMT = '''### Coverage Status

|{date}           |    Hit |  Total | Coverage |
|:--------------------|-------:|-------:|---------:|
|Lines:               | {hitl:>6} | {totl:>6} |  {covl:>7} |
|Functions:           | {hitf:>6} | {totf:>6} |  {covf:>7} |'''

f1 = sys.argv[1]
f2 = sys.argv[2]

html = open(f1).read()
md = open(f2).read()

match = RE.search(html)

covl = match.group('covl')
covf = match.group('covf')
date = match.group('date')
hitf = match.group('hitf')
hitl = match.group('hitl')
totf = match.group('totf')
totl = match.group('totl')

idx1 = md.find('### Coverage Status')
idx2 = md.find('|Functions:', idx1)
idx3 = md.find('\n', idx2)

table = FMT.format(covl=covl, covf=covf, date=date, hitf=hitf, hitl=hitl, totf=totf, totl=totl)

md2 = md[:idx1] + table + md[idx3:]

with open(f2, 'w') as f:
    f.write(md2)
