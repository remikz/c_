#ifndef C_CMP_IMPL_INCLUDED
#define C_CMP_IMPL_INCLUDED

#include "c_cmp.h"

#ifdef __cplusplus
extern "C" {
#endif

#define C_CMP(NAME, TYPE)                                                      \
c_int32_t c_cmp##NAME(const void *a, const void *b)                            \
{                                                                              \
    if (*(TYPE)a < *(TYPE)b) {                                                 \
        return -1;                                                             \
    }                                                                          \
    if (*(TYPE)a > *(TYPE)b) {                                                 \
        return 1;                                                              \
    }                                                                          \
    return 0;                                                                  \
}

#define C_CMP_PTR_REF(NAME, TYPE)                                                      \
c_int32_t c_cmp_ptr_ref##NAME(const void *a, const void *b)                            \
{                                                                              \
    if (**(TYPE*)a < **(TYPE*)b) {                                                 \
        return -1;                                                             \
    }                                                                          \
    if (**(TYPE*)a > **(TYPE*)b) {                                                 \
        return 1;                                                              \
    }                                                                          \
    return 0;                                                                  \
}

#define C_CMP_PTR(NAME, TYPE)                                                  \
c_int32_t c_cmp_ptr##NAME(const TYPE a, const TYPE b)                          \
{                                                                              \
    if (*a < *b) {                                                             \
        return -1;                                                             \
    }                                                                          \
    if (*a > *b) {                                                             \
        return 1;                                                              \
    }                                                                          \
    return 0;                                                                  \
}

#define C_CMP_PTR_SAFE(NAME, TYPE)                                             \
c_int32_t c_cmp_ptr_safe##NAME(const TYPE a, const TYPE b)                     \
{                                                                              \
    if (a && b) {                                                              \
        if (*a < *b) {                                                         \
            return -1;                                                         \
        }                                                                      \
        if (*a > *b) {                                                         \
            return 1;                                                          \
        }                                                                      \
        return 0;                                                              \
    }                                                                          \
    return -1;                                                                 \
}

#define C_CMP_SAFE(NAME, TYPE)                                                 \
c_int32_t c_cmp_safe##NAME(const void *a, const void *b)                       \
{                                                                              \
    if (a && b) {                                                              \
        if (*(TYPE)a < *(TYPE)b) {                                             \
            return -1;                                                         \
        }                                                                      \
        if (*(TYPE)a > *(TYPE)b) {                                             \
            return 1;                                                          \
        }                                                                      \
        return 0;                                                              \
    }                                                                          \
    return -1;                                                                 \
}

#define C_CMP_VALUE(NAME, TYPE)                                                \
c_int32_t c_cmp_value##NAME(const TYPE a, const TYPE b)                        \
{                                                                              \
    if (a < b) {                                                               \
        return -1;                                                             \
    }                                                                          \
    if (a > b) {                                                               \
        return 1;                                                              \
    }                                                                          \
    return 0;                                                                  \
}

C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(C_CMP)
C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(C_CMP_PTR)
C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(C_CMP_PTR_REF)
C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(C_CMP_PTR_SAFE)
C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(C_CMP_SAFE)
C_TYPE_TEMPLATE_NAME_TYPE_VALUE(C_CMP_VALUE)

#undef C_CMP
#undef C_CMP_PTR
#undef C_CMP_PTR_REF
#undef C_CMP_PTR_SAFE
#undef C_CMP_SAFE
#undef C_CMP_VALUE

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/
