#ifndef TEST_C_CMP_INCLUDED
#define TEST_C_CMP_INCLUDED

#include "c_test.h"
#include "c_cmp.h"

void test_c_cmp()
{
    #define TEST(N, T)                                                         \
                                                                               \
    T a##N;                                                                    \
    T b##N;                                                                    \
                                                                               \
    C_MEMORY_NEW(a##N);                                                        \
    C_MEMORY_NEW(b##N);                                                        \
                                                                               \
    *a##N = 0; *b##N = 1;                                                      \
    C_TEST_ASSERT_EQ(c_cmp##N(a##N, b##N), -1, "Expected less than.");         \
    *a##N = 1; *b##N = 0;                                                      \
    C_TEST_ASSERT_EQ(c_cmp##N(a##N, b##N), 1, "Expected greater than.");       \
    *a##N = 0; *b##N = 0;                                                      \
    C_TEST_ASSERT_EQ(c_cmp##N(a##N, b##N), 0, "Expected equal.");              \
                                                                               \
    C_MEMORY_FREE(a##N);                                                       \
    C_MEMORY_FREE(b##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(TEST)
    #undef TEST
}

void test_c_cmp_safe()
{
    #define TEST(N, T)                                                         \
                                                                               \
    T a##N;                                                                    \
    T b##N;                                                                    \
                                                                               \
    C_MEMORY_NEW(a##N);                                                        \
    C_MEMORY_NEW(b##N);                                                        \
                                                                               \
    C_TEST_ASSERT_EQ(c_cmp_safe##N(NULL, NULL), -1, "Expected -1 for nulls."); \
    *a##N = 0; *b##N = 1;                                                      \
    C_TEST_ASSERT_EQ(c_cmp_safe##N(a##N, b##N), -1, "Expected less than.");    \
    *a##N = 1; *b##N = 0;                                                      \
    C_TEST_ASSERT_EQ(c_cmp_safe##N(a##N, b##N), 1, "Expected greater than.");  \
    *a##N = 0; *b##N = 0;                                                      \
    C_TEST_ASSERT_EQ(c_cmp_safe##N(a##N, b##N), 0, "Expected equal.");         \
                                                                               \
    C_MEMORY_FREE(a##N);                                                       \
    C_MEMORY_FREE(b##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(TEST)
    #undef TEST
}

void test_c_cmp_ptr()
{
    #define TEST(N, T)                                                         \
                                                                               \
    T a##N;                                                                    \
    T b##N;                                                                    \
                                                                               \
    C_MEMORY_NEW(a##N);                                                        \
    C_MEMORY_NEW(b##N);                                                        \
                                                                               \
    *a##N = 0; *b##N = 1;                                                      \
    C_TEST_ASSERT_EQ(c_cmp_ptr##N(a##N, b##N), -1, "Expected less than.");     \
    *a##N = 1; *b##N = 0;                                                      \
    C_TEST_ASSERT_EQ(c_cmp_ptr##N(a##N, b##N), 1, "Expected greater than.");   \
    *a##N = 0; *b##N = 0;                                                      \
    C_TEST_ASSERT_EQ(c_cmp_ptr##N(a##N, b##N), 0, "Expected equal.");          \
                                                                               \
    C_MEMORY_FREE(a##N);                                                       \
    C_MEMORY_FREE(b##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(TEST)
    #undef TEST
}

void test_c_cmp_ptr_safe()
{
    #define TEST(N, T)                                                         \
                                                                               \
    T a##N;                                                                    \
    T b##N;                                                                    \
                                                                               \
    C_MEMORY_NEW(a##N);                                                        \
    C_MEMORY_NEW(b##N);                                                        \
                                                                               \
    C_TEST_ASSERT_EQ(c_cmp_ptr_safe##N(NULL, NULL), -1, "Expected -1 for nulls.");\
    *a##N = 0; *b##N = 1;                                                      \
    C_TEST_ASSERT_EQ(c_cmp_ptr_safe##N(a##N, b##N), -1, "Expected less than.");\
    *a##N = 1; *b##N = 0;                                                      \
    C_TEST_ASSERT_EQ(c_cmp_ptr_safe##N(a##N, b##N), 1, "Expected greater than.");\
    *a##N = 0; *b##N = 0;                                                      \
    C_TEST_ASSERT_EQ(c_cmp_ptr_safe##N(a##N, b##N), 0, "Expected equal.");     \
                                                                               \
    C_MEMORY_FREE(a##N);                                                       \
    C_MEMORY_FREE(b##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(TEST)
    #undef TEST
}

void test_c_cmp_value()
{
    #define TEST(N, T)                                                         \
                                                                               \
    T a##N, b##N;                                                              \
                                                                               \
    a##N = 0; b##N = 1;                                                        \
    C_TEST_ASSERT_EQ(c_cmp_value##N(a##N, b##N), -1, "Expected less than.");   \
    a##N = 1; b##N = 0;                                                        \
    C_TEST_ASSERT_EQ(c_cmp_value##N(a##N, b##N), 1, "Expected greater than."); \
    a##N = 0; b##N = 0;                                                        \
    C_TEST_ASSERT_EQ(c_cmp_value##N(a##N, b##N), 0, "Expected equal.");

    C_TYPE_TEMPLATE_NAME_TYPE_VALUE(TEST)
    #undef TEST
}

int suite_test_c_cmp()
{
    C_TEST_ADD_SUITE("suite_test_c_cmp", NULL, NULL);

    C_TEST_ADD_TEST(test_c_cmp);
    C_TEST_ADD_TEST(test_c_cmp_safe);
    C_TEST_ADD_TEST(test_c_cmp_ptr);
    C_TEST_ADD_TEST(test_c_cmp_ptr_safe);
    C_TEST_ADD_TEST(test_c_cmp_value);

    C_TEST_RETURN();
}

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/