#include <c_vector.h>
#include <stdio.h>

void apply_map(c_int64_t item, void *closure)
{
    printf("%d ", (int) item + 100);
}

int main(int argc, char **argv)
{
    c_int64_t i, n;
    c_vector_int64_t v = c_vector_new_int64(0);
    c_vector_int64_t v2 = c_vector_new_int64(0);

    puts("empty:");
    c_vector_print_int64(v);

    n = 3;
    for(i = 0; i < n; ++i) {
        c_vector_append_int64(v, i);
    }

    puts("append:");
    c_vector_print_int64(v);

    for(i = -n; i < n*2; ++i) {
        c_vector_insert_bisect_int64(v, i);
    }

    puts("insert bisect:");
    c_vector_print_int64(v);

    c_vector_reverse_int64(v);
    puts("reverse:");
    c_vector_print_int64(v);

    for(i = 0; i < n; ++i) {
        c_vector_insert_int64(v, 0, -i);
    }

    puts("insert:");
    c_vector_print_int64(v);

    puts("first:");
    printf("%d\n", (int) c_vector_first_int64(v));

    puts("last:");
    printf("%d\n", (int) c_vector_last_int64(v));

    c_vector_copy_int64(v2, v);
    puts("copy:");
    c_vector_print_int64(v2);

    printf("equals: %d\n", (int) c_vector_equals_int64(v2, v));

    c_vector_extend_int64(v2, v);
    puts("extend:");
    c_vector_print_int64(v2);

    c_vector_remove_int64(v2, 0);
    puts("remove:");
    c_vector_print_int64(v2);

    c_vector_swap_at_int64(v2, 0, 1);
    puts("swap at:");
    c_vector_print_int64(v2);

    puts("map:");
    c_vector_map_int64(v2, apply_map, 0);
    printf("\n");

    c_vector_clear_int64(v2);
    puts("clear v2:");
    c_vector_print_int64(v2);

    c_vector_swap_int64(v, v2);
    puts("swap v:");
    c_vector_print_int64(v);
    puts("swap v2:");
    c_vector_print_int64(v2);

    c_vector_free_int64(& v);
    c_vector_free_int64(& v2);

    return 0;
}