#ifndef TEST_C_SLICE_INCLUDED
#define TEST_C_SLICE_INCLUDED

#include "c_test.h"
#include "c_slice.h"
#include <limits.h> // LLONG_M*

void test_c_slice_compute_range_end()
{
    c_slice_t s = NULL;
    c_int64_t first, last, step, size = 10;
    c_uint64_t n;

    s = c_slice_new_end(-1);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  8, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     9, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_end(-5);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  4, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     5, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_end(0);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(n,     0, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_end(4);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  3, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     4, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

}

void test_c_slice_compute_range_end_stride()
{
    c_slice_t s = NULL;
    c_int64_t first, last, step, size = 10;
    c_uint64_t n = 0;

    s = c_slice_new_end_stride(0, 0);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(n,     0, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_end_stride(1, 0);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(n,     0, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_end_stride(-1, 0);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(n,     0, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_end_stride(0, 1);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(n,     0, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_end_stride(1, 1);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  0, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     1, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_end_stride(2, 1);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  1, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     2, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_end_stride(-1, 1);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  8, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     9, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_end_stride(-2, 1);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  7, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     8, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_end_stride(-1, 3);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  6, "Expected last.");
    C_TEST_ASSERT_EQ(step,  3, "Expected step.");
    C_TEST_ASSERT_EQ(n,     3, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");
}

void test_c_slice_compute_range_start()
{
    c_slice_t s = NULL;
    c_int64_t first, last, step, size = 10;
    c_uint64_t n = 0;

    s = c_slice_new_start(-1);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 9, "Expected first.");
    C_TEST_ASSERT_EQ(last,  9, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     1, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start(-2);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 8, "Expected first.");
    C_TEST_ASSERT_EQ(last,  9, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     2, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start(0);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  9, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     10, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start(2);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 2, "Expected first.");
    C_TEST_ASSERT_EQ(last,  9, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     8, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

}

void test_c_slice_compute_range_start_end()
{
    c_slice_t s = NULL;
    c_int64_t first, last, step, size = 10;
    c_uint64_t n = 0;

    s = c_slice_new_start_end(0, 0);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(n,     0, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_end(0, 10);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  9, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     10, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_end(2, 5);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 2, "Expected first.");
    C_TEST_ASSERT_EQ(last,  4, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     3, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_end(-2, 5);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(n,     0, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_end(-2, -5);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(n,     0, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_end(-5, -5);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(n,     0, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_end(-5, 2);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(n,     0, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_end(-5, -2);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 5, "Expected first.");
    C_TEST_ASSERT_EQ(last,  7, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     3, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");
}

void test_c_slice_compute_range_start_end_stride()
{
    c_slice_t s = NULL;
    c_int64_t first, last, step, size = 10;
    c_uint64_t n = 0;

    s = c_slice_new_start_end_stride(0, 0, 0);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(n,     0, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_end_stride(0, 10, 1);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  9, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     10, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_end_stride(0, 100, 1);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  9, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     10, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_end_stride(-10, 10, 1);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  9, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     10, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_end_stride(2, 5, 1);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 2, "Expected first.");
    C_TEST_ASSERT_EQ(last,  4, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     3, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_end_stride(2, 5, 2);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 2, "Expected first.");
    C_TEST_ASSERT_EQ(last,  4, "Expected last.");
    C_TEST_ASSERT_EQ(step,  2, "Expected step.");
    C_TEST_ASSERT_EQ(n,     2, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_end_stride(-2, 5, 1);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(n,     0, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_end_stride(-2, -5, 1);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(n,     0, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_end_stride(-5, -5, 1);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(n,     0, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_end_stride(-5, -5, -1);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(n,     0, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_end_stride(-5, 2, 1);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(n,     0, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_end_stride(-2, -6, -1);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 8, "Expected first.");
    C_TEST_ASSERT_EQ(last,  5, "Expected last.");
    C_TEST_ASSERT_EQ(step,  -1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     4, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_end_stride(-5, -2, 1);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 5, "Expected first.");
    C_TEST_ASSERT_EQ(last,  7, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     3, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_end_stride(-2, -5, -2);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 8, "Expected first.");
    C_TEST_ASSERT_EQ(last,  6, "Expected last.");
    C_TEST_ASSERT_EQ(step,  -2, "Expected step.");
    C_TEST_ASSERT_EQ(n,     2, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_end_stride(-2, -8, -3);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 8, "Expected first.");
    C_TEST_ASSERT_EQ(last,  5, "Expected last.");
    C_TEST_ASSERT_EQ(step,  -3, "Expected step.");
    C_TEST_ASSERT_EQ(n,     2, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_end_stride(-2, -9, -3);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 8, "Expected first.");
    C_TEST_ASSERT_EQ(last,  2, "Expected last.");
    C_TEST_ASSERT_EQ(step,  -3, "Expected step.");
    C_TEST_ASSERT_EQ(n,     3, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");
}

void test_c_slice_compute_range_start_stride()
{
    c_slice_t s = NULL;
    c_int64_t first, last, step, size = 10;
    c_uint64_t n = 0;

    s = c_slice_new_start_stride(0, 0);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(n,     0, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_stride(1, 0);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(n,     0, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_stride(-1, 0);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(n,     0, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_stride(0, 1);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  9, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     10, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_stride(0, 2);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  8, "Expected last.");
    C_TEST_ASSERT_EQ(step,  2, "Expected step.");
    C_TEST_ASSERT_EQ(n,     5, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_stride(0, -1);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  0, "Expected last.");
    C_TEST_ASSERT_EQ(step,  -1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     1, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_stride(0, -2);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  0, "Expected last.");
    C_TEST_ASSERT_EQ(step,  -2, "Expected step.");
    C_TEST_ASSERT_EQ(n,     1, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_stride(1, 1);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 1, "Expected first.");
    C_TEST_ASSERT_EQ(last,  9, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     9, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_stride(1, 2);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 1, "Expected first.");
    C_TEST_ASSERT_EQ(last,  9, "Expected last.");
    C_TEST_ASSERT_EQ(step,  2, "Expected step.");
    C_TEST_ASSERT_EQ(n,     5, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_stride(1, -1);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 1, "Expected first.");
    C_TEST_ASSERT_EQ(last,  0, "Expected last.");
    C_TEST_ASSERT_EQ(step,  -1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     2, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_stride(5, -2);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 5, "Expected first.");
    C_TEST_ASSERT_EQ(last,  1, "Expected last.");
    C_TEST_ASSERT_EQ(step,  -2, "Expected step.");
    C_TEST_ASSERT_EQ(n,     3, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_stride(-1, -1);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 9, "Expected first.");
    C_TEST_ASSERT_EQ(last,  0, "Expected last.");
    C_TEST_ASSERT_EQ(step,  -1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     10, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_stride(-1, -2);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 9, "Expected first.");
    C_TEST_ASSERT_EQ(last,  1, "Expected last.");
    C_TEST_ASSERT_EQ(step,  -2, "Expected step.");
    C_TEST_ASSERT_EQ(n,     5, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_stride(-5, -2);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 5, "Expected first.");
    C_TEST_ASSERT_EQ(last,  1, "Expected last.");
    C_TEST_ASSERT_EQ(step,  -2, "Expected step.");
    C_TEST_ASSERT_EQ(n,     3, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");
}

void test_c_slice_compute_range_stride()
{
    c_slice_t s = NULL;
    c_int64_t first, last, step, size = 10;
    c_uint64_t n = 0;

    s = c_slice_new_stride(0);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(n,     0, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_stride(1);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  9, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     10, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_stride(2);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  8, "Expected last.");
    C_TEST_ASSERT_EQ(step,  2, "Expected step.");
    C_TEST_ASSERT_EQ(n,     5, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_stride(3);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  9, "Expected last.");
    C_TEST_ASSERT_EQ(step,  3, "Expected step.");
    C_TEST_ASSERT_EQ(n,     4, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_stride(-1);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 9, "Expected first.");
    C_TEST_ASSERT_EQ(last,  0, "Expected last.");
    C_TEST_ASSERT_EQ(step,  -1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     10, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_stride(-2);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 9, "Expected first.");
    C_TEST_ASSERT_EQ(last,  1, "Expected last.");
    C_TEST_ASSERT_EQ(step,  -2, "Expected step.");
    C_TEST_ASSERT_EQ(n,     5, "Expected n.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

}

void test_c_slice_copy()
{
    c_slice_t slice = NULL, copy = NULL;

    copy = c_slice_copy(slice);
    C_TEST_ASSERT(copy, "Failed copy of null.");
    c_slice_free(& copy);
    C_TEST_ASSERT_EQ_PTR(NULL, copy, "Failed to free copy.");

    slice = c_slice_new_start_end_stride(-1, 1, 3);
    copy = c_slice_copy(slice);
    C_TEST_ASSERT_EQ(-1, c_slice_start(copy), "Failed copy start.");
    C_TEST_ASSERT_EQ(1, c_slice_end(copy), "Failed copy end.");
    C_TEST_ASSERT_EQ(3, c_slice_stride(copy), "Failed copy stride.");
    c_slice_free(& slice);
    C_TEST_ASSERT_EQ_PTR(NULL, slice, "Failed to free slice.");
    c_slice_free(& copy);
    C_TEST_ASSERT_EQ_PTR(NULL, copy, "Failed to free copy.");
}

void test_c_slice_from_chars_empty()
{
    c_slice_t s = NULL;
    c_bool_t closed = C_FALSE;
    c_uint64_t offset = 0;
    c_int64_t first = -1, last = -1, step = 0, size = 10;
    c_uint64_t n = 0;
    const char *str;

    str = "";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    C_TEST_ASSERT(s, "Expected non null.");
    c_slice_free(& s);

    str = "junk";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    C_TEST_ASSERT(s, "Expected non null.");
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(n,     0, "Expected n.");
    c_slice_free(& s);

    str = ":";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  9, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     10, "Expected n.");
    c_slice_free(& s);

    str = ",";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  9, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     10, "Expected n.");
    c_slice_free(& s);

    str = "::";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  9, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     10, "Expected n.");
    c_slice_free(& s);

    str = ",,";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  9, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     10, "Expected n.");
    c_slice_free(& s);

    str = ":1";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  0, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     1, "Expected n.");
    c_slice_free(& s);
}

void test_c_slice_from_chars_end()
{
    c_slice_t s = NULL;
    c_bool_t closed = C_FALSE;
    c_uint64_t offset = 0;
    c_int64_t first = -1, last = -1, step = 0, size = 10;
    c_uint64_t n = 0;
    const char *str;

    str = ":1";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  0, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     1, "Expected n.");
    c_slice_free(& s);
}

void test_c_slice_from_chars_start()
{
    c_slice_t s = NULL;
    c_bool_t closed = C_FALSE;
    c_uint64_t offset = 0;
    c_int64_t first = -1, last = -1, step = 0, size = 10;
    c_uint64_t n = 0;
    const char *str;

    str = "0";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  0, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     1, "Expected n.");
    c_slice_free(& s);

    str = "-1";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 9, "Expected first.");
    C_TEST_ASSERT_EQ(last,  9, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     1, "Expected n.");
    c_slice_free(& s);

    str = "1";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 1, "Expected first.");
    C_TEST_ASSERT_EQ(last,  1, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     1, "Expected n.");
    c_slice_free(& s);

    str = "1:";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 1, "Expected first.");
    C_TEST_ASSERT_EQ(last,  9, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     9, "Expected n.");
    c_slice_free(& s);

    str = "5:";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 5, "Expected first.");
    C_TEST_ASSERT_EQ(last,  9, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     5, "Expected n.");
    c_slice_free(& s);

    str = "0::2";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  8, "Expected last.");
    C_TEST_ASSERT_EQ(step,  2, "Expected step.");
    C_TEST_ASSERT_EQ(n,     5, "Expected n.");
    c_slice_free(& s);

    str = "5::2";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 5, "Expected first.");
    C_TEST_ASSERT_EQ(last,  9, "Expected last.");
    C_TEST_ASSERT_EQ(step,  2, "Expected step.");
    C_TEST_ASSERT_EQ(n,     3, "Expected n.");
    c_slice_free(& s);

    str = "0::-1";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  0, "Expected last.");
    C_TEST_ASSERT_EQ(step,  -1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     1, "Expected n.");
    c_slice_free(& s);

    str = "5::-1";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 5, "Expected first.");
    C_TEST_ASSERT_EQ(last,  0, "Expected last.");
    C_TEST_ASSERT_EQ(step,  -1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     6, "Expected n.");
    c_slice_free(& s);

    str = "-1::";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 9, "Expected first.");
    C_TEST_ASSERT_EQ(last,  9, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     1, "Expected n.");
    c_slice_free(& s);

    str = "-1::1";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 9, "Expected first.");
    C_TEST_ASSERT_EQ(last,  9, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     1, "Expected n.");
    c_slice_free(& s);

    str = "-1::-1";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 9, "Expected first.");
    C_TEST_ASSERT_EQ(last,  0, "Expected last.");
    C_TEST_ASSERT_EQ(step,  -1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     10, "Expected n.");
    c_slice_free(& s);

    str = "-1::-2";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 9, "Expected first.");
    C_TEST_ASSERT_EQ(last,  1, "Expected last.");
    C_TEST_ASSERT_EQ(step,  -2, "Expected step.");
    C_TEST_ASSERT_EQ(n,     5, "Expected n.");
    c_slice_free(& s);

    str = "1::2";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 1, "Expected first.");
    C_TEST_ASSERT_EQ(last,  9, "Expected last.");
    C_TEST_ASSERT_EQ(step,  2, "Expected step.");
    C_TEST_ASSERT_EQ(n,     5, "Expected n.");
    c_slice_free(& s);

    str = "-9223372036854775808::-9223372036854775808";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(n,     0, "Expected n.");
    c_slice_free(& s);

    str = "9223372036854775807::9223372036854775807";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(n,     0, "Expected n.");
    c_slice_free(& s);
}

void test_c_slice_from_chars_start_end_stride()
{
    c_slice_t s = NULL;
    c_bool_t closed = C_FALSE;
    c_uint64_t offset = 0;
    c_int64_t first = -1, last = -1, step = 0, size = 10;
    c_uint64_t n = 0;
    const char *str;

    str = "0:0:1";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(n,     0, "Expected n.");
    c_slice_free(& s);

    str = "0:123456:1";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  9, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     10, "Expected n.");
    c_slice_free(& s);

    str = "0:123456:2";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  8, "Expected last.");
    C_TEST_ASSERT_EQ(step,  2, "Expected step.");
    C_TEST_ASSERT_EQ(n,     5, "Expected n.");
    c_slice_free(& s);

    str = "-5:123456:2";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 5, "Expected first.");
    C_TEST_ASSERT_EQ(last,  9, "Expected last.");
    C_TEST_ASSERT_EQ(step,  2, "Expected step.");
    C_TEST_ASSERT_EQ(n,     3, "Expected n.");
    c_slice_free(& s);

    str = "-522:123456:2";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  8, "Expected last.");
    C_TEST_ASSERT_EQ(step,  2, "Expected step.");
    C_TEST_ASSERT_EQ(n,     5, "Expected n.");
    c_slice_free(& s);

    str = "-522:123456:-2";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(n,     0, "Expected n.");
    c_slice_free(& s);

    str = "-522:123456:1111";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  0, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1111, "Expected step.");
    C_TEST_ASSERT_EQ(n,     1, "Expected n.");
    c_slice_free(& s);

    str = "-9223372036854775808:-9223372036854775808:-9223372036854775808";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(n,     0, "Expected n.");
    c_slice_free(& s);

    str = "-9223372036854775808:9223372036854775807:9223372036854775807";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  0, "Expected last.");
    C_TEST_ASSERT_EQ(step,  9223372036854775807, "Expected step.");
    C_TEST_ASSERT_EQ(n,     1, "Expected n.");
    c_slice_free(& s);

    str = "0:9:1";
    offset = 1;
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 1, "Expected first.");
    C_TEST_ASSERT_EQ(last,  9, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     9, "Expected n.");
    c_slice_free(& s);

    str = "0:9:1";
    offset = 1;
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 1, "Expected first.");
    C_TEST_ASSERT_EQ(last,  9, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     9, "Expected n.");
    c_slice_free(& s);

    str = "-5:-10:-1";
    offset = 0;
    closed = C_FALSE;
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 5, "Expected first.");
    C_TEST_ASSERT_EQ(last,  1, "Expected last.");
    C_TEST_ASSERT_EQ(step,  -1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     5, "Expected n.");
    c_slice_free(& s);

    str = "0:9:1";
    offset = 0;
    closed = C_TRUE;
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  9, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     10, "Expected n.");
    c_slice_free(& s);

    str = "-5:-10:-1";
    offset = 0;
    closed = C_TRUE;
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 5, "Expected first.");
    C_TEST_ASSERT_EQ(last,  0, "Expected last.");
    C_TEST_ASSERT_EQ(step,  -1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     6, "Expected n.");
    c_slice_free(& s);
}

void test_c_slice_from_chars_stride()
{
    c_slice_t s = NULL;
    c_bool_t closed = C_FALSE;
    c_uint64_t offset = 0;
    c_int64_t first = -1, last = -1, step = 0, size = 10;
    c_uint64_t n = 0;
    const char *str;

    str = "::1";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  9, "Expected last.");
    C_TEST_ASSERT_EQ(step,  1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     10, "Expected n.");
    c_slice_free(& s);

    str = "::0";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(step,  0, "Expected step.");
    C_TEST_ASSERT_EQ(n,     0, "Expected n.");
    c_slice_free(& s);

    str = "::-1";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 9, "Expected first.");
    C_TEST_ASSERT_EQ(last,  0, "Expected last.");
    C_TEST_ASSERT_EQ(step,  -1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     10, "Expected n.");
    c_slice_free(& s);

    str = "::2";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 0, "Expected first.");
    C_TEST_ASSERT_EQ(last,  8, "Expected last.");
    C_TEST_ASSERT_EQ(step,  2, "Expected step.");
    C_TEST_ASSERT_EQ(n,     5, "Expected n.");
    c_slice_free(& s);

    str = "::-2";
    s = c_slice_from_chars(str, strlen(str), offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 9, "Expected first.");
    C_TEST_ASSERT_EQ(last,  1, "Expected last.");
    C_TEST_ASSERT_EQ(step,  -2, "Expected step.");
    C_TEST_ASSERT_EQ(n,     5, "Expected n.");
    c_slice_free(& s);
}

void test_c_slice_from_str()
{
    c_slice_t s = NULL;
    c_bool_t closed = C_FALSE;
    c_uint64_t offset = 0;
    c_int64_t first = -1, last = -1, step = 0, size = 10;
    c_uint64_t n = 0;
    c_str_t str;

    s = c_slice_from_str(NULL, offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(n,     0, "Expected n.");
    c_slice_free(& s);

    str = c_str_new("", 0);
    s = c_slice_from_str(str, offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(n,     0, "Expected n.");
    c_slice_free(& s);
    c_str_free(& str);

    str = c_str_new("-1::-1", 6);
    s = c_slice_from_str(str, offset, closed);
    c_slice_compute_range(s, size, & first, & last, & step, & n);
    C_TEST_ASSERT_EQ(first, 9, "Expected first.");
    C_TEST_ASSERT_EQ(last,  0, "Expected last.");
    C_TEST_ASSERT_EQ(step,  -1, "Expected step.");
    C_TEST_ASSERT_EQ(n,     10, "Expected n.");
    c_slice_free(& s);
    c_str_free(& str);
}

void test_c_slice_new()
{
    c_slice_t slice = NULL;

    slice = c_slice_new_start(-1);
    C_TEST_ASSERT(slice, "Failed to create slice.");
    c_slice_free(& slice);

    slice = c_slice_new_start_end(-1, -1);
    C_TEST_ASSERT(slice, "Failed to create slice.");
    c_slice_free(& slice);

    slice = c_slice_new_start_end_stride(-1, -1, -1);
    C_TEST_ASSERT(slice, "Failed to create slice.");
    c_slice_free(& slice);

    slice = c_slice_new_start_stride(-1, -1);
    C_TEST_ASSERT(slice, "Failed to create slice.");
    c_slice_free(& slice);

    slice = c_slice_new_stride(-1);
    C_TEST_ASSERT(slice, "Failed to create slice.");
    c_slice_free(& slice);

    slice = c_slice_new_end(-1);
    C_TEST_ASSERT(slice, "Failed to create slice.");
    c_slice_free(& slice);

    slice = c_slice_new_end_stride(-1, -1);
    C_TEST_ASSERT(slice, "Failed to create slice.");
    c_slice_free(& slice);

    C_TEST_ASSERT_FALSE(slice, "Failed to free slice.");
}

void test_c_slice_start_end_stride()
{
    C_TEST_ASSERT_EQ(c_slice_start(NULL),  0, "Expected zero.");
    C_TEST_ASSERT_EQ(c_slice_end(NULL),    0, "Expected zero.");
    C_TEST_ASSERT_EQ(c_slice_stride(NULL), 0, "Expected zero.");
}

void test_c_slice_to_str()
{
    c_slice_t s = NULL;
    c_str_t t = NULL;

    t = c_slice_to_str(s);
    C_TEST_ASSERT(t, "Failed to get str for null slice.");
    c_str_free(& t);
    C_TEST_ASSERT_EQ_PTR(NULL, t, "Failed to free str.");

    s = c_slice_new_start_end_stride(LLONG_MIN, LLONG_MIN, LLONG_MIN);
    t = c_slice_to_str(s);
    C_TEST_ASSERT(c_str_data(t), "Expected non null.");
    C_TEST_ASSERT_EQ_STR(c_str_data(t), "-9223372036854775808:-9223372036854775808:-9223372036854775808", "Expected string.");
    c_slice_free(& s);
    c_str_free(& t);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");
    C_TEST_ASSERT_EQ_PTR(NULL, t, "Failed to free str.");
}

void test_c_slice_to_chars_end()
{
    c_slice_t s = NULL;
    const c_int64_t n = 64;
    char t[n];

    t[0] = '\0';

    s = c_slice_new_end(0);
    c_slice_to_chars(s, t, n);
    C_TEST_ASSERT_EQ_STR(t, ":0", "Expected string.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_end(LLONG_MIN);
    c_slice_to_chars(s, t, n);
    C_TEST_ASSERT_EQ_STR(t, ":-9223372036854775808", "Expected string.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_end(LLONG_MAX);
    c_slice_to_chars(s, t, n);
    C_TEST_ASSERT_EQ_STR(t, ":9223372036854775807", "Expected string.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_end_stride(0, 0);
    c_slice_to_chars(s, t, n);
    C_TEST_ASSERT_EQ_STR(t, ":0:0", "Expected string.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_end_stride(LLONG_MIN, LLONG_MIN);
    c_slice_to_chars(s, t, n);
    C_TEST_ASSERT_EQ_STR(t, ":-9223372036854775808:-9223372036854775808", "Expected string.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_end_stride(LLONG_MAX, LLONG_MAX);
    c_slice_to_chars(s, t, n);
    C_TEST_ASSERT_EQ_STR(t, ":9223372036854775807:9223372036854775807", "Expected string.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");
}

void test_c_slice_to_chars_start()
{
    c_slice_t s = NULL;
    const c_int64_t n = 64;
    char t[n];

    t[0] = '\0';

    s = c_slice_new_start(0);
    c_slice_to_chars(s, t, n);
    C_TEST_ASSERT_EQ_STR(t, ":", "Expected string.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start(1);
    c_slice_to_chars(s, t, n);
    C_TEST_ASSERT_EQ_STR(t, "1:", "Expected string.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start(LLONG_MAX);
    c_slice_to_chars(s, t, n);
    C_TEST_ASSERT_EQ_STR(t, "9223372036854775807:", "Expected string.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start(LLONG_MIN);
    c_slice_to_chars(s, t, n);
    C_TEST_ASSERT_EQ_STR(t, "-9223372036854775808:", "Expected string.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_stride(0, 0);
    c_slice_to_chars(s, t, n);
    C_TEST_ASSERT_EQ_STR(t, "0::0", "Expected string.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_stride(1, 1);
    c_slice_to_chars(s, t, n);
    C_TEST_ASSERT_EQ_STR(t, "1:", "Expected string.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_stride(LLONG_MAX, 2);
    c_slice_to_chars(s, t, n);
    C_TEST_ASSERT_EQ_STR(t, "9223372036854775807::2", "Expected string.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_stride(LLONG_MAX, LLONG_MAX);
    c_slice_to_chars(s, t, n);
    C_TEST_ASSERT_EQ_STR(t, "9223372036854775807::9223372036854775807", "Expected string.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_stride(LLONG_MIN, LLONG_MIN);
    c_slice_to_chars(s, t, n);
    C_TEST_ASSERT_EQ_STR(t, "-9223372036854775808::-9223372036854775808", "Expected string.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_end(0, 0);
    c_slice_to_chars(s, t, n);
    C_TEST_ASSERT_EQ_STR(t, "0:0", "Expected string.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_end(LLONG_MIN, LLONG_MIN);
    c_slice_to_chars(s, t, n);
    C_TEST_ASSERT_EQ_STR(t, "-9223372036854775808:-9223372036854775808", "Expected string.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_end(LLONG_MAX, LLONG_MAX);
    c_slice_to_chars(s, t, n);
    C_TEST_ASSERT_EQ_STR(t, "9223372036854775807:9223372036854775807", "Expected string.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    // TODO: start_end_stride
    s = c_slice_new_start_end_stride(0, 0, 0);
    c_slice_to_chars(s, t, n);
    C_TEST_ASSERT_EQ_STR(t, "0:0:0", "Expected string.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_end_stride(0, 0, 1);
    c_slice_to_chars(s, t, n);
    C_TEST_ASSERT_EQ_STR(t, "0:0", "Expected string.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_end_stride(LLONG_MIN, LLONG_MIN, LLONG_MIN);
    c_slice_to_chars(s, t, n);
    C_TEST_ASSERT_EQ_STR(t, "-9223372036854775808:-9223372036854775808:-9223372036854775808", "Expected string.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_start_end_stride(LLONG_MAX, LLONG_MAX, LLONG_MAX);
    c_slice_to_chars(s, t, n);
    C_TEST_ASSERT_EQ_STR(t, "9223372036854775807:9223372036854775807:9223372036854775807", "Expected string.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");
}

void test_c_slice_to_chars_stride()
{
    c_slice_t s = NULL;
    const c_int64_t n = 64;
    char t[n];

    t[0] = '\0';

    s = c_slice_new_stride(0);
    c_slice_to_chars(s, t, n);
    C_TEST_ASSERT_EQ_STR(t, "::0", "Expected string.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_stride(1);
    c_slice_to_chars(s, t, n);
    C_TEST_ASSERT_EQ_STR(t, ":", "Expected string.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_stride(-1);
    c_slice_to_chars(s, t, n);
    C_TEST_ASSERT_EQ_STR(t, "::-1", "Expected string.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_stride(LLONG_MAX);
    c_slice_to_chars(s, t, n);
    C_TEST_ASSERT_EQ_STR(t, "::9223372036854775807", "Expected string.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");

    s = c_slice_new_stride(LLONG_MIN);
    c_slice_to_chars(s, t, n);
    C_TEST_ASSERT_EQ_STR(t, "::-9223372036854775808", "Expected string.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");
}

void test_c_slice_to_vector()
{
    c_slice_t s = NULL;
    c_vector_uint64_t v = NULL;

    s = c_slice_new_start_end_stride(0, 0, 0);
    v = c_slice_to_vector(s, 10);
    C_TEST_ASSERT(v, "Expected non null.");
    C_TEST_ASSERT(c_vector_empty_uint64(v), "Expected empty.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");
    c_vector_free_uint64(& v);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free vector.");

    s = c_slice_new_start_end_stride(0, 10, 2);
    v = c_slice_to_vector(s, 10);
    C_TEST_ASSERT(v, "Expected non null.");
    C_TEST_ASSERT_EQ(c_vector_size_uint64(v), 5, "Expected size.");
    c_slice_free(& s);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free slice.");
    c_vector_free_uint64(& v);
    C_TEST_ASSERT_EQ_PTR(NULL, s, "Failed to free vector.");
}

int suite_test_c_slice()
{
    C_TEST_ADD_SUITE("suite_test_c_slice", NULL, NULL);

    C_TEST_ADD_TEST(test_c_slice_new);
    C_TEST_ADD_TEST(test_c_slice_start_end_stride);
    C_TEST_ADD_TEST(test_c_slice_copy);
    C_TEST_ADD_TEST(test_c_slice_compute_range_start);
    C_TEST_ADD_TEST(test_c_slice_compute_range_end);
    C_TEST_ADD_TEST(test_c_slice_compute_range_stride);
    C_TEST_ADD_TEST(test_c_slice_compute_range_start_end);
    C_TEST_ADD_TEST(test_c_slice_compute_range_start_stride);
    C_TEST_ADD_TEST(test_c_slice_compute_range_end_stride);
    C_TEST_ADD_TEST(test_c_slice_compute_range_start_end_stride);
    C_TEST_ADD_TEST(test_c_slice_to_chars_start);
    C_TEST_ADD_TEST(test_c_slice_to_chars_end);
    C_TEST_ADD_TEST(test_c_slice_to_chars_stride);
    C_TEST_ADD_TEST(test_c_slice_to_str);
    C_TEST_ADD_TEST(test_c_slice_to_vector);
    C_TEST_ADD_TEST(test_c_slice_from_chars_empty);
    C_TEST_ADD_TEST(test_c_slice_from_chars_end);
    C_TEST_ADD_TEST(test_c_slice_from_chars_start);
    C_TEST_ADD_TEST(test_c_slice_from_chars_stride);
    C_TEST_ADD_TEST(test_c_slice_from_chars_start_end_stride);
    C_TEST_ADD_TEST(test_c_slice_from_str);

    C_TEST_RETURN();
}

#endif
/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/