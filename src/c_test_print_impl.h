#ifndef C_TEST_PRINT_IMPL_INCLUDED
#define C_TEST_PRINT_IMPL_INCLUDED

#include "c_test_print.h"
#include "c_time.h"
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif

void c_test_print_header(const char * name, unsigned int nsuites, unsigned int ntests)
{
    C_TEST_PRINT_RAW3("C_Test Running %s Suites=%d Tests=%d\n", name, nsuites, ntests);
}

void c_test_print_start(
        unsigned int pct,
        const char  *name,
        unsigned int major_id,
        unsigned int minor_id)
{
    C_TEST_PRINT_PCT(pct);
    C_TEST_PRINT_INDENT();
    C_TEST_PRINT_RAW1(" %s", name);
    C_TEST_PRINT_RAW2(" Id=%d.%d Starting\n", major_id, minor_id);
}

void c_test_print_status(
        unsigned int pct,
        const char  *str1,
        const char  *str2,
        unsigned int time_nsec)
{
    C_TEST_PRINT_PCT(pct);
    C_TEST_PRINT_INDENT();
    C_TEST_PRINT_RAW(" ");
    C_TEST_PRINT_RAW2("%s %s", str1, str2);
    if (time_nsec) {
        C_TEST_PRINT_RAW(" Time=");
        c_test_print_time(time_nsec);
    }
    C_TEST_PRINT_RAW("\n");
}

void c_test_print_status_assert(
        unsigned int pct,
        const char  *id,
        c_bool_t     color,
        c_bool_t     passed,
        unsigned int time_nsec)
{
    C_TEST_PRINT_PCT(pct);

    if (passed) {
        C_TEST_PRINT_INDENT();
    } else {
        if (color) {
            C_TEST_PRINT_RAW(C_TEST_RED);
        }
        C_TEST_PRINT_RAW("FAIL");
        if (color) {
            C_TEST_PRINT_RAW(C_TEST_NOCOLOR);
        }
    }

    C_TEST_PRINT_RAW(" ");

    if (color) {
        if (passed) {
            C_TEST_PRINT_RAW(C_TEST_DGRAY);
        }
    }

    C_TEST_PRINT_RAW1("Assert Id=%s", id);

    if (time_nsec) {
        C_TEST_PRINT_RAW(" Time=");
        c_test_print_time(time_nsec);
    }

    if (color) {
        if (passed) {
            C_TEST_PRINT_RAW(C_TEST_NOCOLOR);
        }
    }

    C_TEST_PRINT_RAW("\n");
}

extern void c_test_print_time(c_uint64_t nsec)
{
    const int nbuf = 32;
    char buf[nbuf];
    c_time_nsec_str(buf, nbuf, nsec);
    C_TEST_PRINT_RAW1("%s", buf);
}

void c_test_print_total(
        unsigned int pct,
        c_bool_t     color,
        const char  *name,
        unsigned int ntests_fail,
        unsigned int ntests_pass,
        unsigned int nasserts_fail,
        unsigned int nasserts_pass,
        unsigned int time_nsec)
{
    char is_test = 0 == (ntests_fail + ntests_pass);
    char is_final = !is_test && 100 == pct;

    C_TEST_PRINT_PCT(pct);

    if (is_final) {
        C_TEST_PRINT_PASS_OR_FAIL(0 == ntests_fail, color);
    } else if (is_test) {
        C_TEST_PRINT_PASS_OR_FAIL(0 == nasserts_fail, color);
    } else /* is suite */ {
        C_TEST_PRINT_INDENT();
    }

    C_TEST_PRINT_RAW(" ");
    C_TEST_PRINT_RAW1("%s", name);
    if (name && strlen(name)) {
        C_TEST_PRINT_RAW(" ");
    }
    C_TEST_PRINT_RAW("Done");

    if (!is_test) {
        C_TEST_PRINT_RAW2(" Tests=%d/%d", ntests_pass, ntests_fail + ntests_pass);
    }

    C_TEST_PRINT_RAW2(" Asserts=%d/%d Time=", nasserts_pass, nasserts_fail + nasserts_pass);
    c_test_print_time(time_nsec);
    C_TEST_PRINT_RAW("\n");
}

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/