#!/bin/bash
set -e
src=/home/rsz/projects/c_/dev
bld=/tmp/c_/build
llvm=/usr/local/llvm-20160101/bin

export GCOV_PREFIX=$bld/gcov
export GCOV_PREFIX_STRIP=4

rm -rf $bld
mkdir -p $bld
cd $bld

echo Configure.
cmake \
    -G "Unix Makefiles" \
    -DCMAKE_BUILD_TYPE=Debug \
    -DCMAKE_C_COMPILER=$llvm/clang \
    -DCMAKE_C_FLAGS_DEBUG="-DNDEBUG -g3 -gdwarf-2 -fprofile-arcs -ftest-coverage" \
	-DCMAKE_EXE_LINKER_FLAGS="--coverage" \
    -DCMAKE_EXPORT_COMPILE_COMMANDS=ON \
    -DCMAKE_VERBOSE_MAKEFILE=ON \
    $src

echo Build.
make

echo Unit Test.
ctest

echo Memory Test.
$src/scripts/valgrind.sh ./test/test_c_all --color

echo Coverage Test.
cd $GCOV_PREFIX
mkdir -p lcov
cd lcov
find $bld -name "*.gcno" -exec cp "{}" . \;
find $bld -name "*.gcda" -exec cp "{}" . \;
$llvm/llvm-cov gcov -f -b *.gcda

# Not working; use gcc instead.
# lcov -t test -o test.info -c -d . --gcov-tool $src/scripts/llvm-gcov.sh
# genhtml test.info -o clang-coverage
