#ifndef C_SLICE_IMPL_INCLUDED
#define C_SLICE_IMPL_INCLUDED

#include "c_slice_t.h"
#include "c_memory.h"
#include "c_numeric.h"
#include "c_vector_t.h"
#include <limits.h> // LLONG_M*
#include <math.h>   // ceil
#include <stdio.h>  // snprintf
#include <string.h> // strpbrk

#ifdef __cplusplus
extern "C" {
#endif

#define C_SLICE_MIN
void c_slice_compute_range(
        c_slice_t slice,
        c_int64_t size,
        c_int64_t *first,
        c_int64_t *last,
        c_int64_t *step,
        c_uint64_t *n_out)
{
    *first = *last = *step = *n_out = 0;
    c_int64_t n = 0;

    if (slice && slice->stride) {
        if (slice->stride > 0) {
            if ( slice->start < size &&
                    ( (!slice->has_start) ||
                      (!slice->has_end) ||
                      (slice->start < slice->end)
                    )
                ) {

                if (slice->has_start) {
                    if (0 > slice->start) {
                        *first = slice->start + size;
                    } else {
                        *first = slice->start;
                    }
                }

                if (slice->has_end) {
                    if (0 > slice->end) {
                        *last = slice->end + size;
                    } else {
                        *last = slice->end;
                    }
                } else {
                    *last = size;
                }

                *first = c_numeric_min_int64(size-1, c_numeric_max_int64(0, *first));
                *last = c_numeric_min_int64(size, *last) - 1;
                n = ceil( (*last - *first + 1.0) / slice->stride );

                if (n <= 0) {
                    n = *first = *last = *step = 0;
                } else {
                    *step = slice->stride;
                    *last = *first + *step * (n - 1); // Adjusts for stride.
                }
            }
        } else { // Negative stride.
            if ( !slice->has_start || !slice->has_end || slice->start > slice->end) {
                if (slice->has_start) {
                    if (0 > slice->start) {
                        *first = slice->start + size;
                    } else {
                        *first = slice->start;
                    }
                } else {
                    *first = LLONG_MAX;
                }

                if (slice->has_end) {
                    if (0 > slice->end) {
                        *last = slice->end + size + 1;
                    } else {
                        *last = slice->end + 1;
                    }
                }

                *first = c_numeric_min_int64(size-1, c_numeric_max_int64(0,      *first));
                *last  = c_numeric_max_int64(0,      c_numeric_min_int64(size-1, *last));
                n = ceil( (*first - *last + 1.0) / -slice->stride );

                if (n <= 0) {
                    n = *first = *last = *step = 0;
                } else {
                    *step = slice->stride;
                    *last = *first + *step * (n - 1); // Adjusts for stride.
                }
            }
        }
    }

    *n_out = n;
}

c_slice_t c_slice_copy(c_slice_t slice)
{
    c_slice_t copy;
    C_MEMORY_NEW(copy);

    if (slice) {
        copy->start = slice->start;
        copy->has_start = slice->has_start;
        copy->end = slice->end;
        copy->has_end = slice->has_end;
        copy->stride = slice->stride;
        copy->has_stride = slice->has_stride;
    } else {
        copy->start = copy->end = copy->stride = 0;
        copy->has_start = copy->has_end = copy->has_stride = C_FALSE;
    }

    return copy;
}

c_int64_t c_slice_end(c_slice_t slice)
{
    if (slice) {
        return slice->end;
    }
    return 0;
}

void c_slice_free(c_slice_t *slice)
{
    assert(slice);
    assert(*slice);
    C_MEMORY_FREE(*slice);
}

c_slice_t c_slice_from_chars(
        const char *chars,
        c_uint64_t n,
        c_int64_t offset,
        c_bool_t closed)
{
    c_slice_t result = NULL;
    c_int64_t start = 0, end = 0, stride = 1;
    c_bool_t has_start = C_FALSE;
    c_bool_t has_end = C_FALSE;
    const char *next = NULL;
    const char *prev = chars;
    const char *DELIMS = ":,";
    enum c_slice_parse_state_t { START, END, STRIDE };
    c_int8_t state = START;

    if (!n) {
        goto return_empty;
    }

    next = strpbrk(chars, DELIMS);
    while (NULL != next)
    {
        if (START == state) {
            if ((next - prev) > 0) {
                if (sscanf(prev, "%lld", & start) > 0) {
                    has_start = C_TRUE;
                }
            }
            state = END;
        } else if (END == state) {
            if ((next - prev) > 0) {
                if (sscanf(prev, "%lld", & end) > 0) {
                    has_end = C_TRUE;
                }
            }
            state = STRIDE;
        } // else: Will never process stride state in loop.

        prev = next + 1; // Consume delimiter.
        next = strpbrk(prev, DELIMS);
    }

    if (START == state && prev) {
        if (sscanf(prev, "%lld", & start) > 0) {
            // No delimiter. Only value, hence index operation and not slicing.
            has_start = C_TRUE;
            has_end = C_TRUE;
            if (-1 == start) {
                end = LLONG_MAX;
            } else {
                end = start + 1;
            }
        } else {
            goto return_empty;
        }
    } else if (END == state && prev) {
        if (sscanf(prev, "%lld", & end) > 0) {
            has_end = C_TRUE;
        }
    } else if (STRIDE == state && prev) {
        sscanf(prev, "%lld", & stride);
    }

    if (closed && has_end) {
        if (end > 0) {
            end += 1;
        } else {
            end -= 1;
        }
    }

    if (has_start) {
        if (has_end) {
            result = c_slice_new_start_end_stride(start + offset,
                                                  end + offset,
                                                  stride);
        } else {
            result = c_slice_new_start_stride(start + offset, stride);
        }
    } else {
        if (has_end) {
            result = c_slice_new_end_stride(end + offset, stride);
        } else {
            result = c_slice_new_stride(stride);
        }
    }

    return result;

return_empty:
    return c_slice_new_start_end_stride(0, 0, 0);
}

c_slice_t c_slice_from_str(
        const c_str_t str,
        c_int64_t offset,
        c_bool_t closed)
{
    c_slice_t result = NULL;
    c_uint64_t n;

    if (str) {
        n = c_str_len(str);
        if (0 < n) {
            result = c_slice_from_chars(c_str_data(str), n, offset, closed);
        }
    }

    if (NULL == result) {
        result = c_slice_new_start_end_stride(0, 0, 0);
    }

    return result;
}

c_slice_t c_slice_new_start(c_int64_t start)
{
    c_slice_t slice;
    C_MEMORY_NEW(slice);
    slice->start      = start;
    slice->has_start  = C_TRUE;
    slice->end        = 0;
    slice->has_end    = C_FALSE;
    slice->stride     = 1;
    slice->has_stride = C_TRUE;

    return slice;
}

c_slice_t c_slice_new_start_end(c_int64_t start, c_int64_t end)
{
    c_slice_t slice;
    C_MEMORY_NEW(slice);
    slice->start      = start;
    slice->has_start  = C_TRUE;
    slice->end        = end;
    slice->has_end    = C_TRUE;
    slice->stride     = 1;
    slice->has_stride = C_TRUE;

    return slice;
}

c_slice_t c_slice_new_start_end_stride(
                c_int64_t start,
                c_int64_t end,
                c_int64_t stride)
{
    c_slice_t slice;
    C_MEMORY_NEW(slice);
    slice->start      = start;
    slice->has_start  = C_TRUE;
    slice->end        = end;
    slice->has_end    = C_TRUE;
    slice->stride     = stride;
    slice->has_stride = C_TRUE;

    return slice;
}

c_slice_t c_slice_new_start_stride(c_int64_t start, c_int64_t stride)
{
    c_slice_t slice;
    C_MEMORY_NEW(slice);
    slice->start      = start;
    slice->has_start  = C_TRUE;
    slice->end        = 0;
    slice->has_end    = C_FALSE;
    slice->stride     = stride;
    slice->has_stride = C_TRUE;

    return slice;
}

c_slice_t c_slice_new_stride(c_int64_t stride)
{
    c_slice_t slice;
    C_MEMORY_NEW(slice);
    slice->start      = 0;
    slice->has_start  = C_FALSE;
    slice->end        = 0;
    slice->has_end    = C_FALSE;
    slice->stride     = stride;
    slice->has_stride = C_TRUE;

    return slice;
}

c_slice_t c_slice_new_end(c_int64_t end)
{
    c_slice_t slice;
    C_MEMORY_NEW(slice);
    slice->start      = 0;
    slice->has_start  = C_FALSE;
    slice->end        = end;
    slice->has_end    = C_TRUE;
    slice->stride     = 1;
    slice->has_stride = C_TRUE;

    return slice;
}

c_slice_t c_slice_new_end_stride(c_int64_t end, c_int64_t stride)
{
    c_slice_t slice;
    C_MEMORY_NEW(slice);
    slice->start      = 0;
    slice->has_start  = C_FALSE;
    slice->end        = end;
    slice->has_end    = C_TRUE;
    slice->stride     = stride;
    slice->has_stride = C_TRUE;

    return slice;
}

c_int64_t c_slice_start(c_slice_t slice)
{
    if (slice) {
        return slice->start;
    }
    return 0;
}

c_int64_t c_slice_stride(c_slice_t slice)
{
    if (slice) {
        return slice->stride;
    }
    return 0;
}

c_str_t c_slice_to_str(c_slice_t slice)
{
    const int nchars = 64; // Long enough for "-<int64>:-<int64>:-<int64>".
    c_str_t str = c_str_new_clear(nchars);
    char *data = c_str_data(str);

    c_slice_to_chars(slice, data, nchars);
    c_str_compute_size(str);

    return str;
}

void c_slice_to_chars(c_slice_t slice, char *str, c_uint64_t n)
{
    if (!slice) {
        return;
    }

    if (slice->has_start) {
        if (slice->has_end) {
            if (slice->has_stride && 1 != slice->stride) {
                snprintf(str, n, "%lld:%lld:%lld",
                        slice->start,
                        slice->end,
                        slice->stride);
            } else {
                snprintf(str, n, "%lld:%lld",
                        slice->start,
                        slice->end);
            }
        } else {
            if (slice->has_stride && 1 != slice->stride) {
                snprintf(str, n, "%lld::%lld",
                        slice->start,
                        slice->stride);
            } else {
                if (slice->start) {
                    snprintf(str, n, "%lld:",
                            slice->start);
                } else {
                    snprintf(str, n, ":");
                }
            }
        }
    } else if (slice->has_end) {
        if (slice->has_stride && 1 != slice->stride) {
            snprintf(str, n, ":%lld:%lld",
                    slice->end,
                    slice->stride);
        } else {
            snprintf(str, n, ":%lld",
                    slice->end);
        }
    } else if (slice->has_stride) {
        if (1 != slice->stride) {
            snprintf(str, n, "::%lld",
                    slice->stride);
        } else {
            snprintf(str, n, ":");
        }
    } else {
        snprintf(str, n, ":");
    }
}

c_vector_uint64_t c_slice_to_vector(c_slice_t slice, c_uint64_t size)
{
    c_int64_t first = 0, last = 0, step = 0;
    c_uint64_t n = 0;
    c_vector_uint64_t vector = NULL;

    c_slice_compute_range(slice, size, & first, & last, & step, & n);

    if (n) {
        vector = c_vector_new_uint64(n);

        if (step > 0) {
            for(; first <= last; first += step) {
                c_vector_append_uint64(vector, first);
            }
        } else {
            for(; first >= last; first += step) {
                c_vector_append_uint64(vector, first);
            }
        }
    } else {
        vector = c_vector_new_uint64(0);
    }

    return vector;
}

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/
