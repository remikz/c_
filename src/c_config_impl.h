#ifndef C_CONFIG_IMPL_INCLUDED
#define C_CONFIG_IMPL_INCLUDED

#include "c_config.h"
#include "c_type.h"
#include <config.h>
#include <stdio.h>
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif


int c_config(int argc, const char **argv)
{
    int rcode = EXIT_SUCCESS;
    int i;

    #define C_CONFIG_CMP(S)                                                    \
        if (0 == strcmp(argv[i], "--" #S)) {                                   \
            c_config_##S();                                                    \
            continue;                                                          \
        }

    if (1 == argc) {
        rcode = EXIT_FAILURE;
    }

    for(i = 1; i < argc; ++i) {
        C_CONFIG_CMP(all);
        C_CONFIG_CMP(cc);
        C_CONFIG_CMP(cflags);
        C_CONFIG_CMP(cversion);
        C_CONFIG_CMP(date);
        C_CONFIG_CMP(help);
        C_CONFIG_CMP(host);
        C_CONFIG_CMP(includedir);
        C_CONFIG_CMP(libdir);
        C_CONFIG_CMP(libs);
        C_CONFIG_CMP(prefix);
        C_CONFIG_CMP(user);
        C_CONFIG_CMP(version);
        rcode = EXIT_FAILURE;
    }

    if (EXIT_FAILURE == rcode) {
        c_config_help();
    }

    #undef C_CONFIG_CMP

    return rcode;
}

void c_config_all()
{
    c_config_cc();
    c_config_cflags();
    c_config_cversion();
    c_config_date();
    c_config_host();
    c_config_includedir();
    c_config_libdir();
    c_config_libs();
    c_config_prefix();
    c_config_user();
    c_config_version();
}

void c_config_cc()
{
    fprintf(stdout, "--cc=" BUILD_CC "\n");
}

void c_config_cversion()
{
    fprintf(stdout, "--cversion=" BUILD_CVERSION "\n");
}

void c_config_cflags()
{
    fprintf(stdout, "--cflags=" BUILD_CFLAGS "\n");
}

void c_config_date()
{
    fprintf(stdout, "--date=" BUILD_DATE "\n");
}

void c_config_help()
{
    fprintf(stdout, "                                                          \
c_config - Build configuration information for the c_ library.\n               \
\n                                                                             \
Usage: c_config [OPTION]\n                                                     \
\n                                                                             \
Available values for OPTION include:\n                                         \
\n                                                                             \
  --all         Display all options\n                                          \
  --cc          Compiler\n                                                     \
  --cflags      Pre-processor and compiler flags\n                             \
  --cversion    Compiler version\n                                             \
  --date        Date when library was built\n                                  \
  --help        Display this help message and exit\n                           \
  --host        Host machine that built library\n                              \
  --includedir  Include directory\n                                            \
  --libdir      Library search directory\n                                     \
  --libs        Library linking information\n                                  \
  --prefix      Install prefix\n                                               \
  --user        User that built the library\n                                  \
  --version     Library version\n\n");
    c_config_license();
}

void c_config_host()
{
    fprintf(stdout, "--host=" BUILD_HOST "\n");
}

void c_config_includedir()
{
    fprintf(stdout, "--includedir=" BUILD_PREFIX "/include\n");
}

void c_config_libdir()
{
    fprintf(stdout, "--libdir=" BUILD_PREFIX "/lib\n");
}

void c_config_libs()
{
    fprintf(stdout, "--libs=" BUILD_LIBS "\n");
}

void c_config_license()
{
    fprintf(stdout, "c_  Copyright (C) 2016  Remik Ziemlinski\n                \
This program comes with ABSOLUTELY NO WARRANTY.\n                              \
This is free software, and you are welcome to redistribute it\n                \
under the conditions of the GPLv3 license.\n\n");
}

void c_config_prefix()
{
    fprintf(stdout, "--prefix=" BUILD_PREFIX "\n");
}

void c_config_user()
{
    fprintf(stdout, "--user=" BUILD_USER "\n");
}

void c_config_version()
{
    fprintf(stdout, "--version=" VERSION "\n");
}

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/