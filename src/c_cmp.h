#ifndef C_CMP_INCLUDED
#define C_CMP_INCLUDED

#include "c_type.h"

#ifdef __cplusplus
extern "C" {
#endif

/** Casts to typed pointers.
 * @return 0 if equal, <0 if a<b, >0 if a>b.
 */
#define C_CMP(NAME, TYPE)                                                      \
    extern c_int32_t c_cmp##NAME(const void *a, const void *b);

/** Casts to typed pointer pointers.
 * @return 0 if equal, <0 if a<b, >0 if a>b.
 */
#define C_CMP_PTR_REF(NAME, TYPE)                                                      \
    extern c_int32_t c_cmp_ptr_ref##NAME(const void *a, const void *b);

/** Unsafe check of typed pointers. No casting necessary.
 * @return 0 if equal, <0 if a<b, >0 if a>b.
 */
#define C_CMP_PTR(NAME, TYPE)                                                  \
    extern c_int32_t c_cmp_ptr##NAME(const TYPE a, const TYPE b);

/** Check if typed pointers are valid before dereferencing and comparing.
 *  No casting necessary.
 * @return 0 if equal, <0 if a<b, >0 if a>b.
 */
#define C_CMP_PTR_SAFE(NAME, TYPE)                                             \
    extern c_int32_t c_cmp_ptr_safe##NAME(const TYPE a, const TYPE b);

/** Check if pointers are valid before casting, dereferencing and comparing.
 * @return 0 if equal, <0 if a<b, >0 if a>b.
 */
#define C_CMP_SAFE(NAME, TYPE)                                                 \
    extern c_int32_t c_cmp_safe##NAME(const void *a, const void *b);

/** Compare concrete non-pointer value types.
 * @return 0 if equal, <0 if a<b, >0 if a>b.
 */
#define C_CMP_VALUE(NAME, TYPE)                                                \
    extern c_int32_t c_cmp_value##NAME(const TYPE a, const TYPE b);

C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(C_CMP)
C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(C_CMP_SAFE)
C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(C_CMP_PTR)
C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(C_CMP_PTR_REF)
C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(C_CMP_PTR_SAFE)
C_TYPE_TEMPLATE_NAME_TYPE_VALUE(C_CMP_VALUE)

#undef C_CMP
#undef C_CMP_PTR
#undef C_CMP_PTR_REF
#undef C_CMP_PTR_SAFE
#undef C_CMP_SAFE
#undef C_CMP_VALUE

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/
