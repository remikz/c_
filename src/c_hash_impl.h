#ifndef C_HASH_IMPL_INCLUDED
#define C_HASH_IMPL_INCLUDED

#include "c_hash.h"
#include "c_vector_t.h"
#include <string.h> // strnlen

#ifdef __cplusplus
extern "C" {
#endif

#define C_HASH_IDENTITY(NAME, TYPE)                                            \
c_hash_t c_hash_identity##NAME(const TYPE key)                                 \
{                                                                              \
    return (c_hash_t) key;                                                     \
}

#define C_HASH_SHIFT(NAME, TYPE)                                               \
c_hash_t c_hash_shift##NAME(TYPE key)                                          \
{                                                                              \
    return ((c_hash_t) key) >> 2;                                              \
}

c_hash_t c_hash_djb_chars(const c_char_t *chars)
{
    c_uint64_t hash = 5381;
    c_uint64_t n;

    if (chars) {
        n = strnlen(chars, 256);

        for(; n--; ++chars)
        {
            hash = ((hash << 5) + hash) + *chars;
        }
    }

    return hash;
}

c_hash_t c_hash_djb_str(const c_str_t str)
{
    c_uint64_t n = 0, hash = 5381;
    c_char_t *chars;

    if (str) {
        for(chars = str->items, n = str->size; n--; ++chars)
        {
            hash = ((hash << 5) + hash) + *chars;
        }
    }

    return hash;
}

c_uint32_t c_hash_murmur3_32_str(const c_str_t str)
{
    static const c_uint32_t c1 = 0xcc9e2d51;
    static const c_uint32_t c2 = 0x1b873593;
    static const c_uint32_t r1 = 15;
    static const c_uint32_t r2 = 13;
    static const c_uint32_t m = 5;
    static const c_uint32_t n = 0xe6546b64;
    c_uint32_t hash = 0; // seed

    #define ROT32(x, y) ( ((x) << (y)) | ((x) >> (32 - (y))) )

    if (!str) {
        return 0;
    }

    const int nblocks = str->size / 4;
    const c_uint32_t *blocks = (const c_uint32_t *) str->items;
    int i = 0;
    c_uint32_t k;
    for (; i < nblocks; ++i) {
        k = blocks[i];
        k *= c1;
        k = ROT32(k, r1);
        k *= c2;

        hash ^= k;
        hash = ROT32(hash, r2) * m + n;
    }

    const c_uint8_t *tail = (const c_uint8_t *) (str->items + nblocks * 4);
    c_uint32_t k1 = 0;

    switch (str->size & 3) {
        case 3:
            k1 ^= tail[2] << 16;
        case 2:
            k1 ^= tail[1] << 8;
        case 1:
            k1 ^= tail[0];

            k1 *= c1;
            k1 = ROT32(k1, r1);
            k1 *= c2;
            hash ^= k1;
    }

    hash ^= str->size;
    hash ^= (hash >> 16);
    hash *= 0x85ebca6b;
    hash ^= (hash >> 13);
    hash *= 0xc2b2ae35;
    hash ^= (hash >> 16);

#undef ROT32

    return hash;
}

c_hash_t c_hash_murmur64a_str(const c_str_t str)
{
    const c_uint64_t m = 0xc6a4a7935bd1e995;
    const c_uint32_t r = 47;
    c_hash_t hash, tmp;
    const c_uint64_t *data;
    const c_uint64_t *end;
    const unsigned char * data2;

    if (!str) {
        return 0;
    }

    hash = str->size * m;
    data = (const c_uint64_t *) str->items;
    end = data + (str->size / 8);

    while(data != end)
    {
        tmp = *data++;

        tmp *= m;
        tmp ^= tmp >> r;
        tmp *= m;

        hash ^= tmp;
        hash *= m;
    }

    data2 = (const unsigned char*) data;

    switch(str->size & 7)
    {
        case 7: hash ^= ((c_uint64_t)data2[6]) << 48;
        case 6: hash ^= ((c_uint64_t)data2[5]) << 40;
        case 5: hash ^= ((c_uint64_t)data2[4]) << 32;
        case 4: hash ^= ((c_uint64_t)data2[3]) << 24;
        case 3: hash ^= ((c_uint64_t)data2[2]) << 16;
        case 2: hash ^= ((c_uint64_t)data2[1]) << 8;
        case 1: hash ^= ((c_uint64_t)data2[0]);
                hash *= m;
    };

    hash ^= hash >> r;
    hash *= m;
    hash ^= hash >> r;

    return hash;
}

C_HASH_TEMPLATE_NAME_TYPE(C_HASH_IDENTITY)
C_HASH_TEMPLATE_NAME_TYPE_STR(C_HASH_IDENTITY)
C_HASH_TEMPLATE_NAME_TYPE(C_HASH_SHIFT)

#undef C_HASH_IDENTITY
#undef C_HASH_SHIFT

#ifdef __cplusplus
}
#endif

#endif


/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/


