#ifndef TEST_C_TEST_INCLUDED
#define TEST_C_TEST_INCLUDED

#include "c_test.h"

void test_c_test_assert()
{
    C_TEST_ASSERT(1, "Should not fail.");
}

void test_c_test_assert_not()
{
    C_TEST_ASSERT_FALSE(0, "Should not fail.");
}

void test_c_test_assert_near()
{
    C_TEST_ASSERT_NEAR(1.0, 1.1, .11, "Should not fail.");
}

void test_c_test_assert_eq()
{
    C_TEST_ASSERT_EQ(1, 1, "Should not fail.");
}

void test_c_test_assert_neq()
{
    C_TEST_ASSERT_NEQ(1, 2, "Should not fail.");
}

void test_c_test_fail()
{
    C_TEST_ASSERT(0, "Expected assert fail 1.");
    C_TEST_ASSERT(0, "Expected assert fail 2.");
}

int suite_test_c_test_asserts()
{
    C_TEST_ADD_SUITE("suite_test_c_test_asserts", NULL, NULL);
    C_TEST_ADD_TEST(test_c_test_assert);
    C_TEST_ADD_TEST(test_c_test_assert_not);
    C_TEST_ADD_TEST(test_c_test_assert_near);
    C_TEST_ADD_TEST(test_c_test_assert_eq);
    C_TEST_ADD_TEST(test_c_test_assert_neq);
    C_TEST_ADD_TEST_FAIL(test_c_test_fail);

recover:
    C_TEST_RETURN();
}

void test_c_test_suites()
{
    C_TEST_ASSERT(C_TEST_REGISTRY(), "Registry does not exist.");
    C_TEST_ASSERT(c_test_registry_num_suites(C_TEST_REGISTRY()) > 1, "Expected multiple suites.");
    C_TEST_ASSERT(c_test_registry_num_tests(C_TEST_REGISTRY()) > 4, "Expected multiple tests.");
}

void test_c_test_dummy()
{
    C_TEST_ASSERT(1, "Dummy always true.");
}

int suite_test_c_test_suites()
{
    C_TEST_ADD_SUITE("suite_test_c_test_suites_dummy", NULL, NULL);
    C_TEST_ADD_TEST(test_c_test_dummy);

    C_TEST_ADD_SUITE("suite_test_c_test_suites", NULL, NULL);
    C_TEST_ADD_TEST(test_c_test_suites);
recover:
    C_TEST_RETURN();
}

void test_c_test_setup()
{
}

void test_c_test_teardown()
{
}

int suite_test_c_test_suite_setup_teardown()
{
    C_TEST_ADD_SUITE("suite_test_c_test_suite_setup_teardown", test_c_test_setup, test_c_test_teardown);
    C_TEST_ADD_TEST(test_c_test_dummy);
recover:
    C_TEST_RETURN();
}

void test_c_test_files()
{
    char *name1 = "tmp.test_c_test_equal_files.1";
    FILE *f1 = fopen(name1, "w");
    char *name2 = "tmp.test_c_test_equal_files.2";
    FILE *f2 = fopen(name2, "w");
    char *name3 = "tmp.test_c_test_equal_files.3";
    FILE *f3 = fopen(name3, "w");

    C_TEST_ASSERT(f1, "Failed to create tmp file.");
    fprintf(f1, "hello\n");
    C_TEST_ASSERT(fclose(f1) == 0, "Failed to close tmp file.");

    C_TEST_ASSERT(f2, "Failed to create tmp file.");
    fprintf(f2, "world\n");
    C_TEST_ASSERT(fclose(f2) == 0, "Failed to close tmp file.");

    C_TEST_ASSERT(f3, "Failed to create tmp file.");
    C_TEST_ASSERT(fclose(f3) == 0, "Failed to close tmp file.");

    C_TEST_ASSERT_EQ_FILE(name1, name1, "Files not equal");

    C_TEST_ASSERT_FALSE(c_test_files_equal(NULL, NULL), "Missing file expected.");
    C_TEST_ASSERT_FALSE(c_test_files_equal(name1, NULL), "Missing file expected.");
    C_TEST_ASSERT_FALSE(c_test_files_equal(NULL, name1), "Missing file expected.");
    C_TEST_ASSERT_FALSE(c_test_files_equal("missing", "missing"), "Missing files expected.");
    C_TEST_ASSERT_FALSE(c_test_files_equal(name1, "missing"), "Missing file expected.");
    C_TEST_ASSERT_FALSE(c_test_files_equal(name1, name2), "Files expected not equal.");
    C_TEST_ASSERT_FALSE(c_test_files_equal(name2, name1), "Files expected not equal.");
    C_TEST_ASSERT_FALSE(c_test_files_equal(name1, name3), "Empty file expected not equal.");
    C_TEST_ASSERT_FALSE(c_test_files_equal(name3, name1), "Empty file expected not equal.");

    C_TEST_ASSERT(c_test_file_empty(NULL), "Expected empty.");
    C_TEST_ASSERT(c_test_file_empty("missing"), "Expected empty.");
    C_TEST_ASSERT_FALSE(c_test_file_empty(name1), "Expected false.");
    C_TEST_ASSERT(c_test_file_empty(name3), "Expected empty.");

    C_TEST_ASSERT_FALSE(c_test_file_exists(NULL), "Expected non existing.");
    C_TEST_ASSERT_FALSE(c_test_file_exists("missing"), "Expected non existing.");
    C_TEST_ASSERT(c_test_file_exists(name1), "Expected exists.");
}

void test_c_test_xml()
{
    char *name = "tmp.test_c_test_xml";

    c_test_xml(NULL, NULL);
    c_test_xml(C_TEST_REGISTRY(), NULL);
    c_test_xml(NULL, name);
    c_test_xml(C_TEST_REGISTRY(), "");

    c_test_xml(C_TEST_REGISTRY(), name);

    C_TEST_ASSERT_FALSE(c_test_file_empty(name), "Expected non empty.");
}

int suite_test_c_test_files()
{
    C_TEST_ADD_SUITE("suite_test_c_test_files", NULL, NULL);
    C_TEST_ADD_TEST(test_c_test_files);
    C_TEST_ADD_TEST(test_c_test_xml);
recover:
    C_TEST_RETURN();
}

void test_c_test_run_function_fail()
{
    const char *sname = "ignore-dummy-suite";
    const char *tname = "ignore-dummy-test";
    c_test_registry_t reg = c_test_registry_new("ignore-dummy-registry", 0);
    c_test_suite_t suite = c_test_suite_new(sname, NULL, NULL);
    c_test_test_t test = c_test_test_new(tname, NULL);

    C_TEST_ASSERT(reg, "ignore dummy");
    C_TEST_ASSERT(suite, "ignore dummy");
    C_TEST_ASSERT(test, "ignore dummy");

    c_test_add_suite(reg, sname, NULL, NULL);
    c_test_add_test(reg, tname, NULL);
    c_test_set_current_suite(reg, suite);
    c_test_set_current_test(suite, test);
    c_test_assert_pass(reg, 0, "ignore forced fail dummy");
    c_test_run_function(NULL, NULL, test);
    c_test_run_function(reg, suite, test);

    c_test_registry_free(& reg);
    c_test_suite_free(& suite);
    c_test_test_free(& test);
}

void test_c_test_add_test_null()
{
    c_test_add_test(NULL, NULL, NULL);
}

void test_c_test_exit_code_null()
{
    c_test_exit_code(NULL);
}

void test_c_test_registry_null()
{
    c_test_registry_num_suites(NULL);
    c_test_registry_num_tests(NULL);
    c_test_pct(NULL);
    c_test_run(NULL);
}

int suite_test_c_test_edge_cases()
{
    C_TEST_ADD_SUITE("suite_test_c_test_edge_cases", NULL, NULL);
    C_TEST_ADD_TEST(test_c_test_run_function_fail);
    C_TEST_ADD_TEST(test_c_test_add_test_null);
    C_TEST_ADD_TEST(test_c_test_exit_code_null);
    C_TEST_ADD_TEST(test_c_test_registry_null);
recover:
    C_TEST_RETURN();
}

#endif
/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/