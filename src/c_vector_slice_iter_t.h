#ifndef C_VECTOR_SLICE_ITER_T_INCLUDED
#define C_VECTOR_SLICE_ITER_T_INCLUDED

#include "c_vector_slice_iter.h"

#ifdef __cplusplus
extern "C" {
#endif

#define C_VECTOR_SLICE_ITER_T(NAME)                                            \
struct C_VECTOR_SLICE_ITER_TYPE(NAME) {                                        \
    /** First index in iteration. */                                           \
    c_int64_t first;                                                           \
    /** Last index in iteration. */                                            \
    c_int64_t last;                                                            \
    /** Index into vector items array. */                                      \
    c_int64_t position;                                                        \
    /** Step delta increment during iteration. */                              \
    c_int64_t step;                                                            \
    /** Vector timestamp when iterator was created to detect vector changes. */\
    c_uint64_t timestamp;                                                      \
    /** Item provider. */                                                      \
    C_VECTOR_TYPE(NAME) vector;                                                \
};

C_TYPE_TEMPLATE_NAME(C_VECTOR_SLICE_ITER_T)
#undef C_VECTOR_SLICE_ITER_T

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/
