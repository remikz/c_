#ifndef C_VECTOR_FACTORY_INCLUDED
#define C_VECTOR_FACTORY_INCLUDED

#include "c_vector.h"

#ifdef __cplusplus
extern "C" {
#endif

/** Generate new vector with half-open increasing or
 * decreasing integer step sequence [start, stop).
 * @return Vector of values or pointers to values.
 * @see c_vector_linspace()
 */
#define C_VECTOR_RANGE(NAME, TYPE)                                             \
    extern C_VECTOR_TYPE(NAME) c_vector_range##NAME(                           \
                c_int64_t start, c_int64_t stop, c_int64_t step);

C_TYPE_TEMPLATE_NAME_TYPE_VALUE(C_VECTOR_RANGE)
C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(C_VECTOR_RANGE)

#undef C_VECTOR_RANGE

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/