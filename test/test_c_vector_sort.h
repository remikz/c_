#ifndef TEST_C_VECTOR_SORT_INCLUDED
#define TEST_C_VECTOR_SORT_INCLUDED

#include "c_test.h"
#include "c_vector_sort.h"

void test_c_vector_sort_ptr()
{
    #define TEST(N, T)                                                         \
    C_VECTOR_TYPE(N) v##N = 0;                                                 \
    T v1##N;                                                                   \
    T v2##N;                                                                   \
    T v3##N;                                                                   \
    T v4##N;                                                                   \
                                                                               \
    C_MEMORY_NEW(v1##N);                                                       \
    C_MEMORY_NEW(v2##N);                                                       \
    C_MEMORY_NEW(v3##N);                                                       \
    C_MEMORY_NEW(v4##N);                                                       \
                                                                               \
    *v1##N = 1;                                                                \
    *v2##N = 2;                                                                \
    *v3##N = 3;                                                                \
    *v4##N = 4;                                                                \
                                                                               \
    c_vector_sort##N(v##N);                                                    \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    c_vector_append##N(v##N, v3##N);                                           \
    c_vector_append##N(v##N, v2##N);                                           \
    c_vector_append##N(v##N, v4##N);                                           \
    c_vector_append##N(v##N, v1##N);                                           \
    c_vector_sort##N(v##N);                                                    \
    C_TEST_ASSERT_EQ_PTR(c_vector_get##N(v##N, 0), v1##N, "Expected value " #N);\
    C_TEST_ASSERT_EQ_PTR(c_vector_get##N(v##N, 1), v2##N, "Expected value " #N);\
    C_TEST_ASSERT_EQ_PTR(c_vector_get##N(v##N, 2), v3##N, "Expected value " #N);\
    C_TEST_ASSERT_EQ_PTR(c_vector_get##N(v##N, 3), v4##N, "Expected value " #N);\
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    C_MEMORY_FREE(v1##N);                                                      \
    C_MEMORY_FREE(v2##N);                                                      \
    C_MEMORY_FREE(v3##N);                                                      \
    C_MEMORY_FREE(v4##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(TEST)
    #undef TEST
}

void test_c_vector_sort_str()
{
    c_vector_t v = 0;
    c_str_t v1 = c_str_new("s1", 2);
    c_str_t v2 = c_str_new("s2", 2);
    c_str_t v3 = c_str_new("s3", 2);
    c_str_t v4 = c_str_new("s4", 2);

    c_vector_sort_str(v);

    v = c_vector_new(0);
    c_vector_append(v, v4);
    c_vector_append(v, v3);
    c_vector_append(v, v1);
    c_vector_append(v, v2);

    c_vector_sort_str(v);
    C_TEST_ASSERT_EQ_PTR(c_vector_get(v, 0), v1, "Expected value");
    C_TEST_ASSERT_EQ_PTR(c_vector_get(v, 1), v2, "Expected value");
    C_TEST_ASSERT_EQ_PTR(c_vector_get(v, 2), v3, "Expected value");
    C_TEST_ASSERT_EQ_PTR(c_vector_get(v, 3), v4, "Expected value");

    c_vector_free(& v);
    c_str_free(& v1);
    c_str_free(& v2);
    c_str_free(& v3);
    c_str_free(& v4);
}

void test_c_vector_sort_value()
{
    #define TEST(N, T)                                                         \
    C_VECTOR_TYPE(N) v##N = 0;                                                 \
                                                                               \
    c_vector_sort##N(v##N);                                                    \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    c_vector_append##N(v##N, 3);                                               \
    c_vector_append##N(v##N, 2);                                               \
    c_vector_append##N(v##N, 4);                                               \
    c_vector_append##N(v##N, 1);                                               \
    c_vector_append##N(v##N, 0);                                               \
    c_vector_sort##N(v##N);                                                    \
    C_TEST_ASSERT_EQ(c_vector_get##N(v##N, 0), 0, "Expected value " #N);       \
    C_TEST_ASSERT_EQ(c_vector_get##N(v##N, 1), 1, "Expected value " #N);       \
    C_TEST_ASSERT_EQ(c_vector_get##N(v##N, 2), 2, "Expected value " #N);       \
    C_TEST_ASSERT_EQ(c_vector_get##N(v##N, 3), 3, "Expected value " #N);       \
    C_TEST_ASSERT_EQ(c_vector_get##N(v##N, 4), 4, "Expected value " #N);       \
                                                                               \
    c_vector_free##N(& v##N);

    C_TYPE_TEMPLATE_NAME_TYPE_VALUE(TEST)
    #undef TEST
}

void test_c_vector_sorted_ptr()
{
    #define TEST(N, T)                                                         \
    C_VECTOR_TYPE(N) v##N = 0;                                                 \
    C_VECTOR_TYPE(N) w##N = 0;                                                 \
    T v1##N;                                                                   \
    T v2##N;                                                                   \
    T v3##N;                                                                   \
    T v4##N;                                                                   \
                                                                               \
    C_MEMORY_NEW(v1##N);                                                       \
    C_MEMORY_NEW(v2##N);                                                       \
    C_MEMORY_NEW(v3##N);                                                       \
    C_MEMORY_NEW(v4##N);                                                       \
                                                                               \
    *v1##N = 1;                                                                \
    *v2##N = 2;                                                                \
    *v3##N = 3;                                                                \
    *v4##N = 4;                                                                \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    c_vector_append##N(v##N, v4##N);                                           \
    c_vector_append##N(v##N, v1##N);                                           \
    c_vector_append##N(v##N, v3##N);                                           \
    c_vector_append##N(v##N, v2##N);                                           \
                                                                               \
    w##N = c_vector_sorted##N(v##N);                                           \
    C_TEST_ASSERT(w##N, "Expected non-null w" #N);                             \
                                                                               \
    C_TEST_ASSERT_EQ(c_vector_size##N(w##N), 4, "Expected size " #N);          \
    C_TEST_ASSERT_EQ_PTR(c_vector_get##N(w##N, 0), v1##N, "Expected value " #N);\
    C_TEST_ASSERT_EQ_PTR(c_vector_get##N(w##N, 1), v2##N, "Expected value " #N);\
    C_TEST_ASSERT_EQ_PTR(c_vector_get##N(w##N, 2), v3##N, "Expected value " #N);\
    C_TEST_ASSERT_EQ_PTR(c_vector_get##N(w##N, 3), v4##N, "Expected value " #N);\
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    c_vector_free##N(& w##N);                                                  \
    C_MEMORY_FREE(v1##N);                                                      \
    C_MEMORY_FREE(v2##N);                                                      \
    C_MEMORY_FREE(v3##N);                                                      \
    C_MEMORY_FREE(v4##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(TEST)
    #undef TEST
}

void test_c_vector_sorted_value()
{
    #define TEST(N, T)                                                         \
    C_VECTOR_TYPE(N) v##N = 0;                                                 \
    C_VECTOR_TYPE(N) v2##N = 0;                                                \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    c_vector_append##N(v##N, 3);                                               \
    c_vector_append##N(v##N, 2);                                               \
    c_vector_append##N(v##N, 4);                                               \
    c_vector_append##N(v##N, 1);                                               \
    c_vector_append##N(v##N, 0);                                               \
                                                                               \
    v2##N = c_vector_sorted##N(v##N);                                          \
    C_TEST_ASSERT(v2##N, "Expected non-null v2" #N);                           \
                                                                               \
    C_TEST_ASSERT_EQ(c_vector_size##N(v2##N), 5, "Expected size " #N);         \
    C_TEST_ASSERT_EQ(c_vector_get##N(v2##N, 0), 0, "Expected value " #N);      \
    C_TEST_ASSERT_EQ(c_vector_get##N(v2##N, 1), 1, "Expected value " #N);      \
    C_TEST_ASSERT_EQ(c_vector_get##N(v2##N, 2), 2, "Expected value " #N);      \
    C_TEST_ASSERT_EQ(c_vector_get##N(v2##N, 3), 3, "Expected value " #N);      \
    C_TEST_ASSERT_EQ(c_vector_get##N(v2##N, 4), 4, "Expected value " #N);      \
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    c_vector_free##N(& v2##N);

    C_TYPE_TEMPLATE_NAME_TYPE_VALUE(TEST)
    #undef TEST
}

int suite_test_c_vector_sort()
{
    C_TEST_ADD_SUITE("suite_test_c_vector_sort", NULL, NULL);

    C_TEST_ADD_TEST(test_c_vector_sort_ptr);
    C_TEST_ADD_TEST(test_c_vector_sort_str);
    C_TEST_ADD_TEST(test_c_vector_sort_value);
    C_TEST_ADD_TEST(test_c_vector_sorted_ptr);
    C_TEST_ADD_TEST(test_c_vector_sorted_value);

    C_TEST_RETURN();
}

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/
