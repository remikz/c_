#ifndef C_HASHMAP_IMPL_INCLUDED
#define C_HASHMAP_IMPL_INCLUDED

#include "c_hashmap.h"
#include "c_hashmap_t.h"
#include "c_memory.h"
#include "c_log.h"
#include <limits.h> // ULLONG_MAX

#ifdef __cplusplus
extern "C" {
#endif

#define C_HASHMAP_BUCKET(KN, KT, VN, VT)                                       \
c_vector_t c_hashmap_bucket##KN##VN(                                           \
                C_HASHMAP_TYPE(KN, VN) hashmap,                                \
                c_uint64_t bucket_index)                                       \
{                                                                              \
    if (hashmap) {                                                             \
        return c_vector_get(hashmap->buckets, bucket_index);                   \
    }                                                                          \
    return NULL;                                                               \
}

#define C_HASHMAP_BUCKET_GET(KN, KT, VN, VT)                                   \
VT c_hashmap_bucket_get##KN##VN(                                               \
            C_HASHMAP_TYPE(KN, VN) hashmap,                                    \
            c_uint64_t bucket_index,                                           \
            c_hash_t hash,                                                     \
            const KT key)                                                      \
{                                                                              \
    c_vector_t bucket;                                                         \
    C_KEYVALUE_HASH_TYPE(KN, VN) item;                                         \
    c_uint64_t i, n;                                                           \
                                                                               \
    if (hashmap) {                                                             \
        bucket = (c_vector_t) c_vector_get(hashmap->buckets, bucket_index);    \
        if (bucket) {                                                          \
            n = c_vector_size(bucket);                                         \
            for(i = 0; i < n; ++i) {                                           \
                item = c_vector_get(bucket, i);                                \
                if (0 == hashmap->cmp(hash, key, item)) {                      \
                    return c_keyvalue_hash_value##KN##VN(item);                \
                }                                                              \
            }                                                                  \
        }                                                                      \
    }                                                                          \
    return NULL;                                                               \
}

#define C_HASHMAP_BUCKET_PUT(KN, KT, VN, VT)                                   \
VT c_hashmap_bucket_put##KN##VN(                                               \
        C_HASHMAP_TYPE(KN, VN) hashmap,                                        \
        c_uint64_t bucket_index,                                               \
        c_hash_t hash,                                                         \
        KT key,                                                                \
        VT value)                                                              \
{                                                                              \
    C_VECTOR_TYPE(VN) bucket;                                                  \
    C_KEYVALUE_HASH_TYPE(KN, VN) keyvalue;                                     \
    c_int64_t item_index;                                                      \
    VT prev_value = 0;                                                         \
                                                                               \
    if (hashmap) {                                                             \
        bucket = (c_vector_t) c_vector_get(hashmap->buckets, bucket_index);    \
        if (!bucket) {                                                         \
            keyvalue = c_keyvalue_hash_new##KN##VN(hash, key, value);          \
            bucket = c_vector_new_set(1, 1, keyvalue);                         \
            c_vector_set(hashmap->buckets, bucket_index, bucket);              \
            ++hashmap->nitems;                                                 \
        } else {                                                               \
            item_index = c_hashmap_index##KN##VN(hashmap,                      \
                                                 bucket_index,                 \
                                                 hash, key);                   \
            if (0 > item_index) {                                              \
                keyvalue = c_keyvalue_hash_new##KN##VN(hash, key, value);      \
                c_vector_append(bucket, keyvalue);                             \
                ++hashmap->nitems;                                             \
            } else {                                                           \
                keyvalue = c_vector_get(bucket, item_index);                   \
                prev_value = c_keyvalue_hash_value##KN##VN(keyvalue);          \
                c_keyvalue_hash_value_set##KN##VN(keyvalue, value);            \
            }                                                                  \
        }                                                                      \
        ++hashmap->timestamp;                                                  \
    }                                                                          \
                                                                               \
    return prev_value;                                                         \
}

#define C_HASHMAP_BUCKET_REMOVE(KN, KT, VN, VT)                                \
VT c_hashmap_bucket_remove##KN##VN(                                            \
        C_HASHMAP_TYPE(KN, VN) hashmap,                                        \
        c_uint64_t bucket_index,                                               \
        c_hash_t hash,                                                         \
        const KT key)                                                          \
{                                                                              \
    C_VECTOR_TYPE(VN) bucket;                                                  \
    C_KEYVALUE_HASH_TYPE(KN, VN) keyvalue;                                     \
    c_int64_t item_index;                                                      \
    VT prev_value = 0;                                                         \
                                                                               \
    if (hashmap) {                                                             \
        bucket = (c_vector_t) c_vector_get(hashmap->buckets, bucket_index);    \
                                                                               \
        if (bucket) {                                                          \
            item_index = c_hashmap_index##KN##VN(hashmap,                      \
                                                 bucket_index,                 \
                                                 hash, key);                   \
            if (0 <= item_index) {                                             \
                keyvalue = c_vector_get(bucket, item_index);                   \
                prev_value = c_keyvalue_hash_value##KN##VN(keyvalue);          \
                c_keyvalue_hash_free##KN##VN(& keyvalue);                      \
                c_vector_remove_unordered(bucket, item_index);                 \
                --hashmap->nitems;                                             \
                ++hashmap->timestamp;                                          \
            }                                                                  \
        }                                                                      \
    }                                                                          \
                                                                               \
    return prev_value;                                                         \
}

#define C_HASHMAP_FIND_BUCKET(KN, KT, VN, VT)                                  \
c_uint64_t c_hashmap_find_bucket##KN##VN(                                      \
                C_HASHMAP_TYPE(KN, VN) hashmap,                                \
                c_uint64_t first)                                              \
{                                                                              \
    c_uint64_t nbuckets;                                                       \
    c_vector_t bucket;                                                         \
                                                                               \
    if (hashmap) {                                                             \
        nbuckets = c_hashmap_num_buckets##KN##VN(hashmap);                     \
        while (first < nbuckets) {                                             \
            bucket = c_hashmap_bucket##KN##VN(                                 \
                        hashmap,                                               \
                        first);                                                \
            if (c_vector_empty(bucket)) {                                      \
                ++first;                                                       \
            } else {                                                           \
                return first;                                                  \
            }                                                                  \
        }                                                                      \
    }                                                                          \
    return ULLONG_MAX;                                                         \
}

#define C_HASHMAP_FREE(KN, KT, VN, VT)                                         \
void c_hashmap_free##KN##VN(C_HASHMAP_TYPE(KN, VN) *hashmap)                   \
{                                                                              \
    assert(hashmap);                                                           \
    assert(*hashmap);                                                          \
                                                                               \
    if (hashmap && *hashmap) {                                                 \
        c_hashmap_free_buckets##KN##VN(*hashmap);                              \
        C_MEMORY_FREE(*hashmap);                                               \
    }                                                                          \
}

#define C_HASHMAP_FREE_BUCKET(KN, KT, VN, VT)                                  \
void c_hashmap_free_bucket##KN##VN(C_HASHMAP_TYPE(KN, VN) hashmap,             \
                                   c_uint64_t index)                           \
{                                                                              \
    c_uint64_t i = 0, n;                                                       \
    c_vector_t bucket;                                                         \
    C_KEYVALUE_HASH_TYPE(KN, VN) item;                                         \
                                                                               \
    if (hashmap) {                                                             \
        bucket = (c_vector_t) c_vector_get(hashmap->buckets, index);           \
        if (bucket) {                                                          \
            n = c_vector_size(bucket);                                         \
            for(; i < n; ++i) {                                                \
                item = c_vector_get(bucket, i);                                \
                if (item) {                                                    \
                    c_keyvalue_hash_free##KN##VN(& item);                      \
                    --hashmap->nitems;                                         \
                    ++hashmap->timestamp;                                      \
                }                                                              \
            }                                                                  \
            c_vector_free(& bucket);                                           \
        }                                                                      \
    }                                                                          \
}

#define C_HASHMAP_FREE_BUCKETS(KN, KT, VN, VT)                                 \
void c_hashmap_free_buckets##KN##VN(C_HASHMAP_TYPE(KN, VN) hashmap)            \
{                                                                              \
   c_uint64_t i = 0, n;                                                        \
                                                                               \
    if (hashmap) {                                                             \
        n = c_vector_capacity(hashmap->buckets);                               \
        for(; i < n; ++i) {                                                    \
            c_hashmap_free_bucket##KN##VN(hashmap, i);                         \
        }                                                                      \
        c_vector_free(& hashmap->buckets);                                     \
    }                                                                          \
}

#define C_HASHMAP_GET(KN, KT, VN, VT)                                          \
VT c_hashmap_get##KN##VN(C_HASHMAP_TYPE(KN, VN) hashmap, const KT key)         \
{                                                                              \
    c_hash_t hash;                                                             \
    c_uint64_t index;                                                          \
                                                                               \
    if (hashmap) {                                                             \
        hash = hashmap->hash(key);                                             \
        index = hash % c_vector_capacity(hashmap->buckets);                    \
                                                                               \
        return c_hashmap_bucket_get##KN##VN(                                   \
                    hashmap,                                                   \
                    index,                                                     \
                    hash,                                                      \
                    key);                                                      \
    }                                                                          \
    return NULL;                                                               \
}

#define C_HASHMAP_HAS(KN, KT, VN, VT)                                          \
c_bool_t c_hashmap_has##KN##VN(                                                \
            C_HASHMAP_TYPE(KN, VN) hashmap,                                    \
            const KT key)                                                      \
{                                                                              \
    c_hash_t hash;                                                             \
    c_uint64_t bucket_index;                                                   \
    c_int64_t item_index = -1;                                                 \
                                                                               \
    if (hashmap) {                                                             \
        hash = hashmap->hash(key);                                             \
        bucket_index = hash % c_vector_capacity(hashmap->buckets);             \
                                                                               \
        item_index = c_hashmap_index##KN##VN(                                  \
                    hashmap,                                                   \
                    bucket_index,                                              \
                    hash,                                                      \
                    key);                                                      \
    }                                                                          \
    return 0 <= item_index;                                                    \
}

#define C_HASHMAP_INDEX(KN, KT, VN, VT)                                        \
c_int64_t c_hashmap_index##KN##VN(                                             \
            C_HASHMAP_TYPE(KN, VN) hashmap,                                    \
            c_uint64_t bucket_index,                                           \
            c_hash_t hash,                                                     \
            const KT key)                                                      \
{                                                                              \
    C_VECTOR_TYPE(VN) bucket;                                                  \
    C_KEYVALUE_HASH_TYPE(KN, VN) item;                                         \
    c_uint64_t i, n;                                                           \
                                                                               \
    if (hashmap) {                                                             \
        bucket = (c_vector_t) c_vector_get(hashmap->buckets, bucket_index);    \
        if (bucket) {                                                          \
            n = c_vector_size(bucket);                                         \
            for(i = 0; i < n; ++i) {                                           \
                item = c_vector_get(bucket, i);                                \
                if (0 == hashmap->cmp(hash, key, item)) {                      \
                    return i;                                                  \
                }                                                              \
            }                                                                  \
        }                                                                      \
    }                                                                          \
    return -1;                                                                 \
}

#define C_HASHMAP_KEYS(KN, VN)                                                 \
C_VECTOR_TYPE(KN) c_hashmap_keys##KN##VN(C_HASHMAP_TYPE(KN, VN) hashmap)       \
{                                                                              \
    C_VECTOR_TYPE(KN) vector = c_vector_new##KN(0);                            \
    c_vector_t bucket_items;                                                   \
    C_KEYVALUE_HASH_TYPE(KN, VN) keyvalue;                                     \
    c_uint64_t nbuckets, nitems, bucketi = 0, itemi;                           \
                                                                               \
    if (hashmap) {                                                             \
        c_vector_reserve##KN(vector, hashmap->nitems);                         \
        nbuckets = c_vector_capacity(hashmap->buckets);                        \
                                                                               \
        for(; bucketi < nbuckets; ++bucketi) {                                 \
            bucket_items = c_vector_get(hashmap->buckets, bucketi);            \
            nitems = c_vector_size(bucket_items);                              \
            for(itemi = 0; itemi < nitems; ++itemi) {                          \
                keyvalue = (C_KEYVALUE_HASH_TYPE(KN, VN))                      \
                                c_vector_get(bucket_items, itemi);             \
                c_vector_append##KN(                                           \
                    vector,                                                    \
                    c_keyvalue_hash_key##KN##VN(keyvalue));                    \
            }                                                                  \
        }                                                                      \
    }                                                                          \
                                                                               \
    return vector;                                                             \
}

#define C_HASHMAP_LOAD_FACTOR(KN, KT, VN, VT)                                  \
c_float32_t c_hashmap_load_factor##KN##VN(C_HASHMAP_TYPE(KN, VN) hashmap)      \
{                                                                              \
    c_float32_t ratio = 0.0;                                                   \
    c_int64_t i = 0, nonempty = 0, n;                                          \
                                                                               \
    if (hashmap) {                                                             \
        n = c_vector_capacity(hashmap->buckets);                               \
        for(; i < n; ++i) {                                                    \
            if (c_vector_get(hashmap->buckets, i)) {                           \
                ++nonempty;                                                    \
            }                                                                  \
        }                                                                      \
                                                                               \
        ratio = nonempty / (c_float32_t) n;                                    \
    }                                                                          \
    return ratio;                                                              \
}

#define C_HASHMAP_NEW(KN, KT, VN, VT)                                          \
C_HASHMAP_TYPE(KN, VN) c_hashmap_new##KN##VN(                                  \
                            c_uint64_t num_buckets,                            \
                            C_KEYVALUE_HASH_CMP_TYPE(KN, VN) cmp,              \
                            C_HASH_NAME(KN) hash)                              \
{                                                                              \
    C_HASHMAP_TYPE(KN, VN) result;                                             \
    C_MEMORY_NEW(result);                                                      \
                                                                               \
    if (hash) {                                                                \
        result->hash = hash;                                                   \
    } else {                                                                   \
        result->hash = c_hash_identity##KN;                                    \
    }                                                                          \
                                                                               \
    if (cmp) {                                                                 \
        result->cmp = cmp;                                                     \
    } else {                                                                   \
        result->cmp = C_KEYVALUE_HASH_CMP_NAME(KN, VN);                        \
    }                                                                          \
                                                                               \
    result->nitems = 0;                                                        \
    result->timestamp = 0;                                                     \
    assert(num_buckets);                                                       \
    result->buckets = c_vector_new_set(num_buckets, num_buckets, NULL);        \
                                                                               \
    return result;                                                             \
}

#define C_HASHMAP_NUM_BUCKETS(KN, KT, VN, VT)                                  \
c_uint64_t c_hashmap_num_buckets##KN##VN(C_HASHMAP_TYPE(KN, VN) hashmap)       \
{                                                                              \
    if (hashmap) {                                                             \
        return c_vector_capacity(hashmap->buckets);                            \
    }                                                                          \
    return 0;                                                                  \
}

#define C_HASHMAP_NUM_ITEMS(KN, KT, VN, VT)                                    \
c_uint64_t c_hashmap_num_items##KN##VN(C_HASHMAP_TYPE(KN, VN) hashmap)         \
{                                                                              \
    if (hashmap) {                                                             \
        return hashmap->nitems;                                                \
    }                                                                          \
    return 0;                                                                  \
}

#define C_HASHMAP_PUT(KN, KT, VN, VT)                                          \
VT c_hashmap_put##KN##VN(                                                      \
        C_HASHMAP_TYPE(KN, VN) hashmap,                                        \
        KT key,                                                                \
        VT value)                                                              \
{                                                                              \
    c_hash_t hash;                                                             \
    c_uint64_t index;                                                          \
                                                                               \
    if (hashmap) {                                                             \
        hash = hashmap->hash(key);                                             \
        index = hash % c_vector_capacity(hashmap->buckets);                    \
        return c_hashmap_bucket_put##KN##VN(hashmap, index, hash, key, value); \
    }                                                                          \
                                                                               \
    return 0;                                                                  \
}

#define C_HASHMAP_REMOVE(KN, KT, VN, VT)                                       \
VT c_hashmap_remove##KN##VN(                                                   \
        C_HASHMAP_TYPE(KN, VN) hashmap,                                        \
        const KT key)                                                          \
{                                                                              \
    c_hash_t hash;                                                             \
    c_uint64_t index;                                                          \
                                                                               \
    if (hashmap) {                                                             \
        hash = hashmap->hash(key);                                             \
        index = hash % c_vector_capacity(hashmap->buckets);                    \
        return c_hashmap_bucket_remove##KN##VN(                                \
                    hashmap, index, hash, key);                                \
    }                                                                          \
                                                                               \
    return 0;                                                                  \
}

#define C_HASHMAP_TIMESTAMP(KN, KT, VN, VT)                                    \
c_uint64_t c_hashmap_timestamp##KN##VN(C_HASHMAP_TYPE(KN, VN) hashmap)         \
{                                                                              \
    if (hashmap) {                                                             \
        return hashmap->timestamp;                                             \
    }                                                                          \
    return 0;                                                                  \
}

#define C_HASHMAP_VALUES(KN, VN)                                               \
C_VECTOR_TYPE(VN) c_hashmap_values##KN##VN(C_HASHMAP_TYPE(KN, VN) hashmap)     \
{                                                                              \
    C_VECTOR_TYPE(VN) vector = c_vector_new##VN(0);                            \
    c_vector_t bucket_items;                                                   \
    C_KEYVALUE_HASH_TYPE(KN, VN) keyvalue;                                     \
    c_uint64_t nbuckets, nitems, bucketi = 0, itemi;                           \
                                                                               \
    if (hashmap) {                                                             \
        c_vector_reserve##VN(vector, hashmap->nitems);                         \
        nbuckets = c_vector_capacity(hashmap->buckets);                        \
                                                                               \
        for(; bucketi < nbuckets; ++bucketi) {                                 \
            bucket_items = c_vector_get(hashmap->buckets, bucketi);            \
            nitems = c_vector_size(bucket_items);                              \
            for(itemi = 0; itemi < nitems; ++itemi) {                          \
                keyvalue = (C_KEYVALUE_HASH_TYPE(KN, VN))                      \
                                c_vector_get(bucket_items, itemi);             \
                c_vector_append##VN(                                           \
                    vector,                                                    \
                    c_keyvalue_hash_value##KN##VN(keyvalue));                  \
            }                                                                  \
        }                                                                      \
    }                                                                          \
                                                                               \
    return vector;                                                             \
}

C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_BUCKET)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_BUCKET_GET)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_BUCKET_PUT)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_BUCKET_REMOVE)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_FIND_BUCKET)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_FREE_BUCKET)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_FREE_BUCKETS)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_FREE)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_GET)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_HAS)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_INDEX)
C_KEYVALUE_HASH_TEMPLATE_NAME_NUMERIC(C_HASHMAP_KEYS)
C_KEYVALUE_HASH_TEMPLATE_NAME_PTR(C_HASHMAP_KEYS)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_LOAD_FACTOR)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_NEW)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_NUM_ITEMS)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_NUM_BUCKETS)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_PUT)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_REMOVE)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_TIMESTAMP)
C_KEYVALUE_HASH_TEMPLATE_NAME_NUMERIC(C_HASHMAP_VALUES)
C_KEYVALUE_HASH_TEMPLATE_NAME_PTR(C_HASHMAP_VALUES)

#undef C_HASHMAP_BUCKET
#undef C_HASHMAP_BUCKET_GET
#undef C_HASHMAP_BUCKET_PUT
#undef C_HASHMAP_BUCKET_REMOVE
#undef C_HASHMAP_FIND_BUCKET
#undef C_HASHMAP_FREE_BUCKET
#undef C_HASHMAP_FREE_BUCKETS
#undef C_HASHMAP_FREE
#undef C_HASHMAP_GET
#undef C_HASHMAP_HAS
#undef C_HASHMAP_INDEX
#undef C_HASHMAP_KEYS
#undef C_HASHMAP_LOAD_FACTOR
#undef C_HASHMAP_NEW
#undef C_HASHMAP_NUM_BUCKETS
#undef C_HASHMAP_NUM_ITEMS
#undef C_HASHMAP_PUT
#undef C_HASHMAP_REMOVE
#undef C_HASHMAP_TIMESTAMP
#undef C_HASHMAP_VALUES

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/
