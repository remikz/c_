#ifndef C_VECTOR_FIND_IMPL_INCLUDED
#define C_VECTOR_FIND_IMPL_INCLUDED

#include "c_vector_find.h"
#include "c_vector_t.h"
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

#define C_VECTOR_FIND(NAME, TYPE)                                              \
c_int64_t c_vector_find##NAME(const C_VECTOR_TYPE(NAME) vector,                \
                              const TYPE item)                                 \
{                                                                              \
    c_uint64_t i = 0;                                                          \
    for(; i < vector->size; ++i) {                                             \
        if (item == vector->items[i]) {                                        \
            return i;                                                          \
        }                                                                      \
    }                                                                          \
    return -1;                                                                 \
}

#define C_VECTOR_FIND_BISECT(NAME, TYPE)                                       \
c_int64_t c_vector_find_bisect##NAME(const C_VECTOR_TYPE(NAME) vector,         \
                                     const TYPE item)                          \
{                                                                              \
    c_int64_t left, mid, right;                                                \
                                                                               \
    if (vector && vector->size) {                                              \
        left = 0;                                                              \
        right = vector->size - 1;                                              \
        while (left <= right) {                                                \
            mid = (left + right) / 2;                                          \
            if (item == vector->items[mid]) {                                  \
                return mid;                                                    \
            } else if (item > vector->items[mid]) {                            \
                left = mid + 1;                                                \
            } else {                                                           \
                right = mid - 1;                                               \
            }                                                                  \
        }                                                                      \
    }                                                                          \
    return -1;                                                                 \
}

#define C_VECTOR_FIND_BISECT_INSERT(NAME, TYPE)                                \
c_int64_t c_vector_find_bisect_insert##NAME(const C_VECTOR_TYPE(NAME) vector,  \
                                            const TYPE item)                   \
{                                                                              \
    c_int64_t left, mid, right;                                                \
                                                                               \
    if (vector) {                                                              \
        if (vector->size) {                                                    \
            left = 0;                                                          \
            right = vector->size - 1;                                          \
            while (left <= right) {                                            \
                mid = (left + right) / 2;                                      \
                if (item == vector->items[mid]) {                              \
                    return mid;                                                \
                } else if (item > vector->items[mid]) {                        \
                    left = mid + 1;                                            \
                } else {                                                       \
                    right = mid - 1;                                           \
                }                                                              \
            }                                                                  \
            return left;                                                       \
        }                                                                      \
        return 0;                                                              \
    }                                                                          \
    return -1;                                                                 \
}

#define C_VECTOR_FIND_BISECT_INSERT_UDF(NAME, TYPE)                            \
c_int64_t c_vector_find_bisect_insert_udf##NAME(                               \
                const C_VECTOR_TYPE(NAME) vector,                              \
                const TYPE item,                                               \
                c_int32_t (*cmp_udf)(const TYPE, const TYPE))                  \
{                                                                              \
    c_int64_t left, mid, right, cmp_res;                                       \
                                                                               \
    if (vector) {                                                              \
        if (vector->size) {                                                    \
            left = 0;                                                          \
            right = vector->size - 1;                                          \
            while (left <= right) {                                            \
                mid = (left + right) / 2;                                      \
                cmp_res = cmp_udf(item, vector->items[mid]);                   \
                if (0 == cmp_res) {                                            \
                    return mid;                                                \
                } else if (0 < cmp_res) {                                      \
                    left = mid + 1;                                            \
                } else {                                                       \
                    right = mid - 1;                                           \
                }                                                              \
            }                                                                  \
            return left;                                                       \
        }                                                                      \
        return 0;                                                              \
    }                                                                          \
    return -1;                                                                 \
}

#define C_VECTOR_FIND_BISECT_UDF(NAME, TYPE)                                   \
c_int64_t c_vector_find_bisect_udf##NAME(                                      \
                const C_VECTOR_TYPE(NAME) vector,                              \
                const TYPE item,                                               \
                c_int32_t (*cmp_udf)(const TYPE, const TYPE))                  \
{                                                                              \
    c_int64_t left, mid, right, cmp_res;                                       \
                                                                               \
    if (vector && vector->size) {                                              \
        left = 0;                                                              \
        right = vector->size - 1;                                              \
        while (left <= right) {                                                \
            mid = (left + right) / 2;                                          \
            cmp_res = cmp_udf(item, vector->items[mid]);                       \
            if (0 == cmp_res) {                                                \
                return mid;                                                    \
            } else if (0 < cmp_res) {                                          \
                left = mid + 1;                                                \
            } else {                                                           \
                right = mid - 1;                                               \
            }                                                                  \
        }                                                                      \
    }                                                                          \
    return -1;                                                                 \
}

#define C_VECTOR_FIND_MANY(NAME, TYPE)                                         \
c_vector_int64_t c_vector_find_many##NAME(const C_VECTOR_TYPE(NAME) haystack,  \
                                          const C_VECTOR_TYPE(NAME) needles)   \
{                                                                              \
    c_vector_int64_t result = NULL;                                            \
    c_uint64_t i, n;                                                           \
    c_int64_t *result_items;                                                   \
    TYPE *needles_items;                                                       \
                                                                               \
    if (haystack && needles) {                                                 \
        n = c_vector_size##NAME(needles);                                      \
        result = c_vector_new_int64(n);                                        \
        c_vector_resize_int64(result, n);                                      \
        result_items = c_vector_items_int64(result);                           \
        needles_items = c_vector_items##NAME(needles);                         \
                                                                               \
        for(i = 0; i < n; ++i) {                                               \
            result_items[i] = c_vector_find##NAME(haystack, needles_items[i]); \
        }                                                                      \
    }                                                                          \
                                                                               \
    return result;                                                             \
}

#define C_VECTOR_FIND_MANY_UDF(NAME, TYPE)                                     \
extern c_vector_int64_t c_vector_find_many_udf##NAME(                          \
                                const C_VECTOR_TYPE(NAME) haystack,            \
                                const C_VECTOR_TYPE(NAME) needles,             \
                                c_int32_t (*cmp_udf)(const TYPE, const TYPE))  \
{                                                                              \
    c_vector_int64_t result = NULL;                                            \
    c_uint64_t i, n;                                                           \
    c_int64_t *result_items;                                                   \
    TYPE *needles_items;                                                       \
                                                                               \
    if (haystack && needles) {                                                 \
        n = c_vector_size##NAME(needles);                                      \
        result = c_vector_new_int64(n);                                        \
        c_vector_resize_int64(result, n);                                      \
        result_items = c_vector_items_int64(result);                           \
        needles_items = c_vector_items##NAME(needles);                         \
                                                                               \
        for(i = 0; i < n; ++i) {                                               \
            result_items[i] = c_vector_find_udf##NAME(haystack,                \
                                                      needles_items[i],        \
                                                      cmp_udf);                \
        }                                                                      \
    }                                                                          \
                                                                               \
    return result;                                                             \
}

#define C_VECTOR_FIND_RANGE(NAME, TYPE)                                        \
c_int64_t c_vector_find_range##NAME(const C_VECTOR_TYPE(NAME) vector,          \
                                    const TYPE item,                           \
                                    c_uint64_t first, c_uint64_t last)         \
{                                                                              \
    c_uint64_t i;                                                              \
                                                                               \
    if (vector && vector->size) {                                              \
        if (last >= vector->size) {                                            \
            last = vector->size - 1;                                           \
        }                                                                      \
                                                                               \
        for(i = first; i <= last; ++i) {                                       \
            if (item == vector->items[i]) {                                    \
                return i;                                                      \
            }                                                                  \
        }                                                                      \
    }                                                                          \
    return -1;                                                                 \
}

#define C_VECTOR_FIND_RANGE_UDF(NAME, TYPE)                                    \
c_int64_t c_vector_find_range_udf##NAME(                                       \
                const C_VECTOR_TYPE(NAME) vector,                              \
                const TYPE item,                                               \
                c_uint64_t first, c_uint64_t last,                             \
                c_int32_t (*cmp_udf)(const TYPE, const TYPE))                  \
{                                                                              \
    c_uint64_t i;                                                              \
                                                                               \
    if (vector && vector->size) {                                              \
        if (last >= vector->size) {                                            \
            last = vector->size - 1;                                           \
        }                                                                      \
                                                                               \
        for(i = first; i <= last; ++i) {                                       \
            if (0 == cmp_udf(item, vector->items[i])) {                        \
                return i;                                                      \
            }                                                                  \
        }                                                                      \
    }                                                                          \
    return -1;                                                                 \
}

#define C_VECTOR_FIND_SAFE(NAME, TYPE)                                         \
c_int64_t c_vector_find_safe##NAME(const C_VECTOR_TYPE(NAME) vector,           \
                                   const TYPE item)                            \
{                                                                              \
    if (vector && vector->size) {                                              \
        return c_vector_find_range##NAME(vector, item, 0, vector->size-1);     \
    }                                                                          \
    return -1;                                                                 \
}

#define C_VECTOR_FIND_UDF(NAME, TYPE)                                          \
c_int64_t c_vector_find_udf##NAME(                                             \
                const C_VECTOR_TYPE(NAME) vector,                              \
                const TYPE item,                                               \
                c_int32_t (*cmp_udf)(const TYPE, const TYPE))                  \
{                                                                              \
    c_int64_t i = 0;                                                           \
    for(; i < vector->size; ++i) {                                             \
        if (0 == cmp_udf(item, vector->items[i])) {                            \
            return i;                                                          \
        }                                                                      \
    }                                                                          \
    return -1;                                                                 \
}

#define C_VECTOR_FIND_UDF_SAFE(NAME, TYPE)                                     \
c_int64_t c_vector_find_udf_safe##NAME(                                        \
                const C_VECTOR_TYPE(NAME) vector,                              \
                const TYPE item,                                               \
                c_int32_t (*cmp_udf)(const TYPE, const TYPE))                  \
{                                                                              \
    if (vector && vector->size) {                                              \
        return c_vector_find_range_udf##NAME(vector, item, 0, vector->size-1,  \
                                             cmp_udf);                         \
    }                                                                          \
    return -1;                                                                 \
}

#define C_VECTOR_RFIND(NAME, TYPE)                                             \
c_int64_t c_vector_rfind##NAME(const C_VECTOR_TYPE(NAME) vector,               \
                               const TYPE item)                                \
{                                                                              \
    if (vector && vector->size) {                                              \
        return c_vector_rfind_range##NAME(vector, item, 0, vector->size-1);    \
    }                                                                          \
    return -1;                                                                 \
}

#define C_VECTOR_RFIND_RANGE(NAME, TYPE)                                       \
c_int64_t c_vector_rfind_range##NAME(const C_VECTOR_TYPE(NAME) vector,         \
                                     const TYPE item,                          \
                                     c_uint64_t first, c_uint64_t last)        \
{                                                                              \
    c_uint64_t i;                                                              \
                                                                               \
    if (vector && vector->size) {                                              \
        if (last >= vector->size) {                                            \
            last = vector->size - 1;                                           \
        }                                                                      \
                                                                               \
        for(i = last; i >= first; --i) {                                       \
            if (item == vector->items[i]) {                                    \
                return i;                                                      \
            }                                                                  \
        }                                                                      \
    }                                                                          \
    return -1;                                                                 \
}

#define C_VECTOR_RFIND_RANGE_UDF(NAME, TYPE)                                   \
c_int64_t c_vector_rfind_range_udf##NAME(                                      \
                const C_VECTOR_TYPE(NAME) vector,                              \
                const TYPE item,                                               \
                c_uint64_t first, c_uint64_t last,                             \
                c_int32_t (*cmp_udf)(const TYPE, const TYPE))                  \
{                                                                              \
    c_uint64_t i;                                                              \
                                                                               \
    if (vector && vector->size) {                                              \
        if (last >= vector->size) {                                            \
            last = vector->size - 1;                                           \
        }                                                                      \
                                                                               \
        for(i = last; i >= last; --i) {                                        \
            if (0 == cmp_udf(item, vector->items[i])) {                        \
                return i;                                                      \
            }                                                                  \
        }                                                                      \
    }                                                                          \
    return -1;                                                                 \
}

#define C_VECTOR_RFIND_UDF(NAME, TYPE)                                         \
c_int64_t c_vector_rfind_udf##NAME(                                            \
                const C_VECTOR_TYPE(NAME) vector,                              \
                const TYPE item,                                               \
                c_int32_t (*cmp_udf)(const TYPE, const TYPE))                  \
{                                                                              \
    if (vector && vector->size) {                                              \
        return c_vector_rfind_range_udf##NAME(vector, item, 0, vector->size-1, \
                                              cmp_udf);                        \
    }                                                                          \
    return -1;                                                                 \
}

C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_FIND)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_FIND_BISECT)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_FIND_BISECT_INSERT)
C_TYPE_TEMPLATE_NAME_TYPE_PTR(C_VECTOR_FIND_BISECT_INSERT_UDF)
C_TYPE_TEMPLATE_NAME_TYPE_PTR(C_VECTOR_FIND_BISECT_UDF)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_FIND_MANY)
C_TYPE_TEMPLATE_NAME_TYPE_PTR(C_VECTOR_FIND_MANY_UDF)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_FIND_RANGE)
C_TYPE_TEMPLATE_NAME_TYPE_PTR(C_VECTOR_FIND_RANGE_UDF)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_FIND_SAFE)
C_TYPE_TEMPLATE_NAME_TYPE_PTR(C_VECTOR_FIND_UDF)
C_TYPE_TEMPLATE_NAME_TYPE_PTR(C_VECTOR_FIND_UDF_SAFE)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_RFIND)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_RFIND_RANGE)
C_TYPE_TEMPLATE_NAME_TYPE_PTR(C_VECTOR_RFIND_RANGE_UDF)
C_TYPE_TEMPLATE_NAME_TYPE_PTR(C_VECTOR_RFIND_UDF)

#undef C_VECTOR_FIND
#undef C_VECTOR_FIND_BISECT
#undef C_VECTOR_FIND_BISECT_INSERT
#undef C_VECTOR_FIND_BISECT_INSERT_UDF
#undef C_VECTOR_FIND_BISECT_UDF
#undef C_VECTOR_FIND_MANY
#undef C_VECTOR_FIND_MANY_UDF
#undef C_VECTOR_FIND_RANGE
#undef C_VECTOR_FIND_RANGE_UDF
#undef C_VECTOR_FIND_SAFE
#undef C_VECTOR_FIND_UDF
#undef C_VECTOR_FIND_UDF_SAFE
#undef C_VECTOR_RFIND
#undef C_VECTOR_RFIND_RANGE
#undef C_VECTOR_RFIND_RANGE_UDF
#undef C_VECTOR_RFIND_UDF

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/