#ifndef C_JSON_INCLUDED
#define C_JSON_INCLUDED

#include "c_type.h"
#include "c_str.h"

#ifdef __cplusplus
extern "C" {
#endif

C_TYPEDEF(c_json_t)

extern c_bool_t c_json_is_array(c_json_t json);
extern c_bool_t c_json_is_bool(c_json_t json);
extern c_bool_t c_json_is_false(c_json_t json);
extern c_bool_t c_json_is_float(c_json_t json);
extern c_bool_t c_json_is_integer(c_json_t json);
extern c_bool_t c_json_is_null(c_json_t json);
extern c_bool_t c_json_is_number(c_json_t json);
extern c_bool_t c_json_is_object(c_json_t json);
extern c_bool_t c_json_is_str(c_json_t json);
extern c_bool_t c_json_is_true(c_json_t json);
extern c_vector_t  c_json_array(c_json_t json);
extern c_bool_t    c_json_bool(c_json_t json);
extern c_float64_t c_json_float(c_json_t json);
extern c_int64_t   c_json_integer(c_json_t json);
extern c_float64_t c_json_number(c_json_t json);
//extern c_dict_t  c_json_object(c_json_t json);
extern c_str_t     c_json_str(c_json_t json);

#undef T

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/