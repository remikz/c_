#ifndef TEST_C_STR_INCLUDED
#define TEST_C_STR_INCLUDED

#include "c_test.h"
#include "c_str.h"

void test_c_str_cmp()
{
    c_str_t s1 = NULL, s2 = NULL;
    c_int32_t r;

    r = c_str_cmp(s1, s2);
    C_TEST_ASSERT_EQ(r, -1, "Expected result for nulls.");

    s1 = c_str_new("str1", 4);
    r = c_str_cmp(s1, s2);
    C_TEST_ASSERT_EQ(r, 1, "Expected result one null.");

    s2 = c_str_new("str2", 4);
    r = c_str_cmp(s1, s2);
    C_TEST_ASSERT_EQ(r, -1, "Expected result non nulls.");
    c_str_free(& s1);
    c_str_free(& s2);

    s1 = c_str_new("str1", 4);
    s2 = c_str_new("str1 longer", 11);
    r = c_str_cmp(s1, s2);
    C_TEST_ASSERT_EQ(r, -1, "Expected result short vs long.");
    r = c_str_cmp(s2, s1);
    C_TEST_ASSERT_EQ(r, 1, "Expected result long vs short.");
    r = c_str_cmp(s1, s1);
    C_TEST_ASSERT_EQ(r, 0, "Expected result same refs.");
    c_str_free(& s1);
    c_str_free(& s2);

    s1 = c_str_new("str", 3);
    s2 = c_str_new("str", 3);
    C_TEST_ASSERT_EQ(r, 0, "Expected result same strings.");
    c_str_free(& s1);
    c_str_free(& s2);
}

void test_c_str_cmp_voidpp()
{
    c_str_t s1 = NULL, s2 = NULL;
    c_int32_t r;

    r = c_str_cmp_voidpp(& s1, & s2);
    C_TEST_ASSERT_EQ(r, -1, "Expected result for nulls.");

    s1 = c_str_new("str1", 4);
    r = c_str_cmp_voidpp(& s1, & s2);
    C_TEST_ASSERT_EQ(r, 1, "Expected result one null.");

    s2 = c_str_new("str2", 4);
    r = c_str_cmp_voidpp(& s1, & s2);
    C_TEST_ASSERT_EQ(r, -1, "Expected result non nulls.");
    c_str_free(& s1);
    c_str_free(& s2);

    s1 = c_str_new("str1", 4);
    s2 = c_str_new("str1 longer", 11);
    r = c_str_cmp_voidpp(& s1, & s2);
    C_TEST_ASSERT_EQ(r, -1, "Expected result short vs long.");
    r = c_str_cmp_voidpp(& s2, & s1);
    C_TEST_ASSERT_EQ(r, 1, "Expected result long vs short.");
    r = c_str_cmp_voidpp(& s1, & s1);
    C_TEST_ASSERT_EQ(r, 0, "Expected result same refs.");
    c_str_free(& s1);
    c_str_free(& s2);

    s1 = c_str_new("str", 3);
    s2 = c_str_new("str", 3);
    C_TEST_ASSERT_EQ(r, 0, "Expected result same strings.");
    c_str_free(& s1);
    c_str_free(& s2);
}

int suite_test_c_str()
{
    C_TEST_ADD_SUITE("suite_test_c_str", NULL, NULL);

    C_TEST_ADD_TEST(test_c_str_cmp);
    C_TEST_ADD_TEST(test_c_str_cmp_voidpp);

    C_TEST_RETURN();
}

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/
