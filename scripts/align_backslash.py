#!/usr/bin/env python
import sys
import os

if len(sys.argv) < 2:
	sys.exit('Prettify multiline C macros in files.\n\n'
              'usage: {} file [...]'.format(sys.argv[0]))

files = sys.argv[1:]

for fin in files:
    if not os.path.exists(fin):
        continue

    lines_out = []
    with open(fin) as lines_in:
        for line in lines_in:
            line = line.replace('\t', ' '*4)
            line_out = line
            if line.rstrip().endswith('\\'):
                idx = line.rindex('\\')
                strip = line[:idx].rstrip()
                w = len(strip)
                for col in xrange(80, 220, 1):
                    if w < col:
                        pad = ' '*(col - w - 1)
                        line_out = strip + pad + '\\\n'
                        break
            lines_out.append(line_out)

    with open(fin, 'w') as f:
        for line in lines_out:
            f.write(line)
