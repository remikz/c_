#ifndef TEST_C_LOG_INCLUDED
#define TEST_C_LOG_INCLUDED

#include "c_test.h"
#include "c_log.h"
#include <string.h>

void test_c_log()
{
    c_log("DUMMY", stdout, "dummy", 0, "dummy message\n");
}

void test_c_log_str()
{
    const int BUFLEN = 256;
    char buf[BUFLEN];
    char *LEVEL = "DUMMY";
    char *NAME = "test_c_log.h";
    char *MSG = "message";

    c_log_str(LEVEL, buf, BUFLEN, NAME, 11, MSG);

    C_TEST_PRINT1("buf=%s\n", buf);
    C_TEST_ASSERT(strstr(buf, LEVEL), "Did not find substring.");
    C_TEST_ASSERT(strstr(buf, NAME), "Did not find NAME substring.");
    C_TEST_ASSERT(strstr(buf, MSG), "Did not find MSG substring.");
    C_TEST_ASSERT(strlen(buf) > 45, "String too short.");
}

int suite_test_c_log()
{
    C_TEST_ADD_SUITE("suite_test_c_log", NULL, NULL);
    C_TEST_ADD_TEST(test_c_log);
    C_TEST_ADD_TEST(test_c_log_str);
recover:
    C_TEST_RETURN();
}

#endif
/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/