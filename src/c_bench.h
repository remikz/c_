#ifndef C_BENCH_INCLUDED
#define C_BENCH_INCLUDED

#include "c_type.h"

#ifdef __cplusplus
extern "C" {
#endif

#define T c_bench_t
C_TYPEDEF(T)

extern void c_bench_free(T *bench);

extern T    c_bench_new();

/** Print results to stdout. */
extern void c_bench_print(T bench);

extern void c_bench_reset(T bench);

/** Run a function repeatedly and time it.
 * @param ntrials If 0, then run until at least 1 second function time elapses.
 */
extern void c_bench_run(T bench, c_uint64_t ntrials, void (*fun)(void));

/** Before each trial, run setup and then teardown if not they're not NULL. */
extern void c_bench_run_setup_teardown(
        T bench,
        c_uint64_t ntrials,
        void (*fun)(void),
        void (*setup)(void),
        void (*teardown)(void));

/** Print results to pre-allocated char array. */
extern void c_bench_to_chars(T bench, char *chars, c_uint64_t nchars);

#undef T

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/
