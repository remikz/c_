#ifndef C_STR_IMPL_INCLUDED
#define C_STR_IMPL_INCLUDED

#include "c_str.h"
#include "c_vector_t.h"
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif

#define C_STR_CMP(NAME, TYPE, CAST)                                            \
c_int32_t c_str_cmp##NAME(const TYPE s1, const TYPE s2)                        \
{                                                                              \
    c_int32_t r;                                                               \
                                                                               \
    if (s1) {\
        if (s2) {                                                            \
            if ( (CAST s1)->size < (CAST s2)->size) {                              \
                r = memcmp( (CAST s1)->items, (CAST s2)->items, (CAST s1)->size);  \
                return 0 == r ? -1 : r;                                            \
            } else if ((CAST s1)->size > (CAST s2)->size) {                        \
                r = memcmp((CAST s2)->items, (CAST s1)->items, (CAST s2)->size);   \
                return 0 == r ? 1 : r;                                             \
            } else {                                                               \
                return memcmp((CAST s1)->items, (CAST s2)->items, (CAST s1)->size);\
            }                                                                      \
        } else {\
            return 1;\
        }\
    } else {                                                                   \
        return -1;                                                             \
    }                                                                          \
}

c_int32_t c_str_cmp_voidpp(const void *s1, const void *s2)
{
    c_int32_t r;

    if (s1 && *(c_str_t*) s1) {
        if (s2 && *(c_str_t*) s2) {
            if ( (*(c_str_t*) s1)->size < (*(c_str_t*) s2)->size) {
                r = memcmp( (*(c_str_t*) s1)->items,
                            (*(c_str_t*) s2)->items,
                            (*(c_str_t*) s1)->size);
                return 0 == r ? -1 : r;
            } else if ((*(c_str_t*) s1)->size > (*(c_str_t*) s2)->size) {
                r = memcmp( (*(c_str_t*) s2)->items,
                            (*(c_str_t*) s1)->items,
                            (*(c_str_t*) s2)->size);
                return 0 == r ? 1 : r;
            } else {
                return memcmp(  (*(c_str_t*) s1)->items,
                                (*(c_str_t*) s2)->items,
                                (*(c_str_t*) s1)->size);
            }
        } else {
            return 1;
        }
    } else {
        return -1;
    }
}

c_str_t c_str_new(const c_char_t *str, c_uint64_t n)
{
    c_str_t s = c_vector_new_int8(n + 1);

    strncpy(c_str_data(s), str, n);
    c_vector_set_int8(s, n, '\0');
    c_str_compute_size(s);

    return s;
}

c_str_t c_str_new_clear(c_uint64_t size)
 {
     c_str_t s = c_vector_new_set_int8(size, size, '\0');
     c_vector_resize_int8(s, 0);

     return s;
 }

void c_str_compute_size(c_str_t str)
{
    c_uint64_t len;

    if (str) {
        len = strnlen(c_str_data(str), c_str_capacity(str));
        c_vector_resize_int8(str, len);
    }
}

C_STR_CMP(/* empty */,  c_str_t,    /* empty */)
C_STR_CMP(_voidp,       void*,      (c_str_t))

#undef C_STR_CMP

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/
