#ifndef C_VECTOR_SLICE_ITER_INCLUDED
#define C_VECTOR_SLICE_ITER_INCLUDED

#include "c_slice.h"
#include "c_vector.h"

#ifdef __cplusplus
extern "C" {
#endif

/** Safe iterator for slicing a vector. */
#define C_VECTOR_SLICE_ITER_TYPE(NAME) c_vector_slice_iter##NAME##_t
#define C_VECTOR_SLICE_ITER_TYPEDEF(NAME)                                      \
            C_TYPEDEF(C_VECTOR_SLICE_ITER_TYPE(NAME))

/** Increment iterator forward one step. */
#define C_VECTOR_SLICE_ITER_ADVANCE(NAME)                                      \
    extern void c_vector_slice_iter_advance##NAME(                             \
                    C_VECTOR_SLICE_ITER_TYPE(NAME) iter);

#define C_VECTOR_SLICE_ITER_FREE(NAME)                                         \
    extern void c_vector_slice_iter_free##NAME(                                \
                    C_VECTOR_SLICE_ITER_TYPE(NAME) *iter);

#define C_VECTOR_SLICE_ITER_FROM_CHARS(NAME)                                   \
    extern C_VECTOR_SLICE_ITER_TYPE(NAME) c_vector_slice_iter_from_chars##NAME(\
                                            C_VECTOR_TYPE(NAME) vector,        \
                                            const char *chars, c_uint64_t n,   \
                                            c_int64_t offset, c_bool_t closed);

#define C_VECTOR_SLICE_ITER_FROM_STR(NAME)                                     \
    extern C_VECTOR_SLICE_ITER_TYPE(NAME) c_vector_slice_iter_from_str##NAME(  \
                                            C_VECTOR_TYPE(NAME) vector,        \
                                            const c_str_t str,                 \
                                            c_int64_t offset, c_bool_t closed);

/** Get item at current iterator position without checking if state is valid.
 * @return Item at current iterator position. If invalid, then returns 0.
 * @see c_vector_iter_is_valid()
 */
#define C_VECTOR_SLICE_ITER_ITEM(NAME, TYPE)                                   \
    extern TYPE c_vector_slice_iter_item##NAME(                                \
                    C_VECTOR_SLICE_ITER_TYPE(NAME) iter);

/** Checks if iterator and vector are in valid states. Should be used
 * before item access.
 * @see c_vector_iter_item()
 */
#define C_VECTOR_SLICE_ITER_IS_VALID(NAME)                                     \
    extern c_bool_t c_vector_slice_iter_is_valid##NAME(                        \
                    C_VECTOR_SLICE_ITER_TYPE(NAME) iter);

#define C_VECTOR_SLICE_ITER_NEW(NAME)                                          \
    extern C_VECTOR_SLICE_ITER_TYPE(NAME) c_vector_slice_iter_new##NAME(       \
                                            C_VECTOR_TYPE(NAME) vector,        \
                                            c_slice_t slice);

/** @return Current iterator index position. */
#define C_VECTOR_SLICE_ITER_POSITION(NAME)                                     \
    extern c_uint64_t c_vector_slice_iter_position##NAME(                      \
                    C_VECTOR_SLICE_ITER_TYPE(NAME) iter);

C_TYPE_TEMPLATE_NAME(C_VECTOR_SLICE_ITER_TYPEDEF)
C_TYPE_TEMPLATE_NAME(C_VECTOR_SLICE_ITER_ADVANCE)
C_TYPE_TEMPLATE_NAME(C_VECTOR_SLICE_ITER_FREE)
C_TYPE_TEMPLATE_NAME(C_VECTOR_SLICE_ITER_FROM_CHARS)
C_TYPE_TEMPLATE_NAME(C_VECTOR_SLICE_ITER_FROM_STR)
C_TYPE_TEMPLATE_NAME(C_VECTOR_SLICE_ITER_IS_VALID)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_SLICE_ITER_ITEM)
C_TYPE_TEMPLATE_NAME(C_VECTOR_SLICE_ITER_NEW)
C_TYPE_TEMPLATE_NAME(C_VECTOR_SLICE_ITER_POSITION)

#undef C_VECTOR_SLICE_ITER_ADVANCE
#undef C_VECTOR_SLICE_ITER_FREE
#undef C_VECTOR_SLICE_ITER_FROM_CHARS
#undef C_VECTOR_SLICE_ITER_FROM_STR
#undef C_VECTOR_SLICE_ITER_ITEM
#undef C_VECTOR_SLICE_ITER_IS_VALID
#undef C_VECTOR_SLICE_ITER_NEW
#undef C_VECTOR_SLICE_ITER_POSITION

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/
