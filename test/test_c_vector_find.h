#ifndef TEST_C_VECTOR_FIND_INCLUDED
#define TEST_C_VECTOR_FIND_INCLUDED

#include "c_test.h"
#include "c_vector_find.h"
#include "c_memory.h"

#ifdef __cplusplus
extern "C" {
#endif

/** Does not really compare contents, but just addresses to handle void*. */
#define FIND_UDF(NAME, TYPE)                                                   \
c_int32_t test_c_vector_find_udf##NAME(const TYPE a, const TYPE b)             \
{                                                                              \
    if (a == b) {                                                              \
        return 0;                                                              \
    } else if ( a < b ) {                                                      \
        return -1;                                                             \
    } else {                                                                   \
        return 1;                                                              \
    }                                                                          \
}
C_TYPE_TEMPLATE_NAME_TYPE_PTR(FIND_UDF)
#undef FIND_UDF

void test_c_vector_find_bisect_insert_ptr()
{
    #define TEST(N, T)                                                         \
    c_uint64_t n##N = 3;                                                       \
    c_int64_t r##N = 0;                                                        \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
    T p1##N = NULL;                                                            \
    T p2##N = NULL;                                                            \
    T p3##N = NULL;                                                            \
    C_MEMORY_NEW(p2##N);                                                       \
    C_MEMORY_NEW(p3##N);                                                       \
                                                                               \
    r##N = c_vector_find_bisect_insert##N(NULL, 0);                            \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected -1 for null vector " #N);             \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT(c_vector_empty##N(v##N), "Failed empty " #N);                \
    r##N = c_vector_find_bisect_insert##N(v##N, 0);                            \
    C_TEST_ASSERT_EQ(0, r##N, "Expected 0 for empty vector " #N);              \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new_set##N(n##N, n##N, p1##N);                             \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT_EQ(n##N, c_vector_size##N(v##N), "Failed size " #N);         \
    c_vector_set##N(v##N, 2, p2##N);                                           \
    r##N = c_vector_find_bisect_insert##N(v##N, 0);                            \
    C_TEST_ASSERT_EQ(1, r##N, "Expected find to return 1 " #N);                \
    r##N = c_vector_find_bisect_insert##N(v##N, p2##N);                        \
    C_TEST_ASSERT_EQ(2, r##N, "Expected find to return 2 " #N);                \
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);                    \
    C_MEMORY_FREE(p2##N);                                                      \
    C_MEMORY_FREE(p3##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST)
    #undef TEST
}

void test_c_vector_find_bisect_insert_udf_ptr()
{
    #define TEST(N, T)                                                         \
    c_uint64_t n##N = 3;                                                       \
    c_int64_t r##N = 0;                                                        \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
    T p1##N = NULL;                                                            \
    T p2##N = NULL;                                                            \
    T p3##N = NULL;                                                            \
    C_MEMORY_NEW(p2##N);                                                       \
    C_MEMORY_NEW(p3##N);                                                       \
                                                                               \
    r##N = c_vector_find_bisect_insert_udf##N(NULL, 0, test_c_vector_find_udf##N);\
    C_TEST_ASSERT_EQ(-1, r##N, "Expected -1 for null vector " #N);             \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT(c_vector_empty##N(v##N), "Failed empty " #N);                \
    r##N = c_vector_find_bisect_insert_udf##N(v##N, 0, test_c_vector_find_udf##N);\
    C_TEST_ASSERT_EQ(0, r##N, "Expected 0 for empty vector " #N);              \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new_set##N(n##N, n##N, p1##N);                             \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT_EQ(n##N, c_vector_size##N(v##N), "Failed size " #N);         \
    c_vector_set##N(v##N, 2, p2##N);                                           \
    r##N = c_vector_find_bisect_insert_udf##N(v##N, 0, test_c_vector_find_udf##N);\
    C_TEST_ASSERT_EQ(1, r##N, "Expected find to return 1 " #N);                \
    r##N = c_vector_find_bisect_insert_udf##N(v##N, p2##N, test_c_vector_find_udf##N);\
    C_TEST_ASSERT_EQ(2, r##N, "Expected find to return 2 " #N);                \
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);                    \
    C_MEMORY_FREE(p2##N);                                                      \
    C_MEMORY_FREE(p3##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST)
    #undef TEST
}

void test_c_vector_find_bisect_insert_value()
{
    #define TEST(N, T)                                                         \
    c_uint64_t n##N = 4;                                                       \
    c_int64_t r##N = 0;                                                        \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
                                                                               \
    r##N = c_vector_find_bisect_insert##N(NULL, 0);                            \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected not found -1 in null vector " #N);    \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT(c_vector_empty##N(v##N), "Failed empty " #N);                \
    r##N = c_vector_find_bisect_insert##N(v##N, 0);                            \
    C_TEST_ASSERT_EQ(0, r##N, "Expected 0 for empty vector " #N);              \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new_set##N(n##N, n##N, 0);                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT_EQ(n##N, c_vector_size##N(v##N), "Failed size " #N);         \
    c_vector_set##N(v##N, 0, 10);                                              \
    c_vector_set##N(v##N, 1, 20);                                              \
    c_vector_set##N(v##N, 2, 30);                                              \
    c_vector_set##N(v##N, 3, 40);                                              \
                                                                               \
    r##N = c_vector_find_bisect_insert##N(v##N, 0);                            \
    C_TEST_ASSERT_EQ(0, r##N, "Expected find to return 0 for out of bound " #N);\
    r##N = c_vector_find_bisect_insert##N(v##N, 10);                           \
    C_TEST_ASSERT_EQ(0, r##N, "Expected find to return 0 for exact match " #N);\
    r##N = c_vector_find_bisect_insert##N(v##N, 15);                           \
    C_TEST_ASSERT_EQ(1, r##N, "Expected find to return 1 for intermediate " #N);\
    r##N = c_vector_find_bisect_insert##N(v##N, 20);                           \
    C_TEST_ASSERT_EQ(1, r##N, "Expected find to return 1 for exact match " #N);\
    r##N = c_vector_find_bisect_insert##N(v##N, 40);                           \
    C_TEST_ASSERT_EQ(3, r##N, "Expected find to return 3 for exact match  " #N);\
    r##N = c_vector_find_bisect_insert##N(v##N, 100);                          \
    C_TEST_ASSERT_EQ(4, r##N, "Expected find to return 4 for out of bound " #N);\
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_TYPE_VALUE(TEST)
    #undef TEST
}

void test_c_vector_find_bisect_ptr()
{
    #define TEST(N, T)                                                         \
    c_uint64_t n##N = 3;                                                       \
    c_int64_t r##N = 0;                                                        \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
    T p1##N = NULL;                                                            \
    T p2##N = NULL;                                                            \
    T p3##N = NULL;                                                            \
    C_MEMORY_NEW(p2##N);                                                       \
    C_MEMORY_NEW(p3##N);                                                       \
                                                                               \
    r##N = c_vector_find_bisect##N(NULL, 0);                                   \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected not found in null vector " #N);       \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT(c_vector_empty##N(v##N), "Failed empty " #N);                \
    r##N = c_vector_find_bisect##N(v##N, 0);                                   \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected not found in empty vector " #N);      \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new_set##N(n##N, n##N, p1##N);                             \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT_EQ(n##N, c_vector_size##N(v##N), "Failed size " #N);         \
    c_vector_set##N(v##N, 2, p2##N);                                           \
    r##N = c_vector_find_bisect##N(v##N, 0);                                   \
    C_TEST_ASSERT_EQ(1,  r##N, "Expected find to return 1 " #N);               \
    r##N = c_vector_find_bisect##N(v##N, p3##N);                               \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected find to return -1 " #N);              \
    r##N = c_vector_find_bisect##N(v##N, p2##N);                               \
    C_TEST_ASSERT_EQ(2,  r##N, "Expected find to return 2 " #N);               \
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);                    \
    C_MEMORY_FREE(p2##N);                                                      \
    C_MEMORY_FREE(p3##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST)
    #undef TEST
}

void test_c_vector_find_bisect_udf_ptr()
{
    #define TEST(N, T)                                                         \
    c_uint64_t n##N = 3;                                                       \
    c_int64_t r##N = 0;                                                        \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
    T p1##N = NULL;                                                            \
    T p2##N = NULL;                                                            \
    T p3##N = NULL;                                                            \
    C_MEMORY_NEW(p2##N);                                                       \
    C_MEMORY_NEW(p3##N);                                                       \
                                                                               \
    r##N = c_vector_find_bisect_udf##N(NULL, 0, test_c_vector_find_udf##N);    \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected not found in null vector " #N);       \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT(c_vector_empty##N(v##N), "Failed empty " #N);                \
    r##N = c_vector_find_bisect_udf##N(v##N, 0, test_c_vector_find_udf##N);    \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected not found in empty vector " #N);      \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new_set##N(n##N, n##N, p1##N);                             \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT_EQ(n##N, c_vector_size##N(v##N), "Failed size " #N);         \
    c_vector_set##N(v##N, 2, p2##N);                                           \
    r##N = c_vector_find_bisect_udf##N(v##N, 0, test_c_vector_find_udf##N);    \
    C_TEST_ASSERT_EQ(1,  r##N, "Expected find to return 1 " #N);               \
    r##N = c_vector_find_bisect_udf##N(v##N, p3##N, test_c_vector_find_udf##N);\
    C_TEST_ASSERT_EQ(-1, r##N, "Expected find to return -1 " #N);              \
    r##N = c_vector_find_bisect_udf##N(v##N, p2##N, test_c_vector_find_udf##N);\
    C_TEST_ASSERT_EQ(2,  r##N, "Expected find to return 2 " #N);               \
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);                    \
    C_MEMORY_FREE(p2##N);                                                      \
    C_MEMORY_FREE(p3##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST)
    #undef TEST
}

void test_c_vector_find_bisect_value()
{
    #define TEST(N, T)                                                         \
    c_uint64_t n##N = 4;                                                       \
    c_int64_t r##N = 0;                                                        \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
                                                                               \
    r##N = c_vector_find_bisect##N(NULL, 0);                                   \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected not found in null vector " #N);       \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT(c_vector_empty##N(v##N), "Failed empty " #N);                \
    r##N = c_vector_find_bisect##N(v##N, 0);                                   \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected not found in empty vector " #N);      \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new_set##N(n##N, n##N, 0);                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT_EQ(n##N, c_vector_size##N(v##N), "Failed size " #N);         \
    c_vector_set##N(v##N, 0, 1);                                               \
    c_vector_set##N(v##N, 1, 10);                                              \
    c_vector_set##N(v##N, 2, 20);                                              \
    c_vector_set##N(v##N, 3, 30);                                              \
    r##N = c_vector_find_bisect##N(v##N, -1);                                  \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected find to return -1 " #N);              \
    r##N = c_vector_find_bisect##N(v##N, 1);                                   \
    C_TEST_ASSERT_EQ(0,  r##N, "Expected find to return 0 " #N);               \
    r##N = c_vector_find_bisect##N(v##N, 5);                                   \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected find to return -1 " #N);              \
    r##N = c_vector_find_bisect##N(v##N, 10);                                  \
    C_TEST_ASSERT_EQ(1,  r##N, "Expected find to return 1 " #N);               \
    r##N = c_vector_find_bisect##N(v##N, 30);                                  \
    C_TEST_ASSERT_EQ(3,  r##N, "Expected find to return 3 " #N);               \
    r##N = c_vector_find_bisect##N(v##N, 100);                                 \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected find to return -1 " #N);              \
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_TYPE_VALUE(TEST)
    #undef TEST
}

void test_c_vector_find_many_ptr()
{
    #define TEST(N, T)                                                         \
    c_uint64_t n##N = 3;                                                       \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
    C_VECTOR_TYPE(N) d##N = NULL;                                              \
    c_vector_int64_t r##N = NULL;                                              \
    T p1##N = NULL;                                                            \
    T p2##N = NULL;                                                            \
    C_MEMORY_NEW(p1##N);                                                       \
    C_MEMORY_NEW(p2##N);                                                       \
                                                                               \
    r##N = c_vector_find_many##N(NULL, NULL);                                  \
    C_TEST_ASSERT_EQ_PTR(NULL, r##N, "Expected null " #N);                     \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    r##N = c_vector_find_many##N(v##N, NULL);                                  \
    C_TEST_ASSERT_EQ_PTR(NULL, r##N, "Expected null " #N);                     \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    d##N = c_vector_new##N(0);                                                 \
    r##N = c_vector_find_many##N(v##N, d##N);                                  \
    C_TEST_ASSERT_EQ(0, c_vector_size_int64(r##N), "Expected empty result " #N);\
    c_vector_free##N(& v##N);                                                  \
    c_vector_free_int64(& r##N);                                               \
                                                                               \
    v##N = c_vector_new_set##N(n##N, n##N, 0);                                 \
    c_vector_set##N(v##N, 1, p1##N);                                           \
    r##N = c_vector_find_many##N(v##N, d##N);                                  \
    C_TEST_ASSERT_EQ_PTR(0,     c_vector_get##N(v##N, 0), "Expected haystack value " #N);\
    C_TEST_ASSERT_EQ_PTR(p1##N, c_vector_get##N(v##N, 1), "Expected haystack value " #N);\
    C_TEST_ASSERT_EQ_PTR(0,     c_vector_get##N(v##N, 2), "Expected haystack value " #N);\
    C_TEST_ASSERT_EQ(0, c_vector_size_int64(r##N), "Expected empty result " #N);\
    c_vector_free_int64(& r##N);                                               \
                                                                               \
    c_vector_resize_set##N(d##N, n##N, 0);                                     \
    c_vector_set##N(d##N, 1, p2##N);                                           \
    c_vector_set##N(d##N, 2, p1##N);                                           \
    C_TEST_ASSERT_EQ_PTR(0,     c_vector_get##N(d##N, 0), "Expected needle value " #N);\
    C_TEST_ASSERT_EQ_PTR(p2##N, c_vector_get##N(d##N, 1), "Expected needle value " #N);\
    C_TEST_ASSERT_EQ_PTR(p1##N, c_vector_get##N(d##N, 2), "Expected needle value " #N);\
    r##N = c_vector_find_many##N(v##N, d##N);                                  \
    C_TEST_ASSERT_EQ(n##N, c_vector_size_int64(r##N), "Expected some results " #N);\
    C_TEST_ASSERT_EQ(0, c_vector_get_int64(r##N, 0), "Expected not-found at 0 " #N);\
    C_TEST_ASSERT_EQ(-1, c_vector_get_int64(r##N, 1), "Expected not-found at 1 " #N);\
    C_TEST_ASSERT_EQ(1,  c_vector_get_int64(r##N, 2), "Expected to find at 2 " #N);\
    c_vector_free##N(& v##N);                                                  \
    c_vector_free##N(& d##N);                                                  \
    c_vector_free_int64(& r##N);                                               \
    C_MEMORY_FREE(p1##N);                                                      \
    C_MEMORY_FREE(p2##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST)
    #undef TEST
}

void test_c_vector_find_many_udf_ptr()
{
    #define TEST(N, T)                                                         \
    c_uint64_t n##N = 3;                                                       \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
    C_VECTOR_TYPE(N) d##N = NULL;                                              \
    c_vector_int64_t r##N = NULL;                                              \
    T p1##N = NULL;                                                            \
    T p2##N = NULL;                                                            \
    C_MEMORY_NEW(p1##N);                                                       \
    C_MEMORY_NEW(p2##N);                                                       \
                                                                               \
    r##N = c_vector_find_many_udf##N(NULL, NULL, test_c_vector_find_udf##N);   \
    C_TEST_ASSERT_EQ_PTR(NULL, r##N, "Expected null " #N);                     \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    r##N = c_vector_find_many_udf##N(v##N, NULL, test_c_vector_find_udf##N);   \
    C_TEST_ASSERT_EQ_PTR(NULL, r##N, "Expected null " #N);                     \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    d##N = c_vector_new##N(0);                                                 \
    r##N = c_vector_find_many_udf##N(v##N, d##N, test_c_vector_find_udf##N);   \
    C_TEST_ASSERT_EQ(0, c_vector_size_int64(r##N), "Expected empty result " #N);\
    c_vector_free##N(& v##N);                                                  \
    c_vector_free_int64(& r##N);                                               \
                                                                               \
    v##N = c_vector_new_set##N(n##N, n##N, 0);                                 \
    c_vector_set##N(v##N, 1, p1##N);                                           \
    r##N = c_vector_find_many_udf##N(v##N, d##N, test_c_vector_find_udf##N);   \
    C_TEST_ASSERT_EQ_PTR(0,     c_vector_get##N(v##N, 0), "Expected haystack value " #N);\
    C_TEST_ASSERT_EQ_PTR(p1##N, c_vector_get##N(v##N, 1), "Expected haystack value " #N);\
    C_TEST_ASSERT_EQ_PTR(0,     c_vector_get##N(v##N, 2), "Expected haystack value " #N);\
    C_TEST_ASSERT_EQ(0, c_vector_size_int64(r##N), "Expected empty result " #N);\
    c_vector_free_int64(& r##N);                                               \
                                                                               \
    c_vector_resize_set##N(d##N, n##N, 0);                                     \
    c_vector_set##N(d##N, 1, p2##N);                                           \
    c_vector_set##N(d##N, 2, p1##N);                                           \
    C_TEST_ASSERT_EQ_PTR(0,     c_vector_get##N(d##N, 0), "Expected needle value " #N);\
    C_TEST_ASSERT_EQ_PTR(p2##N, c_vector_get##N(d##N, 1), "Expected needle value " #N);\
    C_TEST_ASSERT_EQ_PTR(p1##N, c_vector_get##N(d##N, 2), "Expected needle value " #N);\
    r##N = c_vector_find_many_udf##N(v##N, d##N, test_c_vector_find_udf##N);   \
    C_TEST_ASSERT_EQ(n##N, c_vector_size_int64(r##N), "Expected some results " #N);\
    C_TEST_ASSERT_EQ(0, c_vector_get_int64(r##N, 0), "Expected not-found at 0 " #N);\
    C_TEST_ASSERT_EQ(-1, c_vector_get_int64(r##N, 1), "Expected not-found at 1 " #N);\
    C_TEST_ASSERT_EQ(1,  c_vector_get_int64(r##N, 2), "Expected to find at 2 " #N);\
    c_vector_free##N(& v##N);                                                  \
    c_vector_free##N(& d##N);                                                  \
    c_vector_free_int64(& r##N);                                               \
    C_MEMORY_FREE(p1##N);                                                      \
    C_MEMORY_FREE(p2##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST)
    #undef TEST
}

void test_c_vector_find_many_value()
{
    #define TEST(N, T)                                                         \
    c_uint64_t n##N = 3;                                                       \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
    C_VECTOR_TYPE(N) d##N = NULL;                                              \
    c_vector_int64_t r##N = NULL;                                              \
                                                                               \
    r##N = c_vector_find_many##N(NULL, NULL);                                  \
    C_TEST_ASSERT_EQ_PTR(NULL, r##N, "Expected null " #N);                     \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    r##N = c_vector_find_many##N(v##N, NULL);                                  \
    C_TEST_ASSERT_EQ_PTR(NULL, r##N, "Expected null " #N);                     \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    d##N = c_vector_new##N(0);                                                 \
    r##N = c_vector_find_many##N(v##N, d##N);                                  \
    C_TEST_ASSERT_EQ(0, c_vector_size_int64(r##N), "Expected empty result " #N);\
    c_vector_free##N(& v##N);                                                  \
    c_vector_free_int64(& r##N);                                               \
                                                                               \
    v##N = c_vector_new_set##N(n##N, n##N, 0);                                 \
    c_vector_set##N(v##N, 0, 0);                                               \
    c_vector_set##N(v##N, 1, 1);                                               \
    c_vector_set##N(v##N, 2, 2);                                               \
    C_TEST_ASSERT_EQ(0, c_vector_get##N(v##N, 0), "Expected haystack value " #N);\
    C_TEST_ASSERT_EQ(1, c_vector_get##N(v##N, 1), "Expected haystack value " #N);\
    C_TEST_ASSERT_EQ(2, c_vector_get##N(v##N, 2), "Expected haystack value " #N);\
    r##N = c_vector_find_many##N(v##N, d##N);                                  \
    C_TEST_ASSERT_EQ(0, c_vector_size_int64(r##N), "Expected empty result " #N);\
    c_vector_free_int64(& r##N);                                               \
                                                                               \
    c_vector_resize##N(d##N, 3);                                               \
    c_vector_set##N(d##N, 0, 9);                                               \
    c_vector_set##N(d##N, 1, 2);                                               \
    c_vector_set##N(d##N, 2, 1);                                               \
    C_TEST_ASSERT_EQ(9, c_vector_get##N(d##N, 0), "Expected needle value " #N);\
    C_TEST_ASSERT_EQ(2, c_vector_get##N(d##N, 1), "Expected needle value " #N);\
    C_TEST_ASSERT_EQ(1, c_vector_get##N(d##N, 2), "Expected needle value " #N);\
    r##N = c_vector_find_many##N(v##N, d##N);                                  \
    C_TEST_ASSERT_EQ(n##N, c_vector_size_int64(r##N), "Expected some results " #N);\
    C_TEST_ASSERT_EQ(-1, c_vector_get_int64(r##N, 0), "Expected not-found " #N);\
    C_TEST_ASSERT_EQ(2, c_vector_get_int64(r##N, 1), "Expected to find " #N);  \
    C_TEST_ASSERT_EQ(1, c_vector_get_int64(r##N, 2), "Expected to find " #N);  \
    c_vector_free##N(& v##N);                                                  \
    c_vector_free##N(& d##N);                                                  \
    c_vector_free_int64(& r##N);

    C_TYPE_TEMPLATE_NAME_TYPE_VALUE(TEST)
    #undef TEST
}

void test_c_vector_find_ptr()
{
    #define TEST(N, T)                                                         \
    c_uint64_t n##N = 3;                                                       \
    c_int64_t r##N = 0;                                                        \
    T a##N;                                                                    \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
                                                                               \
    C_MEMORY_NEW(a##N);                                                        \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT(c_vector_empty##N(v##N), "Failed empty " #N);                \
    r##N = c_vector_find##N(v##N, a##N);                                       \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected not found in empty vector " #N);      \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new_set##N(n##N, n##N, 0);                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    c_vector_set##N(v##N, n##N-1, a##N);                                       \
    C_TEST_ASSERT_EQ_PTR(0, c_vector_get##N(v##N, 0), "Expected init value " #N);\
                                                                               \
    C_TEST_ASSERT_EQ_PTR(a##N, c_vector_get##N(v##N, n##N-1), "Expected set value " #N);\
    r##N = c_vector_find##N(v##N, a##N);                                       \
    C_TEST_ASSERT_EQ(n##N-1, r##N, "Expected find to return last index " #N);  \
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);                    \
    C_MEMORY_FREE(a##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST)
    #undef TEST
}

void test_c_vector_find_range_ptr()
{
    #define TEST(N, T)                                                         \
    c_uint64_t n##N = 5;                                                       \
    c_int64_t r##N = 0;                                                        \
    T a##N = NULL;                                                             \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
                                                                               \
    r##N = c_vector_find_range##N(NULL, a##N, 0, n##N);                        \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected not found in null vector " #N);       \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT(c_vector_empty##N(v##N), "Failed empty " #N);                \
    r##N = c_vector_find_range##N(v##N, a##N, 0, 0);                           \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected not found in empty vector " #N);      \
                                                                               \
    r##N = c_vector_find_range##N(v##N, a##N, 0, 10);                          \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected not found in empty vector " #N);      \
                                                                               \
    r##N = c_vector_find_range##N(v##N, a##N, 10, 100);                        \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected not found in empty vector " #N);      \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new_set##N(n##N, n##N, 0);                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
                                                                               \
    C_MEMORY_NEW(a##N);                                                        \
    c_vector_set##N(v##N, n##N-1, a##N);                                       \
    C_TEST_ASSERT_EQ_PTR(0, c_vector_get##N(v##N, 0), "Expected init value " #N);\
    C_TEST_ASSERT_EQ_PTR(a##N, c_vector_get##N(v##N, n##N-1), "Expected set value " #N);\
                                                                               \
    r##N = c_vector_find_range##N(v##N, a##N, 0, 0);                           \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected find nothing " #N);                   \
                                                                               \
    r##N = c_vector_find_range##N(v##N, a##N, 1, 3);                           \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected find nothing " #N);                   \
                                                                               \
    r##N = c_vector_find_range##N(v##N, a##N, 1, 55);                          \
    C_TEST_ASSERT_EQ(n##N-1, r##N, "Expected find to return last index " #N);  \
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);                    \
    C_MEMORY_FREE(a##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST)
    #undef TEST
}

void test_c_vector_find_range_value()
{
    #define TEST(N, T)                                                         \
    c_uint64_t n##N = 5;                                                       \
    c_int64_t r##N = 0;                                                        \
    T a##N = 9;                                                                \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
                                                                               \
    r##N = c_vector_find_range##N(NULL, a##N, 0, n##N);                        \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected not found in null vector " #N);       \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT(c_vector_empty##N(v##N), "Failed empty " #N);                \
    r##N = c_vector_find_range##N(v##N, a##N, 0, 0);                           \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected not found in empty vector " #N);      \
                                                                               \
    r##N = c_vector_find_range##N(v##N, a##N, 0, 10);                          \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected not found in empty vector " #N);      \
                                                                               \
    r##N = c_vector_find_range##N(v##N, a##N, 10, 100);                        \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected not found in empty vector " #N);      \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new_set##N(n##N, n##N, 0);                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    c_vector_set##N(v##N, n##N-1, a##N);                                       \
    C_TEST_ASSERT_EQ(0, c_vector_get##N(v##N, 0), "Expected init value " #N);  \
    C_TEST_ASSERT_EQ(a##N, c_vector_get##N(v##N, n##N-1), "Expected set value " #N);\
                                                                               \
    r##N = c_vector_find_range##N(v##N, a##N, 0, 0);                           \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected find nothing " #N);                   \
                                                                               \
    r##N = c_vector_find_range##N(v##N, a##N, 1, 3);                           \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected find nothing " #N);                   \
                                                                               \
    r##N = c_vector_find_range##N(v##N, a##N, 1, 55);                          \
    C_TEST_ASSERT_EQ(n##N-1, r##N, "Expected find to return last index " #N);  \
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_TYPE_VALUE(TEST)
    #undef TEST
}

void test_c_vector_find_value()
{
    #define TEST(N, T)                                                         \
    c_uint64_t n##N = 3;                                                       \
    c_int64_t r##N = 0;                                                        \
    T a##N = 9;                                                                \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT(c_vector_empty##N(v##N), "Failed empty " #N);                \
    r##N = c_vector_find##N(v##N, a##N);                                       \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected not found in empty vector " #N);      \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new_set##N(n##N, n##N, 0);                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    c_vector_set##N(v##N, n##N-1, a##N);                                       \
    C_TEST_ASSERT_EQ(0, c_vector_get##N(v##N, 0), "Expected init value " #N);  \
                                                                               \
    C_TEST_ASSERT_EQ(a##N, c_vector_get##N(v##N, n##N-1), "Expected set value " #N);\
    r##N = c_vector_find##N(v##N, a##N);                                       \
    C_TEST_ASSERT_EQ(n##N-1, r##N, "Expected find to return last index " #N);  \
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_TYPE_VALUE(TEST)
    #undef TEST
}

void test_c_vector_find_range_udf_ptr()
{
    #define TEST(N, T)                                                         \
    c_uint64_t n##N = 5;                                                       \
    c_int64_t r##N = 0;                                                        \
    T a##N = NULL;                                                             \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
                                                                               \
    r##N = c_vector_find_range_udf##N(NULL, a##N, 0, n##N, test_c_vector_find_udf##N);\
    C_TEST_ASSERT_EQ(-1, r##N, "Expected not found in null vector " #N);       \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT(c_vector_empty##N(v##N), "Failed empty " #N);                \
    r##N = c_vector_find_range_udf##N(v##N, a##N, 0, 0, test_c_vector_find_udf##N);\
    C_TEST_ASSERT_EQ(-1, r##N, "Expected not found in empty vector " #N);      \
                                                                               \
    r##N = c_vector_find_range_udf##N(v##N, a##N, 0, 10, test_c_vector_find_udf##N);\
    C_TEST_ASSERT_EQ(-1, r##N, "Expected not found in empty vector " #N);      \
                                                                               \
    r##N = c_vector_find_range_udf##N(v##N, a##N, 10, 100, test_c_vector_find_udf##N);\
    C_TEST_ASSERT_EQ(-1, r##N, "Expected not found in empty vector " #N);      \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new_set##N(n##N, n##N, 0);                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
                                                                               \
    C_MEMORY_NEW(a##N);                                                        \
    c_vector_set##N(v##N, n##N-1, a##N);                                       \
    C_TEST_ASSERT_EQ_PTR(0, c_vector_get##N(v##N, 0), "Expected init value " #N);\
    C_TEST_ASSERT_EQ_PTR(a##N, c_vector_get##N(v##N, n##N-1), "Expected set value " #N);\
                                                                               \
    r##N = c_vector_find_range_udf##N(v##N, a##N, 0, 0, test_c_vector_find_udf##N);\
    C_TEST_ASSERT_EQ(-1, r##N, "Expected find nothing " #N);                   \
                                                                               \
    r##N = c_vector_find_range_udf##N(v##N, a##N, 1, 3, test_c_vector_find_udf##N);\
    C_TEST_ASSERT_EQ(-1, r##N, "Expected find nothing " #N);                   \
                                                                               \
    r##N = c_vector_find_range_udf##N(v##N, a##N, 1, 55, test_c_vector_find_udf##N);\
    C_TEST_ASSERT_EQ(n##N-1, r##N, "Expected find to return last index " #N);  \
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);                    \
    C_MEMORY_FREE(a##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST)
    #undef TEST
}

void test_c_vector_find_safe_ptr()
{
    #define TEST(N, T)                                                         \
    c_uint64_t n##N = 3;                                                       \
    c_int64_t r##N = 0;                                                        \
    T p##N = NULL;                                                             \
    C_MEMORY_NEW(p##N);                                                        \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
                                                                               \
    r##N = c_vector_find_safe##N(NULL, NULL);                                  \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected not found in null vector " #N);       \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT(c_vector_empty##N(v##N), "Failed empty " #N);                \
    r##N = c_vector_find_safe##N(v##N, NULL);                                  \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected not found NULL in empty vector " #N); \
    r##N = c_vector_find_safe##N(v##N, p##N);                                  \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected not found non-null in empty vector " #N);\
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new_set##N(n##N, n##N, 0);                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    c_vector_set##N(v##N, n##N-1, p##N);                                       \
    C_TEST_ASSERT_EQ_PTR(0, c_vector_get##N(v##N, 0), "Expected init value " #N);\
    C_TEST_ASSERT_EQ_PTR(p##N, c_vector_get##N(v##N, n##N-1), "Expected set value " #N);\
    r##N = c_vector_find_safe##N(v##N, p##N);                                  \
    C_TEST_ASSERT_EQ(n##N-1, r##N, "Expected find to return last index " #N);  \
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);                    \
    C_MEMORY_FREE(p##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST)
    #undef TEST
}

void test_c_vector_find_safe_value()
{
    #define TEST(N, T)                                                         \
    c_uint64_t n##N = 3;                                                       \
    c_int64_t r##N = 0;                                                        \
    T a##N = 9;                                                                \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
                                                                               \
    r##N = c_vector_find_safe##N(NULL, a##N);                                  \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected not found in null vector " #N);       \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT(c_vector_empty##N(v##N), "Failed empty " #N);                \
    r##N = c_vector_find_safe##N(v##N, a##N);                                  \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected not found in empty vector " #N);      \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new_set##N(n##N, n##N, 0);                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    c_vector_set##N(v##N, n##N-1, a##N);                                       \
    C_TEST_ASSERT_EQ(0, c_vector_get##N(v##N, 0), "Expected init value " #N);  \
                                                                               \
    C_TEST_ASSERT_EQ(a##N, c_vector_get##N(v##N, n##N-1), "Expected set value " #N);\
    r##N = c_vector_find_safe##N(v##N, a##N);                                  \
    C_TEST_ASSERT_EQ(n##N-1, r##N, "Expected find to return last index " #N);  \
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_TYPE_VALUE(TEST)
    #undef TEST
}

void test_c_vector_find_udf_ptr()
{
    #define TEST(N, T)                                                         \
    c_uint64_t n##N = 3;                                                       \
    c_int64_t r##N = 0;                                                        \
    T a##N = NULL;                                                             \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT(c_vector_empty##N(v##N), "Failed empty " #N);                \
    r##N = c_vector_find_udf##N(v##N, a##N, test_c_vector_find_udf##N);        \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected not found in empty vector " #N);      \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new_set##N(n##N, n##N, 0);                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_MEMORY_NEW(a##N);                                                        \
    c_vector_set##N(v##N, n##N-1, a##N);                                       \
    C_TEST_ASSERT_EQ_PTR(0, c_vector_get##N(v##N, 0), "Expected init value " #N);\
                                                                               \
    C_TEST_ASSERT_EQ_PTR(a##N, c_vector_get##N(v##N, n##N-1), "Expected set value " #N);\
    r##N = c_vector_find_udf##N(v##N, a##N, test_c_vector_find_udf##N);        \
    C_TEST_ASSERT_EQ(n##N-1, r##N, "Expected find to return last index " #N);  \
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);                    \
    C_MEMORY_FREE(a##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST)
    #undef TEST
}

void test_c_vector_find_udf_safe_ptr()
{
    #define TEST(N, T)                                                         \
    c_uint64_t n##N = 3;                                                       \
    c_int64_t r##N = 0;                                                        \
    T a##N = NULL;                                                             \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
                                                                               \
    r##N = c_vector_find_udf_safe##N(NULL, a##N, test_c_vector_find_udf##N);   \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected not found in null vector " #N);       \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT(c_vector_empty##N(v##N), "Failed empty " #N);                \
    r##N = c_vector_find_udf_safe##N(v##N, a##N, test_c_vector_find_udf##N);   \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected not found in empty vector " #N);      \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new_set##N(n##N, n##N, 0);                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_MEMORY_NEW(a##N);                                                        \
    c_vector_set##N(v##N, n##N-1, a##N);                                       \
    C_TEST_ASSERT_EQ_PTR(0, c_vector_get##N(v##N, 0), "Expected init value " #N);\
                                                                               \
    C_TEST_ASSERT_EQ_PTR(a##N, c_vector_get##N(v##N, n##N-1), "Expected set value " #N);\
    r##N = c_vector_find_udf_safe##N(v##N, a##N, test_c_vector_find_udf##N);   \
    C_TEST_ASSERT_EQ(n##N-1, r##N, "Expected find to return last index " #N);  \
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);                    \
    C_MEMORY_FREE(a##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST)
    #undef TEST
}

void test_c_vector_rfind_ptr()
{
    #define TEST(N, T)                                                         \
    c_uint64_t n##N = 3;                                                       \
    c_int64_t r##N = 0;                                                        \
    T a##N;                                                                    \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
                                                                               \
    C_MEMORY_NEW(a##N);                                                        \
    r##N = c_vector_rfind##N(NULL, a##N);                                      \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected not found in null vector " #N);       \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT(c_vector_empty##N(v##N), "Failed empty " #N);                \
    r##N = c_vector_rfind##N(v##N, a##N);                                      \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected not found in empty vector " #N);      \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new_set##N(n##N, n##N, 0);                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    c_vector_set##N(v##N, n##N-1, a##N);                                       \
    C_TEST_ASSERT_EQ_PTR(0, c_vector_get##N(v##N, 0), "Expected init value " #N);\
                                                                               \
    C_TEST_ASSERT_EQ_PTR(a##N, c_vector_get##N(v##N, n##N-1), "Expected set value " #N);\
    r##N = c_vector_rfind##N(v##N, a##N);                                      \
    C_TEST_ASSERT_EQ(n##N-1, r##N, "Expected find to return last index " #N);  \
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);                    \
    C_MEMORY_FREE(a##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST)
    #undef TEST
}

void test_c_vector_rfind_udf_ptr()
{
    #define TEST(N, T)                                                         \
    c_uint64_t n##N = 3;                                                       \
    c_int64_t r##N = 0;                                                        \
    T a##N = NULL;                                                             \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
                                                                               \
    r##N = c_vector_rfind_udf##N(NULL, a##N, test_c_vector_find_udf##N);       \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected not found in null vector " #N);       \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT(c_vector_empty##N(v##N), "Failed empty " #N);                \
    r##N = c_vector_rfind_udf##N(v##N, a##N, test_c_vector_find_udf##N);       \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected not found in empty vector " #N);      \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new_set##N(n##N, n##N, 0);                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_MEMORY_NEW(a##N);                                                        \
    c_vector_set##N(v##N, n##N-1, a##N);                                       \
    C_TEST_ASSERT_EQ_PTR(0, c_vector_get##N(v##N, 0), "Expected init value " #N);\
                                                                               \
    C_TEST_ASSERT_EQ_PTR(a##N, c_vector_get##N(v##N, n##N-1), "Expected set value " #N);\
    r##N = c_vector_rfind_udf##N(v##N, a##N, test_c_vector_find_udf##N);       \
    C_TEST_ASSERT_EQ(n##N-1, r##N, "Expected find to return last index " #N);  \
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);                    \
    C_MEMORY_FREE(a##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST)
    #undef TEST
}

void test_c_vector_rfind_value()
{
    #define TEST(N, T)                                                         \
    c_uint64_t n##N = 3;                                                       \
    c_int64_t r##N = 0;                                                        \
    T a##N = 9;                                                                \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
                                                                               \
    r##N = c_vector_rfind##N(NULL, a##N);                                      \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected not found in null vector " #N);       \
                                                                               \
    v##N = c_vector_new##N(0);                                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    C_TEST_ASSERT(c_vector_empty##N(v##N), "Failed empty " #N);                \
    r##N = c_vector_rfind##N(v##N, a##N);                                      \
    C_TEST_ASSERT_EQ(-1, r##N, "Expected not found in empty vector " #N);      \
    c_vector_free##N(& v##N);                                                  \
                                                                               \
    v##N = c_vector_new_set##N(n##N, n##N, 0);                                 \
    C_TEST_ASSERT(v##N, "Failed to create vector " #N);                        \
    c_vector_set##N(v##N, n##N-1, a##N);                                       \
    C_TEST_ASSERT_EQ(0, c_vector_get##N(v##N, 0), "Expected init value " #N);  \
                                                                               \
    C_TEST_ASSERT_EQ(a##N, c_vector_get##N(v##N, n##N-1), "Expected set value " #N);\
    r##N = c_vector_rfind##N(v##N, a##N);                                      \
    C_TEST_ASSERT_EQ(n##N-1, r##N, "Expected rfind to return last index " #N); \
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    C_TEST_ASSERT_FALSE(v##N, "Failed to free vector " #N);

    C_TYPE_TEMPLATE_NAME_TYPE_VALUE(TEST)
    #undef TEST
}

int suite_test_c_vector_find()
{
    C_TEST_ADD_SUITE("suite_test_c_vector_find", NULL, NULL);

    C_TEST_ADD_TEST(test_c_vector_find_ptr);
    C_TEST_ADD_TEST(test_c_vector_find_value);

    C_TEST_ADD_TEST(test_c_vector_find_safe_ptr);
    C_TEST_ADD_TEST(test_c_vector_find_safe_value);

    C_TEST_ADD_TEST(test_c_vector_find_range_ptr);
    C_TEST_ADD_TEST(test_c_vector_find_range_value);

    C_TEST_ADD_TEST(test_c_vector_find_udf_ptr);
    C_TEST_ADD_TEST(test_c_vector_find_udf_safe_ptr);

    C_TEST_ADD_TEST(test_c_vector_find_range_udf_ptr);

    C_TEST_ADD_TEST(test_c_vector_find_many_ptr);
    C_TEST_ADD_TEST(test_c_vector_find_many_value);

    C_TEST_ADD_TEST(test_c_vector_find_many_udf_ptr);

    C_TEST_ADD_TEST(test_c_vector_rfind_ptr);
    C_TEST_ADD_TEST(test_c_vector_rfind_value);

    C_TEST_ADD_TEST(test_c_vector_rfind_udf_ptr);

    C_TEST_ADD_TEST(test_c_vector_find_bisect_ptr);
    C_TEST_ADD_TEST(test_c_vector_find_bisect_value);

    C_TEST_ADD_TEST(test_c_vector_find_bisect_udf_ptr);

    C_TEST_ADD_TEST(test_c_vector_find_bisect_insert_ptr);
    C_TEST_ADD_TEST(test_c_vector_find_bisect_insert_value);

    C_TEST_ADD_TEST(test_c_vector_find_bisect_insert_udf_ptr);

    C_TEST_RETURN();
}

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/