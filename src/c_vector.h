#ifndef C_VECTOR_INCLUDED
#define C_VECTOR_INCLUDED

#include "c_type.h"

#ifdef __cplusplus
extern "C" {
#endif

/** c_vector is a dynamic array of contiguous memory.
 * Even with 100% extra capacity, c_vector will use less memory than a
 * linked list, because each list node needs pointers (next, [prev,] data).
 */
#define C_VECTOR_TYPE(NAME) c_vector##NAME##_t
#define C_VECTOR_TYPEDEF(NAME) C_TYPEDEF(C_VECTOR_TYPE(NAME))

/** Add item and doubles capacity if more storage is needed.
    @return True if capacity was increased.
 */
#define C_VECTOR_APPEND(NAME, TYPE)                                            \
    extern c_bool_t c_vector_append##NAME(C_VECTOR_TYPE(NAME) vector, TYPE item);

/** Add item with custom capacity growth rate.
    @param rate If negative, then used as absolute size, otherwise use as
                scale factor (i.e. 2.0 means double capacity).
    @eturn True if capacity was increased.
 */
#define C_VECTOR_APPEND_RATE(NAME, TYPE)                                       \
    extern c_bool_t c_vector_append_rate##NAME(C_VECTOR_TYPE(NAME) vector,     \
                                               TYPE item, c_float32_t rate);

#define C_VECTOR_CAPACITY(NAME, TYPE)                                          \
    extern c_uint64_t c_vector_capacity##NAME(C_VECTOR_TYPE(NAME) vector);

/** Set size to zero, do not change capacity and do not deallocate items. */
#define C_VECTOR_CLEAR(NAME, TYPE)                                             \
    extern void c_vector_clear##NAME(C_VECTOR_TYPE(NAME) vector);

/** Shrink storage memory to fit just the currently stored items. */
#define C_VECTOR_COMPACT(NAME, TYPE)                                           \
    extern void c_vector_compact##NAME(C_VECTOR_TYPE(NAME) vector);

/** Copy items and resize destination if necessary.
 *  Runtime: O(n)
 */
#define C_VECTOR_COPY(NAME, TYPE)                                              \
    extern void c_vector_copy##NAME(C_VECTOR_TYPE(NAME) to,                    \
                                    C_VECTOR_TYPE(NAME) from);

/** Copy items and resize destination if necessary.
 *  Runtime: O(n)
 */
#define C_VECTOR_COPY_UDF(NAME, TYPE)                                          \
    extern void c_vector_copy_udf##NAME(C_VECTOR_TYPE(NAME) to,                \
                                           C_VECTOR_TYPE(NAME) from,           \
                                           c_funp_voidp_voidp_t copy_udf);

#define C_VECTOR_EMPTY(NAME, TYPE)                                             \
    extern c_bool_t c_vector_empty##NAME(C_VECTOR_TYPE(NAME) vector);

/** @return True if same length and all items are equal, or both are NULL. */
#define C_VECTOR_EQUALS(NAME, TYPE)                                            \
    extern c_bool_t c_vector_equals##NAME(C_VECTOR_TYPE(NAME) vector1,         \
                                          C_VECTOR_TYPE(NAME) vector2);

/** Compare with user defined function.
 * @param cmp_udf Return 0 if inputs are equal, and non-zero if not equal.
 * @return True if same length and all items are equal, or both are NULL.
 */
#define C_VECTOR_EQUALS_UDF(NAME, TYPE)                                        \
    extern c_bool_t c_vector_equals_udf##NAME(C_VECTOR_TYPE(NAME) vector1,     \
                                              C_VECTOR_TYPE(NAME) vector2,     \
                                              c_funp_int32_voidp_voidp_t cmp_udf);

/** Appends copy of vector items onto another.
    @return True if destination capacity increased.
 */
#define C_VECTOR_EXTEND(NAME, TYPE)                                            \
    extern c_bool_t c_vector_extend##NAME(C_VECTOR_TYPE(NAME) to,              \
                                          C_VECTOR_TYPE(NAME) from);

#define C_VECTOR_FIRST(NAME, TYPE)                                             \
    extern TYPE c_vector_first##NAME(C_VECTOR_TYPE(NAME) vector);

/** Free memory allocated by vector and the vector pointer itself.
 * @code
 * c_vector_t v = c_vector_new(0);
 * c_vector_free(& v);
 * @endcode
 */
#define C_VECTOR_FREE(NAME, TYPE)                                              \
    extern void c_vector_free##NAME(C_VECTOR_TYPE(NAME) *vector);

#define C_VECTOR_FREE_DEEP(NAME, TYPE)                                         \
    extern void c_vector_free_deep##NAME(C_VECTOR_TYPE(NAME) *vector);

#define C_VECTOR_FREE_DEEP_UDF(NAME, TYPE)                                     \
    extern void c_vector_free_deep_udf##NAME(C_VECTOR_TYPE(NAME) *vector,      \
                                                c_funp_void_voidp_t free_fun);

/** Unsafe get does not check if state or index are valid.
 * This may lead to segfault or invalid memory access.
 */
#define C_VECTOR_GET(NAME, TYPE)                                               \
    extern TYPE c_vector_get##NAME(C_VECTOR_TYPE(NAME) vector, c_uint64_t index);

/** Checks if internal state is valid and index is less than size.
 * @return 0 if vector or index is invalid.
 */
#define C_VECTOR_GET_SAFE(NAME, TYPE)                                          \
    extern TYPE c_vector_get_safe##NAME(C_VECTOR_TYPE(NAME) vector,            \
                                        c_uint64_t index);

#define C_VECTOR_ITEMS(NAME, TYPE)                                             \
    extern TYPE* c_vector_items##NAME(C_VECTOR_TYPE(NAME) vector);

/** Insert item into 0-based index position, and shift subsequent items.
 * Memory reallocation may occur if capacity is insufficient.
 */
#define C_VECTOR_INSERT(NAME, TYPE)                                            \
    extern void c_vector_insert##NAME(C_VECTOR_TYPE(NAME) vector,              \
                                      c_uint64_t index, TYPE item);

/** Insert item into sorted vector and shift subsequent items.
 * Memory reallocation may occur if capacity is insufficient.
 */
#define C_VECTOR_INSERT_BISECT(NAME, TYPE)                                     \
    extern void c_vector_insert_bisect##NAME(C_VECTOR_TYPE(NAME) vector,       \
                                             TYPE item);

/** Insert item into sorted vector and shift subsequent items.
 * Memory reallocation may occur if capacity is insufficient.
 * @ param cmp_udf Comparison function that returns 0 if equal, -1 if less,
 *                 1 if greater than.
 */
#define C_VECTOR_INSERT_BISECT_UDF(NAME, TYPE)                                 \
    extern void c_vector_insert_bisect_udf##NAME(                              \
                        C_VECTOR_TYPE(NAME) vector,                            \
                        TYPE item,                                             \
                        c_funp_int32_voidp_voidp_t cmp_udf);

#define C_VECTOR_LAST(NAME, TYPE)                                              \
    extern TYPE c_vector_last##NAME(C_VECTOR_TYPE(NAME) vector);

/** Apply a user-defined function to each item with an optional closure. */
#define C_VECTOR_MAP(NAME, TYPE)                                               \
    extern void c_vector_map##NAME(C_VECTOR_TYPE(NAME) vector,                 \
                                   void (*apply)(TYPE item, void *closure),    \
                                   void *closure);

#define C_VECTOR_NEW(NAME, TYPE)                                               \
    extern C_VECTOR_TYPE(NAME) c_vector_new##NAME(c_uint64_t capacity);

#define C_VECTOR_NEW_SET(NAME, TYPE)                                           \
    extern C_VECTOR_TYPE(NAME) c_vector_new_set##NAME(c_uint64_t capacity,     \
                                                      c_uint64_t size,         \
                                                      TYPE value);

/** Construct and copy input array of items. */
#define C_VECTOR_NEW_COPY(NAME, TYPE)                                          \
    extern C_VECTOR_TYPE(NAME) c_vector_new_copy##NAME(c_uint64_t capacity,    \
                                                       c_uint64_t size,        \
                                                       TYPE *items);

/** @return New vector with copy of input vector items. */
#define C_VECTOR_NEW_COPY_VECTOR(NAME, TYPE)                                   \
    extern C_VECTOR_TYPE(NAME) c_vector_new_copy_vector##NAME(                 \
                                    C_VECTOR_TYPE(NAME) vector);

/** Print to stdout size, capacity and space-delimited values for debugging
 * with newline appended. */
#define C_VECTOR_PRINT(NAME)                                                   \
    extern void c_vector_print##NAME(C_VECTOR_TYPE(NAME) vector);

/** Remove item at 0-based index, shift and resize vector to preserve order.
 * Runtime: O(n)
 * @return item
 * @see c_vector_remove_unordered() for faster alternative.
 */
#define C_VECTOR_REMOVE(NAME, TYPE)                                            \
    extern TYPE c_vector_remove##NAME(C_VECTOR_TYPE(NAME) vector,              \
                                       c_uint64_t index);

/** Remove item at first index (0), shift and resize vector.
 * Runtime: O(n)
 * @return item
 */
#define C_VECTOR_REMOVE_FIRST(NAME, TYPE)                                      \
    extern TYPE c_vector_remove_first##NAME(C_VECTOR_TYPE(NAME) vector);

/** Remove item at last index (size-1), and do not resize vector.
 * Runtime: O(1)
 * @return item
 */
#define C_VECTOR_REMOVE_LAST(NAME, TYPE)                                       \
    extern TYPE c_vector_remove_last##NAME(C_VECTOR_TYPE(NAME) vector);

/** Remove item at 0-based index by swapping with last item and reducing size
 * by one.
 * Runtime: O(1)
 * @return item
 */
#define C_VECTOR_REMOVE_UNORDERED(NAME, TYPE)                                  \
    extern TYPE c_vector_remove_unordered##NAME(C_VECTOR_TYPE(NAME) vector,    \
                                       c_uint64_t index);

/** Allocate enough storage to hold capacity items.
 * Runtime: O(n)
 */
#define C_VECTOR_RESERVE(NAME, TYPE)                                           \
    extern void c_vector_reserve##NAME(C_VECTOR_TYPE(NAME) vector,             \
                                       c_uint64_t capacity);

/** Increase or decrease number of items.
 * Allocates more storage only if capacity is exceeded.
 * Does not deallocate if new size is smaller.
 * New item values are undefined.
 * Runtime: O(n)
 */
#define C_VECTOR_RESIZE(NAME, TYPE)                                            \
    extern void c_vector_resize##NAME(C_VECTOR_TYPE(NAME) vector,              \
                                      c_uint64_t size);

/** Resize and assign data if increasing size.
    @see c_vector_resize()
 */
#define C_VECTOR_RESIZE_SET(NAME, TYPE)                                        \
    extern void c_vector_resize_set##NAME(C_VECTOR_TYPE(NAME) vector,          \
                                          c_uint64_t size, TYPE item);

/** Reverse items inplace.
 * Runtime: O(n)
 */
#define C_VECTOR_REVERSE(NAME, TYPE)                                           \
    extern void c_vector_reverse##NAME(C_VECTOR_TYPE(NAME) vector);

#define C_VECTOR_SET(NAME, TYPE)                                               \
    extern void c_vector_set##NAME(C_VECTOR_TYPE(NAME) vector,                 \
                                   c_uint64_t index, TYPE item);

#define C_VECTOR_SET_SAFE(NAME, TYPE)                                          \
    extern void c_vector_set_safe##NAME(C_VECTOR_TYPE(NAME) vector,            \
                                        c_uint64_t index, TYPE item);

#define C_VECTOR_SIZE(NAME, TYPE)                                              \
    extern c_uint64_t c_vector_size##NAME(C_VECTOR_TYPE(NAME) vector);

/** Swap item arrays between two vectors.
 *  Runtime: O(1)
 */
#define C_VECTOR_SWAP(NAME, TYPE)                                              \
    extern void c_vector_swap##NAME(C_VECTOR_TYPE(NAME) vector1,               \
                                    C_VECTOR_TYPE(NAME) vector2);

/** Swap two items.
 *  Runtime: O(1)
 */
#define C_VECTOR_SWAP_AT(NAME, TYPE)                                           \
    extern void c_vector_swap_at##NAME(C_VECTOR_TYPE(NAME) vector,             \
                                    c_uint64_t index1, c_uint64_t index2);

#define C_VECTOR_TIMESTAMP(NAME, TYPE)                                         \
    extern c_uint64_t c_vector_timestamp##NAME(C_VECTOR_TYPE(NAME) vector);

C_TYPE_TEMPLATE_NAME(C_VECTOR_TYPEDEF)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_APPEND)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_APPEND_RATE)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_CAPACITY)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_CLEAR)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_COMPACT)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_COPY)
C_TYPE_TEMPLATE_NAME_TYPE_PTR(C_VECTOR_COPY_UDF)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_EMPTY)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_EQUALS)
C_TYPE_TEMPLATE_NAME_TYPE_PTR(C_VECTOR_EQUALS_UDF)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_EXTEND)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_FIRST)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_FREE)
C_TYPE_TEMPLATE_NAME_TYPE_PTR(C_VECTOR_FREE_DEEP)
C_TYPE_TEMPLATE_NAME_TYPE_PTR(C_VECTOR_FREE_DEEP_UDF)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_GET)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_GET_SAFE)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_INSERT)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_INSERT_BISECT)
C_TYPE_TEMPLATE_NAME_TYPE_PTR(C_VECTOR_INSERT_BISECT_UDF)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_ITEMS)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_LAST)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_MAP)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_NEW)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_NEW_COPY)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_NEW_COPY_VECTOR)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_NEW_SET)
C_TYPE_TEMPLATE_NAME_VALUE(C_VECTOR_PRINT)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_REMOVE)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_REMOVE_FIRST)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_REMOVE_LAST)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_REMOVE_UNORDERED)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_RESERVE)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_RESIZE)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_RESIZE_SET)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_REVERSE)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_SET)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_SET_SAFE)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_SIZE)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_SWAP)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_SWAP_AT)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_TIMESTAMP)

#undef C_VECTOR_APPEND
#undef C_VECTOR_APPEND_RATE
#undef C_VECTOR_CAPACITY
#undef C_VECTOR_CLEAR
#undef C_VECTOR_COMPACT
#undef C_VECTOR_COPY
#undef C_VECTOR_COPY_UDF
#undef C_VECTOR_EMPTY
#undef C_VECTOR_EQUALS
#undef C_VECTOR_EQUALS_UDF
#undef C_VECTOR_EXTEND
#undef C_VECTOR_FIRST
#undef C_VECTOR_FREE
#undef C_VECTOR_FREE_DEEP
#undef C_VECTOR_FREE_DEEP_UDF
#undef C_VECTOR_GET
#undef C_VECTOR_GET_SAFE
#undef C_VECTOR_INSERT
#undef C_VECTOR_INSERT_BISECT
#undef C_VECTOR_INSERT_BISECT_UDF
#undef C_VECTOR_ITEMS
#undef C_VECTOR_LAST
#undef C_VECTOR_MAP
#undef C_VECTOR_NEW
#undef C_VECTOR_NEW_COPY
#undef C_VECTOR_NEW_COPY_VECTOR
#undef C_VECTOR_NEW_SET
#undef C_VECTOR_PRINT
#undef C_VECTOR_REMOVE
#undef C_VECTOR_REMOVE_FIRST
#undef C_VECTOR_REMOVE_LAST
#undef C_VECTOR_REMOVE_UNORDERED
#undef C_VECTOR_RESERVE
#undef C_VECTOR_RESIZE
#undef C_VECTOR_RESIZE_SET
#undef C_VECTOR_REVERSE
#undef C_VECTOR_SET
#undef C_VECTOR_SET_SAFE
#undef C_VECTOR_SIZE
#undef C_VECTOR_SWAP
#undef C_VECTOR_SWAP_AT
#undef C_VECTOR_TIMESTAMP

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/