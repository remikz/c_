#ifndef C_JSON_READ_INCLUDED
#define C_JSON_READ_INCLUDED

#include "c_json.h"
#include "c_str.h"
#include "c_vector.h"

#ifdef __cplusplus
extern "C" {
#endif

C_TYPEDEF(c_json_read_t)

/** Read JSON from n char array.
 * @param[out] read_info Will be internally allocated and caller should free it.
 * @see c_json_read_free().
 */
extern c_json_t c_json_read_chars(c_json_read_t *read_info, const char *json, c_uint64_t n);
/** Read JSON text file.
 * @param[out] read_info Will be internally allocated and caller should free it.
 * @see c_json_read_free().
 */
extern c_json_t c_json_read_file(c_json_read_t *read_info, const char *file_name);
extern void     c_json_read_free(c_json_read_t *read_info);
/** Read JSON from string.
 * @param[out] read_info Will be internally allocated and caller should free it.
 * @see c_json_read_free().
 */
extern c_json_t c_json_read_str(c_json_read_t *read_info, c_str_t json);

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/