#ifndef C_JSON_WRITE_IMPL_INCLUDED
#define C_JSON_WRITE_IMPL_INCLUDED

#include "c_json_write.h"

#ifdef __cplusplus
extern "C" {
#endif

struct c_json_write_options_t
{
    /* Print each item on a new line. */
    c_bool_t array_multiline;
    /* Vertically align array square brackets on own line. */
    c_uint8_t bracket_align;
    /* Number of spaces after each colon delimiter. */
    c_uint8_t colon_indent;
    /* Number of spaces after each comma delimiter. */
    c_uint8_t comma_indent;
    /* Vertically align object curly braces on own line.*/
    c_bool_t curly_align;
    /* Number of spaces per left-margin indentation. */
    c_uint8_t indent;
    /* Sort object keys. */
    c_bool_t sort_keys;
};

c_str_t c_json_writes(c_json_t json, c_json_write_options_t options)
{
    c_str_t result;

    return result;
}

#ifdef __cplusplus
}
#endif

#endif
/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/