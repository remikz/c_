#ifndef TEST_C_KEYVALUE_HASH_INCLUDED
#define TEST_C_KEYVALUE_HASH_INCLUDED

#include "c_test.h"
#include "c_keyvalue_hash.h"

void test_c_keyvalue_hash_cmp_numeric()
{
    #define TEST(KN, KT, VN, VT)                                               \
    c_int32_t r##KN##VN;                                                       \
    c_hash_t h##KN##VN;                                                        \
    KT k##KN##VN;                                                              \
    C_KEYVALUE_HASH_TYPE(KN, VN) kv##KN##VN;                                   \
                                                                               \
    r##KN##VN = C_KEYVALUE_HASH_CMP_NAME(KN, VN)(0, 0, NULL);                  \
    C_TEST_ASSERT_EQ(-1, r##KN##VN, "Expected null cmp.");                     \
                                                                               \
    kv##KN##VN = c_keyvalue_hash_new##KN##VN(1, 11, NULL);                     \
                                                                               \
    r##KN##VN = C_KEYVALUE_HASH_CMP_NAME(KN, VN)(1, 0, kv##KN##VN);            \
    C_TEST_ASSERT_EQ(r##KN##VN, -1, "Expected cmp <0.");                       \
    r##KN##VN = C_KEYVALUE_HASH_CMP_NAME(KN, VN)(1, 22, kv##KN##VN);           \
    C_TEST_ASSERT_EQ(r##KN##VN, 1, "Expected cmp >0.");                        \
    r##KN##VN = C_KEYVALUE_HASH_CMP_NAME(KN, VN)(2, 11, kv##KN##VN);           \
    C_TEST_ASSERT_EQ(r##KN##VN, -1, "Expected cmp -1.");                       \
    r##KN##VN = C_KEYVALUE_HASH_CMP_NAME(KN, VN)(1, 11, kv##KN##VN);           \
    C_TEST_ASSERT_EQ(r##KN##VN, 0, "Expected cmp =0.");                        \
                                                                               \
    c_keyvalue_hash_free##KN##VN(& kv##KN##VN);

    C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE_NUMERIC(TEST)
    #undef TEST
}

void test_c_keyvalue_hash_cmp_str()
{
    c_hash_t h = 789;
    c_hash_t h2 = 9999;
    c_str_t k = c_str_new("akey", 4);
    c_str_t k2 = c_str_new("akey2", 5);
    int v = 1234;
    c_int32_t r = -1;

    c_keyvalue_hash_str_t kv = c_keyvalue_hash_new_str(h, k, & v);
    c_keyvalue_hash_str_t kv2 = c_keyvalue_hash_new_str(h2, k2, & v);

    r = c_keyvalue_hash_cmp_str(h, k, kv);
    C_TEST_ASSERT_EQ(r, 0, "Expected equal.");

    r = c_keyvalue_hash_cmp_str(h2, k2, kv2);
    C_TEST_ASSERT_EQ(r, 0, "Expected equal.");

    r = c_keyvalue_hash_cmp_str(h2, k, kv);
    C_TEST_ASSERT_NEQ(r, 0, "Expected hash not equal.");

    r = c_keyvalue_hash_cmp_str(h, k2, kv2);
    C_TEST_ASSERT_NEQ(r, 0, "Expected hash not equal.");

    r = c_keyvalue_hash_cmp_str(h2, k, kv2);
    C_TEST_ASSERT_NEQ(r, 0, "Expected key not equal.");

    c_keyvalue_hash_free_str(& kv);
    c_keyvalue_hash_free_str(& kv2);
    c_str_free(& k);
    c_str_free(& k2);
}

void test_c_keyvalue_hash_hash()
{
    #define TEST(KN, KT, VN, VT)                                               \
    C_KEYVALUE_HASH_TYPE(KN, VN) kv##KN##VN = NULL;                            \
    KT hash##KN##VN = 0;                                                       \
                                                                               \
    kv##KN##VN = c_keyvalue_hash_new##KN##VN(hash##KN##VN, 0, 0);              \
    C_TEST_ASSERT(kv##KN##VN, "Expected new.");                                \
                                                                               \
    C_TEST_ASSERT_EQ(c_keyvalue_hash_hash##KN##VN(kv##KN##VN),                 \
                     hash##KN##VN, "Expected key.");                           \
                                                                               \
    c_keyvalue_hash_free##KN##VN(& kv##KN##VN);

    C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE_NUMERIC(TEST)
    #undef TEST
}

void test_c_keyvalue_hash_hash_str()
{
    c_str_t key = c_str_new("key", 3);
    c_hash_t hash = 1;
    c_keyvalue_hash_str_t kv = c_keyvalue_hash_new_str(hash, key, 0);

    C_TEST_ASSERT_EQ(hash, c_keyvalue_hash_hash_str(kv),
                     "Expected hash.");

    c_keyvalue_hash_free_str(& kv);
    c_str_free(& key);
}

void test_c_keyvalue_hash_key()
{
    #define TEST(KN, KT, VN, VT)                                               \
    C_KEYVALUE_HASH_TYPE(KN, VN) kv##KN##VN = NULL;                            \
    KT key##KN##VN = 0;                                                        \
                                                                               \
    kv##KN##VN = c_keyvalue_hash_new##KN##VN(0, key##KN##VN, 0);               \
    C_TEST_ASSERT(kv##KN##VN, "Expected new.");                                \
                                                                               \
    C_TEST_ASSERT_EQ(c_keyvalue_hash_key##KN##VN(kv##KN##VN),                  \
                     key##KN##VN, "Expected key.");                            \
                                                                               \
    c_keyvalue_hash_free##KN##VN(& kv##KN##VN);

    C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE_NUMERIC(TEST)
    #undef TEST
}

void test_c_keyvalue_hash_key_ptr()
{
    #define TEST(KN, KT, VN, VT)                                               \
    C_KEYVALUE_HASH_TYPE(KN, VN) kv##KN##VN = NULL;                            \
    KT key##KN##VN;                                                            \
    C_MEMORY_NEW(key##KN##VN);                                                 \
                                                                               \
    kv##KN##VN = c_keyvalue_hash_new##KN##VN(0, key##KN##VN, 0);               \
    C_TEST_ASSERT(kv##KN##VN, "Expected new.");                                \
                                                                               \
    C_TEST_ASSERT_EQ_PTR(c_keyvalue_hash_key##KN##VN(kv##KN##VN),              \
                         key##KN##VN, "Expected key.");                        \
                                                                               \
    c_keyvalue_hash_free##KN##VN(& kv##KN##VN);                                \
    C_MEMORY_FREE(key##KN##VN);

    C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE_PTR(TEST)
    #undef TEST
}

void test_c_keyvalue_hash_key_str()
{
    c_str_t key = c_str_new("key", 3);
    c_keyvalue_hash_str_t kv = c_keyvalue_hash_new_str(0, key, 0);

    C_TEST_ASSERT_EQ(c_str_cmp(c_keyvalue_hash_key_str(kv), key),
                     0, "Expected key.");

    c_keyvalue_hash_free_str(& kv);
    c_str_free(& key);
}

void test_c_keyvalue_hash_new()
{
    #define TEST(KN, KT, VN, VT)                                               \
    C_KEYVALUE_HASH_TYPE(KN, VN) kv##KN##VN = NULL;                            \
                                                                               \
    kv##KN##VN = c_keyvalue_hash_new##KN##VN(0, 0, 0);                         \
    C_TEST_ASSERT(kv##KN##VN, "Expected new.");                                \
                                                                               \
    c_keyvalue_hash_free##KN##VN(& kv##KN##VN);                                \
    C_TEST_ASSERT_EQ_PTR(kv##KN##VN, NULL, "Expected free.");                  \
                                                                               \
    c_keyvalue_hash_free##KN##VN(NULL);

    C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE_NUMERIC(TEST)
    C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE_PTR(TEST)
    #undef TEST
}

void test_c_keyvalue_hash_new_str()
{
    c_hash_t h = 789;
    c_str_t k = c_str_new("akey", 4);
    int v = 1234;

    c_keyvalue_hash_str_t kv = c_keyvalue_hash_new_str(h, k, & v);
    C_TEST_ASSERT(kv, "Expected new.");

    c_keyvalue_hash_free_str(& kv);
    C_TEST_ASSERT_FALSE(kv, "Expected free.");

    c_str_free(& k);
}

void test_c_keyvalue_hash_value()
{
    #define TEST(KN, KT, VN, VT)                                               \
    C_KEYVALUE_HASH_TYPE(KN, VN) kv##KN##VN = NULL;                            \
                                                                               \
    kv##KN##VN = c_keyvalue_hash_new##KN##VN(0, 0, 0);                         \
    C_TEST_ASSERT(kv##KN##VN, "Expected new.");                                \
                                                                               \
    C_TEST_ASSERT_EQ_PTR(c_keyvalue_hash_value##KN##VN(kv##KN##VN),            \
                         0, "Expected value.");                                \
                                                                               \
    c_keyvalue_hash_free##KN##VN(& kv##KN##VN);

    C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(TEST)
    #undef TEST
}

void test_c_keyvalue_hash_value_set()
{
    #define TEST(KN, KT, VN, VT)                                               \
    C_KEYVALUE_HASH_TYPE(KN, VN) kv##KN##VN = NULL;                            \
                                                                               \
    kv##KN##VN = c_keyvalue_hash_new##KN##VN(0, 0, 0);                         \
    C_TEST_ASSERT(kv##KN##VN, "Expected new.");                                \
                                                                               \
    c_keyvalue_hash_value_set##KN##VN(kv##KN##VN, 0);                          \
    C_TEST_ASSERT_EQ_PTR(c_keyvalue_hash_value##KN##VN(kv##KN##VN),            \
                         0, "Expected value.");                                \
                                                                               \
    c_keyvalue_hash_free##KN##VN(& kv##KN##VN);

    C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(TEST)
    #undef TEST
}

int suite_test_c_keyvalue_hash()
{
    C_TEST_ADD_SUITE("suite_test_c_keyvalue_hash", NULL, NULL);

    C_TEST_ADD_TEST(test_c_keyvalue_hash_new);
    C_TEST_ADD_TEST(test_c_keyvalue_hash_new_str);
    C_TEST_ADD_TEST(test_c_keyvalue_hash_hash);
    C_TEST_ADD_TEST(test_c_keyvalue_hash_hash_str);
    C_TEST_ADD_TEST(test_c_keyvalue_hash_key);
    C_TEST_ADD_TEST(test_c_keyvalue_hash_key_ptr);
    C_TEST_ADD_TEST(test_c_keyvalue_hash_key_str);
    C_TEST_ADD_TEST(test_c_keyvalue_hash_value);
    C_TEST_ADD_TEST(test_c_keyvalue_hash_value_set);
    C_TEST_ADD_TEST(test_c_keyvalue_hash_cmp_numeric);
    C_TEST_ADD_TEST(test_c_keyvalue_hash_cmp_str);

    C_TEST_RETURN();
}

#endif
/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/