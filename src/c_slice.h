#ifndef C_SLICE_INCLUDED
#define C_SLICE_INCLUDED

#include "c_type.h"
#include "c_str.h"
#include "c_vector.h"

#ifdef __cplusplus
extern "C" {
#endif

/** Slice is a composition of a start and end index with stride,
 * to sample subsets of one dimensional arrays.
 * Follows half-open [start, end) index semantics, which means
 * that if end=10 in a 10 item array, then 9 will be the last sampled index.
 * start=-1 means last index, but end=-1 is the penultimate index.
 * Negative stride is supported too, but is invalid if start < end.
 */
C_TYPEDEF(c_slice_t);

/** Compute closed range 0-based index parameters to sample a vector.
 * @param[in] size Target sample set size that will clamp range to valid values.
 * @param[out] first Inclusive start index.
 * @param[out] last Inclusive end index.
 * @param[out] step Stride skip, positive or negative.
 * @param[out] n Number of items that would result from sampling.
 */
extern void      c_slice_compute_range(c_slice_t slice,
                                       c_int64_t size,
                                       c_int64_t *first,
                                       c_int64_t *last,
                                       c_int64_t *step,
                                       c_uint64_t *n);
/** Copy and allocate a new slice instance.
 * @return Will not return NULL even if input is NULL.
 */
extern c_slice_t c_slice_copy(c_slice_t slice);
extern c_int64_t c_slice_end(c_slice_t slice);
extern void      c_slice_free(c_slice_t *slice);
/** Parse a string with format "[start][:,][end][:,][stride]"
 * @param n Length of input string.
 * @param offset start,end values will add this value. Use for fortran indexing.
 * @param delim Character that delimits values, such as colon or comma.
 * @param closed If the end value is inclusive (closed) or exclusive (open).
 * @return New slice that caller must deallocate. Never returns NULL.
 */
extern c_slice_t c_slice_from_chars(const char *str,
                                    c_uint64_t n,
                                    c_int64_t offset,
                                    c_bool_t closed);
/** @see c_slice_from_chars() */
extern c_slice_t c_slice_from_str(const c_str_t str,
                                  c_int64_t offset,
                                  c_bool_t closed);
extern c_slice_t c_slice_new_start(c_int64_t start);
extern c_slice_t c_slice_new_start_end(c_int64_t start, c_int64_t end);
extern c_slice_t c_slice_new_start_end_stride(c_int64_t start,
                                              c_int64_t end,
                                              c_int64_t stride);
extern c_slice_t c_slice_new_start_stride(c_int64_t start, c_int64_t stride);
extern c_slice_t c_slice_new_stride(c_int64_t stride);
extern c_slice_t c_slice_new_end(c_int64_t end);
extern c_slice_t c_slice_new_end_stride(c_int64_t end, c_int64_t stride);
extern c_int64_t c_slice_start(c_slice_t slice);
extern c_int64_t c_slice_stride(c_slice_t slice);
extern void      c_slice_to_chars(c_slice_t slice, char *str, c_uint64_t n);
extern c_str_t   c_slice_to_str(c_slice_t slice);

/** Convert slice into expanded vector of 0-based increasing or decreasing indices.
 * @param size Cardinality for sequence being sliced.
 * @return Vector with unsigned integers or empty. Will never return NULL.
 */
extern c_vector_uint64_t c_slice_to_vector(c_slice_t slice, c_uint64_t size);

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/
