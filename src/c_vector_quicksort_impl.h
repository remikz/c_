#ifndef C_VECTOR_QUICKSORT_IMPL_INCLUDED
#define C_VECTOR_QUICKSORT_IMPL_INCLUDED

#include "c_vector.h"
#include "c_vector_factory.h"

#ifdef __cplusplus
extern "C" {
#endif

#define SWAP(A, I, J, TMP) {                                                   \
            (TMP) = (A)[(I)];                                                  \
            (A)[(I)] = (A)[(J)];                                               \
            (A)[(J)] = (TMP);                                                  \
        }

#define C_VECTOR_QUICKSORT(NAME, TYPE)                                         \
void c_vector_quicksort##NAME(C_VECTOR_TYPE(NAME) vector)                      \
{                                                                              \
    if (!c_vector_empty##NAME(vector)) {                                       \
        c_vector_quicksort_recurse##NAME(                                      \
            c_vector_items##NAME(vector),                                      \
            0,                                                                 \
            c_vector_size##NAME(vector) - 1);                                  \
    }                                                                          \
}

#define C_VECTOR_QUICKSORT_INDICES(NAME, TYPE)                                 \
c_vector_uint64_t c_vector_quicksort_indices##NAME(                            \
                        const C_VECTOR_TYPE(NAME) vector)                      \
{                                                                              \
    c_vector_uint64_t result;                                                  \
    c_uint64_t size;                                                           \
    C_VECTOR_TYPE(NAME) copy;                                                  \
                                                                               \
    if (!c_vector_empty##NAME(vector)) {                                       \
        size = c_vector_size##NAME(vector);                                    \
        copy = c_vector_new_copy_vector##NAME(vector);                         \
        result = c_vector_range_uint64(0, size, 1);                            \
                                                                               \
        c_vector_quicksort_indices_recurse##NAME(                              \
            c_vector_items##NAME(copy),                                        \
            0,                                                                 \
            size - 1,                                                          \
            c_vector_items_uint64(result));                                    \
                                                                               \
        c_vector_free##NAME(& copy);                                           \
    } else {                                                                   \
        result = c_vector_new_uint64(0);                                       \
    }                                                                          \
                                                                               \
    return result;                                                             \
}

void* c_vector_quicksort_indices_median_of_three(
        void **items,
        c_uint64_t lo,
        c_uint64_t hi,
        c_uint64_t *indices,
        c_funp_int32_cvoidp_cvoidp_t cmp)
{
    c_uint64_t mid = (lo + hi) / 2;
    void *tmp;
    c_uint64_t tmp2;

    if (cmp(items[hi], items[lo]) < 0) {
        SWAP(items, lo, hi, tmp);
        SWAP(indices, lo, hi, tmp2);
    }
    if (cmp(items[mid], items[lo]) < 0) {
        SWAP(items, mid, lo, tmp);
        SWAP(indices, mid, lo, tmp2);
    }
    if (cmp(items[hi], items[mid]) < 0) {
        SWAP(items, hi, mid, tmp);
        SWAP(indices, hi, mid, tmp2);
    }

    return items[mid];
}

c_uint64_t c_vector_quicksort_indices_partition(
        void **items,
        c_uint64_t lo,
        c_uint64_t hi,
        c_uint64_t *indices,
        c_funp_int32_cvoidp_cvoidp_t cmp)
{
    void *pivot = c_vector_quicksort_indices_median_of_three(
                    items, lo, hi, indices, cmp);
    c_uint64_t i = lo - 1;
    c_uint64_t j = hi + 1;
    void *tmp;
    c_uint64_t tmp2;

    while (1) {
        while (1) {
            --j;
            if (cmp(items[j], pivot) <= 0) {
                break;
            }
        }
        while (1) {
            ++i;
            if (cmp(items[i], pivot) >= 0) {
                break;
            }
        }
        if (i < j) {
            SWAP(items, i, j, tmp);
            SWAP(indices, i, j, tmp2);
        } else {
            SWAP(items, lo, j, tmp);
            SWAP(indices, lo, j, tmp2);
            return j;
        }
    }
}

void c_vector_quicksort_indices_recurse(
        void **items,
        c_uint64_t lo,
        c_uint64_t hi,
        c_uint64_t *indices,
        c_funp_int32_cvoidp_cvoidp_t cmp)
{
    c_uint64_t pivot;

    if (lo < hi) {
        pivot = c_vector_quicksort_indices_partition(
                    items, lo, hi, indices, cmp);
        c_vector_quicksort_indices_recurse(items, lo, pivot, indices, cmp);
        c_vector_quicksort_indices_recurse(items, pivot + 1, hi, indices, cmp);
    }
}

c_vector_uint64_t c_vector_quicksort_indices(
                        const c_vector_t vector,
                        c_funp_int32_cvoidp_cvoidp_t cmp)
{
    c_vector_uint64_t result;
    c_uint64_t size;
    c_vector_t copy;

    if (!c_vector_empty(vector)) {
        size = c_vector_size(vector);
        copy = c_vector_new_copy_vector(vector);
        result = c_vector_range_uint64(0, size, 1);

        c_vector_quicksort_indices_recurse(
            c_vector_items(copy),
            0,
            size - 1,
            c_vector_items_uint64(result),
            cmp);

        c_vector_free(& copy);
    } else {
        result = c_vector_new_uint64(0);
    }

    return result;
}

#define C_VECTOR_QUICKSORT_INDICES_MEDIAN_OF_THREE(NAME, TYPE)                 \
TYPE c_vector_quicksort_indices_median_of_three##NAME(                         \
        TYPE *items,                                                           \
        c_uint64_t lo,                                                         \
        c_uint64_t hi,                                                         \
        c_uint64_t *indices)                                                   \
{                                                                              \
    c_uint64_t mid = (lo + hi) / 2;                                            \
    TYPE tmp;                                                                  \
    c_uint64_t tmp2;                                                           \
                                                                               \
    if (items[hi] < items[lo]) {                                               \
        SWAP(items, lo, hi, tmp);                                              \
        SWAP(indices, lo, hi, tmp2);                                           \
    }                                                                          \
    if (items[mid] < items[lo]) {                                              \
        SWAP(items, mid, lo, tmp);                                             \
        SWAP(indices, mid, lo, tmp2);                                          \
    }                                                                          \
    if (items[hi] < items[mid]) {                                              \
        SWAP(items, hi, mid, tmp);                                             \
        SWAP(indices, hi, mid, tmp2);                                          \
    }                                                                          \
    return items[mid];                                                         \
}

#define C_VECTOR_QUICKSORT_INDICES_MEDIAN_OF_THREE_PTR(NAME, TYPE)             \
TYPE c_vector_quicksort_indices_median_of_three##NAME(                         \
        TYPE *items,                                                           \
        c_uint64_t lo,                                                         \
        c_uint64_t hi,                                                         \
        c_uint64_t *indices)                                                   \
{                                                                              \
    c_uint64_t mid = (lo + hi) / 2;                                            \
    TYPE tmp;                                                                  \
    c_uint64_t tmp2;                                                           \
                                                                               \
    if (*(items[hi]) < *(items[lo])) {                                         \
        SWAP(items, lo, hi, tmp);                                              \
        SWAP(indices, lo, hi, tmp2);                                           \
    }                                                                          \
    if (*(items[mid]) < *(items[lo])) {                                        \
        SWAP(items, mid, lo, tmp);                                             \
        SWAP(indices, mid, lo, tmp2);                                          \
    }                                                                          \
    if (*(items[hi]) < *(items[mid])) {                                        \
        SWAP(items, hi, mid, tmp);                                             \
        SWAP(indices, hi, mid, tmp2);                                          \
    }                                                                          \
    return items[mid];                                                         \
}

#define C_VECTOR_QUICKSORT_INDICES_PARTITION(NAME, TYPE)                       \
c_uint64_t c_vector_quicksort_indices_partition##NAME(                         \
        TYPE *items,                                                           \
        c_uint64_t lo,                                                         \
        c_uint64_t hi,                                                         \
        c_uint64_t *indices)                                                   \
{                                                                              \
    TYPE pivot = c_vector_quicksort_indices_median_of_three##NAME(             \
                    items, lo, hi, indices);                                   \
    c_uint64_t i = lo - 1;                                                     \
    c_uint64_t j = hi + 1;                                                     \
    TYPE tmp;                                                                  \
    c_uint64_t tmp2;                                                           \
                                                                               \
    while (1) {                                                                \
        while (1) {                                                            \
            --j;                                                               \
            if (items[j] <= pivot) {                                           \
                break;                                                         \
            }                                                                  \
        }                                                                      \
        while (1) {                                                            \
            ++i;                                                               \
            if (items[i] >= pivot) {                                           \
                break;                                                         \
            }                                                                  \
        }                                                                      \
        if (i < j) {                                                           \
            SWAP(items, i, j, tmp);                                            \
            SWAP(indices, i, j, tmp2);                                         \
        } else {                                                               \
            SWAP(items, lo, j, tmp);                                           \
            SWAP(indices, lo, j, tmp2);                                        \
            return j;                                                          \
        }                                                                      \
    }                                                                          \
}

#define C_VECTOR_QUICKSORT_INDICES_PARTITION_PTR(NAME, TYPE)                   \
c_uint64_t c_vector_quicksort_indices_partition##NAME(                         \
        TYPE *items,                                                           \
        c_uint64_t lo,                                                         \
        c_uint64_t hi,                                                         \
        c_uint64_t *indices)                                                   \
{                                                                              \
    TYPE pivot = c_vector_quicksort_indices_median_of_three##NAME(             \
                    items, lo, hi, indices);                                   \
    c_uint64_t i = lo - 1;                                                     \
    c_uint64_t j = hi + 1;                                                     \
    TYPE tmp;                                                                  \
    c_uint64_t tmp2;                                                           \
                                                                               \
    while (1) {                                                                \
        while (1) {                                                            \
            --j;                                                               \
            if (*items[j] <= *pivot) {                                         \
                break;                                                         \
            }                                                                  \
        }                                                                      \
        while (1) {                                                            \
            ++i;                                                               \
            if (*items[i] >= *pivot) {                                         \
                break;                                                         \
            }                                                                  \
        }                                                                      \
        if (i < j) {                                                           \
            SWAP(items, i, j, tmp);                                            \
            SWAP(indices, i, j, tmp2);                                         \
        } else {                                                               \
            SWAP(items, lo, j, tmp);                                           \
            SWAP(indices, lo, j, tmp2);                                        \
            return j;                                                          \
        }                                                                      \
    }                                                                          \
}

#define C_VECTOR_QUICKSORT_INDICES_RECURSE(NAME, TYPE)                         \
void c_vector_quicksort_indices_recurse##NAME(                                 \
        TYPE *items,                                                           \
        c_uint64_t lo,                                                         \
        c_uint64_t hi,                                                         \
        c_uint64_t *indices)                                                   \
{                                                                              \
    c_uint64_t pivot;                                                          \
                                                                               \
    if (lo < hi) {                                                             \
        pivot = c_vector_quicksort_indices_partition##NAME(                    \
                    items, lo, hi, indices);                                   \
        c_vector_quicksort_indices_recurse##NAME(items, lo, pivot, indices);   \
        c_vector_quicksort_indices_recurse##NAME(items, pivot + 1, hi, indices);\
    }                                                                          \
}

#define C_VECTOR_QUICKSORT_ITERATIVE_RUN(NAME, TYPE)                           \
void c_vector_quicksort_iterative_run##NAME(                                   \
        TYPE *items,                                                           \
        c_uint64_t lo,                                                         \
        c_uint64_t hi)                                                         \
{                                                                              \
    c_uint64_t pivot;                                                          \
    c_vector_uint64_t stack = c_vector_new_uint64(64);                         \
    c_vector_append_uint64(stack, hi);                                         \
    c_vector_append_uint64(stack, lo);                                         \
                                                                               \
    while (c_vector_size_uint64(stack)) {                                      \
        lo = c_vector_remove_last_uint64(stack);                               \
        hi = c_vector_remove_last_uint64(stack);                               \
                                                                               \
        if (lo < hi) {                                                         \
            pivot = c_vector_quicksort_partition##NAME(items, lo, hi);         \
            c_vector_append_uint64(stack, hi);                                 \
            c_vector_append_uint64(stack, pivot + 1);                          \
            /* Do left side first.*/                                           \
            c_vector_append_uint64(stack, pivot);                              \
            c_vector_append_uint64(stack, lo);                                 \
        }                                                                      \
    }                                                                          \
                                                                               \
    c_vector_free_uint64(& stack);                                             \
}

#define C_VECTOR_QUICKSORT_ITERATIVE(NAME, TYPE)                               \
void c_vector_quicksort_iterative##NAME(C_VECTOR_TYPE(NAME) vector)            \
{                                                                              \
    if (!c_vector_empty##NAME(vector)) {                                       \
        c_vector_quicksort_iterative_run##NAME(                                \
            c_vector_items##NAME(vector),                                      \
            0,                                                                 \
            c_vector_size##NAME(vector) - 1);                                  \
    }                                                                          \
}

#define C_VECTOR_QUICKSORT_MEDIAN_OF_THREE(NAME, TYPE)                         \
TYPE c_vector_quicksort_median_of_three##NAME(                                 \
        TYPE *items,                                                           \
        c_uint64_t lo,                                                         \
        c_uint64_t hi)                                                         \
{                                                                              \
    c_uint64_t mid = (lo + hi) / 2;                                            \
    TYPE tmp;                                                                  \
                                                                               \
    if (items[hi] < items[lo]) {                                               \
        SWAP(items, lo, hi, tmp);                                              \
    }                                                                          \
    if (items[mid] < items[lo]) {                                              \
        SWAP(items, mid, lo, tmp);                                             \
    }                                                                          \
    if (items[hi] < items[mid]) {                                              \
        SWAP(items, hi, mid, tmp);                                             \
    }                                                                          \
    return items[mid];                                                         \
}

#define C_VECTOR_QUICKSORT_MEDIAN_OF_THREE_PTR(NAME, TYPE)                     \
TYPE c_vector_quicksort_median_of_three##NAME(                                 \
        TYPE *items,                                                           \
        c_uint64_t lo,                                                         \
        c_uint64_t hi)                                                         \
{                                                                              \
    c_uint64_t mid = (lo + hi) / 2;                                            \
    TYPE tmp;                                                                  \
                                                                               \
    if (*(items[hi]) < *(items[lo])) {                                         \
        SWAP(items, lo, hi, tmp);                                              \
    }                                                                          \
    if (*(items[mid]) < *(items[lo])) {                                        \
        SWAP(items, mid, lo, tmp);                                             \
    }                                                                          \
    if (*(items[hi]) < *(items[mid])) {                                        \
        SWAP(items, hi, mid, tmp);                                             \
    }                                                                          \
    return items[mid];                                                         \
}

void* c_vector_quicksort_median_of_three(
        void **items,
        c_uint64_t lo,
        c_uint64_t hi,
        c_funp_int32_cvoidp_cvoidp_t cmp)
{
    c_uint64_t mid = (lo + hi) / 2;
    void *tmp;

    if (cmp(items[hi], items[lo]) < 0) {
        SWAP(items, lo, hi, tmp);
    }
    if (cmp(items[mid], items[lo]) < 0) {
        SWAP(items, mid, lo, tmp);
    }
    if (cmp(items[hi], items[mid]) < 0) {
        SWAP(items, hi, mid, tmp);
    }

    return items[mid];
}

#define C_VECTOR_QUICKSORT_PARTITION(NAME, TYPE)                               \
c_uint64_t c_vector_quicksort_partition##NAME(                                 \
        TYPE *items,                                                           \
        c_uint64_t lo,                                                         \
        c_uint64_t hi)                                                         \
{                                                                              \
    TYPE pivot = c_vector_quicksort_median_of_three##NAME(items, lo, hi);      \
    c_uint64_t i = lo - 1;                                                     \
    c_uint64_t j = hi + 1;                                                     \
    TYPE tmp;                                                                  \
                                                                               \
    while (1) {                                                                \
        while (1) {                                                            \
            --j;                                                               \
            if (items[j] <= pivot) {                                           \
                break;                                                         \
            }                                                                  \
        }                                                                      \
        while (1) {                                                            \
            ++i;                                                               \
            if (items[i] >= pivot) {                                           \
                break;                                                         \
            }                                                                  \
        }                                                                      \
        if (i < j) {                                                           \
            SWAP(items, i, j, tmp);                                            \
        } else {                                                               \
            SWAP(items, lo, j, tmp);                                           \
            return j;                                                          \
        }                                                                      \
    }                                                                          \
}

c_uint64_t c_vector_quicksort_partition(
    void **items,
    c_uint64_t lo,
    c_uint64_t hi,
    c_funp_int32_cvoidp_cvoidp_t cmp)
{
    void *pivot = c_vector_quicksort_median_of_three(
                            items, lo, hi, cmp);
    c_uint64_t i = lo - 1;
    c_uint64_t j = hi + 1;
    void *tmp;

    while (1) {
        while (1) {
            --j;
            if (cmp(items[j], pivot) <= 0) {
                break;
            }
        }
        while (1) {
            ++i;
            if (cmp(items[i], pivot) >= 0) {
                break;
            }
        }
        if (i < j) {
            SWAP(items, i, j, tmp);
        } else {
            SWAP(items, lo, j, tmp);
            return j;
        }
    }
}

#define C_VECTOR_QUICKSORT_PARTITION_PTR(NAME, TYPE)                           \
c_uint64_t c_vector_quicksort_partition##NAME(                                 \
        TYPE *items,                                                           \
        c_uint64_t lo,                                                         \
        c_uint64_t hi)                                                         \
{                                                                              \
    TYPE pivot = c_vector_quicksort_median_of_three##NAME(                     \
                            items, lo, hi);                                    \
    c_uint64_t i = lo - 1;                                                     \
    c_uint64_t j = hi + 1;                                                     \
    TYPE tmp;                                                                  \
                                                                               \
    while (1) {                                                                \
        while (1) {                                                            \
            --j;                                                               \
            if (*items[j] <= *pivot) {                                         \
                break;                                                         \
            }                                                                  \
        }                                                                      \
        while (1) {                                                            \
            ++i;                                                               \
            if (*items[i] >= *pivot) {                                         \
                break;                                                         \
            }                                                                  \
        }                                                                      \
        if (i < j) {                                                           \
            SWAP(items, i, j, tmp);                                            \
        } else {                                                               \
            SWAP(items, lo, j, tmp);                                           \
            return j;                                                          \
        }                                                                      \
    }                                                                          \
}

#define C_VECTOR_QUICKSORT_RECURSE(NAME, TYPE)                                 \
void c_vector_quicksort_recurse##NAME(                                         \
        TYPE *items,                                                           \
        c_uint64_t lo,                                                         \
        c_uint64_t hi)                                                         \
{                                                                              \
    c_uint64_t pivot;                                                          \
                                                                               \
    if (lo < hi) {                                                             \
        pivot = c_vector_quicksort_partition##NAME(items, lo, hi);             \
        c_vector_quicksort_recurse##NAME(items, lo, pivot);                    \
        c_vector_quicksort_recurse##NAME(items, pivot + 1, hi);                \
    }                                                                          \
}

void c_vector_quicksort_recurse(
        void **items,
        c_uint64_t lo,
        c_uint64_t hi,
        c_funp_int32_cvoidp_cvoidp_t cmp)
{
    c_uint64_t pivot;

    if (lo < hi) {
        pivot = c_vector_quicksort_partition(items, lo, hi, cmp);
        c_vector_quicksort_recurse(items, lo, pivot, cmp);
        c_vector_quicksort_recurse(items, pivot + 1, hi, cmp);
    }
}

extern void c_vector_quicksort(
                    c_vector_t vector,
                    c_funp_int32_cvoidp_cvoidp_t cmp)
{
    if (!c_vector_empty(vector)) {
        c_vector_quicksort_recurse(
            c_vector_items(vector),
            0,
            c_vector_size(vector) - 1,
            cmp);
    }
}

#define C_VECTOR_QUICKSORTED(NAME, TYPE)                                       \
C_VECTOR_TYPE(NAME) c_vector_quicksorted##NAME(C_VECTOR_TYPE(NAME) vector)     \
{                                                                              \
    C_VECTOR_TYPE(NAME) result = c_vector_new_copy_vector##NAME(vector);       \
    c_vector_quicksort##NAME(result);                                          \
    return result;                                                             \
}

c_vector_t c_vector_quicksorted(
                c_vector_t vector,
                c_funp_int32_cvoidp_cvoidp_t cmp)
{
    c_vector_t result = c_vector_new_copy_vector(vector);
    c_vector_quicksort(result, cmp);

    return result;
}

C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(C_VECTOR_QUICKSORT_MEDIAN_OF_THREE_PTR)
C_TYPE_TEMPLATE_NAME_TYPE_VALUE     (C_VECTOR_QUICKSORT_MEDIAN_OF_THREE)
C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(C_VECTOR_QUICKSORT_PARTITION_PTR)
C_TYPE_TEMPLATE_NAME_TYPE_VALUE     (C_VECTOR_QUICKSORT_PARTITION)
C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(C_VECTOR_QUICKSORT_RECURSE)
C_TYPE_TEMPLATE_NAME_TYPE_VALUE     (C_VECTOR_QUICKSORT_RECURSE)
C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(C_VECTOR_QUICKSORT)
C_TYPE_TEMPLATE_NAME_TYPE_VALUE     (C_VECTOR_QUICKSORT) // Depends on funcs above.
C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(C_VECTOR_QUICKSORT_INDICES_MEDIAN_OF_THREE_PTR)
C_TYPE_TEMPLATE_NAME_TYPE_VALUE     (C_VECTOR_QUICKSORT_INDICES_MEDIAN_OF_THREE)
C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(C_VECTOR_QUICKSORT_INDICES_PARTITION_PTR)
C_TYPE_TEMPLATE_NAME_TYPE_VALUE     (C_VECTOR_QUICKSORT_INDICES_PARTITION)
C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(C_VECTOR_QUICKSORT_INDICES_RECURSE)
C_TYPE_TEMPLATE_NAME_TYPE_VALUE     (C_VECTOR_QUICKSORT_INDICES_RECURSE)
C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(C_VECTOR_QUICKSORT_INDICES)
C_TYPE_TEMPLATE_NAME_TYPE_VALUE     (C_VECTOR_QUICKSORT_INDICES)
C_TYPE_TEMPLATE_NAME_TYPE_VALUE     (C_VECTOR_QUICKSORT_ITERATIVE_RUN)
C_TYPE_TEMPLATE_NAME_TYPE_VALUE     (C_VECTOR_QUICKSORT_ITERATIVE)
C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(C_VECTOR_QUICKSORTED)
C_TYPE_TEMPLATE_NAME_TYPE_VALUE     (C_VECTOR_QUICKSORTED)

#undef C_VECTOR_QUICKSORT
#undef C_VECTOR_QUICKSORT_INDICES
#undef C_VECTOR_QUICKSORT_INDICES_MEDIAN_OF_THREE
#undef C_VECTOR_QUICKSORT_INDICES_MEDIAN_OF_THREE_PTR
#undef C_VECTOR_QUICKSORT_INDICES_PARTITION
#undef C_VECTOR_QUICKSORT_INDICES_PARTITION_PTR
#undef C_VECTOR_QUICKSORT_INDICES_RECURSE
#undef C_VECTOR_QUICKSORT_MEDIAN_OF_THREE
#undef C_VECTOR_QUICKSORT_MEDIAN_OF_THREE_PTR
#undef C_VECTOR_QUICKSORT_PARTITION
#undef C_VECTOR_QUICKSORT_PARTITION_PTR
#undef C_VECTOR_QUICKSORT_RECURSE
#undef C_VECTOR_QUICKSORTED

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/