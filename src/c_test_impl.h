#ifndef C_TEST_IMPL_INCLUDED
#define C_TEST_IMPL_INCLUDED

#include <stdio.h>
#include <string.h>
#include "c_memory.h"
#include "c_test.h"
#include "c_time.h"
#include "c_log.h"

#define E c_test_error_t
#define R c_test_registry_t
#define S c_test_suite_t
#define T c_test_test_t

struct E {
    char *message;
    E     next;
};

struct T {
    E             first_error, last_error;
    c_test_funp_t fun;
    c_uint32_t    id;
    char         *name;
    c_uint32_t    nfailed, npassed;
    c_uint64_t    total_nsec;
    T             next;
    c_bool_t      should_fail;
};

struct R {
    char      color;
    S         first_suite, current_suite, last_suite;
    char      *name;
    c_uint64_t total_nsec, last_nsec;
    c_uint32_t nsuites, ntests;
    c_uint32_t ntests_fail, ntests_pass, nasserts_fail, nasserts_pass;
};

struct S {
    T             first_test, current_test, last_test;
    c_uint32_t    id;
    char         *name;
    S             next;
    c_uint64_t    total_nsec;
    c_uint32_t    ntests;
    c_uint32_t    ntests_fail, ntests_pass, nasserts_fail, nasserts_pass;
    c_test_funp_t setup, teardown;
};

void c_test_run_function(R reg, S suite, T test);
void c_test_run_suite(R reg, S suite);

void c_test_add_error(T test, const char *message)
{
    E error;

    if (test && message) {
        error = c_test_error_new(message);
        if (NULL == test->first_error) {
            test->first_error = error;
            test->last_error = error;
        } else {
            test->last_error->next = error;
            test->last_error = error;
        }
    }
}

void c_test_add_suite(R reg, const char *name, c_test_funp_t setup, c_test_funp_t teardown)
{
    S suite;

    if (!reg) {
        return;
    }

    suite = c_test_suite_new(name, setup, teardown);

    if (reg->first_suite) {
        reg->last_suite->next = suite;
        reg->last_suite = suite;
    } else {
        reg->first_suite = suite;
        reg->last_suite = reg->first_suite;
    }

    suite->id = ++reg->nsuites;
}

T c_test_add_test(R reg, const char *name, c_test_funp_t fun)
{
    T test = NULL;

    if (!reg || !reg->last_suite) {
        return test;
    }

    test = c_test_test_new(name, fun);

    if (reg->last_suite->first_test) {
        reg->last_suite->last_test->next = test;
        reg->last_suite->last_test = test;
    } else {
        reg->last_suite->first_test = test;
        reg->last_suite->last_test = test;
    }

    test->id = ++reg->last_suite->ntests;
    ++reg->ntests;

    return test;
}

T c_test_add_test_fail(R reg, const char *name, c_test_funp_t fun)
{
    T test = c_test_add_test(reg, name, fun);

    if (test) {
        test->should_fail = C_TRUE;
    }

    return test;
}

void c_test_assert_pass(R reg, c_bool_t passed, const char *error)
{
    c_uint64_t assert_nsec = c_time_now_nsec() - reg->last_nsec;
    c_uint32_t id_str_len = 32;
    char id_str[id_str_len];

    if (reg && reg->current_suite && reg->current_suite->current_test) {
        if (C_FALSE == passed) {
            c_test_add_error(reg->current_suite->current_test, error);
        }
        if (passed || reg->current_suite->current_test->should_fail) {
            ++reg->current_suite->current_test->npassed;
            ++reg->current_suite->nasserts_pass;
            ++reg->nasserts_pass;
        } else {
            ++reg->current_suite->current_test->nfailed;
            ++reg->current_suite->nasserts_fail;
            ++reg->nasserts_fail;
        }

        snprintf(id_str, id_str_len, "%d.%d.%d",
                 reg->current_suite->id,
                 reg->current_suite->current_test->id,
                 reg->current_suite->current_test->nfailed +
                 reg->current_suite->current_test->npassed);
        c_test_print_status_assert(c_test_pct(reg), id_str, reg->color, passed, assert_nsec);
    }

    reg->last_nsec = c_time_now_nsec();
}

void c_test_error_free(E *error)
{
    if (error && *error) {
        C_MEMORY_FREE((*error)->message);
        c_test_error_free(& (*error)->next);
        C_MEMORY_FREE(*error);
    }
}

E c_test_error_new(const char *message)
{
    E result;
    c_uint32_t len = strlen(message);

    C_MEMORY_NEW_ZERO(result);
    C_MEMORY_NEW_N_BYTES(result->message, len + 1);
    strcpy(result->message, message);

    return result;
}

int c_test_exit_code(R reg)
{
    if (NULL == reg) {
        return -1;
    }

    return reg->nasserts_fail;
}

c_bool_t c_test_file_empty(const char *filename)
{
    c_int64_t len = 0;
    FILE *file = NULL;

    if (!filename) {
        return C_TRUE;
    }

    file = fopen(filename, "r");

    if (!file) {
        return C_TRUE;
    }

    fseek(file, 0L, SEEK_END);
    len = ftell(file);

    if (file) {
        fclose(file);
    }

    return 0 >= len ? C_TRUE : C_FALSE;
}

c_bool_t c_test_file_exists(const char *filename)
{
    FILE *file = NULL;

    if (!filename) {
        return C_FALSE;
    }

    file = fopen(filename, "r");
    if (!file) {
        return C_FALSE;
    }

    if (file) {
        fclose(file);
    }

    return C_TRUE;
}

c_bool_t c_test_files_equal(const char *filename1, const char *filename2)
{
    c_bool_t result = C_FALSE;
    FILE *file1 = NULL, *file2 = NULL;
    c_int64_t i = 0, len1 = 0, len2 = 0;
    char *bytes1 = NULL, *bytes2 = NULL;

    if (!filename1 || !filename2) {
        goto recover;
    }

    file1 = fopen(filename1, "r");
    if (!file1) {
        goto recover;
    }

    file2 = fopen(filename2, "r");
    if (!file2) {
        goto recover;
    }

    fseek(file1, 0L, SEEK_END);
    len1 = ftell(file1);
    rewind(file1);

    fseek(file2, 0L, SEEK_END);
    len2 = ftell(file2);
    rewind(file2);

    if (len1 != len2) {
        goto recover;
    }

    C_MEMORY_NEW_N_BYTES(bytes1, len1);
    C_MEMORY_NEW_N_BYTES(bytes2, len2);

    if (fread(bytes1, sizeof(char), len1, file1) != len1) {
        goto recover;
    }

    if (fread(bytes2, sizeof(char), len2, file2) != len1) {
        goto recover;
    }

    for(i = 0; i < len1; ++i) {
        if (bytes1[i] != bytes2[i]) {
            goto recover;
        }
    }

    result = C_TRUE;

recover:
    if (file1) {
        fclose(file1);
    }

    if (file2) {
        fclose(file2);
    }

    C_MEMORY_FREE(bytes1);
    C_MEMORY_FREE(bytes2);

    return result;
}

void c_test_test_free(T *test)
{
    if (test && *test) {
        c_test_test_free(& (*test)->next);
        c_test_error_free(& (*test)->first_error);
        C_MEMORY_FREE((*test)->name);
        C_MEMORY_FREE(*test);
    }
}

T c_test_test_new(const char *name, c_test_funp_t fun)
{
    T result;
    C_MEMORY_NEW_ZERO(result);

    result->fun = fun;

    if (name) {
        C_MEMORY_NEW_N_BYTES(result->name, strlen(name) + 1);
        strcpy(result->name, name);
    }

    return result;
}

c_uint32_t c_test_registry_num_suites(R reg)
{
    if (reg) {
        return reg->nsuites;
    }
    return 0;
}

c_uint32_t c_test_registry_num_tests(R reg)
{
    if (reg) {
        return reg->ntests;
    }
    return 0;
}

c_uint32_t c_test_pct(R reg)
{
    if (!reg) {
        return 0;
    }

    // Enlarge denominator by one so 100% is only reported at total summary.
    float denom = reg->ntests + 1;
    float numer = 100 * (reg->ntests_pass + reg->ntests_fail);
    c_uint32_t result = numer / denom;
    return result;
}

R c_test_registry_new(const char *name, c_bool_t color)
{
    R reg;
    C_MEMORY_NEW_ZERO(reg);
    C_MEMORY_NEW_N_BYTES(reg->name, strlen(name) + 1);
    strcpy(reg->name, name);
    reg->color = color;

    return reg;
}

void c_test_registry_free(R *reg)
{
    if (reg) {
        c_test_suite_free(& (*reg)->first_suite);
        (*reg)->first_suite = NULL;
        (*reg)->last_suite = NULL;
        C_MEMORY_FREE((*reg)->name);
        C_MEMORY_FREE(*reg);
    }
}

int c_test_run(R reg)
{
    c_uint32_t tic, toc;

    if (!reg) {
        return EXIT_FAILURE;
    }

    tic = c_time_now_nsec();
    c_test_print_header(reg->name, reg->nsuites, reg->ntests);

    for(reg->current_suite = reg->first_suite;
        reg->current_suite;
        reg->current_suite = reg->current_suite->next) {

        c_test_run_suite(reg, reg->current_suite);
    }

    C_TEST_PRINT_HR2(c_test_pct(reg));

    toc = c_time_now_nsec();
    reg->total_nsec = toc - tic;

    c_test_print_total(
        100,
        reg->color,
        reg->name,
        reg->ntests_fail,
        reg->ntests_pass,
        reg->nasserts_fail,
        reg->nasserts_pass,
        reg->total_nsec);

    return c_test_exit_code(reg);
}

void c_test_run_function(R reg, S suite, T test)
{
    c_uint64_t tic, toc;

    if (!test) {
        return;
    }

    if (reg) {
        C_TEST_PRINT_HR(c_test_pct(reg));
        c_test_print_start(c_test_pct(reg), test->name, suite->id, test->id);
        reg->last_nsec = c_time_now_nsec();
    }

    tic = c_time_now_nsec();
    if (test->fun) {
        test->fun();
    }

    if (test->nfailed) {
        if (suite) {
            ++suite->ntests_fail;
        }
        if (reg) {
            ++reg->ntests_fail;
        }
    } else {
        if (suite) {
            ++suite->ntests_pass;
        }
        if (reg) {
            ++reg->ntests_pass;
        }
    }

    toc = c_time_now_nsec();
    test->total_nsec = toc - tic;

    if (reg) {
        c_test_print_total(
            c_test_pct(reg),
            reg->color,
            test->name,
            0, // ntests fail
            0, // ntests pass
            test->nfailed,
            test->npassed,
            test->total_nsec);
    }
}

void c_test_run_suite(R reg, S suite)
{
    c_uint64_t tic, toc, ticSetup, tocSetup, ticTeardown, tocTeardown;

    if (!suite) {
        return;
    }

    C_TEST_PRINT_HR2(c_test_pct(reg));
    c_test_print_start(c_test_pct(reg), suite->name, suite->id, 0);
    tic = c_time_now_nsec();

    if (suite->setup) {
        c_test_print_status(c_test_pct(reg), suite->name, "Setup", 0);
        ticSetup = c_time_now_nsec();
        suite->setup();
        tocSetup = c_time_now_nsec();
        c_test_print_status(c_test_pct(reg), suite->name, "Setup Done",
                            tocSetup - ticSetup);
    }

    for(suite->current_test = suite->first_test;
        suite->current_test;
        suite->current_test= suite->current_test->next) {
        c_test_run_function(reg, suite, suite->current_test);
    }

    C_TEST_PRINT_HR(c_test_pct(reg));

    if (suite->teardown) {
        c_test_print_status(c_test_pct(reg), suite->name, "Teardown", 0);
        ticTeardown = c_time_now_nsec();
        suite->teardown();
        tocTeardown = c_time_now_nsec();
        c_test_print_status(c_test_pct(reg), suite->name, "Teardown Done",
                            tocTeardown - ticTeardown);
    }

    toc = c_time_now_nsec();
    suite->total_nsec = toc - tic;

    c_test_print_total(
        c_test_pct(reg),
        reg->color,
        suite->name,
        suite->ntests_fail,
        suite->ntests_pass,
        suite->nasserts_fail,
        suite->nasserts_pass,
        suite->total_nsec);
}

void c_test_set_current_suite(R reg, S suite)
{
    if (reg) {
        reg->current_suite = suite;
    }
}

void c_test_set_current_test(S suite, T test)
{
    if (suite) {
        suite->current_test = test;
    }
}

c_test_suite_t c_test_suite_new(const char *name, c_test_funp_t setup, c_test_funp_t teardown)
{
    S result;
    C_MEMORY_NEW_ZERO(result);

    if (name) {
        C_MEMORY_NEW_N_BYTES(result->name, strlen(name) + 1);
        strcpy(result->name, name);
    }

    result->setup = setup;
    result->teardown = teardown;

    return result;
}

void c_test_suite_free(S *suite)
{
    if (suite && *suite) {
        c_test_test_free(& (*suite)->first_test);
        c_test_suite_free(& (*suite)->next);
        C_MEMORY_FREE((*suite)->name);
        C_MEMORY_FREE(*suite);
    }
}

void c_test_xml_error(E error, FILE *output)
{
    if (!error) {
        return;
    }

    fprintf(output, "\t\t\t<failure message=\"%s\"/>\n", error->message);
    c_test_xml_error(error->next, output);
}

void c_test_xml_test(T test, FILE *output)
{
    const int time_str_len = 32;
    char time_str[time_str_len];

    if (!test) {
        return;
    }

    c_time_nsec_str(time_str, time_str_len, test->total_nsec);

    fprintf(output, "\t\t<testcase assertions=\"%d\""
                    " name=\"%s\" status=\"%s\" time=\"%s\">\n",
                    test->nfailed, test->name,
                    test->nfailed ? "FAIL" : "PASS",
                    time_str);
    c_test_xml_error(test->first_error, output);
    fprintf(output, "\t\t</testcase>\n");
    c_test_xml_test(test->next, output);
}

void c_test_xml_suite(S suite, FILE *output)
{
    const int time_str_len = 32;
    char time_str[time_str_len];

    if (!suite) {
        return;
    }

    c_time_nsec_str(time_str, time_str_len, suite->total_nsec);

    fprintf(output, "\t<testsuite failures=\"%d\" id=\"%d.0\""
                    " name=\"%s\" tests=\"%d\" time=\"%s\">\n",
                    suite->nasserts_fail, suite->id, suite->name,
                    suite->ntests, time_str);
    c_test_xml_test(suite->first_test, output);
    fprintf(output, "\t</testsuite>\n");
    c_test_xml_suite(suite->next, output);
}

void c_test_xml_suites(R reg, FILE *output)
{
    const int time_str_len = 32;
    char time_str[time_str_len];

    c_time_nsec_str(time_str, time_str_len, reg->total_nsec);

    fprintf(output, "<testsuites failures=\"%d\""
                    " name=\"%s\" tests=\"%d\" time=\"%s\">\n",
                    reg->nasserts_fail, reg->name, reg->ntests, time_str);
    c_test_xml_suite(reg->first_suite, output);
    fprintf(output, "</testsuites>\n");
}

void c_test_xml(R reg, const char *output_file_name)
{
    FILE *file = NULL;

    if (!reg) {
        fprintf(stderr, "ERROR Invalid registry passed to c_test_xml.\n");
        return;
    }

    if (!output_file_name) {
        fprintf(stderr, "ERROR Invalid file name passed to c_test_xml.\n");
        return;
    }

    file = fopen(output_file_name, "w+");

    if (NULL == file) {
        fprintf(stderr, "ERROR Failed to open file %s\n", output_file_name);
        return;
    }

    fprintf(file, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
    c_test_xml_suites(reg, file);

    if (fclose(file)) {
        fprintf(stderr, "ERROR Failed to close file %s\n", output_file_name);
    }
}

#undef F
#undef P
#undef R
#undef S
#undef C_TEST_PASS_STR

#endif
/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/