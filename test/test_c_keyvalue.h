#ifndef TEST_C_KEYVALUE_INCLUDED
#define TEST_C_KEYVALUE_INCLUDED

#include "c_test.h"
#include "c_keyvalue.h"

void test_c_keyvalue_cmp()
{
    #define TEST(KN, KT, VN, VT)                                               \
    C_KEYVALUE_TYPE(KN, VN) kv##KN##VN = NULL;                                 \
                                                                               \
    C_TEST_ASSERT_NEQ(c_keyvalue_cmp##KN##VN(0, 0), 0,                         \
                     "Expected not eq for nulls");                             \
                                                                               \
    kv##KN##VN = c_keyvalue_new##KN##VN(0, 0);                                 \
    C_TEST_ASSERT_EQ(c_keyvalue_cmp##KN##VN(0, kv##KN##VN), 0,                 \
                     "Expected eq");                                           \
    c_keyvalue_free##KN##VN(& kv##KN##VN);

    C_KEYVALUE_TEMPLATE_NAME_TYPE(TEST)
    #undef TEST
}

void test_c_keyvalue_new()
{
    #define TEST(KN, KT, VN, VT)                                               \
    C_KEYVALUE_TYPE(KN, VN) kv##KN##VN = NULL;                                 \
                                                                               \
    kv##KN##VN = c_keyvalue_new##KN##VN(0, 0);                                 \
    C_TEST_ASSERT(kv##KN##VN, "Expected non null.");                           \
    c_keyvalue_free##KN##VN(& kv##KN##VN);                                     \
    C_TEST_ASSERT_EQ_PTR(NULL, kv##KN##VN, "Expected free.");

    C_KEYVALUE_TEMPLATE_NAME_TYPE(TEST)
    #undef TEST
}

int suite_test_c_keyvalue()
{
    C_TEST_ADD_SUITE("suite_test_c_keyvalue", NULL, NULL);

    C_TEST_ADD_TEST(test_c_keyvalue_new);
    C_TEST_ADD_TEST(test_c_keyvalue_cmp);

    C_TEST_RETURN();
}

#endif
/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/