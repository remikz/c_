#ifndef C_STR_INCLUDED
#define C_STR_INCLUDED

#include "c_vector.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef c_int8_t c_char_t;

/** String abstraction that wraps a vector.
 * The size member of the string excludes any terminating null character(s).
 */
typedef c_vector_int8_t c_str_t;

#define c_str_capacity(S)   c_vector_capacity_int8((S))
#define c_str_copy(TO, FRO) c_vector_copy_int8((TO), (FRO))
#define c_str_data(S)       c_vector_items_int8((S))
#define c_str_eq(S1, S2)    c_vector_equal_int8((S1), (S2))
#define c_str_free(S)       c_vector_free_int8((S))
#define c_str_len(S)        c_vector_size_int8((S))
#define c_str_size(S)       c_vector_size_int8((S))
#define c_str_resize(S, N)  c_vector_resize_int8((STR), (N))

/** Compare strings.
 * If s1 is an equal prefix, then it is considered less than.
 * @return 0 if equal, -1 if s1 is less than, 1 if greater.
 */
#define C_STR_CMP(NAME, TYPE, CAST)                                            \
    extern c_int32_t c_str_cmp##NAME(const TYPE s1, const TYPE s2);

/* Compatible signature for standard library algorithms like "sort" that expect
   pointer to c_str_t (which is itself a pointer). */
c_int32_t c_str_cmp_voidpp(const void *s1, const void *s2);

/** Checks length of string up to any null character or capacity
 * and updates size member. */
extern void    c_str_compute_size(c_str_t str);

/** @return New string with all chars set to zero. */
extern c_str_t c_str_new_clear(c_uint64_t size);

/** Copy string to own buffer.
 * @param n Maximum number of chars to copy to guard against non-terminated.
 * @return New vector of chars. Will never return NULL.
 */
extern c_str_t c_str_new(const c_char_t *str, c_uint64_t n);

extern c_str_t c_str_join(c_vector_t strings);
extern c_str_t c_str_join_delim(c_vector_t strings, c_char_t delim);
extern c_str_t c_str_join_delims(c_vector_t strings, c_str_t delim_str);

//extern c_str_t c_str_put(const c_char_t *str, c_uint64_t n);

/* Compare two c_str_t directly without casting pointers. */
C_STR_CMP(/* empty */,  c_str_t,    /* empty */)

/* Castable from void pointer to c_str_t. */
C_STR_CMP(_voidp,       void*,      (c_str_t))

#undef C_STR_CMP

#ifdef __cplusplus
}
#endif

#endif
/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/
