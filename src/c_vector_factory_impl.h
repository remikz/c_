#ifndef C_VECTOR_FACTORY_IMPL_INCLUDED
#define C_VECTOR_FACTORY_IMPL_INCLUDED

#include "c_vector_factory.h"
#include "c_memory.h"

#ifdef __cplusplus
extern "C" {
#endif

#define C_VECTOR_RANGE(NAME, TYPE)                                             \
C_VECTOR_TYPE(NAME) c_vector_range##NAME(                                      \
                        c_int64_t start, c_int64_t stop, c_int64_t step)       \
{                                                                              \
    c_uint64_t i = 0, n;                                                       \
    C_VECTOR_TYPE(NAME) result;                                                \
    TYPE *items;                                                               \
                                                                               \
    if (step) {                                                                \
        n = (stop - start) / step;                                             \
        result = c_vector_new##NAME(n);                                        \
        c_vector_resize##NAME(result, n);                                      \
        items = c_vector_items##NAME(result);                                  \
                                                                               \
        for(; i < n; start += step, ++i) {                                     \
            items[i] = start;                                                  \
        }                                                                      \
    } else {                                                                   \
        result = c_vector_new##NAME(0);                                        \
    }                                                                          \
                                                                               \
    return result;                                                             \
}

#define C_VECTOR_RANGE_PTR(NAME, TYPE)                                         \
C_VECTOR_TYPE(NAME) c_vector_range##NAME(                                      \
                        c_int64_t start, c_int64_t stop, c_int64_t step)       \
{                                                                              \
    c_uint64_t i = 0, n;                                                       \
    C_VECTOR_TYPE(NAME) result;                                                \
    TYPE *items;                                                               \
                                                                               \
    if (step) {                                                                \
        n = (stop - start) / step;                                             \
        result = c_vector_new##NAME(n);                                        \
        c_vector_resize##NAME(result, n);                                      \
        items = c_vector_items##NAME(result);                                  \
                                                                               \
        for(; i < n; start += step, ++i) {                                     \
            C_MEMORY_NEW(items[i]);                                            \
            *items[i] = start;                                                 \
        }                                                                      \
    } else {                                                                   \
        result = c_vector_new##NAME(0);                                        \
    }                                                                          \
                                                                               \
    return result;                                                             \
}

C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(C_VECTOR_RANGE_PTR)
C_TYPE_TEMPLATE_NAME_TYPE_VALUE(C_VECTOR_RANGE)

#undef C_VECTOR_RANGE

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/

