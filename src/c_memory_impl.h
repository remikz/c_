#ifndef C_MEMORY_IMPL_INCLUDED
#define C_MEMORY_IMPL_INCLUDED

#include <stdio.h>

#define PRINT_ERROR(MSG, FILE, LINE, V1) fprintf(stderr, MSG, FILE, LINE, V1)
#define PRINT_ERROR2(MSG, FILE, LINE, V1, V2) \
            fprintf(stderr, MSG, FILE, LINE, V1, V2)

#include "c_memory.h"

void* c_memory_alloc(long long nbytes, const char *file, int line)
{
    void *result;

    assert(nbytes > 0);

    result = malloc(nbytes);

    if (NULL == result) {
        PRINT_ERROR("%s:%d ERROR Failed to allocate nbytes=%llu.\n",
                    file, line, nbytes);
    }

    return result;
}

void* c_memory_alloc_zero(long long nbytes, const char *file, int line)
{
    void *result;

    assert(nbytes> 0);

    result = calloc(nbytes, 1);

    if (NULL == result) {
        PRINT_ERROR("%s:%d ERROR Failed to allocate nbytes=%llu.\n",
                    file, line, nbytes);
    }

    return result;
}

void* c_memory_realloc(void *ptr, long long nbytes, const char *file, int line)
{
    void *result;

    assert(nbytes > 0);

    result = realloc(ptr, nbytes);

    if (NULL == result) {
        PRINT_ERROR2("%s:%d ERROR Failed to allocate ptr=%p nbytes=%llu.\n",
                    file, line, ptr, nbytes);
    }

    return result;
}

#endif/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/