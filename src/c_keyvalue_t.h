#ifndef C_KEYVALUE_T_INCLUDED
#define C_KEYVALUE_T_INCLUDED

#include "c_keyvalue.h"

#ifdef __cplusplus
extern "C" {
#endif

/** A keyvalue type is a node intended for storage in data structures.
 */
#define C_KEYVALUE_T(KNAME, KTYPE, VNAME, VTYPE)                               \
struct C_KEYVALUE_TYPE(KNAME, VNAME) {                                         \
    /** The key data to be compared when hashes are equal. */                  \
    KTYPE key;                                                                 \
    /** The data payload. */                                                   \
    VTYPE value;                                                               \
};

C_KEYVALUE_T(/* empty */,   void*,      /* empty */,    void*)
C_KEYVALUE_TEMPLATE_NAME_TYPE(C_KEYVALUE_T)

#undef C_KEYVALUE_T

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/
