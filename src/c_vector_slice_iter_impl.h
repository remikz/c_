#ifndef C_VECTOR_SLICE_ITER_IMPL_INCLUDED
#define C_VECTOR_SLICE_ITER_IMPL_INCLUDED

#include "c_vector_slice_iter.h"
#include "c_vector_slice_iter_t.h"
#include "c_memory.h"

#ifdef __cplusplus
extern "C" {
#endif

#define C_VECTOR_SLICE_ITER_ADVANCE(NAME)                                      \
void c_vector_slice_iter_advance##NAME(C_VECTOR_SLICE_ITER_TYPE(NAME) iter)    \
{                                                                              \
    if (iter) {                                                                \
        iter->position += iter->step;                                          \
    }                                                                          \
}

#define C_VECTOR_SLICE_ITER_FREE(NAME)                                         \
void c_vector_slice_iter_free##NAME(C_VECTOR_SLICE_ITER_TYPE(NAME) *iter)      \
{                                                                              \
    assert(iter);                                                              \
    assert(*iter);                                                             \
    C_MEMORY_FREE(*iter);                                                      \
}

#define C_VECTOR_SLICE_ITER_FROM_CHARS(NAME)                                   \
C_VECTOR_SLICE_ITER_TYPE(NAME) c_vector_slice_iter_from_chars##NAME(           \
                                    C_VECTOR_TYPE(NAME) vector,                \
                                    const char *chars, c_uint64_t n,           \
                                    c_int64_t offset, c_bool_t closed)         \
{                                                                              \
    c_slice_t slice = c_slice_from_chars(chars, n, offset, closed);            \
    C_VECTOR_SLICE_ITER_TYPE(NAME) result = c_vector_slice_iter_new##NAME(     \
                                                vector, slice);                \
    c_slice_free(& slice);                                                     \
    return result;                                                             \
}

#define C_VECTOR_SLICE_ITER_FROM_STR(NAME)                                     \
C_VECTOR_SLICE_ITER_TYPE(NAME) c_vector_slice_iter_from_str##NAME(             \
                                    C_VECTOR_TYPE(NAME) vector,                \
                                    const c_str_t str,                         \
                                    c_int64_t offset, c_bool_t closed)         \
{                                                                              \
    c_slice_t slice = c_slice_from_str(str, offset, closed);                   \
    C_VECTOR_SLICE_ITER_TYPE(NAME) result = c_vector_slice_iter_new##NAME(     \
                                                vector, slice);                \
    c_slice_free(& slice);                                                     \
    return result;                                                             \
}

#define C_VECTOR_SLICE_ITER_IS_VALID(NAME)                                     \
c_bool_t c_vector_slice_iter_is_valid##NAME(                                   \
            C_VECTOR_SLICE_ITER_TYPE(NAME) iter)                               \
{                                                                              \
    return  iter &&                                                            \
            iter->vector &&                                                    \
            c_vector_size##NAME(iter->vector) > iter->position &&              \
            0 <= iter->position &&                                             \
            c_vector_timestamp##NAME(iter->vector) == iter->timestamp;         \
}

#define C_VECTOR_SLICE_ITER_ITEM(NAME, TYPE)                                   \
TYPE c_vector_slice_iter_item##NAME(C_VECTOR_SLICE_ITER_TYPE(NAME) iter)       \
{                                                                              \
    if (iter && iter->vector) {                                                \
        return c_vector_get_safe##NAME(iter->vector, iter->position);          \
    }                                                                          \
    return 0;                                                                  \
}

#define C_VECTOR_SLICE_ITER_NEW(NAME)                                          \
C_VECTOR_SLICE_ITER_TYPE(NAME) c_vector_slice_iter_new##NAME(                  \
                                    C_VECTOR_TYPE(NAME) vector,                \
                                    c_slice_t slice)                           \
{                                                                              \
    C_VECTOR_SLICE_ITER_TYPE(NAME) iter;                                       \
    c_uint64_t n_unused;                                                       \
                                                                               \
    C_MEMORY_NEW(iter);                                                        \
                                                                               \
    if (vector) {                                                              \
        iter->vector = vector;                                                 \
        c_slice_compute_range(slice,                                           \
                              c_vector_size##NAME(vector),                     \
                              & iter->first,                                   \
                              & iter->last,                                    \
                              & iter->step,                                    \
                              & n_unused);                                     \
        iter->timestamp = c_vector_timestamp##NAME(vector);                    \
        iter->position = iter->first;                                          \
    } else {                                                                   \
        iter->first = iter->last = iter->position = iter->step = 0;            \
        iter->timestamp = 0;                                                   \
        iter->vector = NULL;                                                   \
    }                                                                          \
                                                                               \
    return iter;                                                               \
}

#define C_VECTOR_SLICE_ITER_POSITION(NAME)                                     \
c_uint64_t c_vector_slice_iter_position##NAME(                                 \
                    C_VECTOR_SLICE_ITER_TYPE(NAME) iter)                       \
{                                                                              \
    if (iter) {                                                                \
        return iter->position;                                                 \
    }                                                                          \
    return 0;                                                                  \
}

#ifdef __cplusplus
}
#endif

C_TYPE_TEMPLATE_NAME(C_VECTOR_SLICE_ITER_ADVANCE)
C_TYPE_TEMPLATE_NAME(C_VECTOR_SLICE_ITER_FREE)
C_TYPE_TEMPLATE_NAME(C_VECTOR_SLICE_ITER_FROM_CHARS)
C_TYPE_TEMPLATE_NAME(C_VECTOR_SLICE_ITER_FROM_STR)
C_TYPE_TEMPLATE_NAME(C_VECTOR_SLICE_ITER_IS_VALID)
C_TYPE_TEMPLATE_NAME(C_VECTOR_SLICE_ITER_NEW)
C_TYPE_TEMPLATE_NAME(C_VECTOR_SLICE_ITER_POSITION)
C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_SLICE_ITER_ITEM)

#undef C_VECTOR_SLICE_ITER_ADVANCE
#undef C_VECTOR_SLICE_ITER_FREE
#undef C_VECTOR_SLICE_ITER_FROM_CHARS
#undef C_VECTOR_SLICE_ITER_FROM_STR
#undef C_VECTOR_SLICE_ITER_ITEM
#undef C_VECTOR_SLICE_ITER_IS_VALID
#undef C_VECTOR_SLICE_ITER_NEW
#undef C_VECTOR_SLICE_ITER_POSITION

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/
