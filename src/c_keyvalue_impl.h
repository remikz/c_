#ifndef C_KEYVALUE_IMPL_INCLUDED
#define C_KEYVALUE_IMPL_INCLUDED

#include "c_keyvalue.h"
#include "c_keyvalue_t.h"
#include "c_memory.h"

#ifdef __cplusplus
extern "C" {
#endif

#define C_KEYVALUE_CMP(KNAME, KTYPE, VNAME, VTYPE)                             \
c_int32_t c_keyvalue_cmp##KNAME##VNAME(                                        \
                        const KTYPE key,                                       \
                        const C_KEYVALUE_TYPE(KNAME, VNAME) keyvalue)          \
{                                                                              \
    if (keyvalue) {                                                            \
        return key - keyvalue->key;                                            \
    }                                                                          \
    return -1;                                                                 \
}

#define C_KEYVALUE_FREE(KNAME, KTYPE, VNAME, VTYPE)                            \
void c_keyvalue_free##KNAME##VNAME(C_KEYVALUE_TYPE(KNAME, VNAME) *kv)          \
{                                                                              \
    assert(kv);                                                                \
    assert(*kv);                                                               \
    C_MEMORY_FREE(*kv);                                                        \
}

#define C_KEYVALUE_NEW(KNAME, KTYPE, VNAME, VTYPE)                             \
C_KEYVALUE_TYPE(KNAME, VNAME) c_keyvalue_new##KNAME##VNAME(                    \
                                            KTYPE key,                         \
                                            VTYPE value)                       \
{                                                                              \
    C_KEYVALUE_TYPE(KNAME, VNAME) result;                                      \
    C_MEMORY_NEW(result);                                                      \
                                                                               \
    if (result) {                                                              \
        result->key = key;                                                     \
        result->value = value;                                                 \
    }                                                                          \
    return result;                                                             \
}

C_KEYVALUE_TEMPLATE_NAME_TYPE(C_KEYVALUE_CMP)
C_KEYVALUE_TEMPLATE_NAME_TYPE(C_KEYVALUE_FREE)
C_KEYVALUE_TEMPLATE_NAME_TYPE(C_KEYVALUE_NEW)

#undef C_KEYVALUE_CMP
#undef C_KEYVALUE_FREE
#undef C_KEYVALUE_NEW

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/
