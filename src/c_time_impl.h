#ifndef C_TIME_IMPL_INCLUDED
#define C_TIME_IMPL_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include "c_time.h"

c_uint64_t c_time_now_msec()
{
    struct timeval t;
    c_uint64_t result;

    gettimeofday(& t, NULL);
    result = t.tv_sec*1000LL + t.tv_usec/1000;
    return result;
}

 c_uint64_t c_time_now_nsec()
{
    struct timespec t;
    c_uint64_t result = 0;
    int rcode;

    rcode = clock_gettime(CLOCK_REALTIME, & t);
    if (!rcode) {
        result = t.tv_sec*1000000000LL + t.tv_nsec;
    }
    return result;
}

c_uint64_t c_time_now_usec()
{
    struct timeval t;
    c_uint64_t result;

    gettimeofday(& t, NULL);
    result = t.tv_sec*1000000LL + t.tv_usec;
    return result;
}

void c_time_nsec_str(char *buf, c_uint32_t nbuf, c_uint64_t nsec)
{
    if (nsec > 1e9) {
        snprintf(buf, nbuf, "%gs", nsec/1.0e9);
    } else if (nsec > 1e6) {
        snprintf(buf, nbuf, "%gms", nsec/1.0e6);
    } else if (nsec > 1e3) {
        snprintf(buf, nbuf, "%gus", nsec/1.0e3);
    } else {
        snprintf(buf, nbuf, "%dns", (int) nsec);
    }
}

#endif
/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/