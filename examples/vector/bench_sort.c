#include <c_bench.h>
#include <c_vector_sort.h>
#include <c_vector_quicksort.h>
#include <stdio.h> // printf
#include <stdlib.h> // rand

c_bench_t b = 0;
c_vector_int32_t v = 0;
const int MILLION = 1000000;

void bench_sort_random();
void quicksort_vector();
void quicksort_vector_iterative();
void setup_bench();
void setup_vector_ascending();
void setup_vector_descending();
void setup_vector_identical();
void setup_vector_random();
void sort_vector();
void teardown_bench();
void teardown_vector();

void bench_quicksort_ascending()
{
    setup_bench();
    c_bench_run_setup_teardown(
            b, 0, quicksort_vector, setup_vector_ascending, teardown_vector);
    printf("bench_quicksort_ascending ");
    c_bench_print(b);
    teardown_bench();
}

void bench_quicksort_descending()
{
    setup_bench();
    c_bench_run_setup_teardown(
            b, 0, quicksort_vector, setup_vector_descending, teardown_vector);
    printf("bench_quicksort_descending ");
    c_bench_print(b);
    teardown_bench();
}

void bench_quicksort_identical()
{
    setup_bench();
    c_bench_run_setup_teardown(
            b, 0, quicksort_vector, setup_vector_identical, teardown_vector);
    printf("bench_quicksort_identical ");
    c_bench_print(b);
    teardown_bench();
}

void bench_quicksort_iterative_ascending()
{
    setup_bench();
    c_bench_run_setup_teardown(
            b, 0, quicksort_vector_iterative, setup_vector_ascending, teardown_vector);
    printf("bench_quicksort_iterative_ascending ");
    c_bench_print(b);
    teardown_bench();
}

void bench_quicksort_iterative_descending()
{
    setup_bench();
    c_bench_run_setup_teardown(
            b, 0, quicksort_vector_iterative, setup_vector_descending, teardown_vector);
    printf("bench_quicksort_iterative_descending ");
    c_bench_print(b);
    teardown_bench();
}

void bench_quicksort_iterative_identical()
{
    setup_bench();
    c_bench_run_setup_teardown(
            b, 0, quicksort_vector_iterative, setup_vector_identical, teardown_vector);
    printf("bench_quicksort_iterative_identical ");
    c_bench_print(b);
    teardown_bench();
}

void bench_quicksort_iterative_random()
{
    setup_bench();
    c_bench_run_setup_teardown(
            b, 0, quicksort_vector_iterative, setup_vector_random, teardown_vector);
    printf("bench_quicksort_iterative_random ");
    c_bench_print(b);
    teardown_bench();
}

void bench_quicksort_random()
{
    setup_bench();
    c_bench_run_setup_teardown(
            b, 0, quicksort_vector, setup_vector_random, teardown_vector);
    printf("bench_quicksort_random ");
    c_bench_print(b);
    teardown_bench();
}

void bench_sort_ascending()
{
    setup_bench();
    c_bench_run_setup_teardown(
            b, 0, sort_vector, setup_vector_ascending, teardown_vector);
    printf("bench_sort_ascending ");
    c_bench_print(b);
    teardown_bench();
}

void bench_sort_descending()
{
    setup_bench();
    c_bench_run_setup_teardown(
            b, 0, sort_vector, setup_vector_descending, teardown_vector);
    printf("bench_sort_descending ");
    c_bench_print(b);
    teardown_bench();
}

void bench_sort_identical()
{
    setup_bench();
    c_bench_run_setup_teardown(
            b, 0, sort_vector, setup_vector_identical, teardown_vector);
    printf("bench_sort_identical ");
    c_bench_print(b);
    teardown_bench();
}

void bench_sort_random()
{
    setup_bench();
    c_bench_run_setup_teardown(
            b, 0, sort_vector, setup_vector_random, teardown_vector);
    printf("bench_sort_random ");
    c_bench_print(b);
    teardown_bench();
}

void quicksort_vector()
{
    c_vector_quicksort_int32(v);
}

void quicksort_vector_iterative()
{
    c_vector_quicksort_iterative_int32(v);
}

void setup_bench()
{
    b = c_bench_new();
}

void setup_vector_ascending()
{
    setup_vector_random();
    sort_vector();
}

void setup_vector_descending()
{
    setup_vector_random();
    sort_vector();
    c_vector_reverse_int32(v);
}

void setup_vector_identical()
{
    v = c_vector_new_set_int32(MILLION, MILLION, 0);
}

void setup_vector_random()
{
    int n = MILLION;

    v = c_vector_new_int32(n);

    while (n--) {
        c_vector_append_int32(v, rand());
    }
}

void sort_vector()
{
    c_vector_sort_int32(v);
}

void teardown_bench()
{
    if (b) {
        c_bench_free(& b);
    }
}

void teardown_vector()
{
    if (v) {
        c_vector_free_int32(& v);
    }
}

int main(int argc, char **argv)
{
    bench_sort_random();
    bench_sort_ascending();
    bench_sort_descending();
    bench_sort_identical();

    bench_quicksort_random();
    bench_quicksort_ascending();
    bench_quicksort_descending();
    bench_quicksort_identical();

    bench_quicksort_iterative_random();
    bench_quicksort_iterative_ascending();
    bench_quicksort_iterative_descending();
    bench_quicksort_iterative_identical();

    return 0;
}