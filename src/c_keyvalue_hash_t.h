#ifndef C_KEYVALUE_HASH_T_INCLUDED
#define C_KEYVALUE_HASH_T_INCLUDED

#include "c_keyvalue_hash.h"

#ifdef __cplusplus
extern "C" {
#endif

/** A keyvalue type with a cached hash.
 * The hash member should be used to speedup comparison before more expensive
 * comparison of keys.
 */
#define C_KEYVALUE_HASH_T(KNAME, KTYPE, VNAME, VTYPE)                          \
struct C_KEYVALUE_HASH_TYPE(KNAME, VNAME) {                                    \
    /** Hash computed from key before insertion into data structure. */        \
    c_hash_t hash;                                                             \
    /** The key data to be compared when hashes are equal. */                  \
    KTYPE key;                                                                 \
    /** The data payload. */                                                   \
    VTYPE value;                                                               \
};

C_KEYVALUE_HASH_T(/* empty */,   void*,      /* empty */,    void*);
C_KEYVALUE_HASH_T(_int32,        c_int32_t,  /* empty */,    void*);
C_KEYVALUE_HASH_T(_int16,        c_int16_t,  /* empty */,    void*);
C_KEYVALUE_HASH_T(_int64,        c_int64_t,  /* empty */,    void*);
C_KEYVALUE_HASH_T(_str,          c_str_t,    /* empty */,    void*);
C_KEYVALUE_HASH_T(_uint16,       c_uint16_t, /* empty */,    void*);
C_KEYVALUE_HASH_T(_uint32,       c_uint32_t, /* empty */,    void*);
C_KEYVALUE_HASH_T(_uint64,       c_uint64_t, /* empty */,    void*);

#undef C_KEYVALUE_HASH_T

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/
