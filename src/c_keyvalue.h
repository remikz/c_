#ifndef C_KEYVALUE_INCLUDED
#define C_KEYVALUE_INCLUDED

#include "c_type.h"

#ifdef __cplusplus
extern "C" {
#endif

/** Key and Value types are definable. */
#define C_KEYVALUE_TYPE(KNAME, VNAME) c_keyvalue##KNAME##VNAME##_t

#define C_KEYVALUE_TYPEDEF(KNAME, VNAME)                                       \
            C_TYPEDEF(C_KEYVALUE_TYPE(KNAME, VNAME))

#define C_KEYVALUE_TEMPLATE_NAME(MACRO)                                        \
    MACRO(/* void* */, /* empty */)                                            \
    MACRO(_int16,      /* empty */)                                            \
    MACRO(_int32,      /* empty */)                                            \
    MACRO(_int64,      /* empty */)                                            \
    MACRO(_uint16,     /* empty */)                                            \
    MACRO(_uint32,     /* empty */)                                            \
    MACRO(_uint64,     /* empty */)

#define C_KEYVALUE_TEMPLATE_NAME_TYPE(MACRO)                                   \
    MACRO(_int16,  c_int16_t,  /* empty */, void*)                             \
    MACRO(_int32,  c_int32_t,  /* empty */, void*)                             \
    MACRO(_int64,  c_int64_t,  /* empty */, void*)                             \
    MACRO(_uint16, c_uint16_t, /* empty */, void*)                             \
    MACRO(_uint32, c_uint32_t, /* empty */, void*)                             \
    MACRO(_uint64, c_uint64_t, /* empty */, void*)

#define C_KEYVALUE_CMP_NAME(KNAME, VNAME) c_keyvalue_cmp##KNAME##VNAME
#define C_KEYVALUE_CMP_TYPE(KNAME, VNAME) c_keyvalue_cmp##KNAME##VNAME##_t

#define C_KEYVALUE_CMP_TYPEDEF(KNAME, KTYPE, VNAME, VTYPE)                     \
    typedef c_int32_t (*C_KEYVALUE_CMP_TYPE(KNAME, VNAME))(               \
                            const KTYPE,                                       \
                            const C_KEYVALUE_TYPE(KNAME, VNAME));

/** Compare keyvalue keys.
 * @return 0 if equal, -1 if key is less than keyvalue key, 1 if greater.
 */
#define C_KEYVALUE_CMP(KNAME, KTYPE, VNAME, VTYPE)                             \
    extern c_int32_t C_KEYVALUE_CMP_NAME(KNAME, VNAME)(                        \
                        const KTYPE key,                                       \
                        const C_KEYVALUE_TYPE(KNAME, VNAME) keyvalue);

#define C_KEYVALUE_FREE(KNAME, KTYPE, VNAME, VTYPE)                            \
    extern void c_keyvalue_free##KNAME##VNAME(C_KEYVALUE_TYPE(KNAME, VNAME) *kv);

#define C_KEYVALUE_NEW(KNAME, KTYPE, VNAME, VTYPE)                             \
    extern C_KEYVALUE_TYPE(KNAME, VNAME) c_keyvalue_new##KNAME##VNAME(         \
                                            KTYPE key,                         \
                                            VTYPE value);

C_KEYVALUE_TEMPLATE_NAME(C_KEYVALUE_TYPEDEF)
C_KEYVALUE_TEMPLATE_NAME_TYPE(C_KEYVALUE_CMP_TYPEDEF)
C_KEYVALUE_TEMPLATE_NAME_TYPE(C_KEYVALUE_CMP)
C_KEYVALUE_TEMPLATE_NAME_TYPE(C_KEYVALUE_FREE)
C_KEYVALUE_TEMPLATE_NAME_TYPE(C_KEYVALUE_NEW)

#undef C_KEYVALUE_CMP
#undef C_KEYVALUE_FREE
#undef C_KEYVALUE_NEW

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/