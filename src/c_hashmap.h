#ifndef C_HASHMAP_INCLUDED
#define C_HASHMAP_INCLUDED

#include "c_hash.h"
#include "c_keyvalue_hash.h"

#ifdef __cplusplus
extern "C" {
#endif

/** Hashmap with Key and Value types user-definable.
 * The most common use case is to have a string or numeric key type
 * and void* values to store arbitrary pointers.
 * The hashmap will not own the value memory, but will make and manage copies
 * of key memory.
 * The hash and comparison functions can be user defined.
 */
#define C_HASHMAP_TYPE(KNAME, VNAME) c_hashmap##KNAME##VNAME##_t

#define C_HASHMAP_TYPEDEF(KNAME, VNAME)                                        \
            C_TYPEDEF(C_HASHMAP_TYPE(KNAME, VNAME))

#define C_HASHMAP_TEMPLATE_NAME(MACRO)                                         \
    MACRO(/* empty */, /* empty */)                                            \
    MACRO(_int16,      /* empty */)                                            \
    MACRO(_int32,      /* empty */)                                            \
    MACRO(_int64,      /* empty */)                                            \
    MACRO(_uint16,     /* empty */)                                            \
    MACRO(_uint32,     /* empty */)                                            \
    MACRO(_uint64,     /* empty */)

#define C_HASHMAP_TEMPLATE_NAME_TYPE(MACRO)                                    \
    MACRO(_int16,  c_int16_t,  /* empty */, void*)                             \
    MACRO(_int32,  c_int32_t,  /* empty */, void*)                             \
    MACRO(_int64,  c_int64_t,  /* empty */, void*)                             \
    MACRO(_uint16, c_uint16_t, /* empty */, void*)                             \
    MACRO(_uint32, c_uint32_t, /* empty */, void*)                             \
    MACRO(_uint64, c_uint64_t, /* empty */, void*)

/** @return Vector of keyvalues if bucket_index is valid, otherwise NULL. */
#define C_HASHMAP_BUCKET(KN, KT, VN, VT)                                       \
    extern c_vector_t c_hashmap_bucket##KN##VN(                                \
                        C_HASHMAP_TYPE(KN, VN) hashmap,                        \
                        c_uint64_t bucket_index);

/** @return Item if stored with key. */
#define C_HASHMAP_BUCKET_GET(KN, KT, VN, VT)                                   \
    extern VT c_hashmap_bucket_get##KN##VN(                                    \
                        C_HASHMAP_TYPE(KN, VN) hashmap,                        \
                        c_uint64_t bucket_index,                               \
                        c_hash_t hash,                                         \
                        const KT key);

/** Put value into a bucket, and create new bucket if none yet exists for hash.
 * @return Previous value if any stored with same key.
 */
#define C_HASHMAP_BUCKET_PUT(KN, KT, VN, VT)                                   \
    extern VT c_hashmap_bucket_put##KN##VN(                                    \
                        C_HASHMAP_TYPE(KN, VN) hashmap,                        \
                        c_uint64_t bucket_index,                               \
                        c_hash_t hash,                                         \
                        KT key,                                                \
                        VT value);

/** Remove value if key is stored in bucket.
 * @return Previous value if any stored with same key.
 */
#define C_HASHMAP_BUCKET_REMOVE(KN, KT, VN, VT)                                \
    extern VT c_hashmap_bucket_remove##KN##VN(                                 \
                        C_HASHMAP_TYPE(KN, VN) hashmap,                        \
                        c_uint64_t bucket_index,                               \
                        c_hash_t hash,                                         \
                        const KT key);

/** Find a non-empty bucket index.
 * @param first Start checking from this 0-based index.
 * @return Index of non-empty bucket. If none found, then will be >= capacity.
 */
#define C_HASHMAP_FIND_BUCKET(KN, KT, VN, VT)                                  \
    extern c_uint64_t c_hashmap_find_bucket##KN##VN(                           \
                            C_HASHMAP_TYPE(KN, VN) hashmap,                    \
                            c_uint64_t first);

#define C_HASHMAP_FREE(KN, KT, VN, VT)                                         \
    extern void c_hashmap_free##KN##VN(C_HASHMAP_TYPE(KN, VN) *hashmap);

#define C_HASHMAP_FREE_BUCKET(KN, KT, VN, VT)                                  \
    extern void c_hashmap_free_bucket##KN##VN(C_HASHMAP_TYPE(KN, VN) hashmap,  \
                    c_uint64_t index);                                         \

#define C_HASHMAP_FREE_BUCKETS(KN, KT, VN, VT)                                 \
    extern void c_hashmap_free_buckets##KN##VN(C_HASHMAP_TYPE(KN, VN) hashmap);\

/** @return Item if stored with key. */
#define C_HASHMAP_GET(KN, KT, VN, VT)                                          \
    extern VT c_hashmap_get##KN##VN(                                           \
                        C_HASHMAP_TYPE(KN, VN) hashmap,                        \
                        const KT key);

/** @return True if contains key. */
#define C_HASHMAP_HAS(KN, KT, VN, VT)                                          \
    extern c_bool_t c_hashmap_has##KN##VN(C_HASHMAP_TYPE(KN, VN) hashmap,      \
                                          const KT key);

/** Find index of key in a bucket chain.
 * @param bucket_index Which bucket to search.
 * @return -1 if not found, otherwise index of item in bucket.
 */
#define C_HASHMAP_INDEX(KN, KT, VN, VT)                                        \
    extern c_int64_t c_hashmap_index##KN##VN(                                  \
                        C_HASHMAP_TYPE(KN, VN) hashmap,                        \
                        c_uint64_t bucket_index,                               \
                        c_hash_t hash,                                         \
                        const KT key);

/** If keys are pointers like void* or strings, then caller will get only
 * references and not deep memory copies.
 * @return Vector of all keys in no order. Vector will never be NULL.
 * @warning Not thread-safe and doesn't check if hashmap was changing.
 */
#define C_HASHMAP_KEYS(KN, VN)                                                 \
    extern C_VECTOR_TYPE(KN) c_hashmap_keys##KN##VN(                           \
                            C_HASHMAP_TYPE(KN, VN) hashmap);

/** Computes number of buckets no empty and computes ratio.
 * Runtime: O(n)
 * @return Ratio of buckets used over total buckets possible.
 */
#define C_HASHMAP_LOAD_FACTOR(KN, KT, VN, VT)                                  \
    extern c_float32_t c_hashmap_load_factor##KN##VN(                          \
                            C_HASHMAP_TYPE(KN, VN) hashmap);

/** Create new hashmap that uses numeric keys.
 * @param cmp User function to compare hashes and keys.
 *            If NULL, defaults to comparing keys directly as numeric values.
 * @param hash User function to hash keys.
 *             If NULL, defaults to function that uses key verbatim.
 */
#define C_HASHMAP_NEW(KN, KT, VN, VT)                                          \
    extern C_HASHMAP_TYPE(KN, VN) c_hashmap_new##KN##VN(                       \
                                    c_uint64_t num_buckets,                    \
                                    C_KEYVALUE_HASH_CMP_TYPE(KN, VN) cmp,      \
                                    C_HASH_NAME(KN) hash);

/** @return Number of buckets. */
#define C_HASHMAP_NUM_BUCKETS(KN, KT, VN, VT)                                  \
    extern c_uint64_t c_hashmap_num_buckets##KN##VN(                           \
                            C_HASHMAP_TYPE(KN, VN) hashmap);

/** @return Number of items stored. */
#define C_HASHMAP_NUM_ITEMS(KN, KT, VN, VT)                                    \
    extern c_uint64_t c_hashmap_num_items##KN##VN(                             \
                            C_HASHMAP_TYPE(KN, VN) hashmap);

/** @return Previous item stored with same key, if any. Otherwise, NULL. */
#define C_HASHMAP_PUT(KN, KT, VN, VT)                                          \
    extern VT c_hashmap_put##KN##VN(                                           \
                        C_HASHMAP_TYPE(KN, VN) hashmap,                        \
                        KT key,                                                \
                        VT value);

/** @return Previous item stored with same key, if any. Otherwise, NULL. */
#define C_HASHMAP_REMOVE(KN, KT, VN, VT)                                       \
    extern VT c_hashmap_remove##KN##VN(                                        \
                        C_HASHMAP_TYPE(KN, VN) hashmap,                        \
                        const KT key);

/** @return Previous item stored with same key, if any. Otherwise, NULL. */
#define C_HASHMAP_TIMESTAMP(KN, KT, VN, VT)                                    \
    extern c_uint64_t c_hashmap_timestamp##KN##VN(                             \
                        C_HASHMAP_TYPE(KN, VN) hashmap);

/** If values are pointers like void* or strings, then caller will get only
 * references and not deep memory copies.
 * @return Vector of all values in no order. Vector will never be NULL.
 * @warning Not thread-safe and doesn't check if hashmap was changing.
 */
#define C_HASHMAP_VALUES(KN, VN)                                               \
    extern C_VECTOR_TYPE(VN) c_hashmap_values##KN##VN(                         \
                            C_HASHMAP_TYPE(KN, VN) hashmap);

C_KEYVALUE_HASH_TEMPLATE_NAME(C_HASHMAP_TYPEDEF)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_BUCKET)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_BUCKET_GET)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_BUCKET_PUT)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_BUCKET_REMOVE)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_FIND_BUCKET)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_FREE_BUCKET)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_FREE_BUCKETS)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_FREE)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_GET)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_HAS)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_INDEX)
C_KEYVALUE_HASH_TEMPLATE_NAME_NUMERIC(C_HASHMAP_KEYS)
C_KEYVALUE_HASH_TEMPLATE_NAME_PTR(C_HASHMAP_KEYS)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_LOAD_FACTOR)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_NEW)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_NUM_BUCKETS)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_NUM_ITEMS)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_PUT)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_REMOVE)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_TIMESTAMP)
C_KEYVALUE_HASH_TEMPLATE_NAME_NUMERIC(C_HASHMAP_VALUES)
C_KEYVALUE_HASH_TEMPLATE_NAME_PTR(C_HASHMAP_VALUES)

#undef C_HASHMAP_BUCKET
#undef C_HASHMAP_BUCKET_GET
#undef C_HASHMAP_BUCKET_PUT
#undef C_HASHMAP_BUCKET_REMOVE
#undef C_HASHMAP_FIND_BUCKET
#undef C_HASHMAP_FREE_BUCKET
#undef C_HASHMAP_FREE_BUCKETS
#undef C_HASHMAP_FREE
#undef C_HASHMAP_GET
#undef C_HASHMAP_HAS
#undef C_HASHMAP_INDEX
#undef C_HASHMAP_KEYS
#undef C_HASHMAP_LOAD_FACTOR
#undef C_HASHMAP_NEW
#undef C_HASHMAP_NUM_BUCKETS
#undef C_HASHMAP_NUM_ITEMS
#undef C_HASHMAP_PUT
#undef C_HASHMAP_REMOVE
#undef C_HASHMAP_TIMESTAMP
#undef C_HASHMAP_VALUES

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/
