#ifndef TEST_C_NUMERIC_INCLUDED
#define TEST_C_NUMERIC_INCLUDED

#include "c_test.h"
#include "c_numeric.h"

void test_c_numeric_max()
{
    #define TEST(N, T)                                                         \
    T a##N = 0;                                                                \
    T b##N = 1;                                                                \
                                                                               \
    C_TEST_ASSERT_EQ(b##N, c_numeric_max##N(a##N, b##N), "Expected max.");

    C_TYPE_TEMPLATE_NAME_TYPE_VALUE(TEST)
    #undef TEST
}

void test_c_numeric_min()
{
    #define TEST(N, T)                                                         \
    T a##N = 0;                                                                \
    T b##N = 1;                                                                \
                                                                               \
    C_TEST_ASSERT_EQ(a##N, c_numeric_min##N(a##N, b##N), "Expected min.");

    C_TYPE_TEMPLATE_NAME_TYPE_VALUE(TEST)
    #undef TEST
}

int suite_test_c_numeric()
{
    C_TEST_ADD_SUITE("suite_test_c_numeric", NULL, NULL);

    C_TEST_ADD_TEST(test_c_numeric_max);
    C_TEST_ADD_TEST(test_c_numeric_min);

    C_TEST_RETURN();
}

#endif
/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/