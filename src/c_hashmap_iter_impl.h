#ifndef C_HASHMAP_ITER_IMPL_T_INCLUDED
#define C_HASHMAP_ITER_IMPL_T_INCLUDED

#include "c_hashmap_iter.h"
#include "c_hashmap_iter_t.h"
#include "c_memory.h"

#ifdef __cplusplus
extern "C" {
#endif

#define C_HASHMAP_ITER_ADVANCE(KN, VN)                                         \
void c_hashmap_iter_advance##KN##VN(C_HASHMAP_ITER_TYPE(KN, VN) iter)          \
{                                                                              \
    c_vector_t bucket = NULL;                                                  \
    c_uint64_t nkeyvalues = 0, nbuckets;                                       \
                                                                               \
    if (iter) {                                                                \
        ++iter->keyvalue_index;                                                \
                                                                               \
        bucket = c_hashmap_bucket##KN##VN(iter->hashmap, iter->bucket_index);  \
        nkeyvalues = c_vector_size(bucket);                                    \
                                                                               \
        if (nkeyvalues <= iter->keyvalue_index) {                              \
            iter->keyvalue_index = 0;                                          \
            iter->bucket_index = c_hashmap_find_bucket##KN##VN(                \
                                    iter->hashmap,                             \
                                    iter->bucket_index);                       \
        }                                                                      \
                                                                               \
        ++iter->position;                                                      \
    }                                                                          \
}

#define C_HASHMAP_ITER_FREE(KN, VN)                                            \
void c_hashmap_iter_free##KN##VN(C_HASHMAP_ITER_TYPE(KN, VN) *iter)            \
{                                                                              \
    assert(iter);                                                              \
    assert(*iter);                                                             \
    if (iter) {                                                                \
        C_MEMORY_FREE(*iter);                                                  \
    }                                                                          \
}

#define C_HASHMAP_ITER_HASH(KN, VN)                                            \
c_hash_t c_hashmap_iter_hash##KN##VN(C_HASHMAP_ITER_TYPE(KN, VN) iter)         \
{                                                                              \
    c_vector_t bucket = NULL;                                                  \
    C_KEYVALUE_HASH_TYPE(KN, VN) keyvalue;                                     \
                                                                               \
    if (iter) {                                                                \
        bucket = c_hashmap_bucket##KN##VN(iter->hashmap, iter->bucket_index);  \
        keyvalue = c_vector_get_safe(bucket, iter->keyvalue_index);            \
        if (keyvalue) {                                                        \
            return c_keyvalue_hash_hash##KN##VN(keyvalue);                     \
        }                                                                      \
    }                                                                          \
    return 0;                                                                  \
}

#define C_HASHMAP_ITER_IS_DONE(KN, VN)                                         \
c_bool_t c_hashmap_iter_is_done##KN##VN(C_HASHMAP_ITER_TYPE(KN, VN) iter)      \
{                                                                              \
    if (iter && iter->hashmap) {                                               \
        return c_hashmap_num_items##KN##VN(iter->hashmap) <= iter->position ;  \
    }                                                                          \
    return C_TRUE;                                                             \
}

#define C_HASHMAP_ITER_IS_VALID(KN, VN)                                        \
c_bool_t c_hashmap_iter_is_valid##KN##VN(C_HASHMAP_ITER_TYPE(KN, VN) iter)     \
{                                                                              \
    if (iter && iter->hashmap) {                                               \
        return c_hashmap_timestamp##KN##VN(iter->hashmap) == iter->timestamp;  \
    }                                                                          \
    return C_FALSE;                                                            \
}

#define C_HASHMAP_ITER_KEY(KN, KT, VN, VT)                                     \
KT c_hashmap_iter_key##KN##VN(C_HASHMAP_ITER_TYPE(KN, VN) iter)                \
{                                                                              \
    c_vector_t bucket = NULL;                                                  \
    C_KEYVALUE_HASH_TYPE(KN, VN) keyvalue;                                     \
                                                                               \
    if (iter) {                                                                \
        bucket = c_hashmap_bucket##KN##VN(iter->hashmap, iter->bucket_index);  \
        keyvalue = c_vector_get_safe(bucket, iter->keyvalue_index);            \
        if (keyvalue) {                                                        \
            return c_keyvalue_hash_key##KN##VN(keyvalue);                      \
        }                                                                      \
    }                                                                          \
    return 0;                                                                  \
}


#define C_HASHMAP_ITER_NEW(KN, VN)                                             \
C_HASHMAP_ITER_TYPE(KN, VN) c_hashmap_iter_new##KN##VN(                        \
                                C_HASHMAP_TYPE(KN, VN) hashmap)                \
{                                                                              \
    C_HASHMAP_ITER_TYPE(KN, VN) result;                                        \
    C_MEMORY_NEW(result);                                                      \
                                                                               \
    result->keyvalue_index = 0;                                                \
    result->position = 0;                                                      \
    result->hashmap = hashmap;                                                 \
    if (hashmap) {                                                             \
        result->timestamp = c_hashmap_timestamp##KN##VN(hashmap);              \
        result->bucket_index = c_hashmap_find_bucket##KN##VN(hashmap, 0);      \
    } else {                                                                   \
        result->timestamp = 0;                                                 \
        result->bucket_index = 0;                                              \
    }                                                                          \
                                                                               \
    return result;                                                             \
}

#define C_HASHMAP_ITER_POSITION(KN, VN)                                        \
c_uint64_t c_hashmap_iter_position##KN##VN(C_HASHMAP_ITER_TYPE(KN, VN) iter)   \
{                                                                              \
    if (iter) {                                                                \
        return iter->position;                                                 \
    }                                                                          \
    return 0;                                                                  \
}

#define C_HASHMAP_ITER_RESET(KN, VN)                                           \
void c_hashmap_iter_reset##KN##VN(C_HASHMAP_ITER_TYPE(KN, VN) iter)            \
{                                                                              \
    if (iter) {                                                                \
        iter->bucket_index = 0;                                                \
        iter->keyvalue_index = 0;                                              \
        iter->position = 0;                                                    \
        if (iter->hashmap) {                                                   \
            iter->timestamp = c_hashmap_timestamp##KN##VN(iter->hashmap);      \
        } else {                                                               \
            iter->timestamp = 0;                                               \
        }                                                                      \
    }                                                                          \
}

#define C_HASHMAP_ITER_VALUE(KN, KT, VN, VT)                                   \
VT c_hashmap_iter_value##KN##VN(C_HASHMAP_ITER_TYPE(KN, VN) iter)              \
{                                                                              \
    c_vector_t bucket = NULL;                                                  \
    C_KEYVALUE_HASH_TYPE(KN, VN) keyvalue;                                     \
                                                                               \
    if (iter) {                                                                \
        bucket = c_hashmap_bucket##KN##VN(iter->hashmap, iter->bucket_index);  \
        keyvalue = c_vector_get_safe(bucket, iter->keyvalue_index);            \
        if (keyvalue) {                                                        \
            return c_keyvalue_hash_value##KN##VN(keyvalue);                    \
        }                                                                      \
    }                                                                          \
    return 0;                                                                  \
}

C_KEYVALUE_HASH_TEMPLATE_NAME(C_HASHMAP_ITER_TYPEDEF)
C_KEYVALUE_HASH_TEMPLATE_NAME(C_HASHMAP_ITER_ADVANCE)
C_KEYVALUE_HASH_TEMPLATE_NAME(C_HASHMAP_ITER_FREE)
C_KEYVALUE_HASH_TEMPLATE_NAME(C_HASHMAP_ITER_HASH)
C_KEYVALUE_HASH_TEMPLATE_NAME(C_HASHMAP_ITER_IS_DONE)
C_KEYVALUE_HASH_TEMPLATE_NAME(C_HASHMAP_ITER_IS_VALID)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_ITER_KEY)
C_KEYVALUE_HASH_TEMPLATE_NAME(C_HASHMAP_ITER_NEW)
C_KEYVALUE_HASH_TEMPLATE_NAME(C_HASHMAP_ITER_POSITION)
C_KEYVALUE_HASH_TEMPLATE_NAME(C_HASHMAP_ITER_RESET)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_ITER_VALUE)

#undef C_HASHMAP_ITER_ADVANCE
#undef C_HASHMAP_ITER_FREE
#undef C_HASHMAP_ITER_HASH
#undef C_HASHMAP_ITER_IS_DONE
#undef C_HASHMAP_ITER_IS_VALID
#undef C_HASHMAP_ITER_KEY
#undef C_HASHMAP_ITER_NEW
#undef C_HASHMAP_ITER_POSITION
#undef C_HASHMAP_ITER_RESET
#undef C_HASHMAP_ITER_VALUE

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/
