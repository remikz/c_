#include "test_c_test.h"
#include "test_c_log.h"
#include "test_c_memory.h"
#include "test_c_type.h"
#include "test_c_time.h"
#include "test_c_numeric.h"
#include "test_c_cmp.h"
#include "test_c_bench.h"
#include "test_c_str.h"
#include "test_c_vector.h"
#include "test_c_vector_factory.h"
#include "test_c_vector_find.h"
#include "test_c_vector_iter.h"
#include "test_c_slice.h"
#include "test_c_vector_slice_iter.h"
#include "test_c_vector_sort.h"
#include "test_c_vector_quicksort.h"
#include "test_c_keyvalue.h"
#include "test_c_keyvalue_hash.h"
#include "test_c_hash.h"
#include "test_c_hashmap.h"
#include "test_c_hashmap_iter.h"

C_TEST_REGISTRY_DECLARE();

int main(int argc, char **argv)
{
    int result = EXIT_SUCCESS;
    c_bool_t color = argc > 1 && !strcmp(argv[1], "--color");

    C_TEST_REGISTRY_INIT("test_c_all", color);

    suite_test_c_test_asserts();
    suite_test_c_test_suites();
    suite_test_c_test_suite_setup_teardown();
    suite_test_c_test_files();
    suite_test_c_test_edge_cases();
    suite_test_c_log();
    suite_test_c_memory();
    suite_test_c_types();
    suite_test_c_time();
    suite_test_c_numeric();
    suite_test_c_cmp();
    suite_test_c_bench();
    suite_test_c_str();
    suite_test_c_vector();
    suite_test_c_vector_factory();
    suite_test_c_vector_find();
    suite_test_c_vector_iter();
    suite_test_c_slice();
    suite_test_c_vector_slice_iter();
    suite_test_c_vector_sort();
    suite_test_c_vector_quicksort();
    suite_test_c_keyvalue();
    suite_test_c_keyvalue_hash();
    suite_test_c_hash();
    suite_test_c_hashmap();
    suite_test_c_hashmap_iter();

    C_TEST_RUN();
    C_TEST_XML("test_c_all.xml");

recover:
    C_TEST_SHUTDOWN();

    return result;
}
/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/