#ifndef C_VECTOR_T_INCLUDED
#define C_VECTOR_T_INCLUDED

#include "c_vector.h"

#ifdef __cplusplus
extern "C" {
#endif

#define C_VECTOR_T(NAME, TYPE)                                                 \
struct C_VECTOR_TYPE(NAME) {                                                   \
    TYPE *items;                                                               \
    /** Maximum number of items storable without resizing memory. */           \
    c_uint64_t capacity;                                                       \
    /** Number of items stored. */                                             \
    c_uint64_t size;                                                           \
    /** An increasing stamp that increases during each vector change,
        for iterator validity checks. */                                       \
    c_uint64_t timestamp;                                                      \
};

C_TYPE_TEMPLATE_NAME_TYPE(C_VECTOR_T)
#undef C_VECTOR_T

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/
