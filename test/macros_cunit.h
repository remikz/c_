#ifndef MACROS_CUNIT_INCLUDED
#define MACROS_CUNIT_INCLUDED

#include <CUnit/Basic.h>

#define ADD_SUITE(NAME, SETUP, TEARDOWN)                                       \
    CU_pSuite pSuite = NULL;                                                   \
    pSuite = CU_add_suite(NAME, SETUP, TEARDOWN);                              \
    if (NULL == pSuite) {                                                      \
        goto recover;                                                          \
    }

#define ADD_TEST(NAME, TEST)                                                   \
    if (NULL == CU_add_test(pSuite, NAME, TEST)) {                             \
        goto recover;                                                          \
    }

#define NEW_REGISTRY() REGISTER(CU_initialize_registry)

#define REGISTER_SUITE(SUITE) if (CUE_SUCCESS != SUITE()) { goto recover; }

#define REGISTRY_INIT()

#define REGISTRY_DECLARE()

#define RETURN() return CU_get_error()

#define RUN() CU_basic_run_tests()

#define SHUTDOWN() CU_cleanup_registry()

#endif
/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/