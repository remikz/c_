#ifndef C_LOG_INCLUDED
#define C_LOG_INCLUDED

#include <stdarg.h>
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

#define C_LOG_NOCOLOR "\033[0m"
#define C_LOG_RED     "\033[0;31m"
#define C_LOG_GREEN   "\033[0;32m"
#define C_LOG_YELLOW  "\033[0;33m"
#define C_LOG_MAGENTA "\033[0;35m"

extern void c_log(const char *level, FILE *stream, const char *file, int line, ...);
/* Example output: 2015-07-16 16:29:41.607984 -0400 INFO file.c:123 message */
extern void c_log_str(const char *level, char *output, unsigned int output_size, const char *file, int line, ...);

#define C_LOG(LEVEL, STREAM, ...) { c_log(LEVEL, STREAM, __FILE__, __LINE__, __VA_ARGS__); }

#define C_LOG_DEBUG(...)        { c_log(             "DEBUG",               stdout, __FILE__, __LINE__, __VA_ARGS__); }
#define C_LOG_DEBUG_YELLOW(...) { c_log(C_LOG_YELLOW "DEBUG" C_LOG_NOCOLOR, stdout, __FILE__, __LINE__, __VA_ARGS__); }
#define C_LOG_DEBUG_COLOR(COLOR, ...) if (COLOR) { C_LOG_DEBUG_YELLOW(__VA_ARGS__); } else { C_LOG_DEBUG(__VA_ARGS__); }

#define C_LOG_ERROR(...)     { c_log(          "ERROR",               stdout, __FILE__, __LINE__, __VA_ARGS__); }
#define C_LOG_ERROR_RED(...) { c_log(C_LOG_RED "ERROR" C_LOG_NOCOLOR, stdout, __FILE__, __LINE__, __VA_ARGS__); }
#define C_LOG_ERROR_COLOR(COLOR, ...) if (COLOR) { C_LOG_ERROR_RED(__VA_ARGS__); } else { C_LOG_ERROR(__VA_ARGS__); }

#define C_LOG_INFO(...)       { c_log(            "INFO",               stdout, __FILE__, __LINE__, __VA_ARGS__); }
#define C_LOG_INFO_GREEN(...) { c_log(C_LOG_GREEN "INFO" C_LOG_NOCOLOR, stdout, __FILE__, __LINE__, __VA_ARGS__); }
#define C_LOG_INFO_COLOR(COLOR, ...) if (COLOR) { C_LOG_INFO_GREEN(__VA_ARGS__); } else { C_LOG_INFO(__VA_ARGS__); }

#define C_LOG_WARN(...)         { c_log(              "WARN",               stdout, __FILE__, __LINE__, __VA_ARGS__); }
#define C_LOG_WARN_MAGENTA(...) { c_log(C_LOG_MAGENTA "WARN" C_LOG_NOCOLOR, stdout, __FILE__, __LINE__, __VA_ARGS__); }
#define C_LOG_WARN_COLOR(COLOR, ...) if (COLOR) { C_LOG_WARN_MAGENTA(__VA_ARGS__); } else { C_LOG_WARN(__VA_ARGS__); }

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/