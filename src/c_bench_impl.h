#ifndef C_BENCH_IMPL_INCLUDED
#define C_BENCH_IMPL_INCLUDED

#include "c_bench.h"
#include "c_bench_t.h"
#include "c_memory.h"
#include "c_numeric.h"
#include "c_time.h"
#include <limits.h> // ULLONG_MAX
#include <stdio.h> // puts, snprintf

#ifdef __cplusplus
extern "C" {
#endif

void c_bench_free(c_bench_t *b)
{
    assert(b);
    assert(*b);
    if (b) {
        C_MEMORY_FREE(*b);
    }
}

c_bench_t c_bench_new(c_uint64_t ntrials, void (*fun)(void))
{
    c_bench_t result;
    C_MEMORY_NEW(result);
    c_bench_reset(result);

    return result;
}

void c_bench_print(c_bench_t bench)
{
    const c_uint64_t NCHARS = 256;
    char chars[NCHARS];

    if (bench) {
        c_bench_to_chars(bench, chars, NCHARS);
        puts(chars);
    }
}

void c_bench_reset(c_bench_t bench)
{
    bench->ntrials = 0;
    bench->ns_min = ULLONG_MAX;
    bench->ns_max = 0;
    bench->ns_mean = 0.0;
}

void c_bench_run(c_bench_t bench, c_uint64_t ntrials, void (*fun)(void))
{
    c_bench_run_setup_teardown(bench, ntrials, fun, NULL, NULL);
}

extern void c_bench_run_setup_teardown(
        c_bench_t bench,
        c_uint64_t ntrials,
        void (*fun)(void),
        void (*setup)(void),
        void (*teardown)(void))
{
    c_uint64_t i = 0, sum = 0, t, tic, toc;
    const c_uint64_t ONE_SECOCND_NANOS = 1000000000;

    if (bench && fun) {
        c_bench_reset(bench);

        if (ntrials) {
            while (i++ < ntrials) {
                if (setup) {
                    setup();
                }
                tic = c_time_now_nsec();
                fun();
                toc = c_time_now_nsec();
                t = toc - tic;
                sum += t;
                bench->ns_max = c_numeric_max_uint64(t, bench->ns_max);
                bench->ns_min = c_numeric_min_uint64(t, bench->ns_min);
                if (teardown) {
                    teardown();
                }
            }
        } else {
            while (sum < ONE_SECOCND_NANOS) {
                if (setup) {
                    setup();
                }
                tic = c_time_now_nsec();
                fun();
                toc = c_time_now_nsec();
                t = toc - tic;
                sum += t;
                bench->ns_max = c_numeric_max_uint64(t, bench->ns_max);
                bench->ns_min = c_numeric_min_uint64(t, bench->ns_min);
                ++ntrials;
                if (teardown) {
                    teardown();
                }
            }
        }

        bench->ntrials = ntrials;
        bench->ns_mean = sum / ntrials;
    }
}

void c_bench_to_chars(c_bench_t bench, char *chars, c_uint64_t nchars)
{
    const c_uint64_t N = 32;
    char str_min[N], str_max[N], str_mean[N];
    const char TEMPL[] = "n=%llu min=%s max=%s mean=%s";

    if (bench && chars && nchars) {
        c_time_nsec_str(str_min, N, bench->ns_min);
        c_time_nsec_str(str_max, N, bench->ns_max);
        c_time_nsec_str(str_mean, N, bench->ns_mean);

        snprintf(chars, nchars, TEMPL,
                 bench->ntrials, str_min, str_max, str_mean);
    }
}

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/
