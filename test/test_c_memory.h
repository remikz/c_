#ifndef TEST_C_MEMORY_INCLUDED
#define TEST_C_MEMORY_INCLUDED

#include "c_test.h"
#include "c_memory.h"

void test_c_memory()
{
    int *p1 = NULL;

    C_TEST_ASSERT_FALSE(c_memory_alloc(-1, "dummy", 0), "Expected null.");
    C_TEST_ASSERT_FALSE(c_memory_alloc_zero(-1, "dummy", 0), "Expected null.");

    C_TEST_ASSERT_FALSE(c_memory_realloc(p1, -1, "dummy", 0), "Expected null.");
    p1 = c_memory_realloc(p1, sizeof(int) * 1, "dummy", 0);
    C_TEST_ASSERT(p1, "Expected non-null.");

    p1 = c_memory_realloc(p1, sizeof(int) * 2, "dummy", 0);
    C_TEST_ASSERT(p1, "Expected non-null.");

    free(p1);
}

int suite_test_c_memory()
{
    C_TEST_ADD_SUITE("suite_test_c_memory", NULL, NULL);
    C_TEST_ADD_TEST(test_c_memory);
recover:
    C_TEST_RETURN();
}

#endif
/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/