#ifndef C_KEYVALUE_HASH_IMPL_INCLUDED
#define C_KEYVALUE_HASH_IMPL_INCLUDED

#include "c_keyvalue_hash.h"
#include "c_keyvalue_hash_t.h"
#include "c_vector_t.h"
#include "c_memory.h"
#include <string.h> // memcpy

#ifdef __cplusplus
extern "C" {
#endif

#define C_KEYVALUE_HASH_CMP(KNAME, KTYPE, VNAME, VTYPE)                        \
c_int32_t C_KEYVALUE_HASH_CMP_NAME(KNAME, VNAME)(                              \
                c_hash_t hash,                                                 \
                const KTYPE key,                                               \
                const C_KEYVALUE_HASH_TYPE(KNAME, VNAME) keyvalue)             \
{                                                                              \
    if (keyvalue && hash == keyvalue->hash) {                                  \
        if (key < keyvalue->key) {                                             \
            return -1;                                                         \
        } else if (key > keyvalue->key) {                                      \
            return 1;                                                          \
        } else {                                                               \
            return 0;                                                          \
        }                                                                      \
    }                                                                          \
    return -1;                                                                 \
}

c_int32_t c_keyvalue_hash_cmp_str(                                             \
                c_hash_t hash,                                                 \
                const c_str_t key,                                             \
                const c_keyvalue_hash_str_t keyvalue)                          \
{                                                                              \
    if (key && keyvalue && hash == keyvalue->hash) {                           \
        return c_str_cmp(key, keyvalue->key);                                  \
    }                                                                          \
    return -1;                                                                 \
}

#define C_KEYVALUE_HASH_FREE(KNAME, KTYPE, VNAME, VTYPE)                       \
void c_keyvalue_hash_free##KNAME##VNAME(C_KEYVALUE_HASH_TYPE(KNAME, VNAME) *kv)\
{                                                                              \
    assert(kv);                                                                \
    assert(*kv);                                                               \
    if (kv) {                                                                  \
        C_MEMORY_FREE(*kv);                                                    \
    }                                                                          \
}

void c_keyvalue_hash_free_str(c_keyvalue_hash_str_t *kv)
{
    assert(kv);
    assert(*kv);
    c_str_free((c_str_t*) & (*kv)->key);
    C_MEMORY_FREE(*kv);
}

#define C_KEYVALUE_HASH_HASH(KNAME, KTYPE, VNAME, VTYPE)                       \
c_hash_t c_keyvalue_hash_hash##KNAME##VNAME(                                   \
            C_KEYVALUE_HASH_TYPE(KNAME, VNAME) kv)                             \
{                                                                              \
    if (kv) {                                                                  \
        return kv->hash;                                                       \
    }                                                                          \
    return 0;                                                                  \
}

#define C_KEYVALUE_HASH_KEY(KNAME, KTYPE, VNAME, VTYPE)                        \
KTYPE c_keyvalue_hash_key##KNAME##VNAME(                                       \
                C_KEYVALUE_HASH_TYPE(KNAME, VNAME) kv)                         \
{                                                                              \
    if (kv) {                                                                  \
        return kv->key;                                                        \
    }                                                                          \
    return 0;                                                                  \
}

#define C_KEYVALUE_HASH_NEW(KNAME, KTYPE, VNAME, VTYPE)                        \
C_KEYVALUE_HASH_TYPE(KNAME, VNAME) c_keyvalue_hash_new##KNAME##VNAME(          \
                                            c_hash_t hash,                     \
                                            KTYPE key,                         \
                                            VTYPE value)                       \
{                                                                              \
    C_KEYVALUE_HASH_TYPE(KNAME, VNAME) result;                                 \
    C_MEMORY_NEW(result);                                                      \
                                                                               \
    if (result) {                                                              \
        result->hash = hash;                                                   \
        result->key = key;                                                     \
        result->value = value;                                                 \
    }                                                                          \
    return result;                                                             \
}

c_keyvalue_hash_str_t c_keyvalue_hash_new_str(
                                            c_hash_t hash,
                                            const c_str_t key,
                                            void *value)
{
    c_keyvalue_hash_str_t result;
    C_MEMORY_NEW(result);

    if (result) {
        result->hash = hash;
        result->key = c_str_new_clear(0);
        c_str_copy(result->key, key);
        result->value = value;
    }
    return result;
}

#define C_KEYVALUE_HASH_VALUE(KNAME, KTYPE, VNAME, VTYPE)                      \
VTYPE c_keyvalue_hash_value##KNAME##VNAME(C_KEYVALUE_HASH_TYPE(KNAME, VNAME) kv)\
{                                                                              \
    if (kv) {                                                                  \
        return kv->value;                                                      \
    }                                                                          \
    return 0;                                                                  \
}

#define C_KEYVALUE_HASH_VALUE_SET(KNAME, KTYPE, VNAME, VTYPE)                  \
void c_keyvalue_hash_value_set##KNAME##VNAME(                                  \
                    C_KEYVALUE_HASH_TYPE(KNAME, VNAME) kv,                     \
                    VTYPE value)                                               \
{                                                                              \
    if (kv) {                                                                  \
        kv->value = value;                                                     \
    }                                                                          \
}

C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE_NUMERIC(C_KEYVALUE_HASH_CMP)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE_NUMERIC(C_KEYVALUE_HASH_FREE)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE_PTR(C_KEYVALUE_HASH_FREE)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_KEYVALUE_HASH_HASH)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_KEYVALUE_HASH_KEY)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE_NUMERIC(C_KEYVALUE_HASH_NEW)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE_PTR(C_KEYVALUE_HASH_NEW)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_KEYVALUE_HASH_VALUE)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_KEYVALUE_HASH_VALUE_SET)

#undef C_KEYVALUE_HASH_CMP
#undef C_KEYVALUE_HASH_FREE
#undef C_KEYVALUE_HASH_HASH
#undef C_KEYVALUE_HASH_KEY
#undef C_KEYVALUE_HASH_NEW
#undef C_KEYVALUE_HASH_VALUE
#undef C_KEYVALUE_HASH_VALUE_SET

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/
