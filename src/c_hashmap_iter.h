#ifndef C_HASHMAP_ITER_INCLUDED
#define C_HASHMAP_ITER_INCLUDED

#include "c_hashmap.h"

#ifdef __cplusplus
extern "C" {
#endif

/** Hashmap iterator.
 * Iteration order is not defined.
 */
#define C_HASHMAP_ITER_TYPE(KNAME, VNAME) c_hashmap_iter##KNAME##VNAME##_t

#define C_HASHMAP_ITER_TYPEDEF(KNAME, VNAME)                                   \
            C_TYPEDEF(C_HASHMAP_ITER_TYPE(KNAME, VNAME))

#define C_HASHMAP_ITER_ADVANCE(KN, VN)                                         \
    extern void c_hashmap_iter_advance##KN##VN(                                \
                    C_HASHMAP_ITER_TYPE(KN, VN) iter);

#define C_HASHMAP_ITER_FREE(KN, VN)                                            \
    extern void c_hashmap_iter_free##KN##VN(C_HASHMAP_ITER_TYPE(KN, VN) *iter);

/** @return True if iterator isn't exhausted. */
#define C_HASHMAP_ITER_IS_DONE(KN, VN)                                         \
    extern c_bool_t c_hashmap_iter_is_done##KN##VN(                            \
                        C_HASHMAP_ITER_TYPE(KN, VN) iter);

/** @return True if iterator isn't exhausted and provider hasn't changed. */
#define C_HASHMAP_ITER_IS_VALID(KN, VN)                                        \
    extern c_bool_t c_hashmap_iter_is_valid##KN##VN(                           \
                        C_HASHMAP_ITER_TYPE(KN, VN) iter);

/** Get hash without checking if iterator is in valid state.
 * @see c_hashmap_iter_is_valid()
 */
#define C_HASHMAP_ITER_HASH(KN, VN)                                            \
    extern c_hash_t c_hashmap_iter_hash##KN##VN(                               \
                        C_HASHMAP_ITER_TYPE(KN, VN) iter);

/** Get key without checking if iterator is in valid state.
 * @see c_hashmap_iter_is_valid()
 */
#define C_HASHMAP_ITER_KEY(KN, KT, VN, VT)                                     \
    extern KT c_hashmap_iter_key##KN##VN(C_HASHMAP_ITER_TYPE(KN, VN) iter);

#define C_HASHMAP_ITER_NEW(KN, VN)                                             \
    extern C_HASHMAP_ITER_TYPE(KN, VN) c_hashmap_iter_new##KN##VN(             \
                C_HASHMAP_TYPE(KN, VN) hashmap);

#define C_HASHMAP_ITER_POSITION(KN, VN)                                        \
    extern c_uint64_t c_hashmap_iter_position##KN##VN(                         \
                            C_HASHMAP_ITER_TYPE(KN, VN) iter);

/** Make iterator re-usable and reset position to beginning of hashmap. */
#define C_HASHMAP_ITER_RESET(KN, VN)                                           \
    extern void c_hashmap_iter_reset##KN##VN(C_HASHMAP_ITER_TYPE(KN, VN) iter);

/** Get key without checking if iterator is in valid state.
 * @see c_hashmap_iter_is_valid()
 */
#define C_HASHMAP_ITER_VALUE(KN, KT, VN, VT)                                   \
    extern VT c_hashmap_iter_value##KN##VN(C_HASHMAP_ITER_TYPE(KN, VN) iter);

C_KEYVALUE_HASH_TEMPLATE_NAME(C_HASHMAP_ITER_TYPEDEF)
C_KEYVALUE_HASH_TEMPLATE_NAME(C_HASHMAP_ITER_ADVANCE)
C_KEYVALUE_HASH_TEMPLATE_NAME(C_HASHMAP_ITER_FREE)
C_KEYVALUE_HASH_TEMPLATE_NAME(C_HASHMAP_ITER_HASH)
C_KEYVALUE_HASH_TEMPLATE_NAME(C_HASHMAP_ITER_IS_DONE)
C_KEYVALUE_HASH_TEMPLATE_NAME(C_HASHMAP_ITER_IS_VALID)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_ITER_KEY)
C_KEYVALUE_HASH_TEMPLATE_NAME(C_HASHMAP_ITER_NEW)
C_KEYVALUE_HASH_TEMPLATE_NAME(C_HASHMAP_ITER_POSITION)
C_KEYVALUE_HASH_TEMPLATE_NAME(C_HASHMAP_ITER_RESET)
C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(C_HASHMAP_ITER_VALUE)

#undef C_HASHMAP_ITER_ADVANCE
#undef C_HASHMAP_ITER_FREE
#undef C_HASHMAP_ITER_HASH
#undef C_HASHMAP_ITER_IS_DONE
#undef C_HASHMAP_ITER_IS_VALID
#undef C_HASHMAP_ITER_KEY
#undef C_HASHMAP_ITER_NEW
#undef C_HASHMAP_ITER_POSITION
#undef C_HASHMAP_ITER_RESET
#undef C_HASHMAP_ITER_VALUE

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/
