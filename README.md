## c_ [see-uhn-der-skohr]

c-underscore is a linkable or header-only C library of data structures and algorithms
that are lacking in a standard C library. They are meant to be simple, testable,
extensible and open. Implementations of the latest advances in research or
standards are not intended; nor is the full portability across every platform
and compiler. Instead, c_ exists as a reference for comparison with more complex
and optimized implementations.

## Testing

    # Configure, Build, Unit Test, Memory Test, Coverage Test
    ./scripts/cmake_all_gcc.sh

### Coverage Status

|2016-01-25           |    Hit |  Total | Coverage |
|:--------------------|-------:|-------:|---------:|
|Lines:               |   3677 |   3690 |   99.6 % |
|Functions:           |   2461 |   2461 |  100.0 % |

## Contributing

Before making a Pull Request, please ensure coverage tests are
- 100% for functions
- above 2 sigma (ideally 3) for lines
- free of memory leaks and invalid references

## License

c_  Copyright (C) 2016  Remik Ziemlinski

This program comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to redistribute it under the conditions of the GPLv3 license.
