#ifndef C_TIME_INCLUDED
#define C_TIME_INCLUDED

#include "c_type.h"

extern c_uint64_t c_time_now_msec();
extern c_uint64_t c_time_now_nsec();
extern c_uint64_t c_time_now_usec();
extern void c_time_nsec_str(char *buf, c_uint32_t nbuf, c_uint64_t nsec);

#endif
/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/