#ifndef TEST_C_VECTOR_ITER_INCLUDED
#define TEST_C_VECTOR_ITER_INCLUDED

#include "c_test.h"
#include "c_vector_iter.h"

void test_c_vector_iter_advance()
{
    #define TEST(N)                                                            \
    C_VECTOR_ITER_TYPE(N) i##N = NULL;                                         \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
                                                                               \
    c_vector_iter_advance##N(NULL);                                            \
                                                                               \
    i##N = c_vector_iter_new##N(v##N);                                         \
    c_vector_iter_advance##N(NULL);                                            \
    c_vector_iter_free##N(& i##N);

    C_TYPE_TEMPLATE_NAME(TEST)
    #undef TEST
}


void test_c_vector_iter_distance()
{
    #define TEST(N)                                                            \
    C_VECTOR_ITER_TYPE(N) i1##N = NULL;                                        \
    C_VECTOR_ITER_TYPE(N) i2##N = NULL;                                        \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
    c_int64_t d##N;                                                            \
                                                                               \
    d##N = c_vector_iter_distance##N(NULL, NULL);                              \
    C_TEST_ASSERT_EQ(d##N, 0, "Expected zero.");                               \
                                                                               \
    d##N = c_vector_iter_distance##N(NULL, i2##N);                             \
    C_TEST_ASSERT_EQ(d##N, 0, "Expected zero.");                               \
                                                                               \
    d##N = c_vector_iter_distance##N(i1##N, NULL);                             \
    C_TEST_ASSERT_EQ(d##N, 0, "Expected zero.");                               \
                                                                               \
    v##N = c_vector_new_set##N(2, 2, 0);                                       \
    i1##N = c_vector_iter_new##N(v##N);                                        \
    i2##N = c_vector_iter_new##N(v##N);                                        \
    c_vector_iter_advance##N(i1##N);                                           \
    d##N = c_vector_iter_distance##N(i1##N, i2##N);                            \
    C_TEST_ASSERT_EQ(d##N, 1, "Expected distance.");                           \
                                                                               \
    c_vector_iter_free##N(& i1##N);                                            \
    c_vector_iter_free##N(& i2##N);                                            \
    c_vector_free##N(& v##N);

    C_TYPE_TEMPLATE_NAME(TEST)
    #undef TEST
}

void test_c_vector_iter_is_valid()
{
    #define TEST(N)                                                            \
    C_VECTOR_ITER_TYPE(N) i##N = NULL;                                         \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
    c_bool_t b##N;                                                             \
                                                                               \
    b##N = c_vector_iter_is_valid##N(NULL);                                    \
    C_TEST_ASSERT_EQ(b##N, C_FALSE, "Expected not valid.");                    \
                                                                               \
    v##N = c_vector_new_set##N(1, 1, 0);                                       \
    i##N = c_vector_iter_new##N(v##N);                                         \
    b##N = c_vector_iter_is_valid##N(i##N);                                    \
    C_TEST_ASSERT_EQ(b##N, C_TRUE, "Expected valid.");                         \
                                                                               \
    c_vector_iter_advance##N(i##N);                                            \
    b##N = c_vector_iter_is_valid##N(i##N);                                    \
    C_TEST_ASSERT_EQ(b##N, C_FALSE, "Expected invalid.");                      \
                                                                               \
    c_vector_iter_free##N(& i##N);                                             \
    c_vector_free##N(& v##N);

    C_TYPE_TEMPLATE_NAME(TEST)
    #undef TEST
}

void test_c_vector_iter_item_ptr()
{
    #define TEST(N, T)                                                         \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
    C_VECTOR_ITER_TYPE(N) i##N = NULL;                                         \
    T a##N = NULL;                                                             \
    T p##N = NULL;                                                             \
                                                                               \
    a##N = c_vector_iter_item##N(NULL);                                        \
    C_TEST_ASSERT_EQ_PTR(a##N, 0, "Expected zero for null iter.");             \
                                                                               \
    C_MEMORY_NEW(p##N);                                                        \
    v##N = c_vector_new_set##N(1, 1, p##N);                                    \
    i##N = c_vector_iter_new##N(v##N);                                         \
    a##N = c_vector_iter_item##N(i##N);                                        \
    C_TEST_ASSERT_EQ_PTR(a##N, p##N, "Expected item.");                        \
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    c_vector_iter_free##N(& i##N);                                             \
    C_MEMORY_FREE(p##N);

    C_TYPE_TEMPLATE_NAME_TYPE_PTR(TEST)
    #undef TEST
}

void test_c_vector_iter_item_value()
{
    #define TEST(N, T)                                                         \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
    C_VECTOR_ITER_TYPE(N) i##N = NULL;                                         \
    T a##N;                                                                    \
                                                                               \
    a##N = c_vector_iter_item##N(NULL);                                        \
    C_TEST_ASSERT_EQ(a##N, 0, "Expected zero for null iter.");                 \
                                                                               \
    v##N = c_vector_new_set##N(1, 1, 1);                                       \
    i##N = c_vector_iter_new##N(v##N);                                         \
    a##N = c_vector_iter_item##N(i##N);                                        \
    C_TEST_ASSERT_EQ(a##N, 1, "Expected item.");                               \
                                                                               \
    c_vector_free##N(& v##N);                                                  \
    c_vector_iter_free##N(& i##N);

    C_TYPE_TEMPLATE_NAME_TYPE_VALUE(TEST)
    #undef TEST
}

void test_c_vector_iter_new()
{
    #define TEST(N)                                                            \
    C_VECTOR_ITER_TYPE(N) i##N = NULL;                                         \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
                                                                               \
    i##N = c_vector_iter_new##N(v##N);                                         \
    C_TEST_ASSERT(i##N, "Failed new with null vector.");                       \
    c_vector_iter_free##N(& i##N);                                             \
                                                                               \
    v##N = c_vector_new##N(1);                                                 \
    i##N = c_vector_iter_new##N(v##N);                                         \
    C_TEST_ASSERT(i##N, "Failed new with null vector.");                       \
    c_vector_iter_free##N(& i##N);                                             \
    c_vector_free##N(& v##N);                                                  \

    C_TYPE_TEMPLATE_NAME(TEST)
    #undef TEST
}

void test_c_vector_iter_position()
{
    #define TEST(N)                                                            \
    C_VECTOR_ITER_TYPE(N) i##N = NULL;                                         \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
    c_uint64_t d##N;                                                           \
                                                                               \
    d##N = c_vector_iter_position##N(NULL);                                    \
    C_TEST_ASSERT_EQ(d##N, 0, "Expected zero.");                               \
                                                                               \
    v##N = c_vector_new_set##N(2, 2, 0);                                       \
    i##N = c_vector_iter_new##N(v##N);                                         \
    d##N = c_vector_iter_position##N(i##N);                                    \
    C_TEST_ASSERT_EQ(d##N, 0, "Expected position.");                           \
    c_vector_iter_advance##N(i##N);                                            \
    d##N = c_vector_iter_position##N(i##N);                                    \
    C_TEST_ASSERT_EQ(d##N, 1, "Expected position.");                           \
                                                                               \
    c_vector_iter_free##N(& i##N);                                             \
    c_vector_free##N(& v##N);

    C_TYPE_TEMPLATE_NAME(TEST)
    #undef TEST
}

void test_c_vector_iter_reset()
{
    #define TEST(N)                                                            \
    C_VECTOR_ITER_TYPE(N) i##N = NULL;                                         \
    C_VECTOR_TYPE(N) v##N = NULL;                                              \
    c_bool_t b##N;                                                             \
                                                                               \
    c_vector_iter_reset##N(NULL);                                              \
                                                                               \
    v##N = c_vector_new_set##N(1, 1, 0);                                       \
    i##N = c_vector_iter_new##N(v##N);                                         \
    b##N = c_vector_iter_is_valid##N(i##N);                                    \
    C_TEST_ASSERT_EQ(b##N, C_TRUE, "Expected valid.");                         \
                                                                               \
    c_vector_iter_advance##N(i##N);                                            \
    b##N = c_vector_iter_is_valid##N(i##N);                                    \
    C_TEST_ASSERT_EQ(b##N, C_FALSE, "Expected invalid.");                      \
                                                                               \
    c_vector_iter_reset##N(i##N);                                              \
    b##N = c_vector_iter_is_valid##N(i##N);                                    \
    C_TEST_ASSERT_EQ(b##N, C_TRUE, "Expected valid.");                         \
                                                                               \
    c_vector_iter_free##N(& i##N);                                             \
    c_vector_free##N(& v##N);

    C_TYPE_TEMPLATE_NAME(TEST)
    #undef TEST
}

int suite_test_c_vector_iter()
{
    C_TEST_ADD_SUITE("suite_test_c_vector_iter", NULL, NULL);

    C_TEST_ADD_TEST(test_c_vector_iter_new);
    C_TEST_ADD_TEST(test_c_vector_iter_advance);
    C_TEST_ADD_TEST(test_c_vector_iter_distance);
    C_TEST_ADD_TEST(test_c_vector_iter_position);
    C_TEST_ADD_TEST(test_c_vector_iter_is_valid);
    C_TEST_ADD_TEST(test_c_vector_iter_reset);
    C_TEST_ADD_TEST(test_c_vector_iter_item_ptr);
    C_TEST_ADD_TEST(test_c_vector_iter_item_value);

    C_TEST_RETURN();
}

#endif
/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/