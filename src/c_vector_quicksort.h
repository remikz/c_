#ifndef C_VECTOR_QUICKSORT_INCLUDED
#define C_VECTOR_QUICKSORT_INCLUDED

#include "c_vector.h"

#ifdef __cplusplus
extern "C" {
#endif

/** Inplace quicksort non-pointer values recursively
 * (which can use O(logn) call stack memory on average, O(n) worst case).
 * Tests show it to be on average 75% faster than the standard library sort().
 */
#define C_VECTOR_QUICKSORT(NAME, TYPE)                                         \
    extern void c_vector_quicksort##NAME(C_VECTOR_TYPE(NAME) vector);

/** Inplace quicksort non-pointer values without recursion.
 * Tests show it to be 25% slower than the recursive c_vector_quicksort().
 */
#define C_VECTOR_QUICKSORT_ITERATIVE(NAME, TYPE)                               \
    extern void c_vector_quicksort_iterative##NAME(C_VECTOR_TYPE(NAME) vector);

/** Recursively quicksort inplace with user defined comparison function.
 * @param cmp Function returns 0 if equal, <0 if less than, 0> if greater than.
 */
extern void c_vector_quicksort(
                    c_vector_t vector,
                    c_funp_int32_cvoidp_cvoidp_t cmp);

/** Compute the final sorted indices of the input items without modifying input.
 * @return Sort indices of input vector items. */
#define C_VECTOR_QUICKSORT_INDICES(NAME, TYPE)                                 \
    extern c_vector_uint64_t c_vector_quicksort_indices##NAME(                 \
                                    const C_VECTOR_TYPE(NAME) vector);

/** Recursively compute sorted indices with user defined comparison function. */
extern c_vector_uint64_t c_vector_quicksort_indices(
                            const c_vector_t vector,
                            c_funp_int32_cvoidp_cvoidp_t cmp);

/** Sort three items in array and return the median item. */
#define C_VECTOR_QUICKSORT_MEDIAN_OF_THREE(NAME, TYPE)                         \
    extern TYPE c_vector_quicksort_median_of_three##NAME(                      \
                    TYPE *items,                                               \
                    c_uint64_t lo,                                             \
                    c_uint64_t hi);

extern void* c_vector_quicksort_median_of_three(
                    void **items,
                    c_uint64_t lo,
                    c_uint64_t hi,
                    c_funp_int32_cvoidp_cvoidp_t cmp);

#define C_VECTOR_QUICKSORT_PARTITION(NAME, TYPE)                               \
    extern c_uint64_t c_vector_quicksort_partition##NAME(                      \
                        TYPE *items,                                           \
                        c_uint64_t lo,                                         \
                        c_uint64_t hi);

extern c_uint64_t c_vector_quicksort_partition(
                        void **items,
                        c_uint64_t lo,
                        c_uint64_t hi,
                        c_funp_int32_cvoidp_cvoidp_t cmp);

/** @return New vector sorted.
 * @see c_vector_quicksort() */
#define C_VECTOR_QUICKSORTED(NAME, TYPE)                                       \
    extern C_VECTOR_TYPE(NAME) c_vector_quicksorted##NAME(                     \
                                    C_VECTOR_TYPE(NAME) vector);

/** @return New vector sorted with user defined compare function.
 * @see c_vector_quicksort()
 */
extern c_vector_t c_vector_quicksorted(
                    c_vector_t vector,
                    c_funp_int32_cvoidp_cvoidp_t cmp);

C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(C_VECTOR_QUICKSORT_MEDIAN_OF_THREE)
C_TYPE_TEMPLATE_NAME_TYPE_VALUE     (C_VECTOR_QUICKSORT_MEDIAN_OF_THREE)
C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(C_VECTOR_QUICKSORT_PARTITION)
C_TYPE_TEMPLATE_NAME_TYPE_VALUE     (C_VECTOR_QUICKSORT_PARTITION)
C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(C_VECTOR_QUICKSORT_INDICES)
C_TYPE_TEMPLATE_NAME_TYPE_VALUE     (C_VECTOR_QUICKSORT_INDICES)
C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(C_VECTOR_QUICKSORT)
C_TYPE_TEMPLATE_NAME_TYPE_VALUE     (C_VECTOR_QUICKSORT)
C_TYPE_TEMPLATE_NAME_TYPE_VALUE     (C_VECTOR_QUICKSORT_ITERATIVE)
C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(C_VECTOR_QUICKSORTED)
C_TYPE_TEMPLATE_NAME_TYPE_VALUE     (C_VECTOR_QUICKSORTED)

#undef C_VECTOR_QUICKSORT
#undef C_VECTOR_QUICKSORT_INDICES
#undef C_VECTOR_QUICKSORT_ITERATIVE
#undef C_VECTOR_QUICKSORT_PARTITION
#undef C_VECTOR_QUICKSORT_MEDIAN_OF_THREE
#undef C_VECTOR_QUICKSORTED

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/