#ifndef C_VECTOR_SORT_IMPL_INCLUDED
#define C_VECTOR_SORT_IMPL_INCLUDED

#include "c_vector_sort.h"
#include "c_cmp.h"
#include "c_str.h" // c_str_cmp
#include <stdlib.h> // sort

#ifdef __cplusplus
extern "C" {
#endif

/* Sort vector of value pointers. */
#define C_VECTOR_SORT_PTR(NAME, TYPE)                                          \
void c_vector_sort##NAME(C_VECTOR_TYPE(NAME) vector)                           \
{                                                                              \
    if (vector) {                                                              \
        qsort(c_vector_items##NAME(vector),                                    \
              c_vector_size##NAME(vector),                                     \
              sizeof(TYPE),                                                    \
              c_cmp_ptr_ref##NAME);                                            \
    }                                                                          \
}

/* Sort vector of values. */
#define C_VECTOR_SORT_VALUE(NAME, TYPE)                                        \
void c_vector_sort##NAME(C_VECTOR_TYPE(NAME) vector)                           \
{                                                                              \
    if (vector) {                                                              \
        qsort(c_vector_items##NAME(vector),                                    \
              c_vector_size##NAME(vector),                                     \
              sizeof(TYPE),                                                    \
              c_cmp##NAME##p);                                                 \
    }                                                                          \
}

void c_vector_sort_str(c_vector_t vector)                                      \
{                                                                              \
    if (vector) {                                                              \
        qsort(c_vector_items(vector),                                          \
              c_vector_size(vector),                                           \
              sizeof(c_str_t),                                                 \
              c_str_cmp_voidpp);                                               \
    }                                                                          \
}

#define C_VECTOR_SORTED(NAME, TYPE)                                            \
C_VECTOR_TYPE(NAME) c_vector_sorted##NAME(const C_VECTOR_TYPE(NAME) vector)    \
{                                                                              \
    C_VECTOR_TYPE(NAME) result = c_vector_new##NAME(0);                        \
    c_vector_copy##NAME(result, vector);                                       \
    c_vector_sort##NAME(result);                                               \
                                                                               \
    return result;                                                             \
}

C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(C_VECTOR_SORT_PTR)
C_TYPE_TEMPLATE_NAME_TYPE_VALUE(C_VECTOR_SORT_VALUE)
C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(C_VECTOR_SORTED)
C_TYPE_TEMPLATE_NAME_TYPE_VALUE(C_VECTOR_SORTED)

#undef C_VECTOR_SORT_PTR
#undef C_VECTOR_SORT_VALUE
#undef C_VECTOR_SORTED

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/