#ifndef C_TEST_INCLUDED
#define C_TEST_INCLUDED

#include "c_test_print.h"

#define C_TEST_ASSERT(SAMPLE, MSG)                                             \
    if (0 == SAMPLE) {                                                         \
        C_TEST_PRINT_ASSERT_MARGIN(C_TEST_PCT());                              \
        C_TEST_PRINT_RAW1(" %s\n", MSG);                                       \
        c_test_assert_pass(c_test_registry, 0, MSG);                                  \
    } else {                                                                   \
        c_test_assert_pass(c_test_registry, 1, NULL);                                 \
    }

#define C_TEST_ASSERT_FALSE(SAMPLE, MSG) C_TEST_ASSERT(!SAMPLE, MSG)

#define C_TEST_ASSERT_NEAR(EXPECT, SAMPLE, EPS, MSG)                           \
    C_TEST_ASSERT_NEQ_HANDLER( ((SAMPLE - EXPECT) > EPS) ||                    \
                               ((EXPECT - SAMPLE) > EPS), EXPECT, SAMPLE, MSG);

#define C_TEST_ASSERT_EQ(EXPECT, SAMPLE, MSG)                                  \
    C_TEST_ASSERT_EQ_HANDLER( (EXPECT == SAMPLE), EXPECT, SAMPLE, MSG)

#define C_TEST_ASSERT_EQ_FILE(EXPECT, SAMPLE, MSG)                             \
    if (c_test_files_equal(EXPECT, SAMPLE)) {                                  \
        c_test_assert_pass(c_test_registry, 1, NULL);                                 \
    } else {                                                                   \
        C_TEST_PRINT_ASSERT_MARGIN(C_TEST_PCT());                              \
        C_TEST_PRINT_RAW1(" %s\n", MSG);                                       \
        c_test_assert_pass(c_test_registry, 0, MSG);                                  \
    }

#define C_TEST_ASSERT_EQ_HANDLER(EQ, EXPECT, SAMPLE, MSG)                      \
    if (EQ) {                                                                  \
        c_test_assert_pass(c_test_registry, 1, NULL);                                 \
    } else {                                                                   \
        C_TEST_PRINT_ASSERT_MARGIN(c_test_pct(c_test_registry));               \
        C_TEST_PRINT_RAW3(" %g != %g %s\n", (double) (EXPECT), (double) (SAMPLE), (MSG));      \
        c_test_assert_pass(c_test_registry, 0, MSG);                                  \
    }

#define C_TEST_ASSERT_EQ_PTR(EXPECT, SAMPLE, MSG)                              \
    C_TEST_ASSERT_EQ_PTR_HANDLER( (EXPECT == SAMPLE), EXPECT, SAMPLE, MSG)

#define C_TEST_ASSERT_EQ_PTR_HANDLER(EQ, EXPECT, SAMPLE, MSG)                  \
    if (EQ) {                                                                  \
        c_test_assert_pass(c_test_registry, 1, NULL);                                 \
    } else {                                                                   \
        C_TEST_PRINT_ASSERT_MARGIN(c_test_pct(c_test_registry));               \
        C_TEST_PRINT_RAW3(" %p != %p %s\n", (void*) EXPECT, (void*) SAMPLE, MSG);        \
        c_test_assert_pass(c_test_registry, 0, MSG);                                  \
    }

#define C_TEST_ASSERT_EQ_STR(EXPECT, SAMPLE, MSG)                              \
    C_TEST_ASSERT_EQ_STR_HANDLER(0 == strcmp((EXPECT), (SAMPLE)), (EXPECT), (SAMPLE), (MSG))

#define C_TEST_ASSERT_EQ_STR_HANDLER(EQ, EXPECT, SAMPLE, MSG)                      \
    if (EQ) {                                                                  \
        c_test_assert_pass(c_test_registry, 1, NULL);                                 \
    } else {                                                                   \
        C_TEST_PRINT_ASSERT_MARGIN(c_test_pct(c_test_registry));               \
        C_TEST_PRINT_RAW3(" \"%s\" != \"%s\" %s\n", (EXPECT), (SAMPLE), (MSG));      \
        c_test_assert_pass(c_test_registry, 0, MSG);                                  \
    }

#define C_TEST_ASSERT_NEQ(EXPECT, SAMPLE, MSG)                                 \
    C_TEST_ASSERT_NEQ_HANDLER( (EXPECT == SAMPLE), EXPECT, SAMPLE, MSG)

#define C_TEST_ASSERT_NEQ_PTR(EXPECT, SAMPLE, MSG)                              \
    C_TEST_ASSERT_EQ_PTR_HANDLER( (EXPECT != SAMPLE), EXPECT, SAMPLE, MSG)

#define C_TEST_ASSERT_NEQ_HANDLER(NEQ, EXPECT, SAMPLE, MSG)                    \
    if (NEQ) {                                                                 \
        C_TEST_PRINT_ASSERT_MARGIN(c_test_pct(c_test_registry));               \
        C_TEST_PRINT_RAW3(" %g != %g %s\n", (double) EXPECT, (double) SAMPLE, MSG);      \
        c_test_assert_pass(c_test_registry, 0, MSG);                                  \
    } else {                                                                   \
        c_test_assert_pass(c_test_registry, 1, NULL);                                 \
    }

#define C_TEST_ASSERT_NEQ_PTR_HANDLER(NEQ, EXPECT, SAMPLE, MSG)                  \
    if (NEQ) {                                                                  \
        c_test_assert_pass(c_test_registry, 1, NULL);                                 \
    } else {                                                                   \
        C_TEST_PRINT_ASSERT_MARGIN(c_test_pct(c_test_registry));               \
        C_TEST_PRINT_RAW3(" %p == %p %s\n", (void*) EXPECT, (void*) SAMPLE, MSG);        \
        c_test_assert_pass(c_test_registry, 0, MSG);                                  \
    }

#define C_TEST_PCT() c_test_pct(c_test_registry)

#define C_TEST_PRINT(M)                                                        \
    C_TEST_PRINT_MARGIN(C_TEST_PCT());                                         \
    C_TEST_PRINT_RAW(M);

#define C_TEST_PRINT1(M, V)                                                    \
    C_TEST_PRINT_MARGIN(C_TEST_PCT());                                         \
    C_TEST_PRINT_RAW1(M, V);

#define C_TEST_REGISTRY() c_test_registry
#define C_TEST_ADD_SUITE(NAME, SETUP, TEARDOWN) c_test_add_suite(C_TEST_REGISTRY(), NAME, SETUP, TEARDOWN)
#define C_TEST_ADD_TEST(TEST) c_test_add_test(C_TEST_REGISTRY(), #TEST, TEST)
#define C_TEST_ADD_TEST_NAME(TEST, NAME) c_test_add_test(C_TEST_REGISTRY(), NAME, TEST)
#define C_TEST_ADD_TEST_FAIL(TEST) c_test_add_test_fail(C_TEST_REGISTRY(), #TEST, TEST)
#define C_TEST_ADD_TEST_FAIL_NAME(TEST, NAME) c_test_add_test_fail(C_TEST_REGISTRY(), NAME, TEST)
#define C_TEST_REGISTER_SUITE(SUITE) SUITE()
#define C_TEST_REGISTRY_DECLARE() c_test_registry_t C_TEST_REGISTRY()
#define C_TEST_REGISTRY_INIT(NAME, COLOR) C_TEST_REGISTRY() = c_test_registry_new(NAME, COLOR);
#define C_TEST_RETURN() return c_test_exit_code(C_TEST_REGISTRY())
#define C_TEST_RUN() c_test_run(C_TEST_REGISTRY())
#define C_TEST_SHUTDOWN() c_test_registry_free(& C_TEST_REGISTRY())
#define C_TEST_XML(NAME) c_test_xml(C_TEST_REGISTRY(), NAME)

#define E c_test_error_t
#define R c_test_registry_t
#define S c_test_suite_t
#define T c_test_test_t

typedef struct E *E;
typedef struct T *T;
typedef struct S *S;
typedef struct R *R;
typedef void (*c_test_funp_t)(void);

extern C_TEST_REGISTRY_DECLARE();

extern void         c_test_add_error(T test, const char *message);
extern void         c_test_add_suite(R reg, const char *name,
                                     c_test_funp_t setup,
                                     c_test_funp_t teardown);
extern T            c_test_add_test(R reg, const char *name, c_test_funp_t fun);
extern T            c_test_add_test_fail(R reg, const char *name, c_test_funp_t fun);
extern c_bool_t     c_test_file_empty(const char *filename);
extern c_bool_t     c_test_file_exists(const char *filename);
extern c_bool_t     c_test_files_equal(const char *filename1, const char *filename2);
extern void         c_test_error_free(E *error);
extern E            c_test_error_new(const char *message);
extern int          c_test_exit_code(R reg);
extern void         c_test_assert_pass(R reg, c_bool_t passed, const char *error);
extern c_uint32_t   c_test_pct(R reg);
extern void         c_test_registry_free(R *reg);
extern R            c_test_registry_new(const char *name, c_bool_t color);
extern c_uint32_t   c_test_registry_num_suites(R reg);
extern c_uint32_t   c_test_registry_num_tests(R reg);
extern int          c_test_run(R reg);
extern void         c_test_run_function(R reg, S suite, T test);
extern void         c_test_set_current_suite(R reg, S suite);
extern void         c_test_set_current_test(S suite, T test);
extern void         c_test_suite_free(S *suite);
extern void         c_test_test_free(T *test);
extern T            c_test_test_new(const char *name, c_test_funp_t fun);
extern c_test_suite_t c_test_suite_new(const char *name, c_test_funp_t setup,
                                       c_test_funp_t teardown);
extern void         c_test_xml(R reg, const char *output_file_name);

#undef C_TEST_ASSERT_HANDLER
#undef F
#undef R
#undef S
#undef T

#endif
/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/