#ifndef C_VECTOR_SORT_INCLUDED
#define C_VECTOR_SORT_INCLUDED

#include "c_vector.h"

#ifdef __cplusplus
extern "C" {
#endif

/** Inplace sort non-pointer values using standard library algorithm. */
#define C_VECTOR_SORT(NAME, TYPE)                                              \
    extern void c_vector_sort##NAME(C_VECTOR_TYPE(NAME) vector);

/** @return New vector with non-pointer values sorted with standard library. */
#define C_VECTOR_SORTED(NAME, TYPE)                                            \
    extern C_VECTOR_TYPE(NAME) c_vector_sorted##NAME(                          \
                                    const C_VECTOR_TYPE(NAME) vector);

/** Sort vector of strings with standard library algorithm. */
extern void c_vector_sort_str(c_vector_t vector);

C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(C_VECTOR_SORT)
C_TYPE_TEMPLATE_NAME_TYPE_VALUE(C_VECTOR_SORT)
C_TYPE_TEMPLATE_NAME_TYPE_PTR_NOVOID(C_VECTOR_SORTED)
C_TYPE_TEMPLATE_NAME_TYPE_VALUE(C_VECTOR_SORTED)

#undef C_VECTOR_SORT
#undef C_VECTOR_SORTED

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/