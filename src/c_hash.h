#ifndef C_HASH_INCLUDED
#define C_HASH_INCLUDED

#include "c_type.h"
#include "c_str.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef c_uint64_t c_hash_t;

#define C_HASH_NAME(NAME) c_hash_funp##NAME##_t

/** Closed hash function definition. */
#define C_HASH_TYPEDEF(NAME, TYPE)                                             \
    typedef c_hash_t (*C_HASH_NAME(NAME))(const TYPE);

/** @return Value or pointer address cast to hash type. */
#define C_HASH_IDENTITY(NAME, TYPE)                                            \
    extern c_hash_t c_hash_identity##NAME(const TYPE key);

#define C_HASH_TEMPLATE_NAME_TYPE_VALUE(MACRO)                                 \
    MACRO(_int16,       c_int16_t)                                             \
    MACRO(_int32,       c_int32_t)                                             \
    MACRO(_int64,       c_int64_t)                                             \
    MACRO(_uint16,      c_uint16_t)                                            \
    MACRO(_uint32,      c_uint32_t)                                            \
    MACRO(_uint64,      c_uint64_t)

#define C_HASH_TEMPLATE_NAME_TYPE_PTR(MACRO)                                   \
    MACRO(/* empty */,  void*)                                                 \

#define C_HASH_TEMPLATE_NAME_TYPE_STR(MACRO)                                   \
    MACRO(_str,  c_str_t)                                                      \

#define C_HASH_TEMPLATE_NAME_TYPE(MACRO)                                       \
    C_HASH_TEMPLATE_NAME_TYPE_VALUE(MACRO)                                     \
    C_HASH_TEMPLATE_NAME_TYPE_PTR(MACRO)

#define C_HASH_SHIFT(NAME, TYPE)                                               \
    extern c_hash_t c_hash_shift##NAME(TYPE key);

extern c_hash_t c_hash_djb_chars(const c_char_t *chars);
extern c_hash_t c_hash_djb_str(const c_str_t str);
extern c_uint32_t c_hash_murmur3_32_str(const c_str_t str);
extern c_hash_t   c_hash_murmur64a_str(const c_str_t str);

C_HASH_TEMPLATE_NAME_TYPE(C_HASH_TYPEDEF)
C_HASH_TEMPLATE_NAME_TYPE_STR(C_HASH_TYPEDEF)
C_HASH_TEMPLATE_NAME_TYPE(C_HASH_IDENTITY)
C_HASH_TEMPLATE_NAME_TYPE_STR(C_HASH_IDENTITY)
C_HASH_TEMPLATE_NAME_TYPE(C_HASH_SHIFT)

#undef C_HASH_IDENTITY
#undef C_HASH_SHIFT

#ifdef __cplusplus
}
#endif

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/