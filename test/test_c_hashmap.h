#ifndef TEST_C_HASHMAP_INCLUDED
#define TEST_C_HASHMAP_INCLUDED

#include "c_test.h"
#include "c_hashmap.h"
#include "c_log.h"

c_int32_t cmp_voidp(
            c_hash_t hash,
            const void *key,
            const c_keyvalue_hash_t keyvalue)
{
    c_hash_t hash_kv = c_keyvalue_hash_hash(keyvalue);

    return (hash - hash_kv) +
           *(c_int64_t*) key - *(c_int64_t*) c_keyvalue_hash_key(keyvalue);
}

c_hash_t hash_voidp(const void *a)
{
    return (c_hash_t)a;
}

// Dummy implementation.
c_int32_t c_keyvalue_hash_cmp(
            c_hash_t hash,
            const void *k,
            const c_keyvalue_hash_t kv)
{
    return -1;
}

void test_c_keyvalue_hash_cmp_dummy()
{
    C_TEST_ASSERT_EQ(c_keyvalue_hash_cmp(0, 0, 0), -1, "Expected value.");
}

void test_c_hashmap_keys()
{
    #define TEST(KN, KT, VN, VT)                                               \
                                                                               \
    C_HASHMAP_TYPE(KN, VN) hm##KN##VN = NULL;                                  \
    KT k1##KN##VN = 1;                                                         \
    KT k2##KN##VN = 2;                                                         \
    KT k3##KN##VN = 3;                                                         \
    c_int64_t v1##KN##VN = 1;                                                  \
    c_int64_t v2##KN##VN = 2;                                                  \
    C_VECTOR_TYPE(KN) vec##KN##VN = NULL;                                      \
                                                                               \
    hm##KN##VN = c_hashmap_new##KN##VN(10, NULL, NULL);                        \
    C_TEST_ASSERT(hm##KN##VN, "Expected new hashmap.");                        \
                                                                               \
    c_hashmap_put##KN##VN(hm##KN##VN, k1##KN##VN, & v1##KN##VN);               \
    c_hashmap_put##KN##VN(hm##KN##VN, k2##KN##VN, & v2##KN##VN);               \
                                                                               \
    vec##KN##VN = c_hashmap_keys##KN##VN(hm##KN##VN);                          \
    C_TEST_ASSERT_EQ(c_vector_size##KN(vec##KN##VN), 2,                        \
                     "Expected size " #KN#VN);                                 \
                                                                               \
    C_TEST_ASSERT(c_vector_find##KN(vec##KN##VN, k1##KN##VN) >= 0,             \
                  "Expected to find key k1" #KN#VN);                           \
    C_TEST_ASSERT(c_vector_find##KN(vec##KN##VN, k2##KN##VN) >= 0,             \
                  "Expected to find key k2" #KN#VN);                           \
    C_TEST_ASSERT(c_vector_find##KN(vec##KN##VN, k3##KN##VN) < 0,              \
                  "Expected not to find key k3" #KN#VN);                       \
                                                                               \
    c_hashmap_free##KN##VN(& hm##KN##VN);                                      \
    c_vector_free##KN(& vec##KN##VN);

    C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE_NUMERIC(TEST)
    #undef TEST
}

void test_c_hashmap_keys_ptr()
{
    #define TEST(KN, KT, VN, VT)                                               \
                                                                               \
    C_HASHMAP_TYPE(KN, VN) hm##KN##VN = NULL;                                  \
    c_int64_t k1##KN##VN = 1;                                                  \
    c_int64_t k2##KN##VN = 2;                                                  \
    c_int64_t v1##KN##VN = 1;                                                  \
    c_int64_t v2##KN##VN = 2;                                                  \
    C_VECTOR_TYPE(KN) vec##KN##VN = NULL;                                      \
                                                                               \
    hm##KN##VN = c_hashmap_new##KN##VN(10, cmp_voidp, hash_voidp);             \
    C_TEST_ASSERT(hm##KN##VN, "Expected new hashmap.");                        \
                                                                               \
    c_hashmap_put##KN##VN(hm##KN##VN, & k1##KN##VN, & v1##KN##VN);             \
    c_hashmap_put##KN##VN(hm##KN##VN, & k2##KN##VN, & v2##KN##VN);             \
                                                                               \
    vec##KN##VN = c_hashmap_keys##KN##VN(hm##KN##VN);                          \
    C_TEST_ASSERT_EQ(c_vector_size##KN(vec##KN##VN), 2,                        \
                     "Expected size " #KN#VN);                                 \
                                                                               \
    C_TEST_ASSERT(c_vector_find##KN(vec##KN##VN, & k1##KN##VN) >= 0,           \
                  "Expected to find key k1" #KN#VN);                           \
    C_TEST_ASSERT(c_vector_find##KN(vec##KN##VN, & k2##KN##VN) >= 0,           \
                  "Expected to find key k2" #KN#VN);                           \
                                                                               \
    c_hashmap_free##KN##VN(& hm##KN##VN);                                      \
    c_vector_free##KN(& vec##KN##VN);

    C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE_PTR(TEST)
    #undef TEST
}

void test_c_hashmap_new()
{
    #define TEST(KN, KT, VN, VT)                                               \
                                                                               \
    C_HASHMAP_TYPE(KN, VN) hm##KN##VN = NULL;                                  \
                                                                               \
    hm##KN##VN = c_hashmap_new##KN##VN(0, NULL, NULL);                         \
    C_TEST_ASSERT(hm##KN##VN, "Expected new hashmap.");                        \
    c_hashmap_free##KN##VN(& hm##KN##VN);                                      \
    C_TEST_ASSERT_FALSE(hm##KN##VN, "Expected free hashmap.");

    C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE(TEST)
    #undef TEST
}

void test_c_hashmap_put()
{
    #define TEST(KN, KT, VN, VT)                                               \
                                                                               \
    C_HASHMAP_TYPE(KN, VN) hm##KN##VN = NULL;                                  \
    c_int64_t v1##KN##VN = 1;                                                  \
    c_int64_t v2##KN##VN = 2;                                                  \
    c_int64_t v3##KN##VN = 3;                                                  \
    c_int64_t v4##KN##VN = 4;                                                  \
    VT v##KN##VN = NULL;                                                       \
    c_bool_t has##KN##VN = C_FALSE;                                            \
                                                                               \
    hm##KN##VN = c_hashmap_new##KN##VN(10, NULL, NULL);                        \
    C_TEST_ASSERT(hm##KN##VN, "Expected new hashmap.");                        \
                                                                               \
    v##KN##VN = c_hashmap_put##KN##VN(hm##KN##VN, 1, & v1##KN##VN);            \
    C_TEST_ASSERT_EQ_PTR(v##KN##VN, 0, "Expected null previous value.");       \
                                                                               \
    v##KN##VN = c_hashmap_put##KN##VN(hm##KN##VN, 2, & v2##KN##VN);            \
    C_TEST_ASSERT_EQ_PTR(v##KN##VN, 0, "Expected null previous value.");       \
                                                                               \
    v##KN##VN = c_hashmap_put##KN##VN(hm##KN##VN, 3, & v3##KN##VN);            \
    C_TEST_ASSERT_EQ_PTR(v##KN##VN, 0, "Expected null previous value.");       \
                                                                               \
    /* --------------------------------------------------------------------- */\
    has##KN##VN = c_hashmap_has##KN##VN(hm##KN##VN, 0);                        \
    C_TEST_ASSERT_FALSE(has##KN##VN, "Expected not to have key 0.");           \
                                                                               \
    has##KN##VN = c_hashmap_has##KN##VN(hm##KN##VN, 1);                        \
    C_TEST_ASSERT(has##KN##VN, "Expected to have key 1 " #KN#VN);              \
                                                                               \
    has##KN##VN = c_hashmap_has##KN##VN(hm##KN##VN, 2);                        \
    C_TEST_ASSERT(has##KN##VN, "Expected to have key 2 " #KN#VN);              \
                                                                               \
    has##KN##VN = c_hashmap_has##KN##VN(hm##KN##VN, 3);                        \
    C_TEST_ASSERT(has##KN##VN, "Expected to have key 3 " #KN#VN);              \
                                                                               \
    /* --------------------------------------------------------------------- */\
    v##KN##VN = c_hashmap_put##KN##VN(hm##KN##VN, 1, & v4##KN##VN);            \
    C_TEST_ASSERT(v##KN##VN, "Expected non null " #KN#VN);                     \
    C_TEST_ASSERT_EQ(*(c_int64_t*)v##KN##VN, 1,                                \
                     "Expected previous value 1 " #KN#VN);                     \
                                                                               \
    /* --------------------------------------------------------------------- */\
    v##KN##VN = c_hashmap_get##KN##VN(hm##KN##VN, 0);                          \
    C_TEST_ASSERT_EQ_PTR(v##KN##VN, 0, "Expected null for key 0 " #KN#VN);     \
                                                                               \
    v##KN##VN = c_hashmap_get##KN##VN(hm##KN##VN, 1);                          \
    C_TEST_ASSERT(v##KN##VN, "Expected non-null.");                            \
    C_TEST_ASSERT_EQ(*(c_int64_t*)v##KN##VN, 4,                                \
                     "Expected previous value 4 " #KN#VN);                     \
                                                                               \
    v##KN##VN = c_hashmap_get##KN##VN(hm##KN##VN, 2);                          \
    C_TEST_ASSERT(v##KN##VN, "Expected non-null.");                            \
    C_TEST_ASSERT_EQ(*(c_int64_t*)v##KN##VN, 2,                                \
                     "Expected previous value 2 " #KN#VN);                     \
                                                                               \
    v##KN##VN = c_hashmap_get##KN##VN(hm##KN##VN, 3);                          \
    C_TEST_ASSERT(v##KN##VN, "Expected non-null.");                            \
    C_TEST_ASSERT_EQ(*(c_int64_t*)v##KN##VN, 3,                                \
                     "Expected previous value 3 " #KN#VN);                     \
                                                                               \
    C_TEST_ASSERT_EQ(c_hashmap_num_items##KN##VN(hm##KN##VN), 3,               \
                     "Expected num items.");                                   \
    C_TEST_ASSERT(c_hashmap_load_factor##KN##VN(hm##KN##VN) >= 0.3,            \
                  "Expected load factor.");                                    \
                                                                               \
    c_hashmap_free##KN##VN(& hm##KN##VN);                                      \
    C_TEST_ASSERT_FALSE(hm##KN##VN, "Expected free hashmap.");

    C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE_NUMERIC(TEST)
    #undef TEST
}

void test_c_hashmap_put_ptr()
{
    c_hashmap_t hm = c_hashmap_new(10, cmp_voidp, hash_voidp);
    c_bool_t has = C_FALSE;
    c_int64_t k1 = 1;
    c_int64_t k2 = 2;
    c_int64_t k3 = 3;
    c_int64_t k4 = 4;
    c_int64_t v1 = 1;
    c_int64_t v2 = 2;
    c_int64_t v3 = 3;
    c_int64_t v4 = 4;
    void *v = NULL;

    v = c_hashmap_put(hm, & k1, & v1);
    C_TEST_ASSERT_EQ_PTR(v, 0, "Expected null previous value.");
    v = c_hashmap_put(hm, & k2, & v2);
    C_TEST_ASSERT_EQ_PTR(v, 0, "Expected null previous value.");
    v = c_hashmap_put(hm, & k3, & v3);
    C_TEST_ASSERT_EQ_PTR(v, 0, "Expected null previous value.");

    has = c_hashmap_has(hm, & k4);
    C_TEST_ASSERT_FALSE(has, "Expected not to have key.");
    has = c_hashmap_has(hm, & k1);
    C_TEST_ASSERT(has, "Expected to have key k1.");
    has = c_hashmap_has(hm, & k2);
    C_TEST_ASSERT(has, "Expected to have key k2.");
    has = c_hashmap_has(hm, & k3);
    C_TEST_ASSERT(has, "Expected to have key k3.");

    v = c_hashmap_put(hm, & k1, & v4);
    C_TEST_ASSERT(v, "Expected non null.");
    C_TEST_ASSERT_EQ(*(c_int64_t*)v, 1, "Expected previous value 1.");

    v = c_hashmap_get(hm, & k4);
    C_TEST_ASSERT_EQ_PTR(v, 0, "Expected null for key k4");
    v = c_hashmap_get(hm, & k1);
    C_TEST_ASSERT(v, "Expected non-null.");
    C_TEST_ASSERT_EQ(*(c_int64_t*)v, 4, "Expected previous value.");
    v = c_hashmap_get(hm, & k2);
    C_TEST_ASSERT(v, "Expected non-null.");
    C_TEST_ASSERT_EQ(*(c_int64_t*)v, 2, "Expected previous value.");
    v = c_hashmap_get(hm, & k3);
    C_TEST_ASSERT(v, "Expected non-null.");
    C_TEST_ASSERT_EQ(*(c_int64_t*)v, 3, "Expected previous value.");

    C_TEST_ASSERT_EQ(c_hashmap_num_items(hm), 3, "Expected num items");
    C_TEST_ASSERT(c_hashmap_load_factor(hm) >= 0.3, "Expected load factor.");

    c_hashmap_free(& hm);
    C_TEST_ASSERT_FALSE(hm, "Expected free hashmap.");
}

void test_c_hashmap_put_str()
{
    c_hashmap_str_t hm = c_hashmap_new_str(10, NULL, NULL);
    c_bool_t has = C_FALSE;
    c_str_t k1 = c_str_new("key1", 4);
    c_str_t k2 = c_str_new("key2", 4);
    c_str_t k3 = c_str_new("key3", 4);
    c_str_t k4 = c_str_new("key4", 4);
    c_int64_t v1 = 1;
    c_int64_t v2 = 2;
    c_int64_t v3 = 3;
    c_int64_t v4 = 4;
    void *v = NULL;

    v = c_hashmap_put_str(hm, k1, & v1);
    C_TEST_ASSERT_EQ_PTR(v, 0, "Expected null previous value.");
    v = c_hashmap_put_str(hm, k2, & v2);
    C_TEST_ASSERT_EQ_PTR(v, 0, "Expected null previous value.");
    v = c_hashmap_put_str(hm, k3, & v3);
    C_TEST_ASSERT_EQ_PTR(v, 0, "Expected null previous value.");

    has = c_hashmap_has_str(hm, k4);
    C_TEST_ASSERT_FALSE(has, "Expected not to have key.");
    has = c_hashmap_has_str(hm, k1);
    C_TEST_ASSERT(has, "Expected to have key k1.");
    has = c_hashmap_has_str(hm, k2);
    C_TEST_ASSERT(has, "Expected to have key k2.");
    has = c_hashmap_has_str(hm, k3);
    C_TEST_ASSERT(has, "Expected to have key k3.");

    v = c_hashmap_put_str(hm, k1, & v4);
    C_TEST_ASSERT(v, "Expected non null.");
    C_TEST_ASSERT_EQ(*(c_int64_t*)v, 1, "Expected previous value 1.");

    v = c_hashmap_get_str(hm, k4);
    C_TEST_ASSERT_EQ_PTR(v, 0, "Expected null for key k4");

    v = c_hashmap_get_str(hm, k1);
    C_TEST_ASSERT(v, "Expected non-null.");
    C_TEST_ASSERT_EQ(*(c_int64_t*)v, 4, "Expected previous value.");

    v = c_hashmap_get_str(hm, k2);
    C_TEST_ASSERT(v, "Expected non-null.");
    C_TEST_ASSERT_EQ(*(c_int64_t*)v, 2, "Expected previous value.");

    v = c_hashmap_get_str(hm, k3);
    C_TEST_ASSERT(v, "Expected non-null.");
    C_TEST_ASSERT_EQ(*(c_int64_t*)v, 3, "Expected previous value.");

    C_TEST_ASSERT_EQ(c_hashmap_num_items_str(hm), 3, "Expected num items");
    C_TEST_ASSERT(c_hashmap_load_factor_str(hm) >= 0.3, "Expected load factor.");

    c_hashmap_free_str(& hm);
    C_TEST_ASSERT_FALSE(hm, "Expected free hashmap.");

    c_str_free(& k1);
    c_str_free(& k2);
    c_str_free(& k3);
    c_str_free(& k4);
}

void test_c_hashmap_remove()
{
    #define TEST(KN, KT, VN, VT)                                               \
                                                                               \
    C_HASHMAP_TYPE(KN, VN) hm##KN##VN = NULL;                                  \
    c_int64_t v1##KN##VN = 1;                                                  \
    c_int64_t v2##KN##VN = 2;                                                  \
    VT v##KN##VN = NULL;                                                       \
                                                                               \
    hm##KN##VN = c_hashmap_new##KN##VN(10, NULL, NULL);                        \
    c_hashmap_put##KN##VN(hm##KN##VN, 1, & v1##KN##VN);                        \
    c_hashmap_put##KN##VN(hm##KN##VN, 2, & v2##KN##VN);                        \
                                                                               \
    v##KN##VN = c_hashmap_remove##KN##VN(hm##KN##VN, 3);                       \
    C_TEST_ASSERT_EQ_PTR(v##KN##VN, 0, "Expected nothing removed " #KN#VN);    \
                                                                               \
    v##KN##VN = c_hashmap_remove##KN##VN(hm##KN##VN, 2);                       \
    C_TEST_ASSERT(v##KN##VN, "Expected non null pointer removed " #KN#VN);     \
                                                                               \
    C_TEST_ASSERT_EQ(*(c_int64_t*) v##KN##VN, v2##KN##VN,                      \
                     "Expected value " #KN#VN);                                \
    C_TEST_ASSERT_EQ(c_hashmap_num_items##KN##VN(hm##KN##VN), 1,               \
                     "Expected num items.");                                   \
                                                                               \
    c_hashmap_free##KN##VN(& hm##KN##VN);

    C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE_NUMERIC(TEST)
    #undef TEST
}

void test_c_hashmap_remove_ptr()
{
    c_hashmap_t hm = c_hashmap_new(10, cmp_voidp, hash_voidp);
    c_int64_t k1 = 1;
    c_int64_t k2 = 2;
    c_int64_t k3 = 3;
    c_int64_t v1 = 1;
    c_int64_t v2 = 2;
    c_int64_t v3 = 3;
    void *v = NULL;

    v = c_hashmap_put(hm, & k1, & v1);
    C_TEST_ASSERT_EQ_PTR(v, 0, "Expected null previous value.");
    v = c_hashmap_put(hm, & k2, & v2);
    C_TEST_ASSERT_EQ_PTR(v, 0, "Expected null previous value.");

    v = c_hashmap_remove(hm, & k3);
    C_TEST_ASSERT_EQ_PTR(v, 0, "Expected nothing removed.");

    v = c_hashmap_remove(hm, & k2);
    C_TEST_ASSERT(v, "Expected non null pointer removed.");
    C_TEST_ASSERT_EQ(*(c_int64_t*) v, v2, "Expected value.");

    C_TEST_ASSERT_EQ(c_hashmap_num_items(hm), 1, "Expected num items.");

    c_hashmap_free(& hm);
    C_TEST_ASSERT_FALSE(hm, "Expected free hashmap.");
}

void test_c_hashmap_remove_str()
{
    c_hashmap_str_t hm = c_hashmap_new_str(10, NULL, NULL);
    c_str_t k1 = c_str_new("key1", 4);
    c_str_t k2 = c_str_new("key2", 4);
    c_str_t k3 = c_str_new("key3", 4);
    c_int64_t v1 = 1;
    c_int64_t v2 = 2;
    c_int64_t v3 = 3;
    void *v = NULL;

    v = c_hashmap_put_str(hm, k1, & v1);
    C_TEST_ASSERT_EQ_PTR(v, 0, "Expected null previous value.");
    v = c_hashmap_put_str(hm, k2, & v2);
    C_TEST_ASSERT_EQ_PTR(v, 0, "Expected null previous value.");

    v = c_hashmap_remove_str(hm, k3);
    C_TEST_ASSERT_EQ_PTR(v, 0, "Expected nothing removed.");

    v = c_hashmap_remove_str(hm, k2);
    C_TEST_ASSERT(v, "Expected non null pointer removed.");
    C_TEST_ASSERT_EQ(*(c_int64_t*) v, v2, "Expected value.");

    C_TEST_ASSERT_EQ(c_hashmap_num_items_str(hm), 1, "Expected num items.");

    c_hashmap_free_str(& hm);
    C_TEST_ASSERT_FALSE(hm, "Expected free hashmap.");

    c_str_free(& k1);
    c_str_free(& k2);
    c_str_free(& k3);
}

void test_c_hashmap_values()
{
    #define TEST(KN, KT, VN, VT)                                               \
                                                                               \
    C_HASHMAP_TYPE(KN, VN) hm##KN##VN = NULL;                                  \
    KT k1##KN##VN = 1;                                                         \
    KT k2##KN##VN = 2;                                                         \
    c_int64_t v1##KN##VN = 1;                                                  \
    c_int64_t v2##KN##VN = 2;                                                  \
    c_int64_t v3##KN##VN = 3;                                                  \
    C_VECTOR_TYPE(VN) vec##KN##VN = NULL;                                      \
                                                                               \
    hm##KN##VN = c_hashmap_new##KN##VN(10, NULL, NULL);                        \
    C_TEST_ASSERT(hm##KN##VN, "Expected new hashmap.");                        \
                                                                               \
    c_hashmap_put##KN##VN(hm##KN##VN, k1##KN##VN, & v1##KN##VN);               \
    c_hashmap_put##KN##VN(hm##KN##VN, k2##KN##VN, & v2##KN##VN);               \
                                                                               \
    vec##KN##VN = c_hashmap_values##KN##VN(hm##KN##VN);                        \
    C_TEST_ASSERT_EQ(c_vector_size##VN(vec##KN##VN), 2,                        \
                     "Expected size " #KN#VN);                                 \
                                                                               \
    C_TEST_ASSERT(c_vector_find##VN(vec##KN##VN, & v1##KN##VN) >= 0,           \
                  "Expected to find v1" #KN#VN);                               \
    C_TEST_ASSERT(c_vector_find##VN(vec##KN##VN, & v2##KN##VN) >= 0,           \
                  "Expected to find v2" #KN#VN);                               \
    C_TEST_ASSERT(c_vector_find##VN(vec##KN##VN, & v3##KN##VN) < 0,            \
                  "Expected not to find v3" #KN#VN);                           \
                                                                               \
    c_hashmap_free##KN##VN(& hm##KN##VN);                                      \
    c_vector_free##VN(& vec##KN##VN);

    C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE_NUMERIC(TEST)
    #undef TEST
}

void test_c_hashmap_values_ptr()
{
    #define TEST(KN, KT, VN, VT)                                               \
                                                                               \
    C_HASHMAP_TYPE(KN, VN) hm##KN##VN = NULL;                                  \
    c_int64_t k1##KN##VN = 1;                                                  \
    c_int64_t k2##KN##VN = 2;                                                  \
    c_int64_t v1##KN##VN = 1;                                                  \
    c_int64_t v2##KN##VN = 2;                                                  \
    c_int64_t v3##KN##VN = 3;                                                  \
    C_VECTOR_TYPE(VN) vec##KN##VN = NULL;                                      \
                                                                               \
    hm##KN##VN = c_hashmap_new##KN##VN(10, cmp_voidp, hash_voidp);             \
    C_TEST_ASSERT(hm##KN##VN, "Expected new hashmap.");                        \
                                                                               \
    c_hashmap_put##KN##VN(hm##KN##VN, & k1##KN##VN, & v1##KN##VN);             \
    c_hashmap_put##KN##VN(hm##KN##VN, & k2##KN##VN, & v2##KN##VN);             \
                                                                               \
    vec##KN##VN = c_hashmap_values##KN##VN(hm##KN##VN);                        \
    C_TEST_ASSERT_EQ(c_vector_size##VN(vec##KN##VN), 2,                        \
                     "Expected size " #KN#VN);                                 \
                                                                               \
    C_TEST_ASSERT(c_vector_find##VN(vec##KN##VN, & v1##KN##VN) >= 0,           \
                  "Expected to find v1" #KN#VN);                               \
    C_TEST_ASSERT(c_vector_find##VN(vec##KN##VN, & v2##KN##VN) >= 0,           \
                  "Expected to find v2" #KN#VN);                               \
    C_TEST_ASSERT(c_vector_find##VN(vec##KN##VN, & v3##KN##VN) < 0,            \
                  "Expected not to find v3" #KN#VN);                           \
                                                                               \
    c_hashmap_free##KN##VN(& hm##KN##VN);                                      \
    c_vector_free##VN(& vec##KN##VN);

    C_KEYVALUE_HASH_TEMPLATE_NAME_TYPE_PTR(TEST)
    #undef TEST
}

int suite_test_c_hashmap()
{
    C_TEST_ADD_SUITE("suite_test_c_hashmap", NULL, NULL);

    C_TEST_ADD_TEST(test_c_keyvalue_hash_cmp_dummy);
    C_TEST_ADD_TEST(test_c_hashmap_new);
    C_TEST_ADD_TEST(test_c_hashmap_put);
    C_TEST_ADD_TEST(test_c_hashmap_put_ptr);
    C_TEST_ADD_TEST(test_c_hashmap_put_str);
    C_TEST_ADD_TEST(test_c_hashmap_remove);
    C_TEST_ADD_TEST(test_c_hashmap_remove_ptr);
    C_TEST_ADD_TEST(test_c_hashmap_remove_str);
    C_TEST_ADD_TEST(test_c_hashmap_keys);
    C_TEST_ADD_TEST(test_c_hashmap_keys_ptr);
    C_TEST_ADD_TEST(test_c_hashmap_values);
    C_TEST_ADD_TEST(test_c_hashmap_values_ptr);

    C_TEST_RETURN();
}

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/
