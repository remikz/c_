#ifndef C_MEMORY_INCLUDED
#define C_MEMORY_INCLUDED

#include <assert.h>
#include <stdlib.h>

#define C_MEMORY_FREE(P) if (P) { free(P); (P) = NULL; }

/** @return Pointer with N copied items of type T from SRC to DST. */
#define C_MEMORY_COPY_N_TYPES(DST, SRC, N, T) memcpy((DST), (SRC), (N) * sizeof(T))

/** @return Allocated pointer. */
#define C_MEMORY_NEW(P) ((P) = c_memory_alloc((long long) sizeof *(P), __FILE__, __LINE__))

/** @return Allocated pointer for N bytes. */
#define C_MEMORY_NEW_N_BYTES(P, N) ((P) = c_memory_alloc((long long) (N), __FILE__, __LINE__))

/** @return Allocated pointer for N items with type T. */
#define C_MEMORY_NEW_N_TYPES(P, N, T) ((P) = c_memory_alloc((long long) (N) * sizeof(T), __FILE__, __LINE__))

/** @return Allocated pointer for N bytes set with zeros. */
#define C_MEMORY_NEW_N_BYTES_ZERO(P, N) ((P) = c_memory_alloc_zero((long long) (N), __FILE__, __LINE__))

/** @return Allocated pointer set with zeros. */
#define C_MEMORY_NEW_ZERO(P) ((P) = c_memory_alloc_zero((long long) sizeof *(P), __FILE__, __LINE__))

/** @return Re-allocated pointer. */
#define C_MEMORY_RESIZE(P, N, T) ((P) = c_memory_realloc((P), (long long) (N) * sizeof(T), __FILE__, __LINE__))

/** @return Pointer that has bytes set to a byte. */
#define C_MEMORY_SET(DST, BYTE, NBYTES) memset((DST), (BYTE), (NBYTES))

/** Allocate bytes.
 * @param file Caller source code file name.
 * @param line Caller source code line number in file.
 * @return Allocated pointer.
 */
extern void* c_memory_alloc(long long nbytes, const char *file, int line);

/** Allocate bytes and set all to zero.
 * @param file Caller source code file name.
 * @param line Caller source code line number in file.
 * @return Allocated pointer.
 */
extern void* c_memory_alloc_zero(long long nbytes, const char *file, int line);

/** Reallocate bytes..
 * @param file Caller source code file name.
 * @param line Caller source code line number in file.
 * @return Allocated pointer.
 */
extern void* c_memory_realloc(void *ptr, long long nbytes, const char *file, int line);

#endif

/*
    LICENSE BEGIN

    c_ - A C library of types, data structures, algorithms and utilities.
    Copyright (C) 2016  Remik Ziemlinski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    LICENSE END
*/